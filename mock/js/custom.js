// 一覧チェック一括付けはずし
$(function(){
	// 全てチェック取り付け
	$("#CheckOn").click(function(){
		$(".all_check").attr("checked", "checked");
	});
	// 全てチェック取りはずし
	$("#CheckOff").click(function(){
		$(".all_check").attr("checked", "");
	});
});


//渡された要素名から最後の「_」の後ろの要素数を返す
function getTargetNo(strId){
  var arr = strId.split('_');
  if(arr.length > 0){
      return arr[arr.length - 1];
  }
  return 0;
}
