<?php
/**
 * 基本モデル
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note
 */
class AppModel extends Model {
	var $actsAs = array('BasicValidation');
	var $checkUpdated = false;		// 更新時間チェック

	/**
	 * BIGIN
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 */
	function begin() {
		$db =& ConnectionManager::getDataSource($this->useDbConfig);
		$db->begin($this);
	}


	/**
	 * COMMIT
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 */
	function commit() {
		$db =& ConnectionManager::getDataSource($this->useDbConfig);
		$db->commit($this);
	}


	/**
	 * ROLLBACK
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 */
	function rollback() {
		$db =& ConnectionManager::getDataSource($this->useDbConfig);
		$db->rollback($this);
	}


	/**
	 * 保存前処理
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function beforeSave() {
		// 更新時間チェック（同時更新防止）
		if (!$this->_checkUpdated()) {
			return false;
		}

		//ブランクをNULLに変更
		$this->_setNull();

		return true; //true返さないと保存してくれない
	}


	/**
	 * ブランクにNULLをセット
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     $this->beforeSaveで使用
	 */
	function _setNull() {
		$data =& $this->data[$this->name];

		//ブランクをNULLに変更
		foreach ($data as $key => $val) {
			if (isset($this->_schema[$key])) {
				if (!$this->notEmpty($val) && $this->_schema[$key]['null'] == true) {
					$data[$key] = null;
				}
			}
		}
	}


	/**
	 * 更新時間チェック（同時更新防止）
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function _checkUpdated() {
		$data =& $this->data[$this->name];
		$col  = 'updated';

		if (!isset($data[$this->primaryKey])) {
			$data[$this->primaryKey] = null;
		}
		$params = array(
			'conditions' => array($this->primaryKey => $data[$this->primaryKey]),
			'fields' => array($this->primaryKey, $col),
			'recursive' => -1,
		);

		// 更新時間チェックしない
		if (!$this->checkUpdated) {
			$data[$col] = date('Y-m-d H:i:s', time());
			return true;
		}

		// 新規、更新時間なしの時はチェックしない
		if (empty($data[$this->primaryKey]) || empty($data[$col])) {
			$data[$col] = date('Y-m-d H:i:s', time());
			return true;
		}

		$nowVersion = dateFormat($data[$col], 'Y-m-d H:i:s');

		$dbData = $this->find('first', $params);
		$dbVersion = dateFormat($dbData[$this->name][$col], 'Y-m-d H:i:s');

		if ($dbVersion != $nowVersion) {
			$this->invalidate($col, "他のユーザーが先にこのデータを更新した為、更新できません。もう一度データを読み込みなおしてください。");
			return false;
		}
		$data[$col] = date('Y-m-d H:i:s', time());
		return true;
	}


	/**
	 * queryのオーバーライド
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     キャッシュをデフォルトfalse
	 * @return   mix    真：arr cakeデータ配列 偽：bool false
	 */
	function query() {
		$params = func_get_args();
		if (empty($params[1])) {
			$params[1] = false;
		} else {
			if (is_array($params[1]) && empty($params[2])) {
				$params[2] = false;
			}
		}

		$db =& ConnectionManager::getDataSource($this->useDbConfig);
		return call_user_func_array(array(&$db, 'query'), $params);
	}


	/**
	 * 主キーのデータを取得
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     readメソッドのモデルを初期化しない版(readメソッドと引数が逆なので注意!!)
	 * @param    int    $id        識別種別
	 * @param    arr    $fields    テーブル項目名一覧
	 * @return   mix    真：arr cakeデータ配列 偽(データなし)：bool false
	 */
	function findRead($id, $fields = array()) {
		$params = array(
			'conditions' => array("{$this->name}.{$this->primaryKey}" => $id),
			'fields' => $fields,
		);
		return $this->find('first', $params);
	}


	/**
	 * 削除前チェック基本メソッド
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     各々のモデルでオーバライドして使用
	 * @param    int     $id    主キー
	 * @return   bool    処理結果真偽値
	 */
	function deleteValidates($id) {

		//結果の返却
		if (count($this->validationErrors) > 0) {
			return false;
		}
		return true;
	}


	/**
	 * 削除フラグ
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     削除フラグを立てる
	 * @param    int     $id    主キー
	 * @return   bool    処理結果真偽値
	 */
	function delFlag($id) {
		$params = array(
			'conditions' => array($this->primaryKey => $id),
			'fields' => array($this->primaryKey),
			'recursive' => -1
		);
		$data = $this->find('first', $params);

		if ($data === false) {
			return false;
		}
		$data[$this->name]['del_flag'] = 1;
		$data[$this->name]['deleted'] = date('Y/m/d H:i:s');

		$this->create($data);
		if ($this->save(null, false, array('del_flag', 'deleted', 'updated'))) {
			return true;
		}
		return false;
	}


	/**
	 * paginateCountのオーバーライド
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     ページネートのカウントがgroup by使うと変になるので
	 * @param    arr    $conditions    WHERE条件
	 * @param    int    $recursive     リレーション階層
	 * @param    arr    $extra         検索条件
	 * @return   int    件数
	 */
	function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
	    $parameters = compact('conditions');
	    $this->recursive = $recursive;
	    $count = $this->find('count', array_merge($parameters, $extra));
	    if (isset($extra['group'])) {
	        $count = $this->getAffectedRows();
	    }
	    return $count;
	}
	/**
	 * 画像UP処理
	 * @author   nishishita@okushin.co.jp
	 * @date     2015/02/13
	 * @note     ページネートのカウントがgroup by使うと変になるので
	 * @param    str    $tempFile    元画像パス
	 * @param    str    $newName     新しい画像名
	 * @return   int    件数
	 */
	function upImage($tempFile, $newName) {
		//元画像の写真のサイズを取得します。
		list($width,$height)=getimagesize($tempFile);
		// 拡張子を取得する。
		$ext = pathinfo($tempFile, PATHINFO_EXTENSION);
		//元画像のJPEG・gif・pngファイルを読み込んでおきます。
		if ($ext == "bmp") {
			//元画像のbmpファイルを読み込んでおきます。
			$src=ImageCreateFromBMP($tempFile);
		} else {
			//元画像のJPEG・gif・pngファイルを読み込んでおきます。
			$src=@imagecreatefromstring(file_get_contents($tempFile));
		}

		// Sサイズの画像を保存する
		if ($width>$height){
		    $new_width = S_WIDTH_SIZE;
			$rate = $new_width / $width; //圧縮比
			$new_height = $rate * $height;
		}else{
		    $new_height = S_HEIGHT_SIZE;
			$rate = $new_height / $height; //圧縮比
			$new_width = $rate * $width;
		}
		//縦横比で新たなキャンパスサイズを決定して空の画像を作ります。
		$dst=imagecreatetruecolor($new_width, $new_height);
		//新しい画像に元の画像をコピーします。
		imagecopyresampled($dst,$src,0,0,0,0,$new_width,$new_height,$width,$height);
		header("Content-type: image/png");
		header("Cache-control: no-cache");
		//ファイルに保存します。第3引数の数字はクオリティーなので好みの数値を0～100で入れる。
		imagejpeg($dst, ROOT_PATH . 'upload/' .  $newName . '-s.jpg');

		@chmod(ROOT_PATH . 'upload/' .  $newName . '-s.jpg', 0777);

		//メモリから画像を開放しておく。
		imagedestroy($dst);
	}
}
?>