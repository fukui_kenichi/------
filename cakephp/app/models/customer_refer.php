<?php
/**
 * 顧客検索テーブルモデル
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note
 */
class CustomerRefer extends AppModel {
	var $name = 'CustomerRefer';
	//var $useTable = false;	//使用テーブル名(使用しないときはfalse)

	// 日本語項目名定義
	var $label = array(
		'customer_id' => '顧客ID',
		'refer' => '検索用文字列',
	);

	// バリデーション定義(BasicValidation用)
	var $valid = array(
		'customer_id' => '',
		'refer' => '',
	);


	/**
	 * BasicValidationBehaviorによるバリデーションのロード
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function loadValidate() {

		// 条件によって入力チェック追加
		//$this->valid['xxx'] = 'required | alphaNumeric';

		// バリデーション定義をモデルにセット
		$this->setValidate($this->valid);

		// エラーメッセージをデフォルト以外に変更する
		//$this->validate['email']['valid_email']['message'] = 'カスタムエラーメッセージ';

	}

	/**
	 * 入力チェック(複雑)
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function validates() {
		parent::validates();

		//結果の返却
		if (count($this->validationErrors) > 0) {
			return false;
		}
		return true;
	}


	/**
	 * 指定されたCustomerIdのCustomerReferを生成・更新する関数
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/03
	 * @note
	 * @param    int   $customerId   顧客ID
	 * $return   bool  処理の真偽値
	 */
	function updateByCustomerId($customerId) {
		// Customerデータを取得
		App::import('Model', 'Customer');
		$Customer = new Customer();
		$customerData = $Customer->find('first', array('fields'=>'Customer.*','conditions'=>array('Customer.id'=>$customerId)));

		// 取得できなければ戻る
		if (empty($customerData)) return false;

		// Customerの項目を取得
		App::import('Model', 'CustomerMaster');
		$CustomerMaster = new CustomerMaster();
		$customerTableInfo = $CustomerMaster->getTableInfoForCustomerRefer();

		$refer = '';
		foreach ((array)$customerTableInfo as $key => $val) {
			// address_info(住所)は特別なので飛ばす
			if ($val['CustomerMaster']['field'] == 'address_info') continue;

			// laste event_dateはY/m/dの書式で
			if ($val['CustomerMaster']['field'] == 'last_event_date') $customerData['Customer']['last_event_date'] = dateFormat($customerData['Customer']['last_event_date'], 'Y/m/d');

			// refer文を作成
			$refer .= $val['CustomerMaster']['koumoku']. '='. $customerData['Customer'][$val['CustomerMaster']['field']]. ' ';
		}
		// CustomerMasterにない項目(住所関係)を追加
		$refer .= '郵便番号='. $customerData['Customer']['zip_code']. ' ';
		$refer .= '住所='. $customerData['Customer']['address'];
		$refer = trim($refer);

		// SQL文を作成して実行
		$sql ="INSERT INTO customer_refers (customer_id, refer) VALUES ({$customerId}, '{$refer}') ON DUPLICATE KEY UPDATE refer = VALUES(refer);";
		if (!$this->query($sql)){
			$this->invalidate('error', 'CustomerReferのデータの保存に失敗しました。');
			$this->rollback();
			return false;
		}

		return true;
	}


	/**
	 * Customerにあるデータデータ全てのレコードをCustomerReferに生成・更新する関数
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/04
	 * @note
	 * $return   bool  処理の真偽値
	 */
	function updateAll() {
		// Customerデータを取得
		App::import('Model', 'Customer');
		$Customer = new Customer();
		$customerData = $Customer->find('all', array('fields'=>'Customer.*','conditions'=>array('Customer.del_flag'=>0)));

		// 取得できなければ戻る
		if (empty($customerData)) return true;

		// Customerの項目を取得
		App::import('Model', 'CustomerMaster');
		$CustomerMaster = new CustomerMaster();
		$customerTableInfo = $CustomerMaster->getTableInfoForCustomerRefer();

		foreach ($customerData as $key1 => $val1) {
			$refer = '';
			foreach ((array)$customerTableInfo as $key2 => $val2) {
				// address_info(住所)は特別なので飛ばす
				if ($val2['CustomerMaster']['field'] == 'address_info') continue;

				// laste event_dateはY/m/dの書式で
				if ($val2['CustomerMaster']['field'] == 'last_event_date') $val1['Customer']['last_event_date'] = dateFormat($val1['Customer']['last_event_date'], 'Y/m/d');

				// refer文を作成
				$refer .= $val2['CustomerMaster']['koumoku']. '='. $val1['Customer'][$val2['CustomerMaster']['field']]. ' ';
			}
			// CustomerMasterにない項目(住所関係)を追加
			$refer .= '郵便番号='. $val1['Customer']['zip_code']. ' ';
			$refer .= '住所='. $val1['Customer']['address'];
			$refer = trim($refer);

			// SQL文を作成して実行
			$sql ="INSERT INTO customer_refers (customer_id, refer) VALUES ({$val1['Customer']['id']}, '{$refer}') ON DUPLICATE KEY UPDATE refer = VALUES(refer);";
			if (!$this->query($sql)){
				$this->invalidate('error', 'CustomerReferのデータの保存に失敗しました。');
				$this->rollback();
				return false;
			}
		}

		return true;
	}


	/**
	 * 顧客検索画面で顧客IDを返す関数
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/04
	 * @note
	 * @params   str   $refer   検索文字列
	 * @return   arr   担当者ID一覧
	 */
	function searchCustomerId($refer) {
		$regexFlag = false; // 正規表現を使うかどうかのフラグ

		// 特定の文字列を含む場合は正規表現を使用する
		if (mb_substr_count($refer, '[0-9]', 'UTF-8')) $regexFlag = true;

		// 顧客IDを求めるクエリ
		$params = array(
			'fields'=>array('CustomerRefer.customer_id'),
		);

		// WHERE句 条件設定
		if ($regexFlag) { // 正規表現を使用する場合
			$params['conditions'] = array('CustomerRefer.refer REGEXP'=> $refer);
		} else { // LIKEを使用する場合
			$params['conditions'] = array('CustomerRefer.refer LIKE'=>'%'.$refer.'%');
		}

		return $this->find('all', $params);
	}
}
?>