<?php
/**
 * 顧客テーブルモデル
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note
 */
class Customer extends AppModel {
	var $name = 'Customer';
	//var $useTable = false;	//使用テーブル名(使用しないときはfalse)

	// 日本語項目名定義
	var $label = array(
		'name' => '名前',
		'tel' => '電話番号',
		'mail' => 'メールアドレス',
		'memo' => 'メモ',
		'picture' => '写真',
	);

	// バリデーション定義(BasicValidation用)
	var $valid = array(
		'name' => '',
		'tel' => '',
		'mail' => '',
		'memo' => '',
		'picture' => '',
	);


	/**
	 * BasicValidationBehaviorによるバリデーションのロード
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function loadValidate() {
		App::import('Model', 'CustomerMaster');
		$CustomerMaster = new CustomerMaster();
		$ret = $CustomerMaster->getCustomerTableInfo();

		$lValid = array(
			'name' => 'required | mb_maxLength[256]',
			'tel' => 'tel',
			'mail' => 'mb_maxLength[256]',
			'zip_code' => 'zipcode',
			'pref' => 'integer',
			'address_1' => 'mb_maxLength[256]',
			'address_2' => 'mb_maxLength[256]',
			'memo' => 'mb_maxLength[8000]',
			'picture' => 'mb_maxLength[256]',
		);
		$mastValid = array(
			MASTER_TYPE_CHAR => 'mb_maxLength[256]',	// 文字列（制限あり）
			MASTER_TYPE_TEXT => 'mb_maxLength[8000]',	// 文字列（制限なし）
			MASTER_TYPE_INTG => 'numeric',				// 数値型
			MASTER_TYPE_DTTM => array('_d' => 'date','_t' => 'time'),			// 日付時刻型
			MASTER_TYPE_DATE => 'date',				// 日付型
			MASTER_TYPE_DATE_A => 'date',				// 日付型(アラート)
			MASTER_TYPE_TIME => 'time',				// 時間型
			MASTER_TYPE_LIST => 'mb_maxLength[256]',	// リスト型
			MASTER_TYPE_RADI => 'mb_maxLength[256]',	// ラジオ型
			MASTER_TYPE_CHEK => 'mb_maxLength[256]',	// チェック型
			MASTER_TYPE_FIMG => null,					// 添付写真
			MASTER_TYPE_FILE => null,					// 添付ファイル
			MASTER_TYPE_TELE => 'integer',			// 電話型
		);

		// CustomerMasterデータ分繰り返す
		foreach ($ret as $key => $val) {
			$val = $val['CustomerMaster'];
			$field = $val['field'];
			// 最終更新日はスルー
			if ($field == 'last_event_date'){
				continue;
			}

			// 固定項目
			if (isset($lValid[$field])) {
				// 入力チェックを設定する。
				$this->valid[$field] = $lValid[$field];
				$this->label[$field] = $val['koumoku'];
				continue;
			}
			// 追加項目の入力チェックを設定する。
			$type = $val['type'];
			$valid = "";
			if (isset($mastValid[$type])) {
				if ($type != 4) {
					// 必須の時
					if ($val['required']) {
						$valid = "required | {$mastValid[$type]}";
					} else {
						$valid = $mastValid[$type];
					}
					$this->valid[$field] = $valid;
					$this->label[$field] = $val['koumoku'];
				} else {
					foreach ($mastValid[$type] as $k => $v) {
						$f = $field . $k;
						// 必須の時
						if ($val['required']) {
							$valid = "required | {$v}";
						} else {
							$valid = $v;
						}
						$this->valid[$f] = $valid;
						$this->label[$f] = $val['koumoku'] . $v;
					}
				}
			}
		}

		// 条件によって入力チェック追加
		//$this->valid['xxx'] = 'required | alphaNumeric';

		// バリデーション定義をモデルにセット
		$this->setValidate($this->valid);

		// エラーメッセージをデフォルト以外に変更する
		//$this->validate['email']['valid_email']['message'] = 'カスタムエラーメッセージ';

	}


	/**
	 * 入力チェック(複雑)
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function validates() {
		parent::validates();

		App::import('Model', 'CustomerMaster');
		$CustomerMaster = new CustomerMaster();
		$ret = $CustomerMaster->getCustomerTableInfo();

		$data =& $this->data['Customer'];

		foreach ($ret as $key => $val) {
			$val =& $ret[$key]['CustomerMaster'];
			if ($val['type'] == MASTER_TYPE_DTTM) {	// 日付時間型
				if(empty($data["{$val['field']}_d"]) && !empty($data["{$val['field']}_t"])) {
					$this->invalidate("{$val['field']}_d", "【{$val['koumoku']}date】日付を入力してください。");
				}
				if(!empty($data["{$val['field']}_d"]) && empty($data["{$val['field']}_t"])) {
					$this->invalidate("{$val['field']}_t", "【{$val['koumoku']}time】時間を入力してください。");
				}
			}
		}

		//結果の返却
		if (count($this->validationErrors) > 0) {
			return false;
		}

		return true;

	}


	/**
	 * 画像を本番にUPしていらないファイルを削除する
	 * @author   nishishita@okushin.co.jp
	 * @date     2015/02/17
	 * @note
	 * @param    int    $id    カテゴリID
	 */
	function seiriFile($id) {
		$customer = $this->read(null, $id);
		$customerImg = array('picture');

		//画像を整理する
		foreach ($customerImg as $val){
			$newFile = 'customer_'. $id;
			if (empty($customer['Customer'][$val])){

				//削除処理実行
				if (is_file(ROOT_PATH . 'picture/img/' . $newFile . '.png')){
					@unlink(ROOT_PATH . 'picture/img/' . $newFile . '.png');
				}
				//削除処理実行
				if (is_file(ROOT_PATH . 'picture/img/' . $newFile . '.jpg')){
					@unlink(ROOT_PATH . 'picture/img/' . $newFile . '.jpg');
				}
				//削除処理実行
				if (is_file(ROOT_PATH . 'picture/img/' . $newFile . '.jpeg')){
					@unlink(ROOT_PATH . 'picture/img/' . $newFile . '.jpeg');
				}
				//削除処理実行
				if (is_file(ROOT_PATH . 'picture/img/' . $newFile . '-s.jpg')){
					@unlink(ROOT_PATH . 'picture/img/' . $newFile . '-s.jpg');
				}
			}else{
				$tempFile = ROOT_PATH . 'picture/temp/' . $customer['Customer'][$val];
				//添付ファイルが存在したら添付ファイルを本番にUPする
				if (is_file($tempFile)){
					// 拡張子を取得する。
					$ext = pathinfo($tempFile, PATHINFO_EXTENSION);
					$ext = strtolower($ext);

					//添付ファイルをコピーして本番にUPする
					$this->upImageCustomer($tempFile, $newFile, 'img');
					// テンポラリから正式に移動
					rename($tempFile, ROOT_PATH . 'picture/img/' . $newFile . '.' . $ext);
					@chmod(ROOT_PATH . 'picture/img/' . $newFile . '.' . $ext,0777);
					//添付ファイルを削除する
					@chmod($tempFile,0777);
					@unlink($tempFile);

					//DBのファイル名をきちんとした形で上げ直す
					$newFile = $newFile . '.' . $ext;
					$sql = "UPDATE customers SET {$val} = '{$newFile}' WHERE id = {$id}";
					$this->query($sql);
				}
			}
		}
	}


	/**
	 * 画像UP処理
	 * @author   nishishita@okushin.co.jp
	 * @date     2015/02/13
	 * @note     ページネートのカウントがgroup by使うと変になるので
	 * @param    str    $tempFile    元画像パス
	 * @param    str    $newName     新しい画像名
	 * @return   int    件数
	 */
	function upImageCustomer($tempFile, $newName, $mode = 'img') {
		//元画像の写真のサイズを取得します。
		list($width,$height)=getimagesize($tempFile);
		// 拡張子を取得する。
		$ext = pathinfo($tempFile, PATHINFO_EXTENSION);
		// ファイル名拡張子なし名
		$fileName = pathinfo($newName, PATHINFO_FILENAME);
		//元画像のJPEG・gif・pngファイルを読み込んでおきます。
		if ($ext == "bmp") {
			//元画像のbmpファイルを読み込んでおきます。
			$src=ImageCreateFromBMP($tempFile);
		} else {
			//元画像のJPEG・gif・pngファイルを読み込んでおきます。
			$src=@imagecreatefromstring(file_get_contents($tempFile));
		}

		// Sサイズの画像を保存する
		if ($width>$height){
		    $new_width = S_USER_WIDTH_SIZE;
			$rate = $new_width / $width; //圧縮比
			$new_height = $rate * $height;
		}else{
		    $new_height = S_USER_HEIGHT_SIZE;
			$rate = $new_height / $height; //圧縮比
			$new_width = $rate * $width;
		}

		//縦横比で新たなキャンパスサイズを決定して空の画像を作ります。
		$dst=imagecreatetruecolor($new_width, $new_height);
		//新しい画像に元の画像をコピーします。
		imagecopyresampled($dst,$src,0,0,0,0,$new_width,$new_height,$width,$height);
		header("Content-type: image/png");
		header("Cache-control: no-cache");
		//ファイルに保存します。第3引数の数字はクオリティーなので好みの数値を0～100で入れる。
		if ($mode == 'temp') {
			imagejpeg($dst, ROOT_PATH . 'picture/temp/' .  $fileName . '-s.jpg');
			@chmod(ROOT_PATH . 'picture/temp/' .  $fileName . '-s.png', 0777);
		} else {
			imagejpeg($dst, ROOT_PATH . 'picture/img/' .  $fileName . '-s.jpg');
			@chmod(ROOT_PATH . 'picture/img/' .  $fileName . '-s.png', 0777);
		}

		//メモリから画像を開放しておく。
		imagedestroy($dst);
	}


	/**
	 * 最終イベント(last_event_date)を本日の日付で更新する関数
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/03
	 * @note
	 * @param    int   $customerId    顧客ID
	 * @return   bool  処理結果の真偽
	 */
	function updateLastEventDateTodayByCustomerId($customerId) {
		$today = date('Y/m/d');
		$now = date('Y/m/d H:i:s');

		$sql ="UPDATE customers SET last_event_date = '{$today}', updated = '{$now}' WHERE id = {$customerId};";
		if (!$this->query($sql)) {
			$this->invalidate('error', 'Customerデータの最終イベントの更新に失敗しました。');
			$this->rollback();
			return false;
		}

		return true;
	}


	/**
	 * 緯度経度から候補顧客を取得
	 * @author   fukui@okushin.co.jp
	 * @date     2015/03/12
	 * @note
	 * @return   arr    初期データ配列
	 */
	function getCandidate($lngHigh, $lngLow, $latHigh, $latLow) {
		$returnData = array();

		$params = array(
			'fields' => array(
				'Customer.id',
				'Customer.name',
				'Customer.latitude',
				'Customer.longitude',
			),
			'conditions' => array(
					'Customer.longitude <' => $lngHigh,
					'Customer.longitude >' => $lngLow,
					'Customer.latitude <' => $latHigh,
					'Customer.latitude >' => $latLow,
					'Customer.del_flag' => 0,
			),
			'order' => array('Customer.id asc'),
		);
		$rawData = $this->find('all', $params);

		if (!empty($rawData)) {
			foreach ($rawData as $key => $val) {
				$returnData[$key] = $val['Customer'];
			}
		}

		return $returnData;
	}


	/**
	 * お知らせに引っかかる顧客取得
	 * @author   fukui@okushin.co.jp
	 * @date     2015/05/08
	 * @note
	 * @param    arr    $customerMaster
	 * @return   arr    顧客配列
	 */
	function getAlertCustomer($customerMaster) {
		$select_ti = $customerMaster['CustomerMaster']['select_ti'];
		$field = $customerMaster['CustomerMaster']['field'];

		$now_date = date('Ymd');
		$now_year = date('Y');
		$target_date = date('Ymd', strtotime($now_date . "+{$select_ti}day"));
		$target_year = date('Y', strtotime($now_date . "+{$select_ti}day"));

		$sql = "SELECT id, name, (DATE_FORMAT({$field}, '%m月%d日')) AS day FROM customers WHERE (DATE_FORMAT({$field}, '{$now_year}%m%d') >= '{$now_date}') AND (DATE_FORMAT({$field}, '{$target_year}%m%d') <= '{$target_date}')";

		$return = $this->query($sql);
		return $return;
	}

}
?>