<?php
/**
 * ユーザーテーブルモデル
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note
 */
class User extends AppModel {
	var $name = 'User';
	//var $useTable = false;	//使用テーブル名(使用しないときはfalse)

	// 日本語項目名定義
	var $label = array(
//		'account' => 'ID',
//		'password' => 'パスワード',
//		'name' => '名前',
//		'tel' => '電話番号',
//		'mail' => 'メールアドレス',
//		'memo' => 'メモ',
//		'auth_flag' => '管理者フラグ',
//		'picture' => '写真',
	);

	// バリデーション定義(BasicValidation用)
	var $valid = array(
//		'account' => 'required',
//		'password' => 'required',
//		'name' => 'required',
//		'tel' => 'required',
//		'mail' => 'required',
//		'memo' => 'required',
//		'auth_flag' => 'required',
//		'picture' => '',
	);


	/**
	 * BasicValidationBehaviorによるバリデーションのロード
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function loadValidate() {
		App::import('Model', 'UserMaster');
		$UserMaster = new UserMaster();
		$ret = $UserMaster->getUserTableInfo();

		$pasValid = array(
			'password_01' => 'required | alphaNumeric | mb_maxLength[256]',
			'password_02' => 'required | alphaNumeric | mb_maxLength[256]',
		);
		$pasLabel = array(
			'password_01' => 'パスワード',
			'password_02' => 'パスワード確認',
		);

		$lValid = array(
			'account' => 'required | single | mb_maxLength[256]',
			'password' => 'required | alphaNumeric | mb_maxLength[256]',
			'name' => 'required | mb_maxLength[256]',
			'tel' => 'tel',
			'mail' => 'mb_maxLength[256]',
			'memo' => 'mb_maxLength[8000]',
			'auth_flag' => 'required | mb_maxLength[256]',
			'picture' => 'mb_maxLength[256]',
		);
		$mastValid = array(
			MASTER_TYPE_CHAR => 'mb_maxLength[256]',	// 文字列（制限あり）
			MASTER_TYPE_TEXT => 'mb_maxLength[8000]',	// 文字列（制限なし）
			MASTER_TYPE_INTG => 'numeric',				// 数値型
			MASTER_TYPE_DTTM => array('_d' => 'date','_t' => 'time'),			// 日付時刻型
			MASTER_TYPE_DATE => 'date',				// 日付型
			MASTER_TYPE_TIME => 'time',				// 時間型
			MASTER_TYPE_LIST => 'mb_maxLength[256]',	// リスト型
			MASTER_TYPE_RADI => 'mb_maxLength[256]',	// ラジオ型
			MASTER_TYPE_CHEK => 'mb_maxLength[256]',	// チェック型
			MASTER_TYPE_FIMG => null,					// 添付写真
			MASTER_TYPE_FILE => null,					// 添付ファイル
			MASTER_TYPE_TELE => 'integer',			// 電話型
		);

		// UserMasterデータ分繰り返す
		foreach ($ret as $key => $val) {
			$val = $val['UserMaster'];
			$field = $val['field'];
			// 固定項目
			if (isset($lValid[$field])) {
				// パスワードは２つの項目に分かれる。
				if ($field == "password") {
					foreach ($pasValid as $k => $v) {
						// 入力チェックを設定する。
						$this->valid[$k] = $v;
						$this->label[$k] = $pasLabel[$k];
					}
					continue;
				}
				// 入力チェックを設定する。
				$this->valid[$field] = $lValid[$field];
				$this->label[$field] = $val['koumoku'];
				continue;
			}
			// 追加項目の入力チェックを設定する。
			$type = $val['type'];
			$valid = "";
			if ($mastValid[$type]) {
				if ($type != 4) {
					// 必須の時
					if ($val['required']) {
						$valid = "required | {$mastValid[$type]}";
					} else {
						$valid = $mastValid[$type];
					}
					$this->valid[$field] = $valid;
					$this->label[$field] = $val['koumoku'];
				} else {
					foreach ($mastValid[$type] as $k => $v) {
						$f = $field . $k;
						// 必須の時
						if ($val['required']) {
							$valid = "required | {$v}";
						} else {
							$valid = $v;
						}
						$this->valid[$f] = $valid;
						$this->label[$f] = $val['koumoku'] . $v;
					}
				}
			}
		}

		// 条件によって入力チェック追加
		//$this->valid['xxx'] = 'required | alphaNumeric';

		// バリデーション定義をモデルにセット
		$this->setValidate($this->valid);

		// エラーメッセージをデフォルト以外に変更する
		//$this->validate['email']['valid_email']['message'] = 'カスタムエラーメッセージ';

	}


	/**
	 * 入力チェック(複雑)
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function validates() {
		parent::validates();

		App::import('Model', 'UserMaster');
		$UserMaster = new UserMaster();
		$ret = $UserMaster->getUserTableInfo();

		$data =& $this->data['User'];

		if (!$this->isUnique('account')) {
			$this->invalidate('account', "同じIDのユーザーがすでに登録されています。異なるIDを入力してください。");
		}

		if ($data['password_01'] != $data['password_02']) {
			$this->invalidate('password_01', "パスワードを更新できません。パスワードとパスワード確認の入力内容が一致しません。");
			$this->invalidate('password_02', "");
		}
		foreach ($ret as $key => $val) {
			$val =& $ret[$key]['UserMaster'];
			if ($val['type'] == MASTER_TYPE_DTTM) {	// 日付時間型
				if(empty($data["{$val['field']}_d"]) && !empty($data["{$val['field']}_t"])) {
					$this->invalidate("{$val['field']}_d", "【{$val['koumoku']}date】日付を入力してください。");
				}
				if(!empty($data["{$val['field']}_d"]) && empty($data["{$val['field']}_t"])) {
					$this->invalidate("{$val['field']}_t", "【{$val['koumoku']}time】時間を入力してください。");
				}
			}
		}
		//結果の返却
		if (count($this->validationErrors) > 0) {
			return false;
		}
		return true;
	}


	/**
	 * 個人設定画面入力チェック(複雑)
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/20
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function validatesByKojinEdit() {
//		parent::validates();

		// Userデータのvalidation
		$userData =& $this->data['User'];
		if (empty($userData['account'])) {
			$this->invalidate('account', "IDは必須項目です");
		}elseif (!$this->single($userData['account'])) {
			$this->invalidate('account', "IDは半角英数字で入力してください。");
		}elseif (!$this->isUnique('account')) {
			$this->invalidate('account', "同じIDのユーザーがすでに登録されています。異なるIDを入力してください。");
		}
		if (empty($userData['password_01'])) {
			$this->invalidate('password_01', "パスワードは必須項目です");
		}elseif ($userData['password_01'] != $userData['password_02']) {
			$this->invalidate('password_01', "パスワードを更新できません。パスワードとパスワード確認の入力内容が一致しません。");
			$this->invalidate('password_02', "");
		}

		// CustomerKojinデータのvalidation
		$customerKojinData =& $userData['CustomerKojin'];
		foreach ($customerKojinData as $key => $val) {
			if ((isset($val['ck']['view_flag']) && $val['ck']['view_flag'] == 1) OR !isset($val['ck']['view_flag'])) { // 表示するかどうか
				// 並び順
				if ($val['ck']['number'] == '') {
					$this->validationErrors['CustomerKojin'][$key]['ck']['number'] = 'error';
					$this->invalidate("ck_number_{$key}", '【顧客一覧並び順】表示する項目は必ず並び順に数字を入力してください');
				}
			}
		}

		// BusiKojinデータのvalidation
		$busiKojinData =& $userData['BusiKojin'];
		foreach ($busiKojinData as $key => $val) {
			if ((isset($val['bk']['view_flag']) && $val['bk']['view_flag'] == 1) OR !isset($val['bk']['view_flag'])) { // 表示するかどうか
				// 並び順
				if ($val['bk']['number'] == '') {
					$this->validationErrors['BusiKojin'][$key]['bk']['number'] = 'error';
					$this->invalidate("bk_number_{$key}", '【商談一覧並び順】表示する項目は必ず並び順に数字を入力してください');
				}
			}
		}

		//結果の返却
		if (count($this->validationErrors) > 0) {
			return false;
		}
		return true;
	}


	/**
	 * 画像を本番にUPしていらないファイルを削除する
	 * @author   nishishita@okushin.co.jp
	 * @date     2015/02/17
	 * @note
	 * @param    int    $id    カテゴリID
	 */
	function seiriFile($id) {
		$user = $this->read(null, $id);
		$userImg = array('picture');

		//画像を整理する
		foreach ($userImg as $val){
			$newFile = 'user_'. $id;
			if (empty($user['User'][$val])){

				//削除処理実行
				if (is_file(ROOT_PATH . 'picture/img/' . $newFile . '.png')){
					@unlink(ROOT_PATH . 'picture/img/' . $newFile . '.png');
				}
				//削除処理実行
				if (is_file(ROOT_PATH . 'picture/img/' . $newFile . '.jpg')){
					@unlink(ROOT_PATH . 'picture/img/' . $newFile . '.jpg');
				}
				//削除処理実行
				if (is_file(ROOT_PATH . 'picture/img/' . $newFile . '.jpeg')){
					@unlink(ROOT_PATH . 'picture/img/' . $newFile . '.jpeg');
				}
				//削除処理実行
				if (is_file(ROOT_PATH . 'picture/img/' . $newFile . '-s.jpg')){
					@unlink(ROOT_PATH . 'picture/img/' . $newFile . '-s.jpg');
				}
			}else{
				$tempFile = ROOT_PATH . 'picture/temp/' . $user['User'][$val];
				//添付ファイルが存在したら添付ファイルを本番にUPする
				if (is_file($tempFile)){
					// 拡張子を取得する。
					$ext = pathinfo($tempFile, PATHINFO_EXTENSION);
					//					// 拡張子の取得
//					$ext = preg_replace("/.*\./", null, $user['User'][$val]);
					$ext = strtolower($ext);

//					//新しいファイル名
//					$newName = $newFile . '.png';

					//添付ファイルをコピーして本番にUPする
					$this->upImageUser($tempFile, $newFile, 'img');
					// テンポラリから正式に移動
					rename($tempFile, ROOT_PATH . 'picture/img/' . $newFile . '.' . $ext);
					@chmod(ROOT_PATH . 'picture/img/' . $newFile . '.' . $ext,0777);
					//添付ファイルを削除する
					@chmod($tempFile,0777);
					@unlink($tempFile);

					//DBのファイル名をきちんとした形で上げ直す
					$newFile = $newFile . '.' . $ext;
					$sql = "UPDATE users SET {$val} = '{$newFile}' WHERE id = {$id}";
					$this->query($sql);
				}
			}
		}
	}
	/**
	 * 画像UP処理
	 * @author   nishishita@okushin.co.jp
	 * @date     2015/02/13
	 * @note     ページネートのカウントがgroup by使うと変になるので
	 * @param    str    $tempFile    元画像パス
	 * @param    str    $newName     新しい画像名
	 * @return   int    件数
	 */
	function upImageUser($tempFile, $newName, $mode = 'img') {
		//元画像の写真のサイズを取得します。
		list($width,$height)=getimagesize($tempFile);
		// 拡張子を取得する。
		$ext = pathinfo($tempFile, PATHINFO_EXTENSION);
		// ファイル名拡張子なし名
		$fileName = pathinfo($newName, PATHINFO_FILENAME);
		//元画像のJPEG・gif・pngファイルを読み込んでおきます。
		if ($ext == "bmp") {
			//元画像のbmpファイルを読み込んでおきます。
			$src=ImageCreateFromBMP($tempFile);
		} else {
			//元画像のJPEG・gif・pngファイルを読み込んでおきます。
			$src=@imagecreatefromstring(file_get_contents($tempFile));
		}

		// Sサイズの画像を保存する
		if ($width>$height){
		    $new_width = S_USER_WIDTH_SIZE;
			$rate = $new_width / $width; //圧縮比
			$new_height = $rate * $height;
		}else{
		    $new_height = S_USER_HEIGHT_SIZE;
			$rate = $new_height / $height; //圧縮比
			$new_width = $rate * $width;
		}
		//縦横比で新たなキャンパスサイズを決定して空の画像を作ります。
		$dst=imagecreatetruecolor($new_width, $new_height);
		//新しい画像に元の画像をコピーします。
		imagecopyresampled($dst,$src,0,0,0,0,$new_width,$new_height,$width,$height);
		header("Content-type: image/png");
		header("Cache-control: no-cache");
		//ファイルに保存します。第3引数の数字はクオリティーなので好みの数値を0～100で入れる。
		if ($mode == 'temp') {
			imagejpeg($dst, ROOT_PATH . 'picture/temp/' .  $fileName . '-s.jpg');
			@chmod(ROOT_PATH . 'picture/temp/' .  $fileName . '-s.png', 0777);
		} else {
			imagejpeg($dst, ROOT_PATH . 'picture/img/' .  $fileName . '-s.jpg');
			@chmod(ROOT_PATH . 'picture/img/' .  $fileName . '-s.png', 0777);
		}

		//メモリから画像を開放しておく。
		imagedestroy($dst);
	}



	/**
	 * ユーザ（担当）を選択する際のプルダウンデータ取得
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/02
	 * @note
	 * @param    int   $userId   ユーザID
	 * @return   arr   ユーザー一覧リスト
	 */
	function getUserArr($userId = null) {

		$params = array(
			'order' => array('User.id ASC'),
		);

		// 検索条件作成
		$cond = array();
		$cond['User.mente_flag'] = 0;
		$cond['OR'][0]['User.del_flag'] = 0;
		if (!empty($userId)){
			$cond['OR'][1]['User.id'] = $userId;
		}
		$params['conditions'] = $cond;

		return $this->find('list', $params);;
	}


	/**
	 * 最終作業(last_event_date)を本日の日付で更新する関数
	 * @author   fukui@okushin.co.jp
	 * @date     2015/04/23
	 * @note
	 * @param    int   $userId    ユーザーID
	 * @return   bool  処理結果の真偽
	 */
	function updateLastEventDateTodayByUserId($userId) {
		$today = date('Y/m/d');

		$sql ="UPDATE users SET last_event_date = '{$today}' WHERE id = {$userId};";
		if (!$this->query($sql)) {
			$this->invalidate('error', 'Userデータの最終イベントの更新に失敗しました。');
			$this->rollback();
			return false;
		}

		return true;
	}
}
?>