<?php
/**
 * ファイルアップロードテーブルモデル
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note
 */
class Upload extends AppModel {
	var $name = 'Upload';
	//var $useTable = false;	//使用テーブル名(使用しないときはfalse)

	// 日本語項目名定義
	var $label = array(
		'extension' => '拡張子',
		'file_name' => 'ファイル名',
		'up_file_name' => 'UPファイル名',
		'parent_table' => '親テーブル名',
		'parent_id' => '親テーブルID',
		'parent_field' => '親フィールド名',
	);

	// バリデーション定義(BasicValidation用)
	var $valid = array(
		'extension' => '',
		'file_name' => '',
		'up_file_name' => '',
		'parent_table' => '',
		'parent_id' => '',
		'parent_field' => '',
	);


	/**
	 * BasicValidationBehaviorによるバリデーションのロード
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function loadValidate() {

		// 条件によって入力チェック追加
		//$this->valid['xxx'] = 'required | alphaNumeric';

		// バリデーション定義をモデルにセット
		$this->setValidate($this->valid);

		// エラーメッセージをデフォルト以外に変更する
		//$this->validate['email']['valid_email']['message'] = 'カスタムエラーメッセージ';

	}

	/**
	 * 入力チェック(複雑)
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function validates() {
		parent::validates();

		//結果の返却
		if (count($this->validationErrors) > 0) {
			return false;
		}
		return true;
	}

	/**
	 * 仮保存
	 * @author   nishishita@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function saveFrist($num) {
		$idArr = array();
		$saveData = array();
		for($i = 0;$i < $num;$i ++) {
			$saveData[$i]['id'] = null;
			$saveData[$i]['created'] = date("Y-m-d H:i:s");
			$saveData[$i]['updated'] = date("Y-m-d H:i:s");
		}
		foreach ($saveData as $key => $val) {
			$this->create($val);
			// 保存
			if (!$this->save(null, false)) {
				return false;
			}
			$idArr[$key] = $this->getID();
		}
		return $idArr;
	}

	/**
	 * アップロード時
	 * @author   nishishita@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function saveSecond($idArr,$extension,$filename,$upName) {
		$updated = date("Y-m-d H:i:s");
		foreach ($idArr as $key => $val) {
			$sql = "update uploads
				set extension = '{$extension[$key]}',
					file_name = '{$filename[$key]}',
					up_file_name = '{$upName[$key]}',
					updated = '{$updated}'
				where id = '{$val}'";
			if ($this->query($sql) === false) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 親登録時
	 * @author   nishishita@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function saveThird($idArr,$parentTable,$parentId,$parentField) {
		$updated = date("Y-m-d H:i:s");
		foreach ($idArr as $key => $val) {
			$sql = "update uploads
				set parent_table = '{$parentTable}',
					parent_id = '{$parentId}',
					parent_field = '{$parentField}',
					updated = '{$updated}'
				where id = '{$val}'";
			if ($this->query($sql) === false) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 削除
	 * @author   nishishita@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function deleteFile($id) {
		$params = array(
			'conditions' => array(
				'Upload.id' => $id,
			),
			'fields' => array('Upload.*'),
		);
		$data = $this->find('first', $params);
		$sql = "delete from uploads where id = '{$id}'";
		if ($this->query($sql) === false) {
			return false;
		}
		$upPath = ROOT_PATH . 'upload/';
		$deleteFile = $upPath . $data['Upload']['file_name'];
		unlink($deleteFile);
		$deleteFile = $upPath . $id  . '-s.jpg';
		unlink($deleteFile);
		return true;
	}


	/**
	 * イメージファイル取得
	 * @author   nishishita@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function setImageFile($parentTable,$parentId,$fieldArr,&$data) {
		$data['Image'] = array();
		$params = array(
			'conditions' => array(
				'Upload.parent_table' => $parentTable,
				'Upload.parent_id' => $parentId,
			),
			'fields' => array('Upload.*'),
			'order' => array('Upload.parent_field asc','Upload.id asc'),
		);
		$res = $this->find('all', $params);
//		debug($res);
		if (empty($res)) {
			return;
		}
		// フィールド名順のID順に整理する。
		$list = array();
		foreach ($res as $key => $val) {
			$list[$val['Upload']['parent_field']][$val['Upload']['id']]['up_file_name'] = $val['Upload']['up_file_name'];
			$list[$val['Upload']['parent_field']][$val['Upload']['id']]['file_name'] = $val['Upload']['file_name'];
		}
		// 指定フィールドからNoを取得し、イメージファイル複数アップのポスト情報を生成する。
		foreach ($fieldArr as $key => $val) {
			$i = 1;
			if (isset($list[$key])) {
				foreach ($list[$key] as $id => $v) {
					$data['Image'][$val]["imagename_{$i}"] = $v['up_file_name'];
					$data['Image'][$val]["image_{$i}"] = $v['file_name'];
					$data['Image'][$val]["image_{$i}_id"] = $id;
					$i ++;
				}
			}
		}
//		debug($data['Image']);
		return;

	}


	/**
	 * イメージファイル取得
	 * @author   nishishita@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function setNotimageFile($parentTable,$parentId,$fieldArr,&$data) {
		$data['Notimage'] = array();
		$params = array(
			'conditions' => array(
				'Upload.parent_table' => $parentTable,
				'Upload.parent_id' => $parentId,
			),
			'fields' => array('Upload.*'),
			'order' => array('Upload.parent_field asc','Upload.id asc'),
		);
		$res = $this->find('all', $params);
//		debug($res);
		if (empty($res)) {
			return;
		}
		// フィールド名順のID順に整理する。
		$list = array();
		foreach ($res as $key => $val) {
			$list[$val['Upload']['parent_field']][$val['Upload']['id']]['up_file_name'] = $val['Upload']['up_file_name'];
			$list[$val['Upload']['parent_field']][$val['Upload']['id']]['file_name'] = $val['Upload']['file_name'];
		}
		// 指定フィールドからNoを取得し、イメージファイル複数アップのポスト情報を生成する。
		foreach ($fieldArr as $key => $val) {
			$i = 1;
			if (isset($list[$key])) {
				foreach ($list[$key] as $id => $v) {
					$data['Notimage'][$val]["notimagename_{$i}"] = $v['up_file_name'];
					$data['Notimage'][$val]["notimage_{$i}"] = $v['file_name'];
					$data['Notimage'][$val]["notimage_{$i}_id"] = $id;
					$i ++;
				}
			}
		}
//		debug($data['Image']);
		return;

	}

}
?>