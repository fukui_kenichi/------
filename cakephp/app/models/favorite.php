<?php
/**
 * お気に入りテーブルモデル
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note
 */
class Favorite extends AppModel {
	var $name = 'Favorite';
	//var $useTable = false;	//使用テーブル名(使用しないときはfalse)

	// 日本語項目名定義
	var $label = array(
		'name' => '名前',
		'keyword' => '検索キーワード',
		'user_id' => 'ユーザーID',
	);

	// バリデーション定義(BasicValidation用)
	var $valid = array(
		'name' => 'required | mb_maxLength[256]',
//		'keyword' => '',
//		'user_id' => '',
	);


	/**
	 * BasicValidationBehaviorによるバリデーションのロード
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function loadValidate() {

		// 条件によって入力チェック追加
		//$this->valid['xxx'] = 'required | alphaNumeric';

		// バリデーション定義をモデルにセット
		$this->setValidate($this->valid);

		// エラーメッセージをデフォルト以外に変更する
		//$this->validate['email']['valid_email']['message'] = 'カスタムエラーメッセージ';

	}


	/**
	 * 入力チェック(複雑)
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @param    arr     $login   ログイン情報
	 * @return   bool    処理結果真偽値
	 */
	function validates($login) {
		parent::validates();

		// 検索条件重複チェック
		$fields = array(
			'Favorite.name'=>$this->data['Favorite']['name'],
			'Favorite.user_id'=>$login['User']['id'],
		);
		if (!$this->isUnique($fields, false)) {
			$this->invalidate("name", "同じ名前の検索条件が、お気に入り登録されているため登録できません。違う名前を指定してください。");
		}

		//結果の返却
		if (count($this->validationErrors) > 0) {
			return false;
		}
		return true;
	}


	/**
	 * お気に入りIDからキーワードを取得する関数
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/12
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function getKeyWordById($id) {
		$returnData = null; // 返却用データ

		// データ取得
		$params = array(
			'fields' => array('Favorite.keyword'),
			'conditions' => array(
				'Favorite.id' => $id
			)
		);
		$result = $this->find('first', $params);

		// データ整理
		if (isset($result['Favorite']['keyword'])) {
			$returnData = $result['Favorite']['keyword'];
		}

		return $returnData;
	}
}
?>