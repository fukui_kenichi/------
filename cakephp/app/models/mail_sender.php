<?php
/**
 * メール配信者テーブルモデル
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note  
 */
class MailSender extends AppModel {
	var $name = 'MailSender';
	//var $useTable = false;	//使用テーブル名(使用しないときはfalse)

	// 日本語項目名定義
	var $label = array(
		'mail_id' => 'メール配信ID',
		'user_id' => 'ユーザーID',
		'send_flag' => '配信フラグ',
	);
	
	// バリデーション定義(BasicValidation用)
	var $valid = array(
		'mail_id' => '',
		'user_id' => '',
		'send_flag' => '',
	);
	
	
	/**
	 * BasicValidationBehaviorによるバリデーションのロード
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note     
	 */
	function loadValidate() {
		
		// 条件によって入力チェック追加
		//$this->valid['xxx'] = 'required | alphaNumeric';
		
		// バリデーション定義をモデルにセット
		$this->setValidate($this->valid);
		
		// エラーメッセージをデフォルト以外に変更する
		//$this->validate['email']['valid_email']['message'] = 'カスタムエラーメッセージ';
	
	}
	
	/**
	 * 入力チェック(複雑)
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note     
	 * @return   bool    処理結果真偽値
	 */
	function validates() {
		parent::validates();
		
		//結果の返却
		if (count($this->validationErrors) > 0) {
			return false;
		}
		return true;
	}
	

}
?>