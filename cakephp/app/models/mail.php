<?php
/**
 * メール配信テーブルモデル
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note
 */
class Mail extends AppModel {
	var $name = 'Mail';
	//var $useTable = false;	//使用テーブル名(使用しないときはfalse)

	// 日本語項目名定義
	var $label = array(
		'send_datetime' => '配信日時',
		'status' => '配信状態',
		'subject' => '件名',
		'body' => '本文',
	);

	// バリデーション定義(BasicValidation用)
	var $valid = array(
//		'send_datetime' => '',
//		'status' => '',
		'subject' => 'required | mb_maxLength[256]',
		'body' => 'required',
	);


	/**
	 * BasicValidationBehaviorによるバリデーションのロード
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function loadValidate() {

		// 条件によって入力チェック追加
		//$this->valid['xxx'] = 'required | alphaNumeric';

		// バリデーション定義をモデルにセット
		$this->setValidate($this->valid);

		// エラーメッセージをデフォルト以外に変更する
		//$this->validate['email']['valid_email']['message'] = 'カスタムエラーメッセージ';

	}

	/**
	 * 入力チェック(複雑)
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function validates() {
		parent::validates();

		/* 配信日時 */
		// 配信日付
		if(empty($this->data['Mail']['send_date']) || !$this->date($this->data['Mail']['send_date'])) {
			$this->invalidate('send_date', '【配信日付】日付を入力してください。');
		}
		// 配信時間
		if(empty($this->data['Mail']['send_time']) || !$this->time($this->data['Mail']['send_time'])) {
			$this->invalidate('send_time', '【配信時間】時間を入力してください。');
		}

		// 配信する顧客が選ばれているか
		$noCustomerFlag = true;
		foreach ((array)$this->data['MailSender'] as $key => $val) {
			if (!empty($val['send_flag'])) {
				$noCustomerFlag = false;
				break;
			}
		}
		if ($noCustomerFlag) {
			$this->invalidate('no_customer_flag', '【顧客】配信する顧客が選択されていません。');
		}

		//結果の返却
		if (count($this->validationErrors) > 0) {
			return false;
		}

		return true;
	}


	/**
	 * 指定された日時の前の配信待ちののメールを配信する
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/23
	 * @note
	 * @param    datetime   $datetime    指定日時
	 * @return   bool       処理真偽値
	 */
	function sendMailByDatetime($datetime) {
		// 配信するメール情報取得
		$params = array(
			'fields' => array('Mail.*'),
			'conditions' => array(
				'Mail.send_datetime <=' => $datetime,
				'Mail.status' => array(0, 1),	// 配信待ちのメールが対象
			),
			'recursive' => -1,
		);
		$mail =  $this->find('all', $params);

		// メール配信
		// Qdmail読み込み
		App::import('Component', 'Qdmail');
		$qdmail = new Qdmail();

		foreach ((array)$mail as $key => $val) {
			// メール状態更新
			$saveData['id'] = $val['Mail']['id'];
			$saveData['status'] = 3;	// 実際の配信中
			$this->create($saveData);
			if (!$this->save(null,false)) {
				return false;
			}
		}

		foreach ((array)$mail as $key => $val) {
			// 配信するメール情報取得
			$params = array(
				'fields' => array('Mail.*', 'Customer.mail', 'Customer.name'),
				'conditions' => array(
					'Mail.id' => $val['Mail']['id'],
					'MailSender.send_flag' => 1	// 配信フラグが立っているいるのが対象
				),
				'recursive' => -1,
				'joins' => array(
					array(
						'type' => 'inner',
						'table' => 'mail_senders',
						'alias' => 'MailSender',
						'conditions' => 'MailSender.mail_id = Mail.id'
					),
					array(
						'type' => 'inner',
						'table' => 'customers',
						'alias' => 'Customer',
						'conditions' => 'Customer.id = MailSender.customer_id'
					),
				)
			);
			$mailSender =  $this->find('all', $params);
			foreach ((array)$mailSender as $val2){
				// {{name}}という文字列があれば
		    	$search = array('/{{name}}/s');
				$replace = array($val2['Customer']['name']);
				$val2['Mail']['body'] = reVal($search, $replace, $val2['Mail']['body']);	//エスケープ


				// メール送信
				$qdmail->subject($val2['Mail']['subject']);
				$qdmail->to($val2['Customer']['mail']);		// 送信先
				$qdmail->from(MAIL_ADMIN);					// 送信元
				$qdmail->mtaOption('-f '. MAIL_ADMIN);
				$qdmail->kana(true);
				$qdmail->text($val2['Mail']['body']);		// 本文
				$qdmail->send();							// メール送信処理
			}
			// メール状態更新
			$saveData['id'] = $val['Mail']['id'];
			$saveData['status'] = 2;	// 配信済
			$this->create($saveData);
			if (!$this->save(null,false)) {
				return false;
			}
		}

		return true;
	}
}
?>