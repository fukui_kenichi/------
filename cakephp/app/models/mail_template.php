<?php
/**
 * メールテンプレートテーブルモデル
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note
 */
class MailTemplate extends AppModel {
	var $name = 'MailTemplate';
	//var $useTable = false;	//使用テーブル名(使用しないときはfalse)

	// 日本語項目名定義
	var $label = array(
		'name' => '名前',
		'subject' => '件名',
		'body' => '本文',
	);

	// バリデーション定義(BasicValidation用)
	var $valid = array(
		'name' => 'required | mb_maxLength[256]',
		'subject' => 'mb_maxLength[256]',
		'body' => 'mb_maxLength[8000]',
	);


	/**
	 * BasicValidationBehaviorによるバリデーションのロード
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function loadValidate() {

		// 条件によって入力チェック追加
		//$this->valid['xxx'] = 'required | alphaNumeric';

		// バリデーション定義をモデルにセット
		$this->setValidate($this->valid);

		// エラーメッセージをデフォルト以外に変更する
		//$this->validate['email']['valid_email']['message'] = 'カスタムエラーメッセージ';

	}


	/**
	 * 入力チェック(複雑)
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function validates() {
		parent::validates();

		//結果の返却
		if (count($this->validationErrors) > 0) {
			return false;
		}
		return true;
	}


	/**
	 * メールテンプレート一覧取得関数
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/17
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function getArr() {
		$params = array(
			'conditions' => array(),
			'order' => array('MailTemplate.id ASC'),
		);

		return $this->find('list', $params);;
	}
}
?>