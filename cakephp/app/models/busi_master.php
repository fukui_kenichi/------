<?php
/**
 * 商談マスターテーブルモデル
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note
 */
class BusiMaster extends AppModel {
	var $name = 'BusiMaster';
	//var $useTable = false;	//使用テーブル名(使用しないときはfalse)

	// 日本語項目名定義
	var $label = array(
		'koumoku' => '項目名',
		'type' => '型',
		'default_ti' => '初期値',
		'select_ti' => '選択値',
		'required' => '必須入力',
		'number' => '並び順',
		'default_flag' => 'デフォルトフラグ',
		'field' => 'DBフィールド名',
	);

	// バリデーション定義(BasicValidation用)
	var $valid = array(
		'koumoku' => '',
		'type' => '',
		'default_ti' => '',
		'select_ti' => '',
		'required' => '',
		'number' => '',
		'default_flag' => '',
		'field' => '',
	);


	/**
	 * BasicValidationBehaviorによるバリデーションのロード
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function loadValidate() {

		// 条件によって入力チェック追加
		//$this->valid['xxx'] = 'required | alphaNumeric';

		// バリデーション定義をモデルにセット
		$this->setValidate($this->valid);

		// エラーメッセージをデフォルト以外に変更する
		//$this->validate['email']['valid_email']['message'] = 'カスタムエラーメッセージ';

	}

	/**
	 * 入力チェック(複雑)
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function validates() {
		parent::validates();

		//結果の返却
		if (count($this->validationErrors) > 0) {
			return false;
		}
		return true;
	}


	/**
	 * 一覧保存入力チェック(複雑)
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/18
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function validatesByIndex() {
		if (isset($this->data)) {
			foreach ($this->data['BusiMaster'] as $key => $val) {
				// 削除データはスキップ
				if (isset($val['db_id']) && empty($val['id'])) continue;

				$row = "";
				if (!empty($val['number'])){
					$row = "並び順{$val['number']}行目の";
				}

				if (!empty($val['default_flag'])) { // デフォルト項目ではない項目
					// 項目名
					if (empty($val['koumoku'])) {
						$this->validationErrors[$key]['koumoku'] = 'error';
						$this->invalidate("koumoku_{$key}", "{$row}【項目名】必ず入力してください");
					}

					if ($val['type'] == MASTER_TYPE_LIST || $val['type'] == MASTER_TYPE_RADI) { // リスト型もしくはラジオ型の場合
						// 選択値の必須チェック
						if (empty($val['select_ti'])) {
							$this->validationErrors[$key]['select_ti'] = 'error';
							$this->invalidate("select_ti_{$key}", "{$row}【選択値】リスト型、ラジオ型では必ず入力してください");
						}
//						// 初期値の必須チェック
//						if (empty($val['default_ti'])) {
//							$this->validationErrors[$key]['default_ti'] = 'error';
//							$this->invalidate("default_ti_{$key}", "{$row}【初期値】リスト型、ラジオ型では必ず入力してください");
//						}
						// 初期値の内容が選択値に存在しなければエラー
						if (!empty($val['select_ti']) && !empty($val['default_ti'])){
							$selectArr = explode("\r\n", $val['select_ti']);
							$flag = false;
							foreach ($selectArr as $selectOne){
								if ($selectOne == $val['default_ti']){
									$flag = true;
								}
							}
							if ($flag == false){
								$this->validationErrors[$key]['default_ti'] = 'error';
								$this->invalidate("default_ti_{$key}", "{$row}【初期値】の内容が選択値に存在しません");
							}
						}

					}
				}

				if (!isset($val['db_id'])) { // 新規項目
					// 型
					if (empty($val['type'])) {
						$this->validationErrors[$key]['type'] = 'error';
						$this->invalidate("type_{$key}", "{$row}【型】必ず入力してください");
					}
				}

				// 並び順
				if ($val['number'] == '') {
					$this->validationErrors[$key]['number'] = 'error';
					$this->invalidate("number_{$key}", "{$row}【並び順】必ず数字を入力してください");
				}
			}
		}

		//結果の返却
		if (count($this->validationErrors) > 0) {
			return false;
		}
		return true;
	}


	/**
	 * 一覧画面用データ取得関数
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/18
	 * @return   array   一覧画面用データ
	 */
	function getDataByIndex() {
		// 取得条件設定
		// 有効なデータを全件取得
		$params = array(
			'fields'=>array('BusiMaster.*'),
			'conditions'=>array(),
			'order'=>array('BusiMaster.number ASC'),
		);

		// 検索結果を返します
		return $this->find('all',  $params);
	}


	/**
	 * 並び順整列する
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/17
	 * @note     入力された表示番号、idでソートした順番に
	 * @return   bool 処理結果真偽値
	 */
	function sortRank() {
		// 有効なデータを全件取得
		$params = array(
			'fields'=>array('BusiMaster.id'),
			'order'=>array('BusiMaster.number ASC', 'BusiMaster.id ASC'),
		);
		$result = $this->find('all',  $params);

		// 並び順をソートする
		$this->checkUpdated = false;	// 更新時間チェックOFF
		foreach ($result as $key => $val){
			$rank = $key + 1;
			$id = $val['BusiMaster']['id'];

			$sql = "UPDATE busi_masters SET number = $rank WHERE id ={$id};";
			if ($this->query($sql) === false){
				return false;
			}
		}

		return true;
	}


	/**
	 * Busyテーブル並び順取得
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/27
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function getBusyTableInfo() {
		$params = array(
			'fields' => array("*"),
			'conditions' => array(),
			'order' => array('BusiMaster.number asc'),
			);
		$ret = $this->find('all', $params);
		return $ret;
	}


	/**
	 * CustomerReferにデータを追加するための情報取得
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/02
	 * @note
	 * @return   arr
	 */
	function getTableInfoForBusiRefer() {
		$params = array(
			'fields' => array("*"),
			'conditions' => array(
				'NOT' => array(
					'BusiMaster.type' => array(MASTER_TYPE_FIMG, MASTER_TYPE_FILE)
				)
			),
			'order' => array('BusiMaster.number ASC'),
		);

		return $this->find('all', $params);
	}


	/**
	 * 日付型(アラート)のデータ取得
	 * @author   fukui@okushin.co.jp
	 * @date     2015/05/08
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function getAlertField() {
		$params = array(
			'fields' => array("*"),
			'conditions' => array('BusiMaster.type'=>12),
		);
		$return = $this->find('all', $params);
		return $return;
	}
}
?>