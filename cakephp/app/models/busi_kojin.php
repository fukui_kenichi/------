<?php
/**
 * 商談一覧表示個人設定テーブルモデル
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note
 */
class BusiKojin extends AppModel {
	var $name = 'BusiKojin';
	//var $useTable = false;	//使用テーブル名(使用しないときはfalse)

	// 日本語項目名定義
	var $label = array(
		'busi_master_id' => '商談マスターID',
		'user_id' => 'ユーザーID',
		'view_flag' => '表示フラグ',
		'number' => '並び順',
		'disabled' => '設定変更フラグ',
	);

	// バリデーション定義(BasicValidation用)
	var $valid = array(
		'busi_master_id' => '',
		'user_id' => '',
		'view_flag' => '',
		'number' => '',
		'disabled' => '',
	);


	/**
	 * BasicValidationBehaviorによるバリデーションのロード
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function loadValidate() {

		// 条件によって入力チェック追加
		//$this->valid['xxx'] = 'required | alphaNumeric';

		// バリデーション定義をモデルにセット
		$this->setValidate($this->valid);

		// エラーメッセージをデフォルト以外に変更する
		//$this->validate['email']['valid_email']['message'] = 'カスタムエラーメッセージ';

	}

	/**
	 * 入力チェック(複雑)
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function validates() {
		parent::validates();

		//結果の返却
		if (count($this->validationErrors) > 0) {
			return false;
		}
		return true;
	}


	/**
	 * 個人設定編集で使用するBusiKojinのデータ取得関数
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/23
	 * @note
	 * @params   int $userId   ユーザID
	 * @return   bool 処理結果真偽値
	 */
	function getDataByKojinEdit($userId) {
		$sql = "SELECT bk.*, bm.* FROM busi_kojins as bk
			RIGHT JOIN busi_masters as bm ON bk.user_id = {$userId} AND bk.busi_master_id = bm.id
			WHERE bm.type IS NULL OR (bm.type != ". MASTER_TYPE_FIMG. ' AND bm.type != '. MASTER_TYPE_FILE.
			') ORDER BY bk.view_flag IS NULL ASC, bk.number IS NULL ASC, bk.number ASC,  bk.id IS NULL ASC, bm.number ASC;';

		return $this->query($sql);
	}


	/**
	 * 表示する項目の並び順整列する
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/17
	 * @note     入力された表示番号、idでソートした順番に
	 * @params   int $userId   ユーザID
	 * @return   bool 処理結果真偽値
	 */
	function sortRank($userId) {
		// 有効なデータを全件取得
		$params = array(
			'fields'=>array('BusiKojin.id'),
			'conditions' => array(
				'BusiKojin.user_id' => $userId,
				'BusiKojin.view_flag' => 1,
			),
			'order'=>array('BusiKojin.number ASC', 'BusiKojin.id ASC'),
		);
		$result = $this->find('all',  $params);

		// 並び順をソートする
		$this->checkUpdated = false;	// 更新時間チェックOFF
		foreach ($result as $key => $val){
			$rank = $key + 1;
			$id = $val['BusiKojin']['id'];

			$sql = "UPDATE busi_kojins SET number = $rank WHERE id ={$id};";
			if ($this->query($sql) === false){
				return false;
			}
		}

		return true;
	}


	/**
	 * BusiKojinの項目の並び順を取得する
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/23
	 * @note
	 * @params   int   $userId       ユーザID
	 * @return   arr   $returnData   BusiKojinの項目の並び順
	 */
	function getDataByUserId($userId) {

		$params = array(
			'fields' => array('BusiKojin.*', 'BusiMaster.*'),
			'conditions' => array(
				'BusiKojin.user_id' => $userId,
				'BusiKojin.view_flag' => 1,
			),
			'order' => array('BusiKojin.number ASC'),
			'recursive' => -1,
			'joins' => array(
				array(
					'type' => 'inner',
					'table' => 'busi_masters',
					'alias' => 'BusiMaster',
					'conditions' => array('BusiMaster.id = BusiKojin.busi_master_id')
				),
			)
		);

		return $this->find('all', $params);
	}


	/**
	 * ユーザー新規登録時に個人設定(商談)を登録する
	 * @author   fukui@okushin.co.jp
	 * @date     2015/03/26
	 * @note
	 * @params   int $userId   ユーザID
	 * @return   bool 処理結果真偽値
	 */
	function insertKojin($userId) {
		$sql = "INSERT INTO busi_kojins
				(id, busi_master_id, user_id, view_flag, number, disabled)
				VALUES
				(NULL, '1', '{$userId}', '1', '1', '1'),
				(NULL, '2', '{$userId}', '1', '2', '0'),
				(NULL, '5', '{$userId}', '1', '3', '0');";
		return $this->query($sql);
	}
}
?>