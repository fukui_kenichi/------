<?php
/**
 * 顧客一覧表示個人設定テーブルモデル
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note
 */
class CustomerKojin extends AppModel {
	var $name = 'CustomerKojin';
	//var $useTable = false;	//使用テーブル名(使用しないときはfalse)

	// 日本語項目名定義
	var $label = array(
		'customer_master_id' => '顧客マスターID',
		'user_id' => 'ユーザーID',
		'view_flag' => '表示フラグ',
		'number' => '並び順',
		'disabled' => '設定変更フラグ',
	);

	// バリデーション定義(BasicValidation用)
	var $valid = array(
		'customer_master_id' => '',
		'user_id' => '',
		'view_flag' => '',
		'number' => '',
		'disabled' => '',
	);


	/**
	 * BasicValidationBehaviorによるバリデーションのロード
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function loadValidate() {

		// 条件によって入力チェック追加
		//$this->valid['xxx'] = 'required | alphaNumeric';

		// バリデーション定義をモデルにセット
		$this->setValidate($this->valid);

		// エラーメッセージをデフォルト以外に変更する
		//$this->validate['email']['valid_email']['message'] = 'カスタムエラーメッセージ';

	}


	/**
	 * 入力チェック(複雑)
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function validates() {
		parent::validates();

		//結果の返却
		if (count($this->validationErrors) > 0) {
			return false;
		}
		return true;
	}


	/**
	 * 個人設定編集で使用するCustomerKojinのデータ取得関数
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/23
	 * @note
	 * @params   int $userId   ユーザID
	 * @return   bool 処理結果真偽値
	 */
	function getDataByKojinEdit($userId) {
		$sql = "SELECT ck.*, cm.* FROM customer_kojins as ck
			RIGHT JOIN customer_masters as cm ON ck.user_id = {$userId} AND ck.customer_master_id = cm.id
			WHERE cm.type IS NULL OR (cm.type != ". MASTER_TYPE_FIMG. ' AND cm.type != '. MASTER_TYPE_FILE.
			') ORDER BY ck.view_flag IS NULL ASC, ck.number IS NULL ASC, ck.number ASC,  ck.id IS NULL ASC, cm.number ASC;';

		return $this->query($sql);
	}


	/**
	 * 表示する項目の並び順整列する
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/17
	 * @note     入力された表示番号、idでソートした順番に
	 * @params   int $userId   ユーザID
	 * @return   bool 処理結果真偽値
	 */
	function sortRank($userId) {
		// 有効なデータを全件取得
		$params = array(
			'fields'=>array('CustomerKojin.id'),
			'conditions' => array(
				'CustomerKojin.user_id' => $userId,
				'CustomerKojin.view_flag' => 1,
			),
			'order'=>array('CustomerKojin.number ASC', 'CustomerKojin.id ASC'),
		);
		$result = $this->find('all',  $params);

		// 並び順をソートする
		$this->checkUpdated = false;	// 更新時間チェックOFF
		foreach ($result as $key => $val){
			$rank = $key + 1;
			$id = $val['CustomerKojin']['id'];

			$sql = "UPDATE customer_kojins SET number = $rank WHERE id ={$id};";
			if ($this->query($sql) === false){
				return false;
			}
		}

		return true;
	}


	/**
	 * CustomerKojinの項目の並び順を取得する
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/23
	 * @note
	 * @params   int   $userId       ユーザID
	 * @return   arr   $returnData   CustomerKojinの項目の並び順
	 */
	function getDataByUserId($userId) {

		$params = array(
			'fields' => array('CustomerKojin.*', 'CustomerMaster.*'),
			'conditions' => array(
				'CustomerKojin.user_id' => $userId,
				'CustomerKojin.view_flag' => 1,
			),
			'order' => array('CustomerKojin.number ASC'),
			'recursive' => -1,
			'joins' => array(
				array(
					'type' => 'inner',
					'table' => 'customer_masters',
					'alias' => 'CustomerMaster',
					'conditions' => array('CustomerMaster.id = CustomerKojin.customer_master_id')
				),
			)
		);

		return $this->find('all', $params);
	}


	/**
	 * ユーザー新規登録時に個人設定(顧客)を登録する
	 * @author   fukui@okushin.co.jp
	 * @date     2015/03/26
	 * @note
	 * @params   int $userId   ユーザID
	 * @return   bool 処理結果真偽値
	 */
	function insertKojin($userId) {
		$sql = "INSERT INTO customer_kojins
				(id, customer_master_id, user_id, view_flag, number, disabled)
				VALUES
				(NULL, '1', '{$userId}', '1', '1', '1'),
				(NULL, '7', '{$userId}', '1', '2', '0');";
		return $this->query($sql);
	}
}
?>