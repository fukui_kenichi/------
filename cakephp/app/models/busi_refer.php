<?php
/**
 * 商談検索テーブルモデル
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note
 */
class BusiRefer extends AppModel {
	var $name = 'BusiRefer';
	//var $useTable = false;	//使用テーブル名(使用しないときはfalse)

	// 日本語項目名定義
	var $label = array(
		'busi_id' => '顧客ID',
		'refer' => '検索用文字列',
	);

	// バリデーション定義(BasicValidation用)
	var $valid = array(
		'busi_id' => '',
		'refer' => '',
	);


	/**
	 * BasicValidationBehaviorによるバリデーションのロード
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function loadValidate() {

		// 条件によって入力チェック追加
		//$this->valid['xxx'] = 'required | alphaNumeric';

		// バリデーション定義をモデルにセット
		$this->setValidate($this->valid);

		// エラーメッセージをデフォルト以外に変更する
		//$this->validate['email']['valid_email']['message'] = 'カスタムエラーメッセージ';

	}

	/**
	 * 入力チェック(複雑)
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function validates() {
		parent::validates();

		//結果の返却
		if (count($this->validationErrors) > 0) {
			return false;
		}
		return true;
	}


	/**
	 * 指定されたBusiIdのBusiReferを生成・更新する関数
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/03
	 * @note
	 * @param    int   $busiId   顧客ID
	 * $return   bool  処理の真偽値
	 */
	function updateByBusiId($busiId) {
		// Busyデータを取得
		App::import('Model', 'Busy');
		$Busy = new Busy();
		// 検索速度テストのため一時修正
//		$params = array(
//			'fields' => 'Busy.*, User.*',
//			'conditions' => array('Busy.id'=>$busiId),
//			'joins' => array(
//				array(
//					'type' => 'left',
//					'table' => 'users',
//					'alias' => 'User',
//					'conditions' => 'User.id = Busy.user_id'
//				),
//			)
//		);
		$params = array(
			'fields' => 'Busy.*',
			'conditions' => array('Busy.id'=>$busiId),
		);
		$busyData = $Busy->find('first', $params);

		// 取得できなければ戻る
		if (empty($busyData)) return false;

		// Busiの項目を取得
		App::import('Model', 'BusiMaster');
		$BusiMaster = new BusiMaster();
		$busyTableInfo = $BusiMaster->getTableInfoForBusiRefer();

		// NgramConverter準備
		App::import('Vendor', 'ngram_converter');
		$ngramConverter = new NgramConverter();

		$refer = '';
		$referFll = '';
		foreach ((array)$busyTableInfo as $key => $val) {
			// 検索速度テストのため一時修正
			// user_idは検索文に含めない
//			if ($val['BusiMaster']['field'] == 'user_id') {
//				$refer .= $val['BusiMaster']['koumoku']. '='. $busyData['User']['name']. ' ';
//				continue;
//			}
			if ($val['BusiMaster']['field'] == 'user_id') continue;

			// busi_dateはY/m/dの書式で
			if ($val['BusiMaster']['field'] == 'busi_date') $busyData['Busy']['busi_date'] = dateFormat($busyData['Busy']['busi_date'], 'Y/m/d');

			// refer文を作成
			$refer .= $val['BusiMaster']['koumoku']. '='. $busyData['Busy'][$val['BusiMaster']['field']]. ' ';
			// refer_full文を作成
			$referFull = $ngramConverter->to_fulltext($refer, 2);
		}
		$refer = trim($refer);

		// SQL文を作成して実行
		$sql ="INSERT INTO busi_refers (busi_id, customer_id, refer, refer_full) VALUES ({$busiId}, {$busyData['Busy']['customer_id']}, '{$refer}', '{$referFull}') ON DUPLICATE KEY UPDATE refer = VALUES(refer),  refer_full = VALUES(refer_full);";
		if (!$this->query($sql)){
			$this->invalidate('error', 'BusiReferのデータの保存に失敗しました。');
			$this->rollback();
			return false;
		}

		return true;
	}


	/**
	 * 顧客検索画面で顧客IDを返す関数
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/04
	 * @note
	 * @params   str   $refer   検索文字列
	 * @return   arr   担当者ID一覧
	 */
	function searchCustomerId($refer) {
		// 顧客IDを求めるクエリ
		// NgramConverterを使って検索
//		App::import('Vendor', 'ngram_converter');
//		$ngramConverter = new NgramConverter();
//		$match = $ngramConverter->make_match_sql($refer, 'refer_full', 2);
//		$sql = 'SELECT customer_id FROM busi_refers AS BusiRefer WHERE '. $match . " AND refer LIKE '%{$refer}%';";
////		$result = $this->query($sql);
//
//		return $this->query($sql);

		$regexFlag = false; // 正規表現を使うかどうかのフラグ

		// 特定の文字列を含む場合は正規表現を使用する
		if (mb_substr_count($refer, '[0-9]', 'UTF-8')) $regexFlag = true;

		// 顧客IDを求めるクエリ
		// 通常の検索
		$params = array(
			'fields'=>array('BusiRefer.customer_id'),
		);

		// WHERE句 条件設定
		if ($regexFlag) { // 正規表現を使用する場合
			$params['conditions'] = array('BusiRefer.refer REGEXP'=> $refer);
		} else { // LIKEを使用する場合
			$params['conditions'] = array('BusiRefer.refer LIKE'=>'%'.$refer.'%');
		}

		return $this->find('all', $params);
	}



	/**
	 * Busyにあるデータデータ全てのレコードをBusiReferに生成・更新する関数
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/04
	 * @note
	 * $return   bool  処理の真偽値
	 */
	function updateAll() {
		// Userデータを取得
		App::import('Model', 'Busy');
		$Busy = new Busy();
		$busiData = $Busy->find('all', array('fields'=>'Busy.*'));

		// 取得できなければ戻る
		if (empty($busiData)) return true;

		// BusiMasterの項目を取得
		App::import('Model', 'BusiMaster');
		$BusiMaster = new BusiMaster();
		$busiTableInfo = $BusiMaster->getTableInfoForBusiRefer();

		foreach ($busiData as $key1 => $val1) {

			$refer = '';
			foreach ((array)$busiTableInfo as $key2 => $val2) {
				// refer文を作成
				$refer .= $val2['BusiMaster']['koumoku']. '='. $val1['Busy'][$val2['BusiMaster']['field']]. ' ';
			}
			$refer = trim($refer);

			// SQL文を作成して実行
			$sql ="INSERT INTO busi_refers (busi_id, refer) VALUES ({$val1['Busy']['id']}, '{$refer}') ON DUPLICATE KEY UPDATE refer = VALUES(refer);";
			if (!$this->query($sql)){
				$this->invalidate('error', 'BusiReferのデータの保存に失敗しました。');
				$this->rollback();
				return false;
			}
		}

		return true;
	}

}
?>