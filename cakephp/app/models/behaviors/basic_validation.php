<?php
/**
 * 基本バリデーションビヘイビア
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note
 */
class BasicValidationBehavior extends ModelBehavior {

	var $loaded = array();			// バリデーション定義をロードしたモデル名を保持
	var $autoConvert = true;		// 入力チェック前にコンバート処理を行うか
	var $convert = array();			// コンバート用に入力チェックのルール保持
	var $validation = null;			// validation.phpのオブジェクト

	// エラーメッセージ定義
	var $validateMessage = array(
		// 標準バリデーション
		'required'		=> '【{%label%}】 入力してください',
		'notEmpty' 		=> '【{%label%}】 入力してください',
		'blank' 		=> '【{%label%}】 空でなければなりません',
		'zen' 			=> '【{%label%}】 全角以外の文字が含まれています',
		'single' 		=> '【{%label%}】 半角以外の文字が含まれています',
		'range' 		=> '【{%label%}】 %sより大きく%sより小さい半角数字を入力してください',
		'between' 		=> '【{%label%}】 %s文字以上%2s文字以内の半角文字を入力してください',
		'mb_between' 	=> '【{%label%}】 %s文字以上%2s文字以内の文字を入力してください',
		'minLength' 	=> '【{%label%}】 半角%s文字以上で入力してください',
		'mb_minLength' 	=> '【{%label%}】 %s文字以上で入力してください',
		'maxLength' 	=> '【{%label%}】 半角%s文字以内で入力してください',
		'mb_maxLength' 	=> '【{%label%}】 %s文字以内で入力してください',
		'equalLen'		=> '【{%label%}】 入力された文字数が正しくありません(半角%s文字で入力してください)',
		'mb_equalLen'	=> '【{%label%}】 入力された文字数が正しくありません(%s文字で入力してください)',
		'alphaNumeric' 	=> '【{%label%}】 半角英数字で入力してください',
		'alpha'			=> '【{%label%}】 半角英字以外の文字が含まれています',
		'integer' 		=> '【{%label%}】 半角整数を入力してください',
		'numeric' 		=> '【{%label%}】 半角数字で入力してください',
		'decimal' 		=> '【{%label%}】 小数点第%s位まで半角数字を入力してください',
		'kana'			=> '【{%label%}】 カタカナ以外の文字が含まれています',
		'kana_half'		=> '【{%label%}】 半角カタカナ以外の文字が含まれています',
		'hirakana'		=> '【{%label%}】 ひらがな以外の文字が含まれています',
		'zipcode' 		=> '【{%label%}】 郵便番号形式(xxx-xxxx)で入力してください',
		'postal' 		=> '【{%label%}】 郵便番号形式(xxx-xxxx)で入力してください',
		'tel' 			=> '【{%label%}】 ハイフンなしの半角整数を入力してください',
		'phone' 		=> '【{%label%}】 電話番号形式(xxxxx-xxxxx-xxxxx)で入力してください',
		'mobile' 		=> '【{%label%}】 携帯電話番号形式(xxx-xxxx-xxxx)で入力してください',
		'email' 		=> '【{%label%}】 メールアドレスが不正です',
		'url' 			=> '【{%label%}】 URL形式で入力してください',
		'date' 			=> '【{%label%}】 日付形式で入力してください',
		'time'			=> '【{%label%}】 時:分(+:秒) 形式で入力してください',
		'equalTo' 		=> '【{%label%}】 入力値が「%s」と一致しません',
		'confirm' 		=> '【{%label%}】 入力値が%sと一致しません',
		'cc' 			=> '【{%label%}】 クレジットカード番号として正しくありません',
		'comparison' 	=> '【{%label%}】 入力値が正しくありません',
		'custom' 		=> '【{%label%}】 入力値が正しくありません',
		'boolean' 		=> '【{%label%}】 入力値が正しくありません',
		'extension'		=> '【{%label%}】 拡張子が「%s」ではありません',
		'ip' 			=> '【{%label%}】 IPアドレス形式で入力してください',
		'money' 		=> '【{%label%}】 入力値が正しくありません',
		'multiple' 		=> '【{%label%}】 項目を選択してください',
		'inList'        => '【{%label%}】 入力値が「%s」ではありません',
		'ssn' 			=> '【{%label%}】 社会保障番号形式で入力してください',
		'isUnique'		=> '【{%label%}】 この値は既に登録済です',
	);


	/**
	 * 初期処理
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     ModelBehavior::setup()時に実行される
	 * @param    obj    $model     このビヘイビアの親モデル
	 * @param    arr    $config    cakephpのbehavior setup参照
	 */
	function setup(&$model, $config = array()) {

		// バリデーションクラスを使用できるように読み込み(下記の入力チェックで使用)
		$this->validation =& Validation::getInstance();

	}


	/**
	 * バリデーションの実行前に初期化を行う
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     ModelBehavior::beforeValidate()時に実行される
	 * @param    obj    $model     このビヘイビアの親モデル
	 * @param    arr    $options   cakephpのmodel beforeValidate参照
	 * @return   bool    処理結果真偽値
	 */
	function beforeValidate(&$model, $options = null) {

		// バリデーション定義の読み込み
		if (method_exists($model, 'loadValidate') && ( !isset($this->loaded[$model->name]) || !$this->loaded[$model->name] )){
			$model->loadValidate();
			$this->loaded[$model->name] = true;
		}
		// 整形処理実行
		if($this->autoConvert){
			foreach($this->convert as $i => $arr){
				list($col, $rule) = each($arr);
				$this->convertData($model, $col, $rule);
			}
		}
		return true;

	}


	/**
	 * バリデーションの展開
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     ModelBehavior::beforeValidate()時に実行される
	 *           バリデーション定義をCakePHPのバリデーション配列形式に展開
	 * @param    obj    $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    arr    $arr      バリデーション定義(BasicValidation用)
	 */
	function setValidate(&$model, $arr) {

		foreach($arr as $col => $validate){
			$validate = str_replace(" ", "", $validate);
			$validate = trim($validate, '|');
			$vali_arr = explode('|', $validate);

			// 必須項目判定
			if(in_array('required', $vali_arr)){
				$allowEmpty = false;
				$required   = true;
				// 必須メッセージは優先表示（最後に再設定）
				$tmp = array_flip($vali_arr);
				unset($tmp['required']);
				$vali_arr = array_flip($tmp);
				$vali_arr[] = 'required';
			}else{
				$allowEmpty = true;
				$required   = false;
				if(in_array('notEmpty', $vali_arr)){
					$allowEmpty = false;
				}
			}

			foreach ($vali_arr as $rule) {

				$param = "";
				if (preg_match("/(.*?)\[(.*?)\]/", $rule, $match)) {
					$rule   = $match[1];
					$param  = $match[2];
				}
				if (method_exists($this, 'valid_' . $rule)) {
					$rule = 'valid_' . $rule;
				}
				$msg  = '';
				if (isset($this->validateMessage[$rule])) {
					$msg = $this->validateMessage[$rule];
				} else {
					$msg = $rule;
				}

				// エラーメッセージに項目名を展開(ラベルの配列が定義されていれば)
				$label = $col;
				if (isset($model->label)) {
					if (!($label = $model->label[$col])) {
						$label = $col;
					}
					if (strstr($msg, '{%label%}')) {
						$msg = str_replace('{%label%}', $label, $msg);
					}
				} else {
					// ラベルが未定義なら、ラベルを出力する場所をなくす
					if (strstr($msg, '{%label%}')) {
						$msg = str_replace('[{%label%}] ', '', $msg);
					}
				}

				// 引数をメッセージ内に展開する
				if($param && strstr($msg, '%s')){
					$tmp_p = array();
					foreach(explode(',', $param) as $_p){
						$tmp_p[] = trim($_p);
					}
					$msg = vsprintf($msg, $tmp_p);
				}

				// 項目がリスト型の場合、文言を変更する
				if ($model->Behaviors->attached('List')) {
					$list = $model->getList($col);
					if($rule == 'valid_required' && !empty($list)){
						$msg = str_replace('入力', '選択', $msg);
					}
				}

				$my_rule = $rule;
				if($param != ''){
					$my_rule = array($rule);
					foreach(explode(',', $param) as $p){
						$my_rule[] = trim($p);
					}
				}
				$model->validate[$col][$rule] = array(
					'rule'       => $my_rule,
					'message'    => $msg,
					'required'   => $required,
					'allowEmpty' => $allowEmpty,
				);
				// データ整形用にカラムとルールの対応を保存
				$this->SetConvert($model, $col, $rule);
			}
		}

		$this->loaded[$model->name] = true;

	}


	/**
	 * データ整形用にカラムとルールの対応を保存
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj    $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    str    $col      バリデーション定義のキー
	 * @param    str    $rule     文字チェックのパターン（ルール）
	 */
	function SetConvert(&$model, $col, $rule) {

		$this->convert[][$col] = $rule;

	}


	/**
	 * バリデーション定義毎のデータ整形
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj    $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    str    $col      バリデーション定義のキー
	 * @param    str    $rule     文字チェックのパターン（ルール）
	 */
	function convertData(&$model, $col, $rule) {

		$before = '';
		$after = '';

		if (isset($model->data[$model->name]) && isset($model->data[$model->name][$col])) {

			$before = $model->data[$model->name][$col];
			$after = $model->data[$model->name][$col] = $this->_convert($before, $rule);

		} elseif (isset($model->data[$col])) {

			$before = $model->data[$col];
			$after = $model->data[$col] = $this->_convert($model->data[$col], $rule);
		}
	}


	/**
	 * 入力チェックのルールを使用して文字列整形
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    str    $v        整形する文字列
	 * @param    str    $rule     文字チェックのパターン（ルール）
	 * @return   str    整形した文字列
	 */
	function _convert($v, $rule){

		if($v == '') {
			return $v;
		}
		switch($rule){
			case 'single':
			case 'range':
			case 'between':
			case 'minLength':
			case 'maxLength':
			case 'equalLen':
			case 'alpha':
			case 'alphaNumeric':
			case 'integer':
			case 'numeric':
			case 'decimal':
			case 'email':
			case 'url':
			case 'time':
			case 'cc':
			case 'comparison':
			case 'boolean':
			case 'file':
			case 'extension':
			case 'ip':
			case 'money':
			case 'ssn':
				// 1バイト文字
				$v = mb_convert_kana($v, 'ras');
				$v = preg_replace('/[ー―‐]/u', '-', $v);
				break;

			case 'zen':
				// 全角文字
				$v = mb_convert_kana($v, 'ASKV');
				$v = preg_replace('/[\-]/u', '―', $v);
				break;

			case 'kana':
				// 全角カタカナ文字
				$v = mb_convert_kana($v, 'KVCS');
				$v = preg_replace('/[\-]/u', 'ー', $v);
				break;

			case 'kana_half':
				// 半角カタカナ文字
				$v = mb_convert_kana($v, 'khs');
				$v = preg_replace('/[ｰー―‐]/u', '-', $v);
				break;

			case 'hirakana':
				// 全角ひらかな文字
				$v = mb_convert_kana($v, 'HVcS');
				$v = preg_replace('/[\-]/u', 'ー', $v);
				break;

			case 'tel':
			case 'phone':
			case 'mobile':
				$v = mb_convert_kana($v, 'ras');
				$v = str_replace(array('ー','―','‐'), '-', $v);
				break;

			case 'zipcode':
			case 'postal':
				$v = mb_convert_kana($v, 'ras');
				$v = str_replace(array('ー','―','‐'), '-', $v);
				if(strlen($v) == 7 && preg_match("/^[0-9]+$/", $v)){
					$v = substr($v,0,3) . '-' . substr($v,3);
				}
				break;

			case 'date':
				$v = mb_convert_kana($v, 'ras');
				$v = str_replace(array('ー','―','‐','-', '.'), '/', $v);
				break;
		}
		return $v;

	}


	/**
	 * 指定項目のみバリデーションをかける
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     POSTされたデータのみチェック：例：$this->TestUser->intersectValidate($this->data);
	 *           指定項目のみチェック：        例：$this->TestUser->intersectValidate(array('name', 'email'));
	 *           上記を実行後 if($this->TestUser->validates()) {...}
	 *           詳しくは http://www.exgear.jp/blog/2009/06/basic_validation2/ 参照
	 * @param    obj    $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    arr    $arg
	 */
	function intersectValidate(&$model, $arg) {

		if (method_exists($model, 'loadValidate')){
			$model->loadValidate();
			$this->loaded[$mode->name] = true;
		}
		if(is_scalar($arg)){
			// for 'colA,colB'
			$okVali = array_flip(explode(',', $arg));
		}else{
			if(isset($arg[$model->name])){
				// for normal $data[model][colA]="xxx"
				$okVali = $arg[$model->name];
			}else{
				$cnt = Set::countDim($arg);
				// for saveAll $data[23][colA]="xxx"
				if($cnt == 2){
					$okVali = array_shift($arg);
				}else{
					list($col1, $col2) = each($arg);
					if(is_integer($col1)){
						// for columnArray array('colA', 'colB')
						$okVali = array_flip($arg);
					}else{
						// for columnKeyArray array('colA'=>"xxx", 'colB'=>"yyy")
						$okVali = $arg;
					}
				}
			}
		}
		$model->validate = array_intersect_key($model->validate, $okVali);

	}


	/**
	 * メッセージのカスタマイズ(任意のタイミングで使用)
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj    $model      このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    str    $col        バリデーション定義のキー
	 * @param    str    $rule       文字チェックのパターン（ルール）
	 * @param    str    $message    エラーメッセージ
	 */
	function setMessage(&$model, $col, $rule, $message) {

		if (method_exists($this, 'valid_' . $rule)){
			$rule = 'valid_' . $rule;
		}
		if(isset($model->validate[$col]) && isset($model->validate[$col][$rule])){
			$model->validate[$col][$rule]['message'] = $message;
		}
	}


	/**
	 * バリデーションのクリア(任意のタイミングで使用)
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj    $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 */
	function clearValidate(&$model) {

		$this->loaded[$model->name] = true;
		$model->validate = array();
		$this->convert = array();

	}


	//=================================================================================
	// ここから入力チェック用メソッド
	// true:  入力チェックOK
	// false: 入力チェックNG
	//=================================================================================
	/**
	 * 必須項目チェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function required(&$model, $check) {

		$regex = '/[^\s]+/m';
		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		// 配列の場合(チェックボックス用)
		if (is_array($check)) {
			foreach ($check as $arr_check) {
				if ($arr_check) {
					return true;
				}
			}
			return false;
		}

		if (empty($check) && $check != '0') {
			return false;
		}

		if (!$this->_check($regex, $check)) {
			return false;
		}
		return true;

	}


	/**
	 * 未入力チェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj    $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    str    $check    チェックする文字列
	 * @return   bool   処理結果真偽値
	 */
	function notEmpty(&$model, $check) {

		return $this->required($model, $check);

	}


	/**
	 * 空白文字のみチェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function blank(&$model, $check) {

		$regex = '/[^\\s]/';
		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		// 未入力はOK
		if (!$this->required($model, $check)) {
			return true;
		}

		if ($this->_check($regex, $check)) {
			return false;
		}
		return true;

	}

	/**
	 * 全角文字のみチェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function zen(&$model, $check) {

		$regex = '/(?:\xEF\xBD[\xA1-\xBF]|\xEF\xBE[\x80-\x9F])|[\x20-\x7E]/u';
		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		// 未入力はOK
		if (!$this->required($model, $check)) {
			return true;
		}

		$check = mb_convert_encoding($check, 'UTF-8');
		if ($this->_check($regex, $check)) {
			return false;
		}

		// 半角カタカナも除外
		$regex = '/[ｦ-ﾟ\-\ ]+/u';
		if ($this->_check($regex, $check)) {
			return false;
		}
		return true;
	}


	/**
	 * 半角文字のみチェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function single(&$model, $check) {

		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		// 未入力はOK
		if (!$this->required($model, $check)) {
			return true;
		}

		if(strlen($check) != mb_strlen($check)){
			return false;
		}
		return true;
	}


	/**
	 * 数値範囲チェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @param    int             $lower    最少数
	 * @param    int             $upper    最大数
	 * @return   bool   処理結果真偽値
	 */
	function range(&$model, $check, $lower = null, $upper = null ) {

		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		return $this->validation->range($check, $lower, $upper);
	}


	/**
	 * 半角文字数範囲チェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @param    int             $min      最小文字数
	 * @param    int             $max      最大文字数
	 * @return   bool   処理結果真偽値
	 */
	function between(&$model, $check, $min, $max) {

		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		$length = strlen($check);
		if ($length < $min || $length > $max) {
			return false;
		}
		return true;

	}


	/**
	 * 文字数範囲チェック(マルチバイト対応)
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @param    int             $min      最小文字数
	 * @param    int             $max      最大文字数
	 * @return   bool   処理結果真偽値
	 */
	function mb_between(&$model, $check, $min, $max) {

		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		$length = mb_strlen($check);
		if ($length < $min || $length > $max) {
			return false;
		}
		return true;

	}


	/**
	 * 最小文字数チェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @param    int             $min      最小文字数
	 * @return   bool   処理結果真偽値
	 */
	function minLength(&$model, $check, $min) {

		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		$length = strlen($check);
		if ($length < $min) {
			return false;
		}
		return true;

	}


	/**
	 * 最小文字数チェック(マルチバイト対応)
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @param    int             $min      最小文字数
	 * @return   bool   処理結果真偽値
	 */
	function mb_minLength(&$model, $check, $min) {

		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		$length = mb_strlen($check);
		if ($length < $min) {
			return false;
		}
		return true;

	}


	/**
	 * 最大文字数チェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @param    int             $max      最大文字数
	 * @return   bool   処理結果真偽値
	 */
	function maxLength(&$model, $check, $max) {

		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		$length = strlen($check);
		if ($length > $max) {
			return false;
		}
		return true;

	}


	/**
	 * 最大文字数チェック(マルチバイト対応)
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @param    int             $max      最大文字数
	 * @return   bool   処理結果真偽値
	 */
	function mb_maxLength(&$model, $check, $max) {

		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		$length = mb_strlen($check);
		if ($length > $max) {
			return false;
		}
		return true;

	}

	/**
	 * 文字数一致チェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @param    int             $len      文字数
	 * @return   bool   処理結果真偽値
	 */
	function equalLen(&$model, $check, $len) {

		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		$length = strlen($check);
		if ($length != $len) {
			return false;
		}
		return true;

	}


	/**
	 * 文字数一致チェック(マルチバイト対応)
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @param    int             $len      文字数
	 * @return   bool   処理結果真偽値
	 */
	function mb_equalLen(&$model, $check, $len) {

		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		$length = mb_strlen($check);
		if ($length != $len) {
			return false;
		}
		return true;

	}


	/**
	 * 半角英字チェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function alpha(&$model, $check) {

		$regex = '/[^A-Z]/i';
		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		// 未入力はOK
		if (!$this->required($model, $check)) {
			return true;
		}

		if ($this->_check($regex, $check)) {
			return false;
		}
		return true;

	}


	/**
	 * 半角英数チェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function alphaNumeric(&$model, $check) {

		$regex = '/[^\\dA-Z]/i';
		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		// 未入力はOK
		if (!$this->required($model, $check)) {
			return true;
		}

		if ($this->_check($regex, $check)) {
			return false;
		}
		return true;

	}


	/**
	 * 半角整数チェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function integer(&$model, $check) {

		$regex = '/^\-?[0-9]+$/';
		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		// 未入力はOK
		if (!$this->required($model, $check)) {
			return true;
		}

		if (!$this->_check($regex, $check)) {
			return false;
		}
		return true;

	}


	/**
	 * 半角数字チェック（少数OK）
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function numeric(&$model, $check) {

		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		return is_numeric($check);

	}


	/**
	 * 少数チェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @param    int             $places   少数桁数
	 * @return   bool   処理結果真偽値
	 */
	function decimal(&$model, $check, $places = null) {

		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		return $this->validation->decimal($check, $places, null);
	}


	/**
	 * 全角カタカナチェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function kana(&$model, $check) {

		$regex = '/^[ァ-ヶー　]+$/u';
		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		// 未入力はOK
		if (!$this->required($model, $check)) {
			return true;
		}

		$check = mb_convert_encoding($check, 'UTF-8');
		if (!$this->_check($regex, $check)) {
			return false;
		}
		return true;

	}


	/**
	 * 半角カタカナチェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function kana_half(&$model, $check) {

		$regex = '/^[ｦ-ﾟ\-\ ]+$/u';
		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		// 未入力はOK
		if (!$this->required($model, $check)) {
			return true;
		}

		$check = mb_convert_encoding($check, 'UTF-8');
		if (!$this->_check($regex, $check)) {
			return false;
		}
		return true;

	}


	/**
	 * ひらかなチェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function hirakana(&$model, $check) {

		$regex = '/^[ぁ-んー　]+$/u';
		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		// 未入力はOK
		if (!$this->required($model, $check)) {
			return true;
		}

		$check = mb_convert_encoding($check, 'UTF-8');
		if (!$this->_check($regex, $check)) {
			return false;
		}
		return true;

	}


	/**
	 * 郵便番号チェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function zipcode(&$model, $check) {

		$regex = '/^\d{3}\-\d{4}$/';
		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		// 未入力はOK
		if (!$this->required($model, $check)) {
			return true;
		}

		if (!$this->_check($regex, $check)) {
			return false;
		}
		return true;

	}


	/**
	 * 郵便番号チェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function postal(&$model, $check) {

		return $this->zipcode($model, $check);

	}


	/**
	 * 電話番号チェック(integerに置き換え)
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function tel(&$model, $check) {

		$regex = '/^\-?[0-9]+$/';
		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		// 未入力はOK
		if (!$this->required($model, $check)) {
			return true;
		}

		if (!$this->_check($regex, $check)) {
			return false;
		}
		return true;

	}


	/**
	 * 電話番号チェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function phone(&$model, $check) {

		return $this->tel($model, $check);

	}


	/**
	 * 携帯電話番号チェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function mobile(&$model, $check) {

		$regex = '/^[0-9]{3}\-[0-9]{4}\-[0-9]{4}$/';
		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		// 未入力はOK
		if (!$this->required($model, $check)) {
			return true;
		}

		if (!$this->_check($regex, $check)) {
			return false;
		}
		return true;

	}


	/**
	 * メールアドレスチェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function email(&$model, $check) {

		$regex = "/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/";
		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		// 未入力はOK
		if (!$this->required($model, $check)) {
			return true;
		}

		if (!$this->_check($regex, $check)) {
			return false;
		}
		return true;

	}


	/**
	 * URLチェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function url(&$model, $check) {

		$regex = '/^(https?|ftp)(:\/\/[-_.!~*\'()a-zA-Z0-9;\/?:\@&=+\$,%#]+)$/';
		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		// 未入力はOK
		if (!$this->required($model, $check)) {
			return true;
		}

		if (!$this->_check($regex, $check)) {
			return false;
		}
		return true;

	}


	/**
	 * 日付チェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function date(&$model, $check) {

		$regex = '/^\d{4}[\.\-\/]\d{1,2}[\.\-\/]\d{1,2}$/';
		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		// 未入力はOK
		if (!$this->required($model, $check)) {
			return true;
		}

		if (!$this->_check($regex, $check)) {
			return false;
		}

		$tmp = preg_split('/[\.\-\/]/', $check);
		if (count($tmp) != 3) {
			return false;
		}
		$yyyy = $tmp[0];
		$mm = $tmp[1];
		$dd = $tmp[2];
		if (!checkdate($mm, $dd, $yyyy)) {
			return false;
		}
		return true;

	}


	/**
	 * 時間チェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function time(&$model, $check) {

		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		return $this->validation->time($check);

	}


	/**
	 * 値と型の両方で同じかどうかチェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model         このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check         チェックする文字列（配列（フィールド名：値））
	 * @param    str             $comparedTo    比較する文字列
	 * @return   bool   処理結果真偽値
	 */
	function equalTo(&$model, $check, $comparedTo) {

		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		return $this->validation->equalTo($check, $comparedTo);

	}


	/**
	 * 値と型の両方で同じかどうかチェック(確認項目用)
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model      このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check      チェックする文字列（配列（フィールド名：値））
	 * @param    str             $colName    エラーメッセージ表示用項目名（日本語）
	 * @param    str             $col        比較項目名（変数名）
	 * @return   bool   処理結果真偽値
	 */
	function confirm(&$model, $check, $colName, $col ) {

		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		if(!isset($model->data[$model->name][$col])){
			return false;
		}
		if($check !== $model->data[$model->name][$col]){
			return false;
		}
		return true;
	}


	/**
	 * クレジットカードの番号形式チェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function cc(&$model, $check) {

		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		return $this->validation->cc($check, 'fast', false, null);

	}


	/**
	 * 数値比較チェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     （～より大きい、～より小さい、～以上、～以下、～と等しい、～と等しくないをチェック）
	 * @param    obj             $model       このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check       チェックする文字列（配列（フィールド名：値））
	 * @param    str             $operator    判定文字列　例：[>, >= <, <=, ==, !=]など
	 * @param    int             $check2      比較数値
	 * @return   bool   処理結果真偽値
	 */
	function comparison(&$model, $check, $operator, $check2) {

		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		return $this->validation->comparison($check, $operator, $check2);

	}


	/**
	 * カスタムチェック（preg正規表現）
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @param    str             $regex    チェック用正規表現
	 * @return   bool   処理結果真偽値
	 */
	function custom(&$model, $check, $regex) {

		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		return $this->validation->custom($check, $regex);

	}


   /**
	 * 真偽値形式チェック(0, 1, '0', '1', true, falseのどれかであればtrue)
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function boolean(&$model, $check) {

		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		return $this->validation->boolean($check);

	}


	/**
	 * 拡張子チェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model         このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check         チェックする文字列（配列（フィールド名：値））
	 * @param    mix(str,arr)    $extensions    チェック用拡張子リスト（「/」区切り文字列又は配列）
	 * @return   bool   処理結果真偽値
	 */
	function extension(&$model, $check, $extensions) {

		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		if (!is_array($extensions)) {
			$extensions = explode('/', $extensions);
		}

		return $this->validation->extension($check, $extensions);

	}


	/**
	 * ファイル形式チェック（未開発）
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function file(&$model, $check) {

		return false;

//		if (is_array($check)) {
//			list($key, $check) = each($check);
//		}
//
//		return $this->validation->file($check);

	}


	/**
	 * IPv4の形式チェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function ip(&$model, $check) {

		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		return $this->validation->ip($check);

	}


	/**
	 * 通貨形式チェック（日本未対応）
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @param    unknown_type    $symbolPosition ???不明???
	 * @return   bool   処理結果真偽値
	 */
	function money(&$model, $check, $symbolPosition) {

		return false;

//		if (is_array($check)) {
//			list($key, $check) = each($check);
//		}
//
//		return $this->validation->money($check, $symbolPosition);

	}


	/**
	 * 複数選択項目に定義した文字リストが含まれているか定義数以上選択されているかチェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model      このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check      チェックする文字列（配列（フィールド名：値））
	 * @param    arr             $options    例：array('in' => array('あああ', 'いいい', 'ううう'), 'min' => 1, 'max' => 3)のような連想配列
	 * @return   bool   処理結果真偽値
	 */
	function multiple(&$model, $check, $options) {

		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		return $this->validation->multiple($check, $options);

	}


	/**
	 * 定義した文字のリストに含まれるかチェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @param    mix(str,arr)    $list     チェック用リスト（「/」区切り文字列又は配列）
	 * @return   bool   処理結果真偽値
	 */
	function inList(&$model, $check, $list) {

		if (is_array($check)) {
			list($key, $check) = each($check);
		}

		if (!is_array($list)) {
			$list = explode('/', $list);
		}

		return $this->validation->inList($check, $list);

	}


	/**
	 * 社会保障番号形式チェック（日本未対応）
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
\	 * @param    unknown_type    $regex    ???不明???
	 * @param    unknown_type    $country  ???不明???
	 * @return   bool   処理結果真偽値
	 */
	function ssn(&$model, $check, $regex, $country) {

		return false;

//		if (is_array($check)) {
//			list($key, $check) = each($check);
//		}
//
//		return $this->validation->ssn($check, $regex, $country);

	}



	/**
	 * 正規表現チェックメソッド
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    obj             $model    このビヘイビアの親モデル(メソッドを呼び出す時はこの引数はないものとして使用する)
	 * @param    mix(str,arr)    $check    チェックする文字列（配列（フィールド名：値））
	 * @return   bool   処理結果真偽値
	 */
	function _check($regex, $check) {

		if (!preg_match($regex, $check)) {
			return false;
		}
		return true;

	}

}
