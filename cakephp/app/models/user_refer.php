<?php
/**
 * ユーザー検索テーブルモデル
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note
 */
class UserRefer extends AppModel {
	var $name = 'UserRefer';
	//var $useTable = false;	//使用テーブル名(使用しないときはfalse)

	// 日本語項目名定義
	var $label = array(
		'user_id' => 'ユーザーID',
		'refer' => '検索用文字列',
	);

	// バリデーション定義(BasicValidation用)
	var $valid = array(
		'user_id' => '',
		'refer' => '',
	);


	/**
	 * BasicValidationBehaviorによるバリデーションのロード
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function loadValidate() {

		// 条件によって入力チェック追加
		//$this->valid['xxx'] = 'required | alphaNumeric';

		// バリデーション定義をモデルにセット
		$this->setValidate($this->valid);

		// エラーメッセージをデフォルト以外に変更する
		//$this->validate['email']['valid_email']['message'] = 'カスタムエラーメッセージ';

	}

	/**
	 * 入力チェック(複雑)
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function validates() {
		parent::validates();

		//結果の返却
		if (count($this->validationErrors) > 0) {
			return false;
		}
		return true;
	}


	/**
	 * 指定されたUserIdのUserReferを生成・更新する関数
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/03
	 * @note
	 * @param    int   $userId   ユーザーID
	 * $return   bool  処理の真偽値
	 */
	function updateByUserId($userId) {
		// Userデータを取得
		App::import('Model', 'User');
		$User = new User();
		$userData = $User->find('first', array('fields'=>'User.*','conditions'=>array('User.id'=>$userId)));

		// 取得できなければ戻る
		if (empty($userData)) return false;

		// Userの項目を取得
		App::import('Model', 'UserMaster');
		$UserMaster = new UserMaster();
		$userTableInfo = $UserMaster->getTableInfoForUserRefer();

		$refer = '';
		foreach ((array)$userTableInfo as $key => $val) {
			// password、auth_flagは飛ばす
			if ($val['UserMaster']['field'] == 'password' || $val['UserMaster']['field'] == 'auth_flag') continue;

			// refer文を作成
			$refer .= $val['UserMaster']['koumoku']. '='. $userData['User'][$val['UserMaster']['field']]. ' ';
		}
		$refer = trim($refer);

		// SQL文を作成して実行
		$sql ="INSERT INTO user_refers (user_id, refer) VALUES ({$userId}, '{$refer}') ON DUPLICATE KEY UPDATE refer = VALUES(refer);";
		if (!$this->query($sql)){
			$this->invalidate('error', 'UserReferのデータの保存に失敗しました。');
			$this->rollback();
			return false;
		}

		return true;
	}


	/**
	 * Userにあるデータデータ全てのレコードをUserReferに生成・更新する関数
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/04
	 * @note
	 * $return   bool  処理の真偽値
	 */
	function updateAll() {
		// Userデータを取得
		App::import('Model', 'User');
		$User = new User();
		$userData = $User->find('all', array('fields'=>'User.*','conditions'=>array('User.del_flag'=>0)));

		// 取得できなければ戻る
		if (empty($userData)) return true;

		// Userの項目を取得
		App::import('Model', 'UserMaster');
		$UserMaster = new UserMaster();
		$userTableInfo = $UserMaster->getTableInfoForUserRefer();

		foreach ($userData as $key1 => $val1) {

			$refer = '';
			foreach ((array)$userTableInfo as $key2 => $val2) {
				// password、auth_flagは飛ばす
				if ($val2['UserMaster']['field'] == 'password' || $val2['UserMaster']['field'] == 'auth_flag') continue;

				// refer文を作成
				$refer .= $val2['UserMaster']['koumoku']. '='. $val1['User'][$val2['UserMaster']['field']]. ' ';
			}
			$refer = trim($refer);

			// SQL文を作成して実行
			$sql ="INSERT INTO user_refers (user_id, refer) VALUES ({$val1['User']['id']}, '{$refer}') ON DUPLICATE KEY UPDATE refer = VALUES(refer);";
			if (!$this->query($sql)){
				$this->invalidate('error', 'UserReferのデータの保存に失敗しました。');
				$this->rollback();
				return false;
			}
		}

		return true;
	}


	/**
	 * 顧客検索画面で顧客IDを返す関数
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/04
	 * @note
	 * @params   str   $refer   検索文字列
	 * @return   arr   担当者ID一覧
	 */
	function searchCustomerId($refer) {
		$regexFlag = false; // 正規表現を使うかどうかのフラグ

		// 特定の文字列を含む場合は正規表現を使用する
		if (mb_substr_count($refer, '[0-9]', 'UTF-8')) $regexFlag = true;

		// 顧客IDを求めるクエリ
		$params = array(
			'fields'=>array('Busy.customer_id'),
			'order'=>array('Busy.customer_id ASC'),
			'joins' => array(
				array(
					'type' => 'inner',
					'table' => 'busies',
					'alias' => 'Busy',
					'conditions' => 'Busy.user_id = UserRefer.user_id'
				),
			)
		);

		// WHERE句 条件設定
		if ($regexFlag) { // 正規表現を使用する場合
			$params['conditions'] = array('UserRefer.refer REGEXP'=> $refer);
		} else { // LIKEを使用する場合
			$params['conditions'] = array('UserRefer.refer LIKE'=>'%'.$refer.'%');
		}

		return $this->find('all', $params);

//		$sql = "SELECT Busy.customer_id FROM user_refers AS UserRefer inner JOIN busies AS Busy ON (Busy.user_id = UserRefer.user_id) WHERE UserRefer.refer REGEXP :pattern ORDER BY Busy.customer_id ASC";
//		$sqlArr = array('pattern' => $refer);
//		return $this->query($sql, $sqlArr);
//		$sql = "SELECT Busy.customer_id FROM user_refers AS UserRefer inner JOIN busies AS Busy ON (Busy.user_id = UserRefer.user_id) WHERE UserRefer.refer REGEXP '". $refer ."' ORDER BY Busy.customer_id ASC";
//		return $this->query($sql);
	}

}
?>