<?php
/**
 * CTI受信モデル
 * @author   fukui@okushin.co.jp
 * @date     2015/03/17
 * @note
 */
class CtiCount extends AppModel {
	var $name = 'CtiCount';
	//var $useTable = false;	//使用テーブル名(使用しないときはfalse)

	/**
	 * 顧客別入電数保存
	 * @author   fukui@okushin.co.jp
	 * @date     2015/03/17
	 * @note
	 * @param    int     $customer_id    顧客ID
	 * @return   bool    処理結果真偽値
	 */
	function saveCount($customer_id) {
		$ctiCount['customer_id'] = $customer_id;
		if (!$this->save($ctiCount)){
			$this->invalidate('error', '顧客別入電数のデータの保存に失敗しました。');
			return false;
		}
		return true;
	}


}
?>