<?php
/**
 * トークンモデル
 * @author   fukui@okushin.co.jp
 * @date     2015/03/17
 * @note
 */
class Token extends AppModel {
	var $name = 'Token';
	//var $useTable = false;	//使用テーブル名(使用しないときはfalse)

	/**
	 * トークン保存
	 * @author   fukui@okushin.co.jp
	 * @date     2015/06/10
	 * @note
	 * @param    int     $user_id    ユーザーID
	 * @param    str     $token      トークン
	 * @return   bool    処理結果真偽値
	 */
	function saveToken($user_id, $token) {
		$params = array('conditions'=>array('Token.token'=>$token));
		$saveData = $this->find('first', $params);

		$saveData['Token']['user_id'] = $user_id;
		$saveData['Token']['token'] = $token;
		$this->create($saveData);
		if (!$this->save(null, false)){
			$this->invalidate('error', 'トークンのデータの保存に失敗しました。');
			return false;
		}
		return true;
	}


}
?>