<?php
/**
 * メール受信モデル
 * @author   fukui@okushin.co.jp
 * @date     2015/03/17
 * @note
 */
class MailCount extends AppModel {
	var $name = 'MailCount';
	//var $useTable = false;	//使用テーブル名(使用しないときはfalse)

	/**
	 * 顧客別メール受信数保存
	 * @author   fukui@okushin.co.jp
	 * @date     2015/03/17
	 * @note
	 * @param    int     $customer_id    顧客ID
	 * @return   bool    処理結果真偽値
	 */
	function saveCount($customer_id) {
		$mailCount['customer_id'] = $customer_id;
		if (!$this->save($mailCount)){
			$this->invalidate('error', '顧客別メール受信数のデータの保存に失敗しました。');
			return false;
		}
		return true;
	}


}
?>