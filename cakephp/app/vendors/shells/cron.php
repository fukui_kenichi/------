<?php
/**
 * クーロンで実行されるシェル(またはシェル動作確認用コントローラ)
 * @author   fukui@okushin.co.jp
 * @date     2015/03/17
 * @note     ローカル環境での動作確認時はファイル名をcron_controller.phpとする
 *           本番環境ではapp/vendors/shellsにcron.phpという名前で置く。
 *           コマンドラインでの実行はappの上のディレクトリで
 *           % cake/console/cake cron get_mail を実行する
 */
//class CronController extends AppController // Shellの時はコメントアウトする
class CronShell extends Shell         // Controllerの時はコメントアウトする
{
	var $name = 'Cron';
	var $uses = array('Token', 'CustomerMaster', 'MailCount', 'Customer', 'Busy', 'Mail', 'BusiMaster', 'BusiRefer', 'Upload', 'User', 'Calender');	//使用モデル

	/**
	 * 定期的に処理を実行する
	 * @author   fukui@okushin.co.jp
	 * @date	 2015/04/10
	 * @note
	 */
	function teiki_cron(){
		$this->get_mail();
		$this->send_mail();
		$this->get_google();
	}

	/**
	 * 夜中に一度処理を実行する
	 * @author   fukui@okushin.co.jp
	 * @date	 2015/04/10
	 * @note
	 */
	function one_cron(){
		$this->busi_update();
		$this->del_upfile();
		$this->alert_push();
	}

	/**
	 * Googleカレンダーを取得する
	 * @author   fukui@okushin.co.jp
	 * @date	 2015/04/10
	 * @note
	 */
	function get_google(){
		$params = array(
			'fields'=>array('User.id', 'User.url'),
			'conditions'=>array('User.del_flag'=>0),
		);
		$user = $this->User->find('all', $params);

		foreach ($user as $val){
			if (empty($val['User']['url'])){
				continue;
			}

			// 3ヶ月前までのデータ
			$start = date("Y-m-d",strtotime("-3 month"));
			// 1年後までのデータ
			$end = date("Y-m-d",strtotime("+1 year"));
			$now = date('Y-m-d H:i:s');

			$string = @file_get_contents(trim($val['User']['url'], ' ')."?max-results=10000&orderby=starttime&sortorder=descend&singleevents=true&start-min={$start}&start-max={$end}");
			$xml = @simplexml_load_string($string);
			if (!empty($xml)){
				foreach ($xml->entry as $entry) {
					$temp = (array)$entry->link[0];
					$temp = strstr($temp['@attributes']['href'], 'eid=');
					$eid = mb_ltrim($temp, 'eid=');

					$title = stripslashes($entry->title);
					$content = stripslashes($entry->content);
					$content = str_replace(array("期間: ","開始日: ","(月)","(火)","(水)","(木)","(金)","(土)","(日)","(JST)"," "), '', $content);

					// eidが同じ物は更新するようにする
					$params = array(
						'conditions'=>array(
							'Calender.user_id'=>$val['User']['id'],
							'Calender.eid'=>$eid,
						)
					);
					$calender = $this->Calender->find('first', $params);

					$calender['Calender']['subject'] = $title;
					$calender['Calender']['user_id'] = $val['User']['id'];
					$calender['Calender']['color'] = 0;
					$calender['Calender']['eid'] = $eid;
					$calender['Calender']['parent_table'] = 'google';
					if (strpos($content, '～') == 10){
						// 日またがり予定
						$calender['Calender']['start_time'] = substr($content, 0, 10) . ' 00:00:00';
						$calender['Calender']['end_time'] = substr($content, 13, 10) . ' 00:00:00';
						$calender['Calender']['is_allday_event'] = 1;
					}elseif (strpos($content, '～') == 15){
						// 時間予定
						$calender['Calender']['start_time'] = substr($content, 0, 10) . ' '. substr($content, 10, 5);
						$calender['Calender']['end_time'] = substr($content, 0, 10) . ' '. substr($content, 18, 5);
						$calender['Calender']['is_allday_event'] = 0;
					}else{
						// 1日予定
						$calender['Calender']['start_time'] = substr($content, 0, 10) . ' 00:00:00';
						$calender['Calender']['end_time'] = substr($content, 0, 10) . ' 00:00:00';
						$calender['Calender']['is_allday_event'] = 1;
					}
					$this->Calender->create($calender);
					$this->Calender->save(null, false);
				}
			}
			// 更新期間内で上書きされていないデータはgoogleで削除されているので削除しておく
			$sql = "DELETE FROM calenders WHERE user_id = {$val['User']['id']} AND parent_table = 'google' AND start_time > '{$start}' AND updated < '{$now}'";
			$this->Calender->query($sql);
		}
	}

	/**
	 * 親に紐づいていないファイルを削除する
	 * @author   fukui@okushin.co.jp
	 * @date	 2015/04/10
	 * @note
	 */
	function del_upfile(){
		$params = array('conditions'=>array('Upload.parent_id'=>NULL));
		$upload = $this->Upload->find('all', $params);
		foreach ((array)$upload as $val){
			// ファイルの削除
			$path = ROOT_PATH . 'upload/' . $val['Upload']['file_name'];
			unlink($path);
			// 画像の場合、縮小ファイルの削除
			if ($val['Upload']['extension'] == 'jpg' || $val['Upload']['extension'] == 'jpeg' || $val['Upload']['extension'] == 'png'){
				$path = ROOT_PATH . 'upload/' . $val['Upload']['id'] . '-s.' . $val['Upload']['extension'];
				unlink($path);
			}
			$this->Upload->delete($val['Upload']['id']);
		}
	}

	/**
	 * 商談マスタが更新されていればBusiReferを作り直す
	 * @author   fukui@okushin.co.jp
	 * @date	 2015/04/10
	 * @note
	 */
	function busi_update(){
		// 1日以内に商談マスタが更新されているか
		$date = date('Y/m/d H:i:s', strtotime('-1 day'));
		$params = array('conditions'=>array('updated >'=>$date));
		$busiMasterArr = $this->BusiMaster->find('all', $params);

		if (!empty($busiMasterArr)){
			// BusiReferの更新
			$this->BusiRefer->updateAll();
		}
	}

	/**
	 * メールサーバーに接続してメールを取得してmail_countsテーブルに格納する
	 * @author   fukui@okushin.co.jp
	 * @date	 2015/03/17
	 * @note
	 */
	function get_mail(){
		//メールサーバーに接続してメールを取得してくる
		$mbox = @imap_open("{" . MAIL_SERVER . ":110/pop3/notls}INBOX", MAIL_ACCOUNT, MAIL_PASS);

		if ($mbox) {
			//メールの件数を取得
			$mboxes = imap_check($mbox);
			$mail_cnt = $mboxes->Nmsgs;

			//メールが1通以上あればこの処理を通る
			if ($mail_cnt > 0){
				//メールの件数分だけこの処理を通る
				for ($i = 1; $i <= $mail_cnt; $i++){

					//ヘッダー情報取得
					$head = imap_header($mbox, $i);
					//メールアドレス取得
					$mail = $head->from[0]->mailbox . '@' . $head->from[0]->host;

					//メールアドレスから顧客取得
					$params = array('conditions'=>array("Customer.mail" => $mail));
					$customer = $this->Customer->find('first', $params);

					//顧客情報があればメール受信数テーブルに保存
					if(!empty($customer)){
						$this->MailCount->saveCount($customer['Customer']['id']);
					}

					//保存したらメールサーバーから削除するようにフラグを立てておく
					imap_delete($mbox, $i);
				}
			}
			//メールサーバに対して処理を行う
			imap_expunge($mbox);
			//メールサーバーとの接続を解除する
			imap_close($mbox);
		}
	}


	/**
	 * メール配信
	 * @author   nakata@okushin.co.jp
	 * @date     2015/01/15
	 * @note
	 */
	function send_mail(){
		$this->layout = 'ajax';
		/// レンダリングなし
		$this->autoRender = false;
		/// debugコードを非表示
		Configure::write('debug', 0);

		$now = date('Y/m/d H:i:s'); // 今の日時

		// 今の日時より前で配信待ちのメールを配信する
		if (!$this->Mail->sendMailByDatetime($now)){
			$this->log('メール配信に失敗しました。', 'cron_log');
		}

		return true;
	}


	/**
	 * ApnsPHPでアラートを送る
	 * @author   fukui@okushin.co.jp
	 * @date     2015/06/15
	 * @note
	 */
	function alert_push(){
		$tokenArr = $this->Token->find('all');
		$alertCount = $this->getAlertCount();

		if (!empty($tokenArr) && !empty($alertCount)){
			App::import('Vendor', 'ApnsPHP/Log/Interface', array('file'=>'ApnsPHP/Log/Interface.php'));
			App::import('Vendor', 'ApnsPHP/Log/Embedded', array('file'=>'ApnsPHP/Log/Embedded.php'));
			App::import('Vendor', 'ApnsPHP/Abstract', array('file'=>'ApnsPHP/Abstract.php'));
			App::import('Vendor', 'ApnsPHP/Exception', array('file'=>'ApnsPHP/Exception.php'));
			App::import('Vendor', 'ApnsPHP/Push', array('file'=>'ApnsPHP/Push.php'));
			App::import('Vendor', 'ApnsPHP/Message', array('file'=>'ApnsPHP/Message.php'));

			$path1 = APP . "vendors/ApnsPHP/certificates/server_certificates_production.pem";
			$path2 = APP . "vendors/ApnsPHP/certificates/entrust_root_certification_authority.pem";

			$push = new ApnsPHP_Push(
				ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION,
				$path1
			);
			$push->setRootCertificationAuthority($path2);
			$push->connect();

			foreach ($tokenArr as $val){
				$message = new ApnsPHP_Message($val['Token']['token']);//device token
				$message->setBadge(1);
				$message->setSound();
				$message->setText("{$alertCount}件のお知らせがあります");
				$message->setExpiry(30);
				$push->add($message);
				$push->send();
				$push->disconnect();
			}
		}

	}

	/**
	 * お知らせアラート件数取得
	 * @author   fukui@okushin.co.jp
	 * @date     2015/06/10
	 * @return   arr    お知らせ件数
	 */
	function getAlertCount() {
		//お知らせ（顧客）アラート件数取得
		$alertCustomerCount = $this->getAlertCustomerCount();

		//お知らせ（商談）アラート件数取得
		$alertBusyCount = $this->getAlertBusyCount();

		//両方の足し算
		$alertCount = $alertCustomerCount + $alertBusyCount;
		return $alertCount;
	}

	/**
	 * お知らせ（顧客）アラート件数取得
	 * @author   fukui@okushin.co.jp
	 * @date     2015/06/10
	 * @return   arr    お知らせ件数
	 */
	function getAlertCustomerCount() {
		$alertCount = 0;

		// 日付型(アラート)のデータ取得
		$alertField = $this->CustomerMaster->getAlertField();
		if (empty($alertField)){
			return $alertCount;
		}

		foreach ($alertField as $key=>$val){
			// お知らせに引っかかる顧客取得
			$alertCustomer = $this->Customer->getAlertCustomer($val);
			if (empty($alertCustomer)){
				continue;
			}
			$alertCount += count($alertCustomer);
		}
		return $alertCount;
	}

	/**
	 * お知らせ（商談）アラート件数取得
	 * @author   fukui@okushin.co.jp
	 * @date     2015/06/10
	 * @return   arr    お知らせ件数
	 */
	function getAlertBusyCount() {
		$alertCount = 0;

		// 日付型(アラート)のデータ取得
		$alertField = $this->BusiMaster->getAlertField();
		if (empty($alertField)){
			return $alertCount;
		}

		foreach ($alertField as $key=>$val){
			// お知らせに引っかかる商談取得
			$alertBusy = $this->Busy->getAlertBusy($val);
			if (empty($alertBusy)){
				continue;
			}
			$alertCount += count($alertBusy);
		}
		return $alertCount;
	}
}
?>