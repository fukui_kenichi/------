<?php
/**
 * 基本ヘルパー
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note     
 */

class AppHelper extends Helper {
	var $Session = null;
	
	/**
	 * 初期処理
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     
	 */
	function __construct() {
		// ヘルパーでセッションを使えるようにしておく
		$this->Session = new SessionComponent();
		
		parent::__construct();
	}
}
?>