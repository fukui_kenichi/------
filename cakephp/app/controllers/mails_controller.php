<?php
/**
 * メール配信テーブルコントローラー
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note
 */
class MailsController extends AppController {
	var $name = 'Mails';
	var $uses = array('Mail', 'MailSender', 'MailTemplate', 'Customer', 'CustomerMaster', 'CustomerKojin');
	var $components = array('MailService');
	var $paginate = array(
		'Mail' => array(
			'fields' => array(
				'*',
			),
			'conditions' => array(),
			'order' => array('Mail.send_datetime DESC'),
			'limit' => 10,
			'recursive' => -1,
		),
		'Customer' => array(
			'fields' => array('Customer.*'),
			'conditions' => array('Customer.del_flag' => 0),
			'order' => array('Customer.updated DESC'),
			'limit' => 10,
			'recursive' => -1,
		),
	);


	/**
	 * 初期処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function beforeFilter() {
		parent::beforeFilter();

		// 顧客画面の検索結果メッセージを消去
		if ($this->Session->check('search-message')) {
			$this->Session->delete('search-message');
		}

		// ここに追加検索初期値があれば定義
		//$this->paginate['Mail']['condition']['xxx'] = 'xxx';
	}


	/**
	 * メール配信テーブル一覧ページ
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function index() {
		// 戻る
		if (isset($this->params['form']['cancel_x'])) {
			$prevPage = $this->Session->read('prevPage');
			if (!empty($prevPage)){
				$this->redirect("/{$prevPage['url']}");
			}else{
				$this->redirect('/top/index');
			}
		}

		// ページ数取得
		$page = null;
		if(isset($this->passedArgs['page'])){
			$page = $this->passedArgs['page'];
		}
		// ソートキー取得
		$sort = null;
		if(isset($this->passedArgs['sort'])){
			$sort = $this->passedArgs['sort'];
		}
		// 並び順取得
		$direction = null;
		if(isset($this->passedArgs['direction'])){
			$direction = $this->passedArgs['direction'];
		}
		$this->set('page',$page);
		$this->set('sort',$sort);
		$this->set('direction',$direction);

		// 削除
		if (isset($this->params['form']['del'])) {
			if ($this->MailService->delete($this->params['form']['hdnId'])) {
				$this->redirect('/mails/index');
			}
		}

		// 検索条件作成
		if (isset($this->params['form']['search'])) {
			$this->MailService->createParams($this->data['Search'], $this->paginate);
		}

		// ページ切替
		if (isset($this->params['form']['refresh'])) {
			// ページリミット設定
			$this->paginate['Mail']['limit'] = $this->params['form']['page_row'];
			$this->HoldPaginate->saveParams($this->paginate);
			if (!empty($this->params['form']['page_num']) && ereg("^[0-9]+$", $this->params['form']['page_num'])){
				$this->redirect('/mails/index/page:' . $this->params['form']['page_num']);
			}
		}

		$this->HoldPaginate->setPaginate();				// ページネート情報取得
		$list = $this->paginate();						// 一覧データ取得
		$this->HoldPaginate->savePaginate();			// ページネート情報保持
		$this->set('list', $list);
		$holdPaginate =  $this->Session->read('holdPaginate');
		$this->set('limit',$holdPaginate[$this->name][$this->action]['Mail']['limit']); // 行数
	}


	/**
	 * メール配信テーブル編集ページ
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @param    int    $id    メール配信テーブルID
	 */
	function edit($id = null) {
		// 戻る
		if (isset($this->params['form']['cancel_x'])) {
			$prevPage = $this->Session->read('prevPage');
			if (!empty($prevPage) && empty($id)){
				$this->redirect("/{$prevPage['url']}");
			}else{
				$this->redirect('/mails/index');
			}
		}

		// 登録
		if (isset($this->params['form']['save']) || isset($this->params['form']['save_x'])) {
			// 画面のメール配信フラグをセションのデータをマージ
			$mailSenderArr = $this->Session->read('mailSenderArr');
			foreach ($this->data['MailSender'] as $key => $val) {
				$mailSenderArr[$key]['send_flag'] = $val['send_flag'];
			}
			$this->data['MailSender'] = $mailSenderArr;
			// 保存処理
			if ($this->MailService->save()){
				$this->redirect('/mails/index');
			}
		}

		/* メール部分 */
		// 編集開始時のデータ読み込み
		if (empty($this->data) && $id != "") {
			$params = array(
				'conditions' => array('Mail.id' => $id),
				'fields' => array('Mail.*'),
			);
			$this->data = $this->Mail->find('first', $params);
			// send_datetimeをsend_dateとsend_timeに分解する
			$this->data['Mail']['send_date'] = dateFormat($this->data['Mail']['send_datetime'], 'Y/m/d');
			$this->data['Mail']['send_time'] = dateFormat($this->data['Mail']['send_datetime'], 'H:i');

			if (!$this->data || !empty($this->data['Mail']['del_flag'])) {
				$this->redirect('/mails/index');
			}
		}

		// 初期設定
		if (empty($this->data)) {
			$this->data = $this->MailService->getIni(); //初期値設定
		}
		/* 顧客一覧ページネート */
		$params = array();
		if (empty($id)) {
			/* 新規の場合は、条件を設定しなおす */
			// 条件取得
			$holdPaginate =  $this->Session->read('holdPaginate');
			// 条件設定
			if (isset($holdPaginate['Customers']['index']['Customer'])) {
				$params = $holdPaginate['Customers']['index']['Customer'];
			} elseif (isset($holdPaginate['Mails']['edit']['Customer'])) {
				$params = $holdPaginate['Mails']['edit']['Customer'];
			}
			$params['conditions']['NOT'] = array('Customer.mail' => '');
			if (!empty($params)) $this->paginate['Customer']['conditions'] = $params['conditions'];
		} else {
			// 既存メールの際のページネートの設定
			$this->paginate['Customer']['joins'] = array(
								array(
									'type' => 'inner',
									'table' => 'mails',
									'alias' => 'Mail',
									'conditions' => array('Mail.id' => $id)
								),
								array(
									'type' => 'inner',
									'table' => 'mail_senders',
									'alias' => 'MailSender',
									'conditions' => array(
										'MailSender.mail_id = Mail.id',
										'MailSender.customer_id = Customer.id',
									),
								)
							);
			$this->HoldPaginate->saveParams($this->paginate); // ページネート情報の保存
		}

		/* メール配信一覧 */
		if (empty($this->data['MailSender'])) {
			if (empty($id)) {
				// 新規データ
				unset($params['limit']);
				$params['fields'] = array('Customer.id');
				// メール配信フラグを初期化
				$customer = $this->Customer->find('list', $params);

				$this->data['MailSender'] = array();
				foreach  ((array)$customer as $key => $val) {
					$this->data['MailSender'][$key]['send_flag'] = 1;
				}
			} else {
				/* 既存データの場合は、MailSenderから取得する */
				$mailSenderArr = $this->MailSender->find('all', array('conditions'=>array('MailSender.mail_id'=>$id)));
				// 整形
				foreach((array)$mailSenderArr as $key => $val) {
					$this->data['MailSender'][$val['MailSender']['customer_id']] = $val['MailSender'];
				}
			}
			// メール配信フラグをセションに保持
			$this->Session->write('mailSenderArr', $this->data['MailSender']);
		} else {
			// エラーなどで戻ってきた場合
			// 画面のメール配信フラグをセションのデータをマージ
			$mailSenderArr = $this->Session->read('mailSenderArr');
			foreach ($this->data['MailSender'] as $key => $val) {
				$mailSenderArr[$key]['send_flag'] = $val['send_flag'];
			}
			$this->data['MailSender'] = $mailSenderArr;
			// メール配信フラグをセションに保持
			$this->Session->write('mailSenderArr', $this->data['MailSender']);
		}

		$page = 1;
		$pageRow = 10;
		$sort = null;
		$direction = null;
		// smartTable機能からの送信
		if (isset($this->params['form']['smartTableSubmit'])) {
			// ページ取得
			if(isset($this->params['form']['page_num'])){
				$page = $this->params['form']['page_num'];
			}
			// ソートキー取得
			if(isset($this->params['form']['sort'])){
				$sort = $this->params['form']['sort'];
			}
			// 並び順取得
			if(isset($this->params['form']['direction'])){
				$direction = $this->params['form']['direction'];
			}
			// ページ行数取得
			if(isset($this->params['form']['page_row'])){
				$pageRow = $this->params['form']['page_row'];
			}

			// ページネートに設定
			$this->paginate['Customer']['limit'] = $pageRow;
			$this->paginate['Customer']['offset'] = $pageRow*($page-1);
			if (!empty($sort) && !empty($direction)) {
				$this->paginate['Customer']['order'][0] = 'Customer.'. $sort. ' '. $direction;
			}
			$this->HoldPaginate->saveParams($this->paginate); // ページネート情報の保存
		}
		$this->set('page', $page);
		$this->set('sort', $sort);
		$this->set('direction', $direction);

		$this->HoldPaginate->setPaginate();				// ページネート情報取得
		$list = $this->paginate('Customer');			// 一覧データ取得
		$this->HoldPaginate->savePaginate();			// ページネート情報保持
		$this->set('list', $list);
		$holdPaginate =  $this->Session->read('holdPaginate');
		$this->set('limit',$holdPaginate[$this->name][$this->action]['Customer']['limit']);
		$this->__setViewByEdit();	// viewに必要な情報をセット
	}


	/**
	 * メール配信テーブル情報ページ
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/20
	 * @note
	 * @param    int    $id    メール配信テーブルID
	 */
	function detail($id) {
		// 戻る
		if (isset($this->params['form']['cancel']) || isset($this->params['form']['cancel_x'])) {
			$this->redirect('/mails/index');
		}

		/* メール部分 */
		// 編集開始時のデータ読み込み
		if (empty($this->data) && $id != "") {
			$params = array(
				'conditions' => array('Mail.id' => $id),
				'fields' => array('Mail.*'),
			);
			$this->data = $this->Mail->find('first', $params);

			if (!$this->data || !empty($this->data['Mail']['del_flag'])) {
				$this->redirect('/mails/index');
			}
		}

		// 初期設定
		if (empty($this->data)) {
			$this->data = $this->MailService->getIni(); //初期値設定
		}
		/* 顧客一覧ページネート */
		$params = array();
		// 既存メールの際のページネートの設定
		$this->paginate['Customer']['joins'] = array(
							array(
								'type' => 'inner',
								'table' => 'mails',
								'alias' => 'Mail',
								'conditions' => array('Mail.id' => $id)
							),
							array(
								'type' => 'inner',
								'table' => 'mail_senders',
								'alias' => 'MailSender',
								'conditions' => array(
									'MailSender.mail_id = Mail.id',
									'MailSender.customer_id = Customer.id',
								),
							)
						);
		$this->HoldPaginate->saveParams($this->paginate); // ページネート情報の保存

		/* メール配信一覧 */
		if (empty($this->data['MailSender'])) {
			if (empty($id)) {
				// 新規データ
				unset($params['limit']);
				$params['fields'] = array('Customer.id');
				// メール配信フラグを初期化
				$customer = $this->Customer->find('list', $params);
				foreach  ((array)$customer as $key => $val) {
					$this->data['MailSender'][$key]['send_flag'] = 1;
				}
			} else {
				/* 既存データの場合は、MailSenderから取得する */
				$mailSenderArr = $this->MailSender->find('all', array('conditions'=>array('MailSender.mail_id'=>$id)));
				// 整形
				foreach((array)$mailSenderArr as $key => $val) {
					$this->data['MailSender'][$val['MailSender']['customer_id']] = $val['MailSender'];
				}
			}
			// メール配信フラグをセションに保持
			$this->Session->write('mailSenderArr', $this->data['MailSender']);
		} else {
			// エラーなどで戻ってきた場合
			// 画面のメール配信フラグをセションのデータをマージ
			$mailSenderArr = $this->Session->read('mailSenderArr');
			foreach ($this->data['MailSender'] as $key => $val) {
				$mailSenderArr[$key]['send_flag'] = $val['send_flag'];
			}
			$this->data['MailSender'] = $mailSenderArr;
			// メール配信フラグをセションに保持
			$this->Session->write('mailSenderArr', $this->data['MailSender']);
		}

		$page = 1;
		$pageRow = 10;
		$sort = null;
		$direction = null;
		// smartTable機能からの送信
		if (isset($this->params['form']['smartTableSubmit'])) {
			// ページ取得
			if(isset($this->params['form']['page_num'])){
				$page = $this->params['form']['page_num'];
			}
			// ソートキー取得
			if(isset($this->params['form']['sort'])){
				$sort = $this->params['form']['sort'];
			}
			// 並び順取得
			if(isset($this->params['form']['direction'])){
				$direction = $this->params['form']['direction'];
			}
			// ページ行数取得
			if(isset($this->params['form']['page_row'])){
				$pageRow = $this->params['form']['page_row'];
			}

			// ページネートに設定
			$this->paginate['Customer']['limit'] = $pageRow;
			$this->paginate['Customer']['offset'] = $pageRow*($page-1);
			if (!empty($sort) && !empty($direction)) {
				$this->paginate['Customer']['order'][0] = 'Customer.'. $sort. ' '. $direction;
			}
			$this->HoldPaginate->saveParams($this->paginate); // ページネート情報の保存
		}
		$this->set('page', $page);
		$this->set('sort', $sort);
		$this->set('direction', $direction);

		$this->HoldPaginate->setPaginate();				// ページネート情報取得
		$list = $this->paginate('Customer');			// 一覧データ取得
		$this->HoldPaginate->savePaginate();			// ページネート情報保持
		$this->set('list', $list);
		$holdPaginate =  $this->Session->read('holdPaginate');
		$this->set('limit',$holdPaginate[$this->name][$this->action]['Customer']['limit']);
		$this->__setViewByEdit();	// viewに必要な情報をセット
	}


	/**
	 * メール配信テーブル削除確認ページ
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/20
	 * @note
	 * @param    int    $id    メール配信テーブルID
	 */
	function del_conf($id) {
		// 戻る
		if (isset($this->params['form']['cancel']) || isset($this->params['form']['cancel_x'])) {
			$this->redirect('/mails/index');
		}

		// 削除
		if (isset($this->params['form']['del']) || isset($this->params['form']['del_x'])) {
			if ($this->MailService->delete($id)) {
				$this->redirect('/mails/index/');
			}
		}

		/* メール部分 */
		// 編集開始時のデータ読み込み
		if (empty($this->data) && $id != "") {
			$params = array(
				'conditions' => array('Mail.id' => $id),
				'fields' => array('Mail.*'),
			);
			$this->data = $this->Mail->find('first', $params);

			if (!$this->data || !empty($this->data['Mail']['del_flag'])) {
				$this->redirect('/mails/index');
			}
		}

		// 初期設定
		if (empty($this->data)) {
			$this->data = $this->MailService->getIni(); //初期値設定
		}
		/* 顧客一覧ページネート */
		$params = array();
		// 既存メールの際のページネートの設定
		$this->paginate['Customer']['joins'] = array(
							array(
								'type' => 'inner',
								'table' => 'mails',
								'alias' => 'Mail',
								'conditions' => array('Mail.id' => $id)
							),
							array(
								'type' => 'inner',
								'table' => 'mail_senders',
								'alias' => 'MailSender',
								'conditions' => array(
									'MailSender.mail_id = Mail.id',
									'MailSender.customer_id = Customer.id',
								),
							)
						);
		$this->HoldPaginate->saveParams($this->paginate); // ページネート情報の保存

		/* メール配信一覧 */
		if (empty($this->data['MailSender'])) {
			if (empty($id)) {
				// 新規データ
				unset($params['limit']);
				$params['fields'] = array('Customer.id');
				// メール配信フラグを初期化
				$customer = $this->Customer->find('list', $params);
				foreach  ((array)$customer as $key => $val) {
					$this->data['MailSender'][$key]['send_flag'] = 1;
				}
			} else {
				/* 既存データの場合は、MailSenderから取得する */
				$mailSenderArr = $this->MailSender->find('all', array('conditions'=>array('MailSender.mail_id'=>$id)));
				// 整形
				foreach((array)$mailSenderArr as $key => $val) {
					$this->data['MailSender'][$val['MailSender']['customer_id']] = $val['MailSender'];
				}
			}
			// メール配信フラグをセションに保持
			$this->Session->write('mailSenderArr', $this->data['MailSender']);
		} else {
			// エラーなどで戻ってきた場合
			// 画面のメール配信フラグをセションのデータをマージ
			$mailSenderArr = $this->Session->read('mailSenderArr');
			foreach ($this->data['MailSender'] as $key => $val) {
				$mailSenderArr[$key]['send_flag'] = $val['send_flag'];
			}
			$this->data['MailSender'] = $mailSenderArr;
			// メール配信フラグをセションに保持
			$this->Session->write('mailSenderArr', $this->data['MailSender']);
		}

		$page = 1;
		$pageRow = 10;
		$sort = null;
		$direction = null;
		// smartTable機能からの送信
		if (isset($this->params['form']['smartTableSubmit'])) {
			// ページ取得
			if(isset($this->params['form']['page_num'])){
				$page = $this->params['form']['page_num'];
			}
			// ソートキー取得
			if(isset($this->params['form']['sort'])){
				$sort = $this->params['form']['sort'];
			}
			// 並び順取得
			if(isset($this->params['form']['direction'])){
				$direction = $this->params['form']['direction'];
			}
			// ページ行数取得
			if(isset($this->params['form']['page_row'])){
				$pageRow = $this->params['form']['page_row'];
			}

			// ページネートに設定
			$this->paginate['Customer']['limit'] = $pageRow;
			$this->paginate['Customer']['offset'] = $pageRow*($page-1);
			if (!empty($sort) && !empty($direction)) {
				$this->paginate['Customer']['order'][0] = 'Customer.'. $sort. ' '. $direction;
			}
			$this->HoldPaginate->saveParams($this->paginate); // ページネート情報の保存
		}
		$this->set('page', $page);
		$this->set('sort', $sort);
		$this->set('direction', $direction);

		$this->HoldPaginate->setPaginate();				// ページネート情報取得
		$list = $this->paginate('Customer');			// 一覧データ取得
		$this->HoldPaginate->savePaginate();			// ページネート情報保持
		$this->set('list', $list);
		$holdPaginate =  $this->Session->read('holdPaginate');
		$this->set('limit',$holdPaginate[$this->name][$this->action]['Customer']['limit']);
		$this->__setViewByEdit();	// viewに必要な情報をセット
	}


	/**
	 * ユーザーテーブル編集ページに必要な変数準備
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function __setViewByEdit() {
		// MailTemplate
		$this->set('mailTemplateArr', $this->MailTemplate->getArr());

		// CustomerKojin
		$customerKojin = $this->CustomerKojin->getDataByUserId($this->login['User']['id']);
		$this->set('customerKojin', $customerKojin);
	}


	/** AJAX関数 **/
	/**
	 * テンプレート設置
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/17
	 * @note
	 */
	function ajax_get_template() {
		$this->layout = 'ajax';
		Configure::write('debug', 0);

		$id = "";
		if (!empty($this->params['form']['id'])) {
			$id = $this->params['form']['id'];
		}

		$body = "";
		if (!empty($this->params['form']['body'])) {
			$body = $this->params['form']['body'] . "\r\n";
		}

		$result = $this->MailTemplate->read(null, $id);
		$this->data['Mail']['subject'] = $result['MailTemplate']['subject'];
		$this->data['Mail']['body'] = $result['MailTemplate']['body'];
	}
}
?>