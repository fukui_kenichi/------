<?php
/**
 * ユーザーマスターテーブルコントローラー
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note
 */
class UserMastersController extends AppController {
	var $name = 'UserMasters';
	var $uses = array('User', 'UserMaster', 'UserRefer');
	var $components = array('UserMasterService');
	var $paginate = array(
		'UserMaster' => array(
			'fields' => array(
				'*',
			),
			'conditions' => array(),
			'order' => array('UserMaster.id asc'),
			'limit' => 50,
			'recursive' => -1,
		)
	);


	/**
	 * 初期処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function beforeFilter() {
		parent::beforeFilter();

		// ここに追加検索初期値があれば定義
		//$this->paginate['UserMaster']['condition']['xxx'] = 'xxx';
	}


	/**
	 * ユーザーマスターテーブル一覧ページ
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function index() {
		// 戻る
		if (isset($this->params['form']['cancel_x'])) {
			$prevPage = $this->Session->read('prevPage');
			if (!empty($prevPage)){
				$this->redirect("/{$prevPage['url']}");
			}else{
				$this->redirect("/top/index");
			}
		}

		// 登録・更新
		if (isset($this->params['form']['save_x'])) {
			if ($this->UserMasterService->saveByIndex()){
				$this->redirect('/user_masters/index');
			}
		}

		// 一覧開始時のデータ読み込み
		if (empty($this->data)) {
			// 表示するデータを取得して使える形にする
			$this->data = $this->UserMasterService->getDataByIndex();
		}

	}
}
?>