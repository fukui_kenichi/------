<?php
/**
 * トップページコントローラー
 * @author   fukui@okushin.co.jp
 * @date     2015/03/06
 * @note
 */
class TopController extends AppController {
	var $name = 'Top';
	var $uses = array('Busy', 'BusiMaster', 'Customer', 'CustomerMaster');
	var $components = array('TopService');
	var $layout = 'top';

	/**
	 * 初期処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/03/06
	 * @note
	 */
	function beforeFilter() {
		parent::beforeFilter();

		// ここに追加検索初期値があれば定義
		//$this->paginate['Kojin']['condition']['xxx'] = 'xxx';
	}


	/**
	 * トップページ
	 * @author   fukui@okushin.co.jp
	 * @date     2015/03/06
	 * @note
	 */
	function index() {
		// メール配信
		if (isset($this->params['form']['send_mail'])) {
			$params = array(
				'fields' => '*',
				'conditions'=>array(
					'Customer.del_flag'=>0,
					'Customer.id'=>explode(',',$this->data['Customer']['id'])
				),
				'order'=>'Customer.id DESC'
			);
			$holdPaginate['Customers']['index']['Customer'] = $params;
			$this->Session->write('holdPaginate', $holdPaginate);
			$this->redirect('/mails/edit');
		}

		// お知らせ(顧客)アラート一覧取得
		$alertCustomerArr = $this->TopService->getAlertCustomerList();
		$this->set('alertCustomerArr', $alertCustomerArr);

		// お知らせ(商談)アラート一覧取得
		$alertBusyArr = $this->TopService->getAlertBusyList();
		$this->set('alertBusyArr', $alertBusyArr);

		$alertCount = 0;
		foreach((array)$alertCustomerArr as $val){
			$alertCount = $alertCount + count($val['value']);
		}
		foreach((array)$alertBusyArr as $val){
			$alertCount = $alertCount + count($val['value']);
		}
		$this->set('alertCount', $alertCount);

	}
}
?>