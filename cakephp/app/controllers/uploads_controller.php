<?php
/**
 * ファイルアップロードテーブルコントローラー
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note
 */
class UploadsController extends AppController {
	var $name = 'Uploads';
	var $uses = array('Upload','UserMaster');
	var $components = array('UploadService');
	var $paginate = array(
		'Upload' => array(
			'fields' => array(
				'*',
			),
			'conditions' => array(),
			'order' => array('Upload.id asc'),
			'limit' => 50,
			'recursive' => -1,
		)
	);


	/**
	 * 初期処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function beforeFilter() {
		parent::beforeFilter();

		// ここに追加検索初期値があれば定義
		//$this->paginate['Upload']['condition']['xxx'] = 'xxx';
	}


	/**
	 * ファイルアップロードテーブル一覧ページ
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function index() {

		// 削除
		if (isset($this->params['form']['del'])) {
			if ($this->UploadService->delete($this->params['form']['hdnId'])) {
				$this->redirect('/uploads/index');
			}
		}



		// 検索条件作成
		if (isset($this->params['form']['search'])) {
			$this->UploadService->createParams($this->data['Search'], $this->paginate);
		}

		// ページ切替
		if (isset($this->params['form']['refresh'])) {
			if (!empty($this->params['form']['page_num']) && ereg("^[0-9]+$", $this->params['form']['page_num'])){
				$this->redirect('/uploads/index/page:' . $this->params['form']['page_num']);
			}
		}

		$this->HoldPaginate->setPaginate();				// ページネート情報取得
		$list = $this->paginate();						// 一覧データ取得
		$this->HoldPaginate->savePaginate();			// ページネート情報保持
		$this->set('list', $list);

	}


	/**
	 * ファイルアップロードテーブル編集ページ
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @param    int    $id    ファイルアップロードテーブルID
	 */
	function edit($id = null) {

		// 戻る
		if (isset($this->params['form']['cancel'])) {
			$this->redirect('/uploads/index');
		}

		// 登録
		if (isset($this->params['form']['save'])) {
			if ($this->UploadService->save()){
				$this->redirect('/uploads/index');
			}
		}

		// 編集開始時のデータ読み込み
		if (empty($this->data) && $id != "") {
			$params = array(
				'conditions' => array('Upload.id' => $id),
				'fields' => array('Upload.*'),
			);
			$this->data = $this->Upload->find('first', $params);
			if (!$this->data || !empty($this->data['Upload']['del_flag'])) {
				$this->redirect('/uploads/index');
			}
		}

		// 初期設定
		if (empty($this->data)) {
			$this->data = $this->UploadService->getIni(); //初期値設定
		}
	}
	/**
	 * イメージファイルの複数アップロード処理
	 * @author   nishishita@okushin.co.jp
	 * @date     2015/02/09
	 * @note
	 */
	function img_upload() {
		$this->layout = 'ajax';
		/// レンダリングなし
		$this->autoRender = false;
		/// debugコードを非表示
//		Configure::write('debug', 2);

		//$extStr = '(gif|png|jpg|jpeg|bmp)';	// アップロードできる拡張子
		$extStr = '(png|jpg|jpeg)';	// アップロードできる拡張子

		// GETの値からどのアップロードか取得
		$koumoku = $this->params['url']['koumoku'];
		$this->set('koumoku', $koumoku);
		$model = $this->params['url']['model'];
		$this->set('model', $model);
//		debug($koumoku);
//		debug($model);
//		debug($this->data);
		// ファイル選択の確認
		if (!$this->data[$model]['up_image_'.$koumoku][0]) {
			$this->set('error', 'アップロードするファイルを選択してください');
			$this->render('img_upload','ajax');
		} else {
			$filename = array();
			$upName = array();
			// 選択されたファイルをチェックする。
			foreach ($this->data[$model]['up_image_'.$koumoku] as $key => $val) {
				$path_parts = pathinfo($val['name']);
				$ext = pathinfo($val['name'], PATHINFO_EXTENSION);
				// 拡張子チェック
				if (!preg_match('/'. $extStr. '/i', $ext)) {
					$extErr = preg_replace('/\|/', ' | ', $extStr);
					$this->set('error', "アップロードできない拡張子のファイルがあります。　※アップロード可能：{$extErr}");
					$this->render('img_upload','ajax');
					return;
				}
				// 最大文字数
				if (!$this->User->mb_maxLength($val['name'], 200)) {
					$this->set('error', "200文字以下のファイル名のファイルを選択してください。");
					$this->render('img_upload','ajax');
					return;
				}
				// マックスファイルサイズチェック
				if ($this->data[$model]['up_image_'.$koumoku][$key]['size'] > MAX_IMAGE_FILE_SIZE) {
					$max_size = MAX_IMAGE_FILE_SIZE / 1000000;
					$this->set('error', "ファイルサイズが大きすぎるため、アップロードできません。{$max_size}MB以下のファイルを選択してください。");
					$this->render('img_upload','ajax');
					return;
				}
				$filename[$key] = uniqid(rand(0,1000)) . '.' . $path_parts['extension'];
				$upName[$key] = $val['name'];
			}
			// IDだけDB保存
			$result = $this->Upload->saveFrist(count($upName));
			if (!$result) {
				$this->set('error', "DB追加に失敗しました。");
				$this->render('img_upload','ajax');
				return;
			}
			// DBのIDファイル名へ変更する。
			foreach($this->data[$model]['up_image_'.$koumoku] as $key => $val) {
				$path_parts = pathinfo($val['name']);
				$filename[$key] = $result[$key] . '.' . $path_parts['extension'];
				$extension[$key] = $path_parts['extension'];
			}
			$upPath = ROOT_PATH . 'upload/';
			$upFile = array();
			if (is_writeable($upPath)) {
				foreach($this->data[$model]['up_image_'.$koumoku] as $key => $val)	{
					rename($this->data[$model]['up_image_'.$koumoku][$key]['tmp_name'], $upPath . $filename[$key]);
					@chmod($upPath . $filename[$key],0777);
					$this->Upload->upImage($upPath . $filename[$key],$result[$key]);
					$upFile[$key] = ROOT_URL . 'upload/' . $filename[$key];
				}
				$this->set('filename', $filename);
				$this->set('upFile', $upFile);
				$this->set('upName', $upName);
				$this->set('idArr', $result);
				// 拡張子、ファイル名、元々のファイル名を保存する。
				$result = $this->Upload->saveSecond($result, $extension, $filename, $upName);
				if(!$result) {
					$this->set('error', "DB更新に失敗しました。");
				}
				$this->render('img_upload','ajax');
			} else {
				$this->set('error', '画像フォルダ(' . $upPath . ')に書き込みできません。');
				$this->render('img_upload','ajax');
			}
		}
	}

	/**
	 * イメージファイル以外の複数アップロード処理
	 * @author   nishishita@okushin.co.jp
	 * @date     2015/02/09
	 * @note
	 */
		function notimg_upload() {
		$this->layout = 'ajax';
		/// レンダリングなし
		$this->autoRender = false;
		/// debugコードを非表示
		Configure::write('debug', 0);

//		$extStr = '(doc|docx|xls|xlsx|pdf|swf|txt|ppt|pptx|zip)';	// アップロードできる拡張子 deleted by nakata 20150227 その他のファイルは拡張子を制限しない


		// GETの値からどのアップロードか取得
		$koumoku = $this->params['url']['koumoku'];
		$this->set('koumoku', $koumoku);
		$model = $this->params['url']['model'];
		$this->set('model', $model);
//		debug($koumoku);
//		debug($model);
//		debug($this->data);
		// ファイル選択の確認
		if (!$this->data[$model]['up_notimage_'.$koumoku][0]) {
			$this->set('error', 'アップロードするファイルを選択してください');
			$this->render('notimg_upload','ajax');
		} else {
			$filename = array();
			$upName = array();
			// 選択されたファイルをチェックする。
			foreach ($this->data[$model]['up_notimage_'.$koumoku] as $key => $val) {
				$path_parts = pathinfo($val['name']);
				$ext = pathinfo($val['name'], PATHINFO_EXTENSION);
				// 拡張子チェック deleted by nakata 20150227 その他のファイルは拡張子を制限しない
//				if (!preg_match('/'. $extStr. '/i', $ext)) {
//					$extErr = preg_replace('/\|/', ' | ', $extStr);
//					$this->set('error', "アップロードできない拡張子のファイルがあります。　※アップロード可能：{$extErr}");
//					$this->render('notimg_upload','ajax');
//					return;
//				}
				// 最大文字数
				if (!$this->User->mb_maxLength($val['name'], 200)) {
					$this->set('error', "200文字以下のファイル名のファイルを選択してください。");
					$this->render('notimg_upload','ajax');
					return;
				}
				// マックスファイルサイズチェック
				if ($this->data[$model]['up_notimage_'.$koumoku][$key]['size'] > MAX_OTHER_FILE_SIZE) {
					$max_size = MAX_OTHER_FILE_SIZE / 1000000;
					$this->set('error', "ファイルサイズが大きすぎるため、アップロードできません。{$max_size}MB以下のファイルを選択してください。");
					$this->render('notimg_upload','ajax');
					return;
				}
				$filename[$key] = uniqid(rand(0,1000)) . '.' . $path_parts['extension'];
				$upName[$key] = $val['name'];
			}
			// IDだけDB保存
			$result = $this->Upload->saveFrist(count($upName));
			if (!$result) {
				$this->set('error', "DB追加に失敗しました。");
				$this->render('img_upload','ajax');
				return;
			}
			// DBのIDファイル名へ変更する。
			foreach($this->data[$model]['up_notimage_'.$koumoku] as $key => $val) {
				$path_parts = pathinfo($val['name']);
				$filename[$key] = $result[$key] . '.' . $path_parts['extension'];
				$extension[$key] = $path_parts['extension'];
			}
			$upPath = ROOT_PATH . 'upload/';
			$upFile = array();
			if (is_writeable($upPath)) {
				foreach($this->data[$model]['up_notimage_'.$koumoku] as $key => $val)	{
					rename($this->data[$model]['up_notimage_'.$koumoku][$key]['tmp_name'], $upPath . $filename[$key]);
					@chmod($upPath . $filename[$key],0777);
					$upFile[$key] = ROOT_URL . 'upload/' . $filename[$key];
				}

				$this->set('filename', $filename);
				$this->set('upFile', $upFile);
				$this->set('upName', $upName);
				$this->set('idArr', $result);
				// 拡張子、ファイル名、元々のファイル名を保存する。
				$result = $this->Upload->saveSecond($result, $extension, $filename, $upName);
				if(!$result) {
					$this->set('error', "DB更新に失敗しました。");
				}
				$this->render('notimg_upload','ajax');
			} else {
				$this->set('error', '画像フォルダ(' . $upPath . ')に書き込みできません。');
				$this->render('notimg_upload','ajax');
			}
		}
	}


	/**
	 * ファイルダウンロード
	 * @author   nishishita@okushin.co.jp
	 * @date     2014/07/11
	 * @note
	 * @param    str    $name    画像ファイル名
	 */
	function download($id) {
		$message = $this->Upload->read(null, $id);

		$filename = $message['Upload']['up_file_name'];
		//エンコードの変更
//		$filename = $this->__changeEncode($filename);

		$path_parts = pathinfo($filename);
		$fname = $id .'.' . $path_parts['extension'];

		$filePath = ROOT_PATH . 'upload/' .  $fname;
//		debug($filePath);
		set_time_limit(1000);
		ini_set("memory_limit", "512M");
		ini_set("max_input_time", "1000");

		$this->autoRender = false; // Viewを使用しない
		Configure::write('debug', 0); // debugコードを非表示
		$filename = mb_convert_encoding($filename, "SJIS", "auto");
		header ("Content-disposition: attachment; filename={$filename}");
		header ("Content-type: application/octet-stream; name={$filename}");

		ob_end_clean(); //ファイル破損を防ぐ //出力バッファのゴミ捨て

		$buf = file_get_contents($filePath);
		echo $buf;
		exit;
	}

}
?>