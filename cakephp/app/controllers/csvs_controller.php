<?php
/**
 * CSV出力コントローラー
 * @author   fukui@okushin.co.jp
 * @date     2015/09/02
 * @note
 */
class CsvsController extends AppController {
	var $name = 'Csvs';
	var $uses = array('CustomerMaster', 'BusiMaster', 'Customer', 'Busy');
	var $components = array('CsvService');


	/**
	 * 初期処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function beforeFilter() {
		parent::beforeFilter();

		// ここに追加検索初期値があれば定義
		//$this->paginate['Kojin']['condition']['xxx'] = 'xxx';
	}


	/**
	 * CSV出力画面
	 * @author   fukui@okushin.co.jp
	 * @date     2015/09/02
	 * @note
	 */
	function index() {
		// 戻る
		if (isset($this->params['form']['cancel_x'])) {
			$prevPage = $this->Session->read('prevPage');
			if (!empty($prevPage)){
				$this->redirect("/{$prevPage['url']}");
			}else{
				$this->redirect("/top/index");
			}
		}

		// 顧客情報CSV出力
		if (isset($this->params['form']['down_customer'])) {
			$dataArr = $this->CsvService->getCustomerCsv();
			$this->set('dataArr', $dataArr);

			$this->autoRender = false;
			$this->layout = false;
			$this->render('down_csv');
		}

		// 商談情報CSV出力
		if (isset($this->params['form']['down_busy'])) {
			$dataArr = $this->CsvService->getBusyCsv();
			$this->set('dataArr', $dataArr);

			$this->autoRender = false;
			$this->layout = false;
			$this->render('down_csv');
		}
	}

}
?>