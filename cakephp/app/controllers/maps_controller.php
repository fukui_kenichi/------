<?php
/**
 * 顧客地図情報コントローラー
 * @author   fukui@okushin.co.jp
 * @date     2015/03/06
 * @note
 */
class MapsController extends AppController {
	var $name = 'Maps';
	var $uses = array('Customer');


	/**
	 * 初期処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/03/06
	 * @note
	 */
	function beforeFilter() {
		parent::beforeFilter();

		// ここに追加検索初期値があれば定義
		//$this->paginate['Kojin']['condition']['xxx'] = 'xxx';
	}


	/**
	 * 顧客地図情報（戻る画面取得）
	 * @author   fukui@okushin.co.jp
	 * @date     2015/03/06
	 * @note
	 */
	function before_index() {
		$prevPage = $this->Session->read('prevPage');
		$this->Session->write('prevMapPage', $prevPage);
		$this->redirect('/maps/index');
	}


	/**
	 * 顧客地図情報
	 * @author   fukui@okushin.co.jp
	 * @date     2015/03/06
	 * @note
	 */
	function index($latitude = null, $longitude = null) {
		// 戻る
		if (isset($this->params['form']['cancel_x'])) {
			$prevMapPage = $this->Session->read('prevMapPage');
			if (!empty($prevMapPage)){
				$this->redirect("/{$prevMapPage['url']}");
			}else{
				$this->redirect("/top/index");
			}
		}
		if (!empty($latitude) && !empty($longitude)) {
			$this->set('latitude', $latitude);
			$this->set('longitude', $longitude);
		}
	}


	/**
	 * 地図検索候補顧客取得AJAX関数
	 * @author   fukui@okushin.co.jp
	 * @date     2014/03/12
	 * @note
	 */
	function ajax_get_candidate() {
		Configure::write('debug', 0);
		$this->autoRender = false; // jsonデータを返すだけなので、レンダー処理はなし

		$returnData = array(); // 返却する配列の初期化

		$centerLng = "";
		if (!empty($this->params['form']['centerLng'])){
			$centerLng = $this->params['form']['centerLng'];
		}
		$centerLat = "";
		if (!empty($this->params['form']['centerLat'])){
			$centerLat = $this->params['form']['centerLat'];
		}
		$zoom = "";
		if (!empty($this->params['form']['zoom'])){
			$zoom = $this->params['form']['zoom'];
		}

		// 地図の中心座標・倍率の情報をセションに保持
		$this->Session->write('mapCenterLng', $centerLng);
		$this->Session->write('mapCenterLat', $centerLat);
		$this->Session->write('mapZoom', $zoom);

		// 候補顧客取得
		$returnData = $this->Customer->getCandidate($this->params['form']['lngHigh'], $this->params['form']['lngLow'],
			$this->params['form']['latHigh'], $this->params['form']['latLow']);

		echo json_encode($returnData);
	}
}
?>