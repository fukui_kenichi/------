<?php
/**
 * メールテンプレートテーブルコントローラー
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note
 */
class MailTemplatesController extends AppController {
	var $name = 'MailTemplates';
	var $uses = array('MailTemplate', 'Mail');
	var $components = array('MailTemplateService');
	var $paginate = array(
		'MailTemplate' => array(
			'fields' => array(
				'*',
			),
			'conditions' => array(),
			'order' => array('MailTemplate.id asc'),
			'limit' => 10,
			'recursive' => -1,
		)
	);


	/**
	 * 初期処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function beforeFilter() {
		parent::beforeFilter();

//		$this->paginate['MailTemplate']['limit'] = 3;
//		$this->HoldPaginate->saveParams($this->paginate);

		// ここに追加検索初期値があれば定義
		//$this->paginate['MailTemplate']['condition']['xxx'] = 'xxx';
	}


	/**
	 * メールテンプレートテーブル一覧ページ
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function index() {
		// 戻る
		if (isset($this->params['form']['cancel_x'])) {
			$prevPage = $this->Session->read('prevPage');
			if (!empty($prevPage)){
				$this->redirect("/{$prevPage['url']}");
			}else{
				$this->redirect('/top/index');
			}
		}

		// ページ取得
		$page = null;
		if(isset($this->passedArgs['page'])){
			$page = $this->passedArgs['page'];
		}
		// ソートキー取得
		$sort = null;
		if(isset($this->passedArgs['sort'])){
			$sort = $this->passedArgs['sort'];
		}
		// 並び順取得
		$direction = null;
		if(isset($this->passedArgs['direction'])){
			$direction = $this->passedArgs['direction'];
		}
		$this->set('page',$page);
		$this->set('sort',$sort);
		$this->set('direction',$direction);


		// 検索条件作成
		if (isset($this->params['form']['search'])) {
			$this->MailTemplateService->createParams($this->data['Search'], $this->paginate);
		}

		// ページ切替
		if (isset($this->params['form']['refresh'])) {
			// ページリミット設定
			$this->paginate['MailTemplate']['limit'] = $this->params['form']['page_row'];
			$this->HoldPaginate->saveParams($this->paginate);
			if (!empty($this->params['form']['page_num']) && ereg("^[0-9]+$", $this->params['form']['page_num'])){
				$this->redirect('/mail_templates/index/page:' . $this->params['form']['page_num']);
			}
		}


		$this->HoldPaginate->setPaginate();				// ページネート情報取得
		$list = $this->paginate();						// 一覧データ取得
		$this->HoldPaginate->savePaginate();			// ページネート情報保持
		$this->set('list', $list);
		$holdPaginate =  $this->Session->read('holdPaginate');
		$this->set('limit',$holdPaginate[$this->name][$this->action]['MailTemplate']['limit']);

	}


	/**
	 * メールテンプレートテーブル編集ページ
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @param    int    $id    メールテンプレートテーブルID
	 */
	function edit($id = null) {

		// 戻る
		if (isset($this->params['form']['cancel_x'])) {
			$this->redirect('/mail_templates/index');
		}

		// 登録
		if (isset($this->params['form']['save_x'])) {
			if ($this->MailTemplateService->save()){
				$this->redirect('/mail_templates/index');
			}
		}

		// メール配信情報画面からの遷移の場合
		if(isset($this->passedArgs['mail_id'])){
			// 指定されたメールの内容をテンプレートの値に設定
			$mail = $this->Mail->find('first', array('conditions'=>array('Mail.id'=>$this->passedArgs['mail_id'])));
			if (!empty($mail)) {
				$this->data['MailTemplate']['subject'] = $mail['Mail']['subject'];
				$this->data['MailTemplate']['body'] = $mail['Mail']['body'];
			}
		}

		// 編集開始時のデータ読み込み
		if (empty($this->data) && $id != "") {
			$params = array(
				'conditions' => array('MailTemplate.id' => $id),
				'fields' => array('MailTemplate.*'),
			);
			$this->data = $this->MailTemplate->find('first', $params);
			if (!$this->data || !empty($this->data['MailTemplate']['del_flag'])) {
				$this->redirect('/mail_templates/index');
			}
		}

		// 初期設定
		if (empty($this->data)) {
			$this->data = $this->MailTemplateService->getIni(); //初期値設定
		}
	}



	/**
	 * メールテンプレートテーブル情報
	 * @author   nishishita@okushin.co.jp
	 * @date     2015/03/05
	 * @note
	 * @param    int    $id    メールテンプレートテーブルID
	 */
	function detail($id = null) {

		// 戻る
		if (isset($this->params['form']['cancel_x'])) {
			$this->redirect('/mail_templates/index');
		}


		// 編集開始時のデータ読み込み
		if (empty($this->data) && $id != "") {
			$params = array(
				'conditions' => array('MailTemplate.id' => $id),
				'fields' => array('MailTemplate.*'),
			);
			$this->data = $this->MailTemplate->find('first', $params);
			if (!$this->data || !empty($this->data['MailTemplate']['del_flag'])) {
				$this->redirect('/mail_templates/index');
			}
		}
	}



	/**
	 * メールテンプレートテーブル削除情報
	 * @author   nishishita@okushin.co.jp
	 * @date     2015/03/05
	 * @note
	 * @param    int    $id    メールテンプレートテーブルID
	 */
	function del_conf($id = null) {

		// 戻る
		if (isset($this->params['form']['cancel_x'])) {
			$this->redirect('/mail_templates/index');
		}
		// 戻る
		if (isset($this->params['form']['del_x'])) {
			if ($this->MailTemplate->delete($id)) {
				$this->redirect('/mail_templates/index');
			}
		}


		// 編集開始時のデータ読み込み
		if (empty($this->data) && $id != "") {
			$params = array(
				'conditions' => array('MailTemplate.id' => $id),
				'fields' => array('MailTemplate.*'),
			);
			$this->data = $this->MailTemplate->find('first', $params);
			if (!$this->data || !empty($this->data['MailTemplate']['del_flag'])) {
				$this->redirect('/mail_templates/index');
			}
		}
	}

}
?>