<?php
/**
 * ログインコントローラー
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note
 */
class LoginController extends AppController {

	var $name = 'Login';
	var $uses = array('Token');
	var $layout = 'login';

	/**
	 * 初期処理
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 */
	function beforeFilter() {

	}

	/**
	 * ログインページ
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 */
	function index() {
		$errMsg = "";

		// トークンがあればhiddenに持たす
		if(!empty($this->params['url']['token'])){
			$this->data['User']['token'] = $this->params['url']['token'];
		}
		if (isset($this->params['form']['login_x'])) {
			$cond = array(
				'conditions' => array(
					'User.account' => $this->data['User']['account'],
					'User.password' => $this->data['User']['password'],
					'User.del_flag' => 0,
				),
			);
			$this->login = $this->User->find('first', $cond);

			if (empty($this->login)) {
				$errMsg = 'ID・パスワードが正しくありません';
			} else {
				// トークンがあれば保存
				if (!empty($this->data['User']['token'])){
					$this->Token->saveToken($this->login['User']['id'], $this->data['User']['token']);
				}
				$this->Session->write('login', $this->login);
				$this->redirect('/top/index');
			}
		}
		$this->set('errMsg', $errMsg);
	}
}
?>