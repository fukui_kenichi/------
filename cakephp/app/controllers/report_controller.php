<?php
/**
 * レポートコントローラー
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note
 */
class ReportController extends AppController {

	var $name = 'Report';
	var $uses = array('Customer', 'CtiCount', 'Mail');
	var $components = array('ReportService');
	var $layout = 'report';

	/**
	 * 初期処理
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 */
	function beforeFilter() {
		parent::beforeFilter();

	}

	/**
	 * レポートページ
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 */
	function index() {
		// 新規顧客VS保有顧客
		$this->ReportService->setCustomerWariai();

		// 過去3か月分のメール配信数
		$this->ReportService->setMailWariai();

		// 過去3か月分のCTI受信数
		$this->ReportService->setCtiCount();

		// 過去3か月分の獲得顧客数
		$this->ReportService->setCustomerCount();
	}
}
?>