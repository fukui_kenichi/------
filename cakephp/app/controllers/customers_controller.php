<?php
/**
 * 顧客テーブルコントローラー
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note
 */
class CustomersController extends AppController {
	var $name = 'Customers';
	var $uses = array('Customer', 'CustomerMaster', 'CustomerKojin', 'CustomerRefer', 'Upload',
						'Busy', 'BusiMaster', 'BusiKojin', 'BusiRefer', 'UserRefer', 'Favorite', 'CtiCount');
	var $components = array('CustomerService', 'UploadService');
	var $paginate = array(
		'Customer' => array(
			'fields' => array('*'),
			'conditions' => array('Customer.del_flag' => 0),
			'order' => array('Customer.updated DESC'),
			'limit' => 10,
			'recursive' => -1,
		),
	);
	var $pageRow = 0;


	/**
	 * 初期処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function beforeFilter() {
		parent::beforeFilter();

		// ここに追加検索初期値があれば定義
		//$this->paginate['Customer']['condition']['xxx'] = 'xxx';
	}


	/**
	 * 顧客テーブル一覧ページ(顧客管理)
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function index() {
		// お気に入りから飛んできていたらキーワードの設定
		if (isset($this->passedArgs['favoriteId'])) {
			$this->data['Search']['text'] = $this->Favorite->getKeywordById($this->passedArgs['favoriteId']);
		}

		// セションより検索条件を取得
		if (empty($this->data)){
			$this->HoldPaginate->setSearchForm();			// 検索条件情報取得
		}

		// ページ取得
		$page = null;
		if(isset($this->passedArgs['page'])){
			$page = $this->passedArgs['page'];
		}
		// ソートキー取得
		$sort = null;
		if(isset($this->passedArgs['sort'])){
			$sort = $this->passedArgs['sort'];
		}
		// 並び順取得
		$direction = null;
		if(isset($this->passedArgs['direction'])){
			$direction = $this->passedArgs['direction'];
		}
		// 表示件数
		$limit = null;
		if(isset($this->passedArgs['limit'])){
			$limit = $this->passedArgs['limit'];
			$this->paginate['Customer']['limit'] = $limit;
		}
		$this->set('page',$page);
		$this->set('sort',$sort);
		$this->set('direction',$direction);
		$this->set('limit',$limit);

		// 検索条件作成
		if (isset($this->params['form']['search']) || isset($this->params['form']['search_x']) || !empty($this->data['Search']['text'])) {
			$this->CustomerService->createParams($this->data['Search'], $this->paginate);
		}

		// お気に入り追加
		if (isset($this->params['form']['add_favorite']) || isset($this->params['form']['add_favorite_x'])) {
			// 検索キーワードが入っているか
			$conditionText = mb_trim($this->data['Search']['text']);
			if (empty($conditionText)) {
				$this->Session->write('search-message', 'お気に入りに追加するには、検索キーワードが必要です。');
			} else {
				$this->HoldPaginate->saveSearchForm();			// 検索条件情報保持
				$this->redirect('/favorites/edit');
			}
		}

		// メール配信
		if (isset($this->params['form']['send_mail']) || isset($this->params['form']['send_mail_x'])) {
			$this->redirect('/mails/edit');
		}

		// ページ切替

		if (isset($this->params['form']['refresh'])) {
			// ページリミット設定
			$this->HoldPaginate->saveParams($this->paginate);
			if (!empty($this->params['form']['page_num']) && ereg("^[0-9]+$", $this->params['form']['page_num'])){
				$this->redirect("/customers/index/{$id}/page:" . $this->params['form']['page_num'] . "/limit:" . $this->params['form']['page_row']);
			}
		}

		// 顧客情報保存後に来たら保存した顧客だけを出す
		$entry_customer_id = $this->Session->read('entry_customer_id');
		if (!empty($entry_customer_id)){
			$this->paginate['Customer']['conditions']['Customer.id'] = $entry_customer_id;
			$this->Session->delete('entry_customer_id');
		}
		$this->HoldPaginate->setPaginate();				// ページネート情報取得
		$list = $this->paginate('Customer');						// 一覧データ取得
		$this->HoldPaginate->savePaginate();			// ページネート情報保持
		$this->HoldPaginate->saveSearchForm();			// 検索条件情報保持
		$this->set('list', $list);
		$holdPaginate =  $this->Session->read('holdPaginate');

		$this->__setViewByIndex();								// viewに必要な変数をセット
	}

	/**
	 * 顧客テーブル一覧ページ(商談登録)
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function index_busi() {
		$this->index();
		$this->render('/customers/index');
	}

	/**
	 * 顧客テーブル一覧ページ(メール配信)
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function index_mail() {
		$this->index();
		$this->render('/customers/index');
	}


	/**
	 * ユーザーテーブル一覧に必要な変数準備
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/23
	 * @note
	 */
	function __setViewByIndex() {
		$customerKojin = $this->CustomerKojin->getDataByUserId($this->login['User']['id']);
		$this->set('customerKojin', $customerKojin);
	}


	/**
	 * 顧客テーブル編集ページ
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @param    int    $id    顧客テーブルID
	 */
	function edit($id = null) {
		// 戻る
		if (isset($this->params['form']['cancel_x'])) {
			$this->redirect('/customers/index');
		}

		// 登録
		if (isset($this->params['form']['save_x'])) {
			if ($this->CustomerService->save()){
				$customer_id = $this->Customer->getID();
				$this->Session->write('entry_customer_id', $customer_id);
				$this->redirect("/customers/index/search:clear");
			}
		}

		// 編集開始時のデータ読み込み
		if (empty($this->data) && $id != "") {
			$params = array(
				'conditions' => array('Customer.id' => $id),
				'fields' => array('Customer.*'),
			);
			$this->data = $this->Customer->find('first', $params);
			if (!$this->data || !empty($this->data['Customer']['del_flag'])) {
				$this->redirect('/customers/index');
			}

			// CustomerMastar個別の設定
			$this->CustomerService->setCustomerByCustomerMaster($this->data);

			// イメージ、イメージではないファイル情報をセットする。
			$this->CustomerService->setUploadFile($this->data['Customer']['id'],$this->data);
		}

		// 初期設定
		if (empty($this->data)) {
			$this->data = $this->CustomerService->getIni(); //初期値設定
		}

		$this->__setViewByEdit();	// viewに必要な情報をセット

	}


	/**
	 * ユーザーテーブル編集ページに必要な変数準備
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function __setViewByEdit() {
		// CustomerMaster
		$customerMst = $this->CustomerMaster->find('all', array('order'=>array('CustomerMaster.number ASC')));
		$this->set('customerMst', $customerMst);
	}


	/**
	 * 顧客テーブル情報ページに必要な変数設定
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function __setViewByDetail() {
		// CustomerMaster
		$customerMst = $this->CustomerMaster->find('all', array('order'=>array('CustomerMaster.number asc')));
		$this->set('customerMst', $customerMst);

		// BusiKojin
		$busiKojin = $this->BusiKojin->getDataByUserId($this->login['User']['id']);
		$this->set('busiKojin', $busiKojin);
	}


	/**
	 * 顧客テーブル詳細ページ
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/03
	 * @note
	 * @param    int    $id    ユーザーテーブルID
	 */
	function detail($id = null) {

		// 編集開始時のデータ読み込み
		if (empty($this->data) && $id != "") {
			$params = array(
				'conditions' => array('Customer.id' => $id),
				'fields' => array('Customer.*'),
			);
			$this->data = $this->Customer->find('first', $params);
			if (!$this->data || !empty($this->data['Customer']['del_flag'])) {
				$this->redirect('/customers/index');
			}
			// CustomerMastar個別の設定
			$this->CustomerService->setCustomerByCustomerMaster($this->data);

			// イメージ、イメージではないファイル情報をセットする。
			$this->CustomerService->setUploadFile($this->data['Customer']['id'], $this->data);
		}

		// 初期設定
		if (empty($this->data)) {
			$this->data = $this->CustomerService->getIni(); //初期値設定
		}

		$this->__setViewByDetail();	// viewに必要な情報をセット

	}


	/**
	 * 顧客テーブル削除確認ページ
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/03
	 * @note
	 * @param    int    $id    ユーザーテーブルID
	 */
	function del_conf($id = null) {
		// 戻る
		if (isset($this->params['form']['cancel_x'])) {
			$this->redirect('/customers/index');
		}

		// 削除
		if (isset($this->params['form']['del_x'])) {
			if ($this->CustomerService->delFlag($id)) {
				$this->redirect('/customers/index');
			}
		}

		// 編集開始時のデータ読み込み
		if (empty($this->data) && $id != "") {
			$params = array(
				'conditions' => array('Customer.id' => $id),
				'fields' => array('Customer.*'),
			);
			$this->data = $this->Customer->find('first', $params);
			if (!$this->data || !empty($this->data['Customer']['del_flag'])) {
				$this->redirect('/customers/index');
			}
			// CustomerMastar個別の設定
			$this->CustomerService->setCustomerByCustomerMaster($this->data);

			// イメージ、イメージではないファイル情報をセットする。
			$this->CustomerService->setUploadFile($this->data['Customer']['id'], $this->data);
		}

		// 初期設定
		if (empty($this->data)) {
			$this->data = $this->CustomerService->getIni(); //初期値設定
		}

		$this->__setViewByDetail();	// viewに必要な情報をセット

	}


	/**
	 * 画像アップロード
	 * @author   nishishita@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function upload() {
		$this->layout = 'ajax';
		/// レンダリングなし
		$this->autoRender = false;
		/// debugコードを非表示
		Configure::write('debug', 0);
		$extStr = '(png|jpg|jpeg)';	// アップロードできる拡張子

		// GETの値からどのアップロードか取得
		$koumoku = $this->params['url']['koumoku'];
		$this->set('koumoku', $koumoku);
		$model = $this->params['url']['model'];
		$this->set('model', $model);
		if (!$this->data[$model.'Edit']['up_'.$koumoku]) {
			$this->set('error', 'アップロードするファイルを選択してください');
			$this->render('upload','ajax');
		} else {
			$filename = '';
			$path_parts = pathinfo($this->data[$model.'Edit']['up_'.$koumoku]['name']);
			$ext = pathinfo($this->data[$model.'Edit']['up_'.$koumoku]['name'], PATHINFO_EXTENSION);
			// 拡張子チェック
			if (!preg_match('/'. $extStr. '/i', $ext)) {
				$extErr = preg_replace('/\|/', ' | ', $extStr);
				$this->set('error', "アップロードできない拡張子のファイルがあります。　※アップロード可能：{$extErr}");
				$this->render('upload','ajax');
				return;
			}
			// 最大文字数
			if (!$this->User->mb_maxLength($this->data[$model.'Edit']['up_'.$koumoku]['name'], 200)) {
				$this->set('error', "200文字以下のファイル名のファイルを選択してください。");
				$this->render('upload','ajax');
				return;
			}
			// マックスファイルサイズチェック
			if ($this->data[$model.'Edit']['up_'.$koumoku]['size'] > MAX_IMAGE_FILE_SIZE) {
				$max_size = MAX_IMAGE_FILE_SIZE / 1000000;
				$this->set('error', "ファイルサイズが大きすぎるため、アップロードできません。{$max_size}MB以下のファイルを選択してください。");
				$this->render('upload','ajax');
				return;
			}
			$filename = uniqid(rand(0,1000)). '.' . $ext;
			$upName = $this->data[$model.'Edit']['up_'.$koumoku]['name'];
			if ($filename) {
				$upPath = ROOT_PATH . 'picture/temp/';
				if (is_writeable($upPath)) {
					$this->Customer->upImageCustomer($this->data[$model.'Edit']['up_'.$koumoku]['tmp_name'],$filename,'temp');
					rename($this->data[$model.'Edit']['up_'.$koumoku]['tmp_name'], $upPath . $filename);
					@chmod($upPath . $filename,0777);

					$this->set('filename', $filename);
					$upFile = ROOT_URL . 'picture/temp/';
					$fileNm = pathinfo($filename, PATHINFO_FILENAME);
					$this->set('upFile', $upFile . $fileNm . '-s.jpg');
					$this->render('upload','ajax');
				} else {
					$this->set('error', '画像フォルダ(' . $upPath . ')に書き込みできません。');
					$this->render('upload','ajax');
				}
			}
		}
	}


	/**
	 * CTI受信ページ
	 * @author   fukui@okushin.co.jp
	 * @date     2015/03/17
	 * @note
	 * @param    str    $tel    電話番号
	 */
	function tel($tel = null) {
		// 電話番号の顧客情報取得
		$params = array('conditions'=>array('Customer.tel'=>$tel, 'Customer.tel IS NOT NULL'));
		$customer = $this->Customer->find('first', $params);

		// 顧客情報があれば顧客詳細へ、無ければ新規顧客登録へ
		if (!empty($customer)){
			// 顧客別入電数保存
			$this->CtiCount->saveCount($customer['Customer']['id']);
			$this->redirect("/busies/index/{$customer['Customer']['id']}");
		}else{
			$this->redirect('/customers/edit');
		}

	}


    //--------------------------------------------------------------------------------
	// 画像の取り消し
	//--------------------------------------------------------------------------------
	function delete() {
		$this->layout = 'ajax';
		/// レンダリングなし
		$this->autoRender = false;
		/// debugコードを非表示
		Configure::write('debug', 0);

		// GETの値からどのアップロードか取得
		$koumoku = $this->params['url']['koumoku'];
		$this->set('koumoku', $koumoku);
		$model = $this->params['url']['model'];
		$this->set('model', $model);
		$this->render('delete','ajax');

	}


	/** AJAX関数 **/
	/**
	 * 同じ顧客名がいるかチェック
	 * @author   fukui@okushin.co.jp
	 * @date     2015/04/22
	 * @note
	 */
	function ajax_check_customer() {
		$this->layout = 'ajax';
		/// レンダリングなし
		$this->autoRender = false;
		/// debugコードを非表示
		Configure::write('debug', 0);

		$id = "";
		if (!empty($this->params['form']['id'])) {
			$id = $this->params['form']['id'];
		}

		$name = "";
		if (!empty($this->params['form']['name'])) {
			$name = $this->params['form']['name'];
		}

		$params = array(
			'conditions'=>array(
				'Customer.name'=>$name,
				'Customer.del_flag'=>0,
			)
		);
		if (!empty($id)){
			$params['conditions']['Customer.id !='] = $id;
		}
		$result = $this->Customer->find('all', $params);
		if (empty($result)){
			echo '0';
		}else{
			echo '1';
		}
	}

}
?>