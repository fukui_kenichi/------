<?php
/**
 * お気に入りテーブルコントローラー
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note
 */
class FavoritesController extends AppController {
	var $name = 'Favorites';
	var $uses = array('Favorite');
	var $components = array('FavoriteService');


	/**
	 * 初期処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function beforeFilter() {
		parent::beforeFilter();

		// ここに追加検索初期値があれば定義
	}


	/**
	 * お気に入りテーブル編集ページ
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @param    int    $id    お気に入りテーブルID
	 */
	function edit($id = null) {
		// 戻る
		if (isset($this->params['form']['cancel']) || isset($this->params['form']['cancel_x'])) {
			$this->redirect('/customers/index');
		}

		// 登録
		if (isset($this->params['form']['save']) || isset($this->params['form']['save_x'])) {
			if ($this->FavoriteService->save()){
				$this->redirect('/customers/index');
			}
		}

		if (empty($this->data) && $id != "") {
			// 編集開始時のデータ読み込み
			$params = array(
				'conditions' => array('Favorite.id' => $id),
				'fields' => array('Favorite.*'),
			);
			$this->data = $this->Favorite->find('first', $params);
			if (!$this->data || !empty($this->data['Favorite']['del_flag'])) {
				$this->redirect('/favorites/index');
			}
		}

		// 初期設定
		if (empty($this->data)) {
			$this->data = $this->FavoriteService->getIni(); //初期値設定
		}

	}


	/**
	 * 顧客テーブル削除確認ページ
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/10
	 * @note
	 * @param    int    $id    お気に入りID
	 */
	function del_conf($id) {
		// 戻る
		if (isset($this->params['form']['cancel_x'])) {
			$this->redirect('/customers/index');
		}

		// 削除
		if (isset($this->params['form']['del_x'])) {
			if ($this->FavoriteService->delete($id)) {
				$this->redirect('/customers/index');
			}
		}

		if (empty($this->data) && $id != "") {
			// データ読み込み
			$params = array(
				'conditions' => array('Favorite.id' => $id),
				'fields' => array('Favorite.*'),
			);
			$this->data = $this->Favorite->find('first', $params);
		}

		// 初期設定
		if (empty($this->data)) {
			$this->data = $this->FavoriteService->getIni(); //初期値設定
		}

	}
}
?>