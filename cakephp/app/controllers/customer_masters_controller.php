<?php
/**
 * 顧客マスターテーブルコントローラー
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note
 */
class CustomerMastersController extends AppController {
	var $name = 'CustomerMasters';
	var $uses = array('Customer', 'CustomerMaster', 'CustomerKojin', 'CustomerRefer');
	var $components = array('CustomerMasterService');
	var $paginate = array(
		'CustomerMaster' => array(
			'fields' => array(
				'*',
			),
			'conditions' => array(),
			'order' => array('CustomerMaster.id asc'),
			'limit' => 50,
			'recursive' => -1,
		)
	);


	/**
	 * 初期処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function beforeFilter() {
		parent::beforeFilter();

		// ここに追加検索初期値があれば定義
		//$this->paginate['CustomerMaster']['condition']['xxx'] = 'xxx';
	}


	/**
	 * 顧客マスターテーブル一覧ページ
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function index() {
		// 戻る
		if (isset($this->params['form']['cancel_x'])) {
			$prevPage = $this->Session->read('prevPage');
			if (!empty($prevPage)){
				$this->redirect("/{$prevPage['url']}");
			}else{
				$this->redirect("/top/index");
			}
		}

		// 登録・更新
		if (isset($this->params['form']['save_x'])) {
			if ($this->CustomerMasterService->saveByIndex()){
				$this->redirect('/customer_masters/index');
			}
		}

		// 一覧開始時のデータ読み込み
		if (empty($this->data)) {
			// 表示するデータを取得して使える形にする
			$this->data = $this->CustomerMasterService->getDataByIndex();
		}

	}

}
?>