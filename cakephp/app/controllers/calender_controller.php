<?php
//*******************************************************************************
// モジュール名 : カレンダーコントローラー
// Create       : 2011/02/09 fukui
// Production   : Okushin System Ltd.
//*******************************************************************************

class CalenderController extends AppController {
    var $name = 'Calender';
    var $uses = array('Calender');
    var $layout = 'calender';

	/**
	 * 初期処理
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 */
	function beforeFilter() {
	}

	/**
	 * カレンダーIDを持たして商談登録画面へ
	 * @author   fukui@okushin.co.jp
	 * @date     2015/05/26
	 * @note
	 * @param    int    $id    カレンダーID
	 */
	function jump_busy($id) {
		$calender = $this->Calender->read(null, $id);

		// カレンダーの予定をセッションに持たせておく
		$data['busi_date'] = date('Y/m/d', strtotime($calender['Calender']['start_time']));
		$data['memo'] = $calender['Calender']['subject'];
		if ($calender['Calender']['is_allday_event'] == 0){ // 1日予定かどうか
			$data['start_time'] = date('H:i', strtotime($calender['Calender']['start_time']));
			$data['end_time'] = date('H:i', strtotime($calender['Calender']['end_time']));
		}else{
			$data['start_time'] = '';
			$data['end_time'] = '';
		}
		$this->Session->write('calender', $data);

		$this->redirect('/customers/index_busi/search:clear');
    }

    //--------------------------------------------------------------------------------
    //一覧ページ
    //--------------------------------------------------------------------------------
    function index() {
		parent::beforeFilter();
    }

    //--------------------------------------------------------------------------------
    //カレンダー情報更新
    //--------------------------------------------------------------------------------
    function getCalender() {
    	$this->layout = 'ajax';
        Configure::write('debug', 0); // debugコードを非表示
    	$method = $_GET["method"];
		switch ($method) {
		    case "list":
		        $ret = $this->__listCalendar($_POST["showdate"], $_POST["viewtype"]);
		        break;
		}
		echo json_encode($ret);
		exit;
    }


	function __listCalendarByRange($sd, $ed){
		$ret = array();
		$ret['events'] = array();
		$ret["issort"] =true;
		$ret["start"] = php2JsTime($sd);
		$ret["end"] = php2JsTime($ed);
		$ret['error'] = null;

		try{
			$login = $this->Session->read("login");
			$user_id = $login['User']['id'];
			$sql = "select * from calenders where user_id = '{$user_id}' AND start_time between '".php2MySqlTime($sd)."' AND '". php2MySqlTime($ed)."'";
			$result = $this->User->query($sql);

			foreach($result as $value){
				foreach ($value as $key=>$val){
					$user = $this->User->read(null, $val['user_id']);
					$userName = "";
					if (!empty($user)){
						$userName = '('.$user['User']['name'].')';
					}

					$ret['events'][] = array(
							$val['id'],
							$val['subject']/*福井.$userName*/,
							php2JsTime(mySql2PhpTime($val['start_time'])),
							php2JsTime(mySql2PhpTime($val['end_time'])),
							$val['is_allday_event'],
							0,
							'',
							$val['color'],
							1,
							$val['parent_table'],
							$val['parent_id'],
					);
				}
			}
		}catch(Exception $e){
			$ret['error'] = $e->getMessage();
		}
		return $ret;
	}

	function __listCalendar($day, $type){
		$phpTime = js2PhpTime($day);
		switch($type){
			case "month":
				$st = mktime(0, 0, 0, date("m", $phpTime), 1, date("Y", $phpTime));
				$et = mktime(0, 0, -1, date("m", $phpTime)+1, 1, date("Y", $phpTime));
				break;
			case "week":
				$monday = date("d", $phpTime) - date('N', $phpTime) + 1;
				$st = mktime(0,0,0,date("m", $phpTime), $monday, date("Y", $phpTime));
				$et = mktime(0,0,-1,date("m", $phpTime), $monday+7, date("Y", $phpTime));
				break;
			case "day":
				$st = mktime(0, 0, 0, date("m", $phpTime), date("d", $phpTime), date("Y", $phpTime));
				$et = mktime(0, 0, -1, date("m", $phpTime), date("d", $phpTime)+1, date("Y", $phpTime));
				break;
		}
		return $this->__listCalendarByRange($st, $et);
	}
}
?>