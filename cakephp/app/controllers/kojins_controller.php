<?php
/**
 * 個人設定コントローラー
 * @author   nakata@okushin.co.jp
 * @date     2015/02/20
 * @note
 */
class KojinsController extends AppController {
	var $name = 'Kojins';
	var $uses = array('User', 'CustomerMaster', 'BusiMaster', 'CustomerKojin', 'BusiKojin');
	var $components = array('KojinService');


	/**
	 * 初期処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function beforeFilter() {
		parent::beforeFilter();

		// ここに追加検索初期値があれば定義
		//$this->paginate['Kojin']['condition']['xxx'] = 'xxx';
	}


	/**
	 * 個人設定編集ページ
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @param    int    $id    ユーザーテーブルID
	 */
	function edit() {
		$id = $this->login['User']['id'];
		// 戻る
		if (isset($this->params['form']['cancel_x'])) {
			$prevPage = $this->Session->read('prevPage');
			if (!empty($prevPage)){
				$this->redirect("/{$prevPage['url']}");
			}else{
				$this->redirect("/top/index");
			}
		}

		// 登録
		if (isset($this->params['form']['save_x'])) {
			if ($this->KojinService->save()){
				$this->redirect('/kojins/edit');
			}
		}

		// 編集開始時のデータ読み込み
		if (empty($this->data) && $id != "") {
			$this->data = $this->KojinService->getDataByEdit($id);
			if (!$this->data || !empty($this->data['User']['del_flag'])) {
				$this->redirect('/top/index');
			}
		}

		// 初期設定
		$this->__viewSet();
	}


	/**
	 * ユーザーテーブル編集ページ
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @param    int    $id    ユーザーテーブルID
	 */
	function __viewSet() {
		$params = array(
			'fields' => array("*"),
			'conditions' => array(),
			'order' => array('CustomerMaster.number ASC'),
			);
		$userMst = $this->CustomerMaster->find('all', $params);
		$this->set('userMst', $userMst);
	}
}
?>