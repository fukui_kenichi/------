<?php
/**
 * ログインコンポーネント
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note     ログインチェックや権限エリアの排他チェックを行う
 */
class LoginComponent extends Object {
	var $modeName = 'User';
	var $authArea = array();	// 権限エリア(未実装)
		
	/**
	 * 初期処理
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     controllerのbeforeFilterメソッドの前に呼び出される
	 * @param    obj    $controller    コントローラオブジェクト
	 */
	function initialize(&$controller) {
		$this->Controller =& $controller;
		$this->modelName  =  $controller->modelClass;
		
		// HTTPSリダイレクト
		if (HTTPS_MODE) {
			if (empty($_SERVER['HTTPS'])) {
				header("Location: https://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
			    exit;
			}
		}
	}
	
	
	/**
	 * 初期処理
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     controllerのbeforeFilterメソッドの後に呼び出される
	 * @param    obj    $controller    コントローラオブジェクト
	 */
	function startup(&$controller) {
		
	}
	
	
	/**
	 * ログインチェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     
	 * @return   bool    処理結果真偽値
	 */
	function checkLogin() {
		$C              =& $this->Controller;
		$modelName      =  $this->modelName;
		$controllerName =  $C->name;

		//ログインチェック
		if(!$C->Session->check('login')){
			return false;
		}
		//ログインユーザーの情報を取得
		$login = $C->Session->read("login");
		
		//ログインユーザー情報再更新
		$params = array(
			'conditions' => array(
				'User.id' => $login['User']['id'],
			)
		);
		$C->login = $C->User->find('first', $params);
		if (!$C->login) {
			return false;
		}
		
		$C->Session->write('login', $C->login);
		$C->set('login', $C->login);
		return true;
	}
	
	
	/**
	 * 権限チェック(未実装)
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     
	 * @return   bool    処理結果真偽値
	 */
	function checkAuth() {
		
	}

}

?>