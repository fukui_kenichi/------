<?php
/**
 * 基本サービスコンポーネント
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note     コントローラーとモデルの間でイベント処理を記述する基本クラス
 */
class BasicServiceComponent extends Object {
	
	var $modeName = null;
	
	/**
	 * 初期処理
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     controllerのbeforeFilterメソッドの前に呼び出される
	 * @param    obj    $controller    コントローラオブジェクト
	 */
	function initialize(&$controller) {
		$this->Controller =& $controller;
		$this->modelName  =  $controller->modelClass;
	}
	
	
	/**
	 * 初期処理
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     controllerのbeforeFilterメソッドの後に呼び出される
	 * @param    obj    $controller    コントローラオブジェクト
	 */
	function startup(&$controller) {
		
	}
	
	
	/**
	 * セッションに一旦保存した入力データがあるかチェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     確認画面等でフォームデータがあるかチェックする
	 * @return   bool    処理結果真偽値
	 */
	function checkPageData() {
		$C              =& $this->Controller;
		$controllerName =  $C->name;
		
		return $C->Session->check($controllerName. 'EditData');
	}
	
	
	/**
	 * セッションに一旦保存した入力データ取得
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     確認画面等でフォームデータをセッションから取得
	 * @return   mix    セッションに保存した値
	 */
	function readPageData() {
		$C              =& $this->Controller;
		$controllerName =  $C->name;
		
		return $C->Session->read($controllerName. 'EditData');
	}
	
	
	/**
	 * 入力データをセッションに保存
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     編集画面等でフォームデータをセッションに保存
	 */
	function writePageData() {
		$C              =& $this->Controller;
		$controllerName =  $C->name;
		
		$C->Session->write($controllerName. 'EditData', $C->data);
	}
	
	
	/**
	 * セッションに保存した入力データを破棄
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     編集画面等でフォームデータのセッションを破棄
	 */
	function delPageData() {
		$C              =& $this->Controller;
		$controllerName =  $C->name;
		
		$C->Session->delete($controllerName. 'EditData');
	}
	
	
	/**
	 * 入力チェック基本処理
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     入力チェックのみ基本メソッド
	 *           複数のモデルで入力チェックする場合オーバーライドする
	 * @return   bool    処理結果真偽値
	 */
	function validates() {
		$C              =& $this->Controller;
		$modelName      =  $this->modelName;

		$C->{$modelName}->create($C->data);
		// 入力チェック
		if (!$C->{$modelName}->validates()){
			return false;
		}
		return true;
	}
	
	
	/**
	 * 保存基本処理
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     入力チェックと保存の基本メソッド
	 *           複数のモデルで入力チェック、保存する場合オーバーライドする
	 * @return   bool    処理結果真偽値
	 */
	function save() {
		$C              =& $this->Controller;
		$modelName      =  $this->modelName;
		$controllerName =  $C->name;
		$mode           =  (empty($C->data[$modelName]['id'])) ? '追加' : '更新';
		
		$C->{$modelName}->create($C->data);
		// 入力チェック
		if (!$C->{$modelName}->validates()){
			return false;
		}
		// 保存
		$C->{$modelName}->checkUpdated = true;	// 更新時間チェックON
		
		if (!$C->{$modelName}->save(null, false)){
			$C->{$modelName}->invalidate('error', "データの{$mode}に失敗しました。");
			return false;
		}
		
		$C->Session->delete($controllerName. 'EditData');
		$C->Session->write('system-message', "データを{$mode}しました。");
		return true;
	}
	
	
	/**
	 * 削除基本処理
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     削除チェックと削除の基本メソッド
	 *           複数のモデルで入力チェック、削除する場合オーバーライドする
	 * $param    int    $id     削除したいデータの主キー
	 * @return   bool    処理結果真偽値
	 */
	function delete($id) {
		$C         =& $this->Controller;
		$modelName =  $this->modelName;
		
		// 入力チェック
		if (!$C->{$modelName}->deleteValidates($id)){
			return false;
		}
		// 削除
		if (!$C->{$modelName}->delete($id)){
			$C->{$modelName}->invalidate('error', "データの削除に失敗しました。");
			return false;
		}
		
		$C->Session->write('system-message', "データを削除しました。");
		return true;
	}
	
	
	/**
	 * 削除基本処理
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     削除の基本メソッド
	 *           複数のモデルで削除する場合オーバーライドする
	 * $param    int    $id     削除したいデータの主キー
	 * @return   bool    処理結果真偽値
	 */
	function del($id) {
		return $this->delete($id);
	}
	
	
	/**
	 * 削除フラグ更新基本処理
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     削除チェックと削除フラグ更新の基本メソッド
	 *           複数のモデルで入力チェック、削除フラグ立てたい場合オーバーライドする
	 * $param    int    $id     削除フラグ立てたいデータの主キー
	 * @return   bool    処理結果真偽値
	 */
	function delFlag($id) {
		$C         =& $this->Controller;
		$modelName =  $this->modelName;
		
		// 入力チェック
		if (!$C->{$modelName}->deleteValidates($id)){
			return false;
		}
		// 削除
		if (!$C->{$modelName}->delFlag($id)){
			$C->{$modelName}->invalidate('error', "データの削除に失敗しました。");
			return false;
		}
		
		$C->Session->write('system-message', "データを削除しました。");
		return true;
	}

}

?>