<?php
/**
 * 顧客テーブルサービスコンポーネント
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note     コントローラーとモデルの間でイベント処理を記述するクラス
 */
class CustomerServiceComponent extends BasicServiceComponent {

	/**
	 * 検索条件作成
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note     ページネート用検索条件作成
	 * @param    arr    $data               入力された検索条件
	 * @param    arr    $defaultPaginate    デフォルト検索条件
	 */
	function createParams($data, $defaultPaginate) {
		$C    =& $this->Controller;
		$cond =  array(); // 検索条件
		$customerCheckArr = array(); // チェック用顧客配列

		// 検索条件が空欄なら処理しない
		$conditionText = mb_trim($data['text']);
		// 20150424の会議で全件出すように変更
//		if (empty($conditionText)) {
//			$C->Session->write('search-message', '検索キーワードを入力してください。');
//			return false;
//		}

		// 検索条件を分解
//		$conditionText = str_replace('　', ' ', $conditionText); // 全角空白の半角化はしない（テーブル項目名で全角空白が使用される可能性があるため
		$conditionArr = preg_split("/[ \t\n\f\r]+/", $conditionText);
		$conditionCount = count($conditionArr); // 条件の数を取得
		// ユーザー検索テーブル検索
		foreach ((array)$conditionArr as $key1 => $val1) {
			// 検索
			$result = $C->UserRefer->searchCustomerId($val1);

			// 結果を格納
			foreach ((array)$result as $key2 => $val2) {
				$customerCheckArr[$val2['Busy']['customer_id']][$key1] = 1; // 該当検索条件にフラグを立てる
			}

		}

		// 商談検索テーブル検索
		foreach ((array)$conditionArr as $key1 => $val1) {
			// 検索
			$result = $C->BusiRefer->searchCustomerId($val1);

			// 結果を格納
			foreach ((array)$result as $key2 => $val2) {
				$customerCheckArr[$val2['BusiRefer']['customer_id']][$key1] = 1; // 該当検索条件にフラグを立てる
			}

		}

		// 顧客検索テーブル検索
		foreach ((array)$conditionArr as $key1 => $val1) {
			// 検索
			$result = $C->CustomerRefer->searchCustomerId($val1);

			// 結果を格納
			foreach ((array)$result as $key2 => $val2) {
				$customerCheckArr[$val2['CustomerRefer']['customer_id']][$key1] = 1; // 該当検索条件にフラグを立てる
			}

		}

		// 該当顧客IDの抽出
		$customerIdArr = array();
		$customerCount = 0;
		foreach ((array)$customerCheckArr as $key => $val) {
			// 検索条件にひとつでも抜けがある場合は除外
			for ($i = 0; $i < $conditionCount; $i++ ) {
				if (empty($val[$i])) continue 2;
			}

			// 顧客IDの抽出
			$customerIdArr[] = $key;
			$customerCount++;
//			if ($customerCount > 1000) { // 検索結果が1000件を超えたらエラー
//				$C->Session->write('search-message', '検索結果が1000件を超えました。検索条件を見直してください。');
//				return false;
//			}
		}

		// デフォルトの検索条件取得
		if (isset($defaultPaginate['Customer']['conditions'])) {
			$cond = $defaultPaginate['Customer']['conditions'];
		}

		// 検索条件に該当する顧客のIDを設定
		if (!empty($customerIdArr)) {
			$cond['Customer.id'] = $customerIdArr;
		}

		// 表示メッセージ設定
		if ($customerCount == 0) {
			$C->Session->write('search-message', '一致する検索結果はありませんでした。');
			$cond['Customer.id'] = null;
		} else {
			$C->Session->write('search-message', $customerCount. '件の検索結果が見つかりました。');
		}

		// 検索条件をページネートに設定
		$defaultPaginate['Customer']['conditions'] = $cond;

		// 検索条件を一旦セッションに保存
		$C->HoldPaginate->saveParams($defaultPaginate);
	}


	/**
	 * 削除フラグ更新基本処理の上書き
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/02
	 * @note     削除チェックと削除フラグ更新の基本メソッド
	 *           複数のモデルで入力チェック、削除フラグ立てたい場合オーバーライドする
	 * $param    int    $id     削除フラグ立てたいデータの主キー
	 * @return   bool    処理結果真偽値
	 */
	function delFlag($id) {
		$C         =& $this->Controller;

		// 入力チェック
		if (!$C->Customer->deleteValidates($id)){
			return false;
		}

		$C->Customer->begin(); // トランザクション開始

		// Customerのデータの削除フラグを立てる
		if (!$C->Customer->delFlag($id)){
			$C->Customer->invalidate('error', "Customerデータの削除処理に失敗しました。");
			return false;
		}

		// CustomerReferのデータを物理削除
		// SQL文を作成して実行
		$sql ="DELETE FROM customer_refers WHERE customer_id = {$id};";
		if (!$C->CustomerRefer->query($sql)){
			$C->CustomerRefer->invalidate('error', 'CustomerReferデータの物理削除に失敗しました。');
			$C->CustomerRefer->rollback();
			return false;
		}

		// コミットしてメッセージ設定
		$C->Customer->commit();
		$C->Session->write('system-message', "データを削除しました。");

		return true;
	}


	/**
	 * 保存処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @return   bool    処理結果真偽値
	 */
	function save() {
		$C       =& $this->Controller;
		$mode    =  (empty($C->data['Customer']['id'])) ? '追加' : '更新';


		$ret = $C->CustomerMaster->getCustomerTableInfo();

		// Customer情報だけにする。
		$image = array();
		$notimage = array();
		if (!empty($C->data['Image'])) {
			$image = $C->data['Image'];
		}
		if (!empty($C->data['Notimage'])) {
			$notimage = $C->data['Notimage'];
		}
		unset($C->data['Image']);
		unset($C->data['Notimage']);
		foreach ($ret as $key => $val) {
			$val =& $ret[$key]['CustomerMaster'];
			if ($val['type'] == MASTER_TYPE_DTTM) { // 日付時刻型の場合日付と時刻を連結する
				$C->data['Customer']["{$val['field']}"] = $C->data['Customer']["{$val['field']}_d"] . " " . $C->data['Customer']["{$val['field']}_t"];
			}
			if ($val['type'] == MASTER_TYPE_CHEK) { // チェック型の場合チェックされた内容を保持する
				// 改行を\nに統一
				$text = preg_replace(array('/\r\n/','/\r/','/\n/'), "\n", $val['select_ti'] );
				$listArr = explode("\n",$text);

				$temp = "";
				foreach ((array)$C->data['Customer']["{$val['field']}"] as $k=>$v){
					if ($v == 1){
						$temp .= $listArr[$k] . "\r\n";
					}
				}
				$C->data['Customer']["{$val['field']}"] = mb_rtrim($temp, "\r\n");
			}
		}

		// 住所が空欄なら緯度経度を空欄に
		if (empty($C->data['Customer']['address'])) {
			$C->data['Customer']['latitude'] = null;
			$C->data['Customer']['longitude'] = null;
		}

		// 入力チェック
		$C->Customer->create($C->data);
		if (!$C->Customer->validates()){
			$C->data['Image'] = $image;
			$C->data['Notimage'] = $notimage;
			return false;
		}

		$C->Customer->begin();

		$C->Customer->checkUpdated = true;	// 更新時間チェックON

		// 保存
		if (!$C->Customer->save(null, false)){
			$C->Customer->invalidate('error', "データの{$mode}に失敗しました。");
			$C->Customer->rollback();
			return false;
		}
		$id = $C->Customer->getID();
		// 写真
		$C->Customer->seiriFile($id);
		// イメージファイル、イメージ以外のファイルの保存
		if(!$C->CustomerService->saveUploadFile($id, $image, $notimage)){
			$C->Customer->invalidate('error', "uploadsの保存に失敗しました。");
			$C->Customer->rollback();
			return false;
		}

		// CustomerReferの更新
		if (!$C->CustomerRefer->updateByCustomerId($id)){
			$C->CustomerRefer->rollback();
			return false;
		}

		$C->Session->write('system-message', "データを{$mode}しました。");
		$C->Customer->commit();
		$C->data['Image'] = $image;
		$C->data['Notimage'] = $notimage;

		return true;
	}


	/**
	 * 初期データ取得
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note     編集画面の初期値設定で使用
	 * @return   arr    初期データ配列
	 */
	function getIni() {
		$data = array();

		return $data;
	}


	/**
	 * CustomerMaster情報からCustomer情報を作成する。
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/24
	 * @note
	 * @return   arr    初期データ配列
	 */
	function setCustomerByCustomerMaster(&$data) {
		$C       =& $this->Controller;

		$ret = $C->CustomerMaster->getCustomerTableInfo();
		foreach ($ret as $key => $val) {
			$val =& $ret[$key]['CustomerMaster'];
			if ($val['type'] == MASTER_TYPE_DTTM) {	// 日付時間型
				if (!empty($data['Customer']["{$val['field']}"])) {
					$dtArr = explode(' ',$data['Customer']["{$val['field']}"]);
					$data['Customer']["{$val['field']}_d"] = dateFormat($dtArr[0],'Y/m/d');
					$data['Customer']["{$val['field']}_t"] = dateFormat($dtArr[1],'H:i');
				}
			}
			if ($val['type'] == MASTER_TYPE_DATE) {	// 日付型
				if (!empty($data['Customer']["{$val['field']}"]) && $data['Customer']["{$val['field']}"] != '0000-00-00') {
					$data['Customer']["{$val['field']}"] = dateFormat($data['Customer']["{$val['field']}"],'Y/m/d');
				}
			}
			if ($val['type'] == MASTER_TYPE_TIME) {	// 時間型
				if (!empty($data['Customer']["{$val['field']}"]) && $data['Customer']["{$val['field']}"] != '0000-00-00') {
					$data['Customer']["{$val['field']}"] = dateFormat($data['Customer']["{$val['field']}"],'H:i');
				}
			}
			if ($val['type'] == MASTER_TYPE_INTG) {	// 数値型
				if (!empty($data['Customer']["{$val['field']}"])) {
					$data['Customer']["{$val['field']}"] = rtrim($data['Customer']["{$val['field']}"],'0');
					$data['Customer']["{$val['field']}"] = rtrim($data['Customer']["{$val['field']}"],'.');
				}
			}
		}

	}


	/**
	 * ファイルアップロード処理
	 * @author   nakata@okushin.co.jpa
	 * @date     2015/02/26
	 * @note
	 * @param    str    テーブル名
	 * @param    int    ID
	 * @param    arr    イメージファイル群
	 * @param    arr    イメージ以外ファイル群
	 * @return   arr    初期データ配列
	 */
	function saveUploadFile($parentId, $image,  $notimage) {
		$C       =& $this->Controller;
		$imgArr = array();
		$notimgArr = array();

		// フィールド変換テーブル
		$params = array(
			'fields' => array("*"),
			'conditions' => array(),
			'order' => array('CustomerMaster.number asc'),
			);
		$userMst = $C->CustomerMaster->find('all', $params);
		foreach ($userMst as $key => $val) {
			$val = $val['CustomerMaster'];
			if ($val['type'] == 9) {
				$imgArr[$val['number']] = $val['field'];
			}
		}
		// イメージファイルの最終登録
		if (!$C->UploadService->saveUpload('customers',$parentId,$image, $imgArr)) {
			return false;
		}
		foreach ($userMst as $key => $val) {
			$val = $val['CustomerMaster'];
			if ($val['type'] == 10) {
				$notimgArr[$val['number']] = $val['field'];
			}
		}
		// イメージ以外のファイルの最終登録
		if (!$C->UploadService->saveUpload('customers',$parentId,$notimage, $notimgArr)) {
			return false;
		}
		return true;
	}


	/**
	 * ファイルアップロード情報 Viewへの定義
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/24
	 * @note
	 * @param    int    親ID
	 * @param    arr    $this->data
	 * @return   arr    初期データ配列
	 */
	function setUploadFile($parentId, &$data) {
		$C       =& $this->Controller;
		$imgArr = array();
		$notimgArr = array();

		$params = array(
			'fields' => array("*"),
			'conditions' => array(),
			'order' => array('CustomerMaster.number asc'),
			);
		$customerMst = $C->CustomerMaster->find('all', $params);
		foreach ($customerMst as $key => $val) {
			$val = $val['CustomerMaster'];
			if ($val['type'] == 9) {
				$imgArr[$val['field']] = $val['number'];
			}
		}
		// イメージファイルアップロード情報 Viewへの定義
		$C->Upload->setImageFile('customers',$parentId,$imgArr,$data);
		// ファイル取得のフィールドテーブル作成（現状仮作成）フィールド項目追加機能が実装されれば修正が必要
		foreach ($customerMst as $key => $val) {
			$val = $val['CustomerMaster'];
			if ($val['type'] == 10) {
				$notimgArr[$val['field']] = $val['number'];
			}
		}
		// イメージ以外のファイルアップロード情報 Viewへの定義
		$C->Upload->setNotimageFile('customers',$parentId,$notimgArr,$data);
	}

}
?>