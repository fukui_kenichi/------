<?php
/**
 * ユーザーテーブルサービスコンポーネント
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note     コントローラーとモデルの間でイベント処理を記述するクラス
 */
class UserServiceComponent extends BasicServiceComponent {

	/**
	 * 検索条件作成
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note     ページネート用検索条件作成
	 * @param    arr    $data               入力された検索条件
	 * @param    arr    $defaultPaginate    デフォルト検索条件
	 */
	function createParams($data, $defaultPaginate) {
		$C    =& $this->Controller;
		$cond =  array();

		// デフォルトの検索条件取得
		if (isset($defaultPaginate['User']['conditions'])) {
			$cond = $defaultPaginate['User']['conditions'];
		}

		// 検索条件追加-------------------------------------------


		$defaultPaginate['User']['conditions'] = $cond;

		// 検索条件を一旦セッションに保存
		$C->HoldPaginate->saveParams($defaultPaginate);
	}


	/**
	 * 削除フラグ更新基本処理の上書き
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/02
	 * @note     削除チェックと削除フラグ更新の基本メソッド
	 *           複数のモデルで入力チェック、削除フラグ立てたい場合オーバーライドする
	 * $param    int    $id     削除フラグ立てたいデータの主キー
	 * @return   bool    処理結果真偽値
	 */
	function delFlag($id) {
		$C         =& $this->Controller;

		// 入力チェック
		if (!$C->User->deleteValidates($id)){
			return false;
		}

		$C->User->begin(); // トランザクション開始

		// Userのデータの削除フラグを立てる
		if (!$C->User->delFlag($id)){
			$C->User->invalidate('error', "Userデータの削除処理に失敗しました。");
			return false;
		}

		// UserReferのデータを物理削除
		// SQL文を作成して実行
		$sql ="DELETE FROM user_refers WHERE user_id = {$id};";
		if (!$C->UserRefer->query($sql)){
			$C->UserRefer->invalidate('error', 'UserReferデータの物理削除に失敗しました。');
			$C->UserRefer->rollback();
			return false;
		}

		// コミットしてメッセージ設定
		$C->User->commit();
		$C->Session->write('system-message', "データを削除しました。");

		return true;
	}


	/**
	 * 保存処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @return   bool    処理結果真偽値
	 */
	function save() {
		$C       =& $this->Controller;
		$mode    =  (empty($C->data['User']['id'])) ? '追加' : '更新';
		$errFlag = false;

		$ret = $C->UserMaster->getUserTableInfo();

		// User情報だけにする
		$image = array();
		$notimage = array();
		if (!empty($C->data['Image'])) {
			$image = $C->data['Image'];
		}
		if (!empty($C->data['Notimage'])) {
			$notimage = $C->data['Notimage'];
		}
		unset($C->data['Image']);
		unset($C->data['Notimage']);
		$C->data['User']['password'] = $C->data['User']['password_01'];
		foreach ($ret as $key => $val) {
			$val =& $ret[$key]['UserMaster'];
			if ($val['type'] == MASTER_TYPE_DTTM) { // 日付時刻型の場合日付と時刻を連結する
				$C->data['User']["{$val['field']}"] = $C->data['User']["{$val['field']}_d"] . " " . $C->data['User']["{$val['field']}_t"];
			}
			if ($val['type'] == MASTER_TYPE_CHEK) { // チェック型の場合チェックされた内容を保持する
				// 改行を\nに統一
				$text = preg_replace(array('/\r\n/','/\r/','/\n/'), "\n", $val['select_ti'] );
				$listArr = explode("\n",$text);

				$temp = "";
				foreach ((array)$C->data['User']["{$val['field']}"] as $k=>$v){
					if ($v == 1){
						$temp .= $listArr[$k] . "\r\n";
					}
				}
				$C->data['User']["{$val['field']}"] = mb_rtrim($temp, "\r\n");
			}
		}

		$C->User->create($C->data);

		// 入力チェック
		if (!$C->User->validates()){
			$errFlag = true;
		}

		if ($errFlag) {
			$C->data['Image'] = $image;
			$C->data['Notimage'] = $notimage;
			return false;
		}

		$C->User->begin();

		$C->User->checkUpdated = true;	// 更新時間チェックON

		// 保存
		if (!$C->User->save(null, false)){
			$C->User->invalidate('error', "データの{$mode}に失敗しました。");
			$C->User->rollback();
			return false;
		}
		$id = $C->User->getID();
		// 写真
		$C->User->seiriFile($id);
		// イメージファイル、イメージ以外のファイルの保存
		if(!$C->UploadService->saveUploadFile('users', $id, $image, $notimage)){
			$C->User->invalidate('error', "uploadsの保存に失敗しました。");
			$C->User->rollback();
			return false;
		}

		// UserReferの更新
		if (!$C->UserRefer->updateByUserId($id)){
			$C->UserRefer->rollback();
			return false;
		}

		// ユーザー新規登録時に個人設定(顧客)を登録する
		if (empty($C->data['User']['id'])){
			if (!$C->CustomerKojin->insertKojin($id)){
				$C->CustomerKojin->rollback();
				return false;
			}
		}

		// ユーザー新規登録時に個人設定(商談)を登録する
		if (empty($C->data['User']['id'])){
			if (!$C->BusiKojin->insertKojin($id)){
				$C->BusiKojin->rollback();
				return false;
			}
		}

		// ユーザー新規登録時にデフォルトのお気に入りを登録する
		if (empty($C->data['User']['id'])){
			$favorite['Favorite']['user_id'] = $id;
			$favorite['Favorite']['name'] = $C->data['User']['name'] . 'の商談';
			$favorite['Favorite']['keyword'] = $C->data['User']['name'];
			if (!$C->Favorite->save($favorite, false)){
				$C->Favorite->rollback();
				return false;
			}
		}

		$C->Session->write('system-message', "データを{$mode}しました。");
		$C->User->commit();
		$C->data['Image'] = $image;
		$C->data['Notimage'] = $notimage;
		return true;
	}


	/**
	 * 初期データ取得
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note     編集画面の初期値設定で使用
	 * @return   arr    初期データ配列
	 */
	function getIni() {
		$data = array();

		return $data;
	}



	/**
	 * UserMaster情報からUser情報を作成する。
	 * @author   nishishita@okushin.co.jp
	 * @date     2015/02/18
	 * @note
	 * @return   arr    初期データ配列
	 */
	function setUserByUserMaster(&$data) {
		$C       =& $this->Controller;

		$ret = $C->UserMaster->getUserTableInfo();
		foreach ($ret as $key => $val) {
			$val =& $ret[$key]['UserMaster'];
			if ($val['type'] == MASTER_TYPE_DTTM) {	// 日付時間型
				if (!empty($data['User']["{$val['field']}"])) {
					$dtArr = explode(' ',$data['User']["{$val['field']}"]);
					$data['User']["{$val['field']}_d"] = dateFormat($dtArr[0],'Y/m/d');
					$data['User']["{$val['field']}_t"] = dateFormat($dtArr[1],'H:i');
				}
			}
			if ($val['type'] == MASTER_TYPE_DATE) {	// 日付型
				if (!empty($data['User']["{$val['field']}"]) && $data['User']["{$val['field']}"] != '0000-00-00') {
					$data['User']["{$val['field']}"] = dateFormat($data['User']["{$val['field']}"],'Y/m/d');
				}
			}
			if ($val['type'] == MASTER_TYPE_TIME) {	// 時間型
				if (!empty($data['User']["{$val['field']}"]) && $data['User']["{$val['field']}"] != '0000-00-00') {
					$data['User']["{$val['field']}"] = dateFormat($data['User']["{$val['field']}"],'H:i');
				}
			}
			if ($val['type'] == MASTER_TYPE_INTG) {	// 数値型
				if (!empty($data['User']["{$val['field']}"])) {
					$data['User']["{$val['field']}"] = rtrim($data['User']["{$val['field']}"],'0');
					$data['User']["{$val['field']}"] = rtrim($data['User']["{$val['field']}"],'.');
				}
			}
		}
	}


}

?>