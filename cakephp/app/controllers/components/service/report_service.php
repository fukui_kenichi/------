<?php
/**
 * レポートサービスコンポーネント
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note     コントローラーとモデルの間でイベント処理を記述するクラス
 */
class ReportServiceComponent extends BasicServiceComponent {

	/**
	 * 新規顧客VS保有顧客
	 * @author   fukui@okushin.co.jp
	 * @date     2015/04/30
	 */
	function setCustomerWariai() {
		$C       =& $this->Controller;

		// 今月の1日を取得
		$swithDate = getStartMonth(date('Y/m/d'));

		// 先月以前の顧客数取得
		$params = array(
			'conditions'=>array(
				'Customer.del_flag'=>0,
				'Customer.created <'=>$swithDate
			),
		);
		$oldCount = $C->Customer->find('count', $params);

		// 今月の顧客数取得
		$params = array(
			'conditions'=>array(
				'Customer.del_flag'=>0,
				'Customer.created >='=>$swithDate
			),
		);
		$newCount = $C->Customer->find('count', $params);

		// 合計の顧客数計算
		$totalCount = $oldCount + $newCount;
		if ($totalCount == 0){
			$totalCount = 1;
		}

		// 顧客割合計算
		$newWariai = round($newCount/$totalCount*100, 2);
		$oldWariai = round($oldCount/$totalCount*100, 2);

		$par_cus = array("新規顧客 {$newCount}件"=>$newWariai, "保有顧客 {$oldCount}件"=>$oldWariai);
		$C->set('par_cus', $par_cus);

	}


	/**
	 * 過去3か月分のメール配信数
	 * @author   fukui@okushin.co.jp
	 * @date     2015/04/30
	 */
	function setMailWariai() {
		$C       =& $this->Controller;

		// 今月の1日を取得
		$date = getStartMonth(date('Y/m/d'));

		// 過去3月分取得
		$one = date('Y-m',strtotime($date));
		$two = date("Y-m",strtotime("{$date} -1 month"));
		$three = date("Y-m",strtotime("{$date} -2 month"));
		$one_m = date('n',strtotime($date));
		$two_m = date("n",strtotime("{$date} -1 month"));
		$three_m = date("n",strtotime("{$date} -2 month"));

		// 今月のメール配信数取得
		$params = array(
			'conditions'=>array(
				'MailSender.send_flag'=>1,
				'Mail.status'=>2,
				'Mail.send_datetime LIKE'=>'%'.$one.'%',
			),
			'joins' => array(
				array(
					'type' => 'left',
					'table' => 'mail_senders',
					'alias' => 'MailSender',
					'conditions' => "Mail.id = MailSender.mail_id"
				),
			)
		);
		$oneCount = $C->Mail->find('count', $params);

		// 先月のメール配信数取得
		$params = array(
			'conditions'=>array(
				'MailSender.send_flag'=>1,
				'Mail.status'=>2,
				'Mail.send_datetime LIKE'=>'%'.$two.'%',
			),
			'joins' => array(
				array(
					'type' => 'left',
					'table' => 'mail_senders',
					'alias' => 'MailSender',
					'conditions' => "Mail.id = MailSender.mail_id"
				),
			)
		);
		$twoCount = $C->Mail->find('count', $params);

		// 先々月のメール配信数取得
		$params = array(
			'conditions'=>array(
				'MailSender.send_flag'=>1,
				'Mail.status'=>2,
				'Mail.send_datetime LIKE'=>'%'.$three.'%',
			),
			'joins' => array(
				array(
					'type' => 'left',
					'table' => 'mail_senders',
					'alias' => 'MailSender',
					'conditions' => "Mail.id = MailSender.mail_id"
				),
			)
		);
		$threeCount = $C->Mail->find('count', $params);

		// 合計のメール配信数計算
		$totalCount = $oneCount + $twoCount + $threeCount;
		if ($totalCount == 0){
			$totalCount = 1;
		}

		// 顧客割合計算
		$oneWariai = round($oneCount/$totalCount*100, 2);
		$twoWariai = round($twoCount/$totalCount*100, 2);
		$threeWariai = round($threeCount/$totalCount*100, 2);

		$mail = array(
			"{$three_m}月 {$threeCount}件"=>$threeWariai,
			"{$two_m}月 {$twoCount}件"=>$twoWariai,
			"{$one_m}月 {$oneCount}件"=>$oneWariai);
		$C->set('mail', $mail);

		$title = $three_m . ',' . $two_m . ',' . $one_m;
		$C->set('title', $title);

	}


	/**
	 * 過去3か月分のCTI受信数
	 * @author   fukui@okushin.co.jp
	 * @date     2015/04/30
	 */
	function setCtiCount() {
		$C       =& $this->Controller;

		// 今月の1日を取得
		$date = getStartMonth(date('Y/m/d'));

		// 過去3月分取得
		$one = date('Y-m',strtotime($date));
		$two = date("Y-m",strtotime("{$date} -1 month"));
		$three = date("Y-m",strtotime("{$date} -2 month"));
		$one_m = date('n',strtotime($date));
		$two_m = date("n",strtotime("{$date} -1 month"));
		$three_m = date("n",strtotime("{$date} -2 month"));

		// 今月のCTI受信数取得
		$params = array(
			'conditions'=>array(
				'CtiCount.created LIKE'=>'%'.$one.'%',
			),
		);
		$oneCount = $C->CtiCount->find('count', $params);

		// 先月のCTI受信数取得
		$params = array(
			'conditions'=>array(
				'CtiCount.created LIKE'=>'%'.$two.'%',
			),
		);
		$twoCount = $C->CtiCount->find('count', $params);

		// 先々月のCTI受信数取得
		$params = array(
			'conditions'=>array(
				'CtiCount.created LIKE'=>'%'.$three.'%',
			),
		);
		$threeCount = $C->CtiCount->find('count', $params);

		$cti = array($three_m=>$threeCount, $two_m=>$twoCount, $one_m=>$oneCount);
		$C->set('cti', $cti);

	}


	/**
	 * 過去3か月分の獲得顧客数
	 * @author   fukui@okushin.co.jp
	 * @date     2015/04/30
	 */
	function setCustomerCount() {
		$C       =& $this->Controller;

		// 今月の1日を取得
		$date = getStartMonth(date('Y/m/d'));

		// 過去3月分取得
		$one = date('Y-m',strtotime($date));
		$two = date("Y-m",strtotime("{$date} -1 month"));
		$three = date("Y-m",strtotime("{$date} -2 month"));
		$one_m = date('n',strtotime($date));
		$two_m = date("n",strtotime("{$date} -1 month"));
		$three_m = date("n",strtotime("{$date} -2 month"));

		// 今月のCTI受信数取得
		$params = array(
			'conditions'=>array(
				'Customer.created LIKE'=>'%'.$one.'%',
			),
		);
		$oneCount = $C->Customer->find('count', $params);

		// 先月のCTI受信数取得
		$params = array(
			'conditions'=>array(
				'Customer.created LIKE'=>'%'.$two.'%',
			),
		);
		$twoCount = $C->Customer->find('count', $params);

		// 先々月のCTI受信数取得
		$params = array(
			'conditions'=>array(
				'Customer.created LIKE'=>'%'.$three.'%',
			),
		);
		$threeCount = $C->Customer->find('count', $params);

		$customer = array($three_m=>$threeCount, $two_m=>$twoCount, $one_m=>$oneCount);
		$C->set('customer', $customer);

	}
}

?>