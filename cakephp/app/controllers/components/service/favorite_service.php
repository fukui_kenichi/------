<?php
/**
 * お気に入りテーブルサービスコンポーネント
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note     コントローラーとモデルの間でイベント処理を記述するクラス
 */
class FavoriteServiceComponent extends BasicServiceComponent {

	/**
	 * 検索条件作成
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note     ページネート用検索条件作成
	 * @param    arr    $data               入力された検索条件
	 * @param    arr    $defaultPaginate    デフォルト検索条件
	 */
	function createParams($data, $defaultPaginate) {
		$C    =& $this->Controller;
		$cond =  array();

		// デフォルトの検索条件取得
		if (isset($defaultPaginate['Favorite']['conditions'])) {
			$cond = $defaultPaginate['Favorite']['conditions'];
		}

		// 検索条件追加-------------------------------------------


		$defaultPaginate['Favorite']['conditions'] = $cond;

		// 検索条件を一旦セッションに保存
		$C->HoldPaginate->saveParams($defaultPaginate);
	}


	/**
	 * 削除処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * $param    int    $id     削除したいデータの主キー
	 * @return   bool    処理結果真偽値
	 */
	function delete($id) {
		$C =& $this->Controller;

		// 入力チェック
		if (!$C->Favorite->deleteValidates($id)){
			return false;
		}
		// 削除
		if (!$C->Favorite->delete($id)){
			$C->Favorite->invalidate('error', 'お気に入りのデータの削除に失敗しました。');
			return false;
		}

		$C->Session->write('system-message', 'お気に入りのデータを削除しました。');
		return true;
	}


	/**
	 * 保存処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @return   bool    処理結果真偽値
	 */
	function save() {
		$C       =& $this->Controller;
		$mode    =  (empty($C->data['Favorite']['id'])) ? '追加' : '更新';
		$errFlag = false;

		// ログインユーザIDをuser_idに設定
		$C->data['Favorite']['user_id'] = $C->login['User']['id'];

		$C->Favorite->create($C->data);

		// 入力チェック
		if (!$C->Favorite->validates($C->login)){
			return false;
		}

		$C->Favorite->begin();

		$C->Favorite->checkUpdated = true;	// 更新時間チェックON

		// 保存
		if (!$C->Favorite->save(null, false)){
			$C->Favorite->invalidate('error', "お気に入りデータの{$mode}に失敗しました。");
			$C->Favorite->rollback();
			return false;
		}

		$C->Session->write('system-message', "お気に入りデータを{$mode}しました。");
		$C->Favorite->commit();
		return true;
	}


	/**
	 * 初期データ取得
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note     編集画面の初期値設定で使用
	 * @return   arr    初期データ配列
	 */
	function getIni() {
		$C       =& $this->Controller;
		$returnData['Favorite']['keyword'] = null; // 返却用配列初期化

		// 検索条件文字列を取得
		if ($C->Session->check('searchForm')) {
			$searchForm = $C->Session->read('searchForm');
			if (isset($searchForm['Customers']['index']['text'])) {
				$returnData['Favorite']['keyword'] = $searchForm['Customers']['index']['text'];
			}
		}

		return $returnData;
	}



}

?>