<?php
/**
 * 商談テーブルサービスコンポーネント
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note     コントローラーとモデルの間でイベント処理を記述するクラス
 */
class BusyServiceComponent extends BasicServiceComponent {

	/**
	 * 検索条件作成
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note     ページネート用検索条件作成
	 * @param    arr    $data               入力された検索条件
	 * @param    arr    $defaultPaginate    デフォルト検索条件
	 */
	function createParams($data, $defaultPaginate) {
		$C    =& $this->Controller;
		$cond =  array();

		// デフォルトの検索条件取得
		if (isset($defaultPaginate['Busy']['conditions'])) {
			$cond = $defaultPaginate['Busy']['conditions'];
		}

		// 検索条件追加-------------------------------------------


		$defaultPaginate['Busy']['conditions'] = $cond;

		// 検索条件を一旦セッションに保存
		$C->HoldPaginate->saveParams($defaultPaginate);
	}


	/**
	 * 削除処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * $param    int    $id     削除したいデータの主キー
	 * @return   bool    処理結果真偽値
	 */
	function delete($id) {
		$C =& $this->Controller;

		// 入力チェック
		if (!$C->Busy->deleteValidates($id)){
			return false;
		}

		$C->Busy->begin(); // トランザクション開始

		// Busyのデータを物理削除
		if (!$C->Busy->delete($id)){
			$C->Busy->invalidate('error', 'Busyデータの物理削除に失敗しました。');
			return false;
		}

		// BusiReferのデータを物理削除
		// SQL文を作成して実行
		$sql ="DELETE FROM busi_refers WHERE busi_id = {$id};";
		if (!$C->BusiRefer->query($sql)){
			$C->BusiRefer->invalidate('error', 'BusiReferデータの物理削除に失敗しました。');
			$C->BusiRefer->rollback();
			return false;
		}

		// Uploadのデータを削除
		// SQL文を作成して実行
		$sql ="UPDATE uploads SET
				parent_table = NULL, parent_id = NULL, parent_field = NULL
				WHERE parent_table = 'busies' AND parent_id = {$id};";
		if (!$C->Busy->query($sql)){
			$C->Busy->invalidate('error', 'uoloadデータの削除に失敗しました。');
			$C->Busy->rollback();
			return false;
		}

		// Calenderのデータを物理削除
		// SQL文を作成して実行
		$sql ="DELETE FROM calenders WHERE parent_id = {$id} AND parent_table = 'busies';";
		if (!$C->Calender->query($sql)){
			$C->Calender->invalidate('error', 'Calenderデータの物理削除に失敗しました。');
			$C->Calender->rollback();
			return false;
		}

		// コミットしてメッセージ設定
		$C->Busy->commit();
		$C->Session->write('system-message', 'データを削除しました。');

		return true;
	}


	/**
	 * 初期データ取得
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/02
	 * @note     編集画面の初期値設定で使用
	 * @param    int    顧客ID
	 * @return   arr    初期データ配列
	 */
	function getIni($customerId) {
		$C       =& $this->Controller;
		$returnData = array();

		$returnData['Busy']['customer_id'] = $customerId; // 顧客IDを設定
		$returnData['Busy']['user_id'] = $C->login['User']['id']; // 担当者IDを設定

		return $returnData;
	}


	/**
	 * 保存処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @return   bool    処理結果真偽値
	 */
	function save() {
		$C       =& $this->Controller;
		$mode    =  (empty($C->data['Busy']['id'])) ? '追加' : '更新';

		$ret = $C->BusiMaster->getBusyTableInfo();

		// Busy情報だけにする。
		$image = array();
		$notimage = array();
		if (!empty($C->data['Image'])) {
			$image = $C->data['Image'];
		}
		if (!empty($C->data['Notimage'])) {
			$notimage = $C->data['Notimage'];
		}
		unset($C->data['Image']);
		unset($C->data['Notimage']);
		foreach ($ret as $key => $val) {
			$val =& $ret[$key]['BusiMaster'];
			if ($val['type'] == MASTER_TYPE_DTTM) { // 日付時刻型の場合日付と時刻を連結する
				$C->data['Busy']["{$val['field']}"] = $C->data['Busy']["{$val['field']}_d"] . " " . $C->data['Busy']["{$val['field']}_t"];
			}
			if ($val['type'] == MASTER_TYPE_CHEK) { // チェック型の場合チェックされた内容を保持する
				// 改行を\nに統一
				$text = preg_replace(array('/\r\n/','/\r/','/\n/'), "\n", $val['select_ti'] );
				$listArr = explode("\n",$text);

				$temp = "";
				foreach ((array)$C->data['Busy']["{$val['field']}"] as $k=>$v){
					if ($v == 1){
						$temp .= $listArr[$k] . "\r\n";
					}
				}
				$C->data['Busy']["{$val['field']}"] = mb_rtrim($temp, "\r\n");
			}
		}

		// 入力チェック
		$C->Busy->create($C->data);
		if (!$C->Busy->validates()){
			$C->data['Image'] = $image;
			$C->data['Notimage'] = $notimage;
			return false;
		}

		$C->Busy->begin();

		$C->Busy->checkUpdated = true;	// 更新時間チェックON

		// 保存
		if (!$C->Busy->save(null, false)){
			$C->Busy->invalidate('error', "データの{$mode}に失敗しました。");
			$C->Busy->rollback();
			return false;
		}
		$id = $C->Busy->getID();
		// 写真
		$C->Busy->seiriFile($id);
		// イメージファイル、イメージ以外のファイルの保存
		if(!$C->BusyService->saveUploadFile($id, $image, $notimage)){
			$C->Busy->invalidate('error', "uploadsの保存に失敗しました。");
			$C->Busy->rollback();
			return false;
		}

		// BusiReferの更新
		if (!$C->BusiRefer->updateByBusiId($id)){
			$C->BusiRefer->rollback();
			return false;
		}

		// Customer関係のUPDATE
		if (!empty($C->data['Busy']['customer_id'])) {
			// Customerの最終イベントの更新
			if (!$C->Customer->updateLastEventDateTodayByCustomerId($C->data['Busy']['customer_id'])){
				$C->Customer->rollback();
				return false;
			}

			// CustomerReferの更新
			if (!$C->CustomerRefer->updateByCustomerId($C->data['Busy']['customer_id'])){
				$C->CustomerRefer->rollback();
				return false;
			}
		}

		// User関係のUPDATE
		if (!empty($C->data['Busy']['user_id'])) {
			// Userの最終イベントの更新
			if (!$C->User->updateLastEventDateTodayByUserId($C->data['Busy']['user_id'])){
				$C->User->rollback();
				return false;
			}

			// UserReferの更新
			if (!$C->UserRefer->updateByUserId($C->data['Busy']['user_id'])){
				$C->UserRefer->rollback();
				return false;
			}
		}
		// カレンダーデータの登録
		$params = array('conditions'=>array('Calender.parent_table'=>'busies', 'Calender.parent_id'=>$id));
		$calender = $C->Calender->find('first', $params); // カレンダー情報取得

		$customer = $C->Customer->read(null, $C->data['Busy']['customer_id']); // 顧客情報取得

		$calender['Calender']['subject'] = $customer['Customer']['name'] . '様商談';
		$calender['Calender']['user_id'] = $C->data['Busy']['user_id'];
		if (!empty($C->data['Busy']['start_time']) && !empty($C->data['Busy']['end_time'])){
			$calender['Calender']['start_time'] = $C->data['Busy']['busi_date'] . ' ' . $C->data['Busy']['start_time'];
			$calender['Calender']['end_time'] = $C->data['Busy']['busi_date'] . ' ' . $C->data['Busy']['end_time'];
			$calender['Calender']['is_allday_event'] = "0";
		}else{
			$calender['Calender']['start_time'] = $C->data['Busy']['busi_date'] . ' 00:00:00';
			$calender['Calender']['end_time'] = $C->data['Busy']['busi_date'] . ' 00:00:00';
			$calender['Calender']['is_allday_event'] = "1";
		}
		$calender['Calender']['color'] = NULL;
		$calender['Calender']['parent_table'] = "busies";
		$calender['Calender']['parent_id'] = $id;
		$C->Calender->create($calender);
		if (!$C->Calender->save(null, false)){
			$C->Calender->rollback();
			return false;
		}


		$C->Session->write('system-message', "データを{$mode}しました。");
		$C->Busy->commit();
		$C->data['Image'] = $image;
		$C->data['Notimage'] = $notimage;

		return true;
	}


	/**
	 * BusiMaster情報からBusy情報を作成する。
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/24
	 * @note
	 * @return   arr    初期データ配列
	 */
	function setBusyByBusiMaster(&$data) {
		$C       =& $this->Controller;

		$ret = $C->BusiMaster->getBusyTableInfo();
		foreach ($ret as $key => $val) {
			$val =& $ret[$key]['BusiMaster'];
			if ($val['type'] == MASTER_TYPE_DTTM) {	// 日付時間型
				if (!empty($data['Busy']["{$val['field']}"])) {
					$dtArr = explode(' ',$data['Busy']["{$val['field']}"]);
					$data['Busy']["{$val['field']}_d"] = dateFormat($dtArr[0],'Y/m/d');
					$data['Busy']["{$val['field']}_t"] = dateFormat($dtArr[1],'H:i');
				}
			}
			if ($val['type'] == MASTER_TYPE_DATE) {	// 日付型
				if (!empty($data['Busy']["{$val['field']}"]) && $data['Busy']["{$val['field']}"] != '0000-00-00') {
					$data['Busy']["{$val['field']}"] = dateFormat($data['Busy']["{$val['field']}"],'Y/m/d');
				}
			}
			if ($val['type'] == MASTER_TYPE_TIME) {	// 時間型
				if (!empty($data['Busy']["{$val['field']}"]) && $data['Busy']["{$val['field']}"] != '0000-00-00') {
					$data['Busy']["{$val['field']}"] = dateFormat($data['Busy']["{$val['field']}"],'H:i');
				}
			}
			if ($val['type'] == MASTER_TYPE_INTG) {	// 数値型
				if (!empty($data['Busy']["{$val['field']}"])) {
					$data['Busy']["{$val['field']}"] = rtrim($data['Busy']["{$val['field']}"],'0');
					$data['Busy']["{$val['field']}"] = rtrim($data['Busy']["{$val['field']}"],'.');
				}
			}
		}

	}


	/**
	 * ファイルアップロード処理
	 * @author   nakata@okushin.co.jpa
	 * @date     2015/02/26
	 * @note
	 * @param    str    テーブル名
	 * @param    int    ID
	 * @param    arr    イメージファイル群
	 * @param    arr    イメージ以外ファイル群
	 * @return   arr    初期データ配列
	 */
	function saveUploadFile($parentId, $image,  $notimage) {
		$C       =& $this->Controller;
		$imgArr = array();
		$notimgArr = array();

		// フィールド変換テーブル
		$params = array(
			'fields' => array("*"),
			'conditions' => array(),
			'order' => array('BusiMaster.number asc'),
			);
		$userMst = $C->BusiMaster->find('all', $params);
		foreach ($userMst as $key => $val) {
			$val = $val['BusiMaster'];
			if ($val['type'] == 9) {
				$imgArr[$val['number']] = $val['field'];
			}
		}
		// イメージファイルの最終登録
		if (!$C->UploadService->saveUpload('busies',$parentId,$image, $imgArr)) {
			return false;
		}
		foreach ($userMst as $key => $val) {
			$val = $val['BusiMaster'];
			if ($val['type'] == 10) {
				$notimgArr[$val['number']] = $val['field'];
			}
		}
		// イメージ以外のファイルの最終登録
		if (!$C->UploadService->saveUpload('busies',$parentId,$notimage, $notimgArr)) {
			return false;
		}
		return true;
	}


	/**
	 * ファイルアップロード情報 Viewへの定義
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/27
	 * @note
	 * @param    int    親ID
	 * @param    arr    $this->data
	 * @return   arr    初期データ配列
	 */
	function setUploadFile($parentId, &$data) {
		$C       =& $this->Controller;
		$imgArr = array();
		$notimgArr = array();

		$params = array(
			'fields' => array("*"),
			'conditions' => array(),
			'order' => array('BusiMaster.number asc'),
			);
		$busyMst = $C->BusiMaster->find('all', $params);
		foreach ($busyMst as $key => $val) {
			$val = $val['BusiMaster'];
			if ($val['type'] == 9) {
				$imgArr[$val['field']] = $val['number'];
			}
		}
		// イメージファイルアップロード情報 Viewへの定義
		$C->Upload->setImageFile('busies',$parentId,$imgArr,$data);
		// ファイル取得のフィールドテーブル作成（現状仮作成）フィールド項目追加機能が実装されれば修正が必要
		foreach ($busyMst as $key => $val) {
			$val = $val['BusiMaster'];
			if ($val['type'] == 10) {
				$notimgArr[$val['field']] = $val['number'];
			}
		}
		// イメージ以外のファイルアップロード情報 Viewへの定義
		$C->Upload->setNotimageFile('busies',$parentId,$notimgArr,$data);
	}

}

?>