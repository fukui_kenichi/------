<?php
/**
 * 個人設定サービスコンポーネント
 * @author   nakata@okushin.co.jp
 * @date     2015/02/20
 * @note     コントローラーとモデルの間でイベント処理を記述するクラス
 */
class KojinServiceComponent extends BasicServiceComponent {


	/**
	 * 保存処理
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/20
	 * @note
	 * @param    int   $id   ユーザーID
	 * @return   bool    処理結果真偽値
	 */
	function getDataByEdit($id) {
		$C       =& $this->Controller;
		$returnData = array(); // 返却用配列

		// ユーザー情報取得
		$userData = $C->User->find('first', array('conditions'=>array('User.id'=>$id)));

		if (!empty($userData)) {
			$returnData['User'] = $userData['User'];

			// パスワード欄
			$returnData['User']['password_01'] = $userData['User']['password'];
			$returnData['User']['password_02'] = $userData['User']['password'];
		}

		if (!empty($userData['User']['id'])) {
			// CustomerKojin情報取得
			$cKojinData = $C->CustomerKojin->getDataByKojinEdit($userData['User']['id']);
			$returnData['User']['CustomerKojin'] = $cKojinData;

			// BusiKojin情報取得
			$bKojinData = $C->BusiKojin->getDataByKojinEdit($userData['User']['id']);
			$returnData['User']['BusiKojin'] = $bKojinData;
		}

		return $returnData;
	}


	/**
	 * 保存処理
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/20
	 * @return   bool    処理結果真偽値
	 */
	function save() {
		$C       =& $this->Controller;
		$mode    =  (empty($C->data['Kojin']['id'])) ? '追加' : '更新';

		// パスワードをセット
		$C->data['User']['password'] = $C->data['User']['password_01'];

		// データ設定
		$C->User->create($C->data);

		// 入力チェック
		if (!$C->User->validatesByKojinEdit()){
			return false;
		}

		//
		$C->User->begin();

		// 保存
		if (!$C->User->save(null, false)){
			$C->User->invalidate('error', "ユーザーの更新に失敗しました。");
			$C->User->rollback();
			return false;
		}

		// ログインユーザのID取得
		$userId = $C->login['User']['id'];

		// CustomerKojinデータ保存
		if (!empty($C->data['User']['CustomerKojin'])) {
			foreach ($C->data['User']['CustomerKojin'] as $key => $val) {
				$saveData = $val['ck'];
				$saveData['number'] = (empty($val['ck']['view_flag']))? null: $val['ck']['number'];

				$saveData['user_id'] = $userId; // ユーザID
				$saveData['customer_master_id'] = $val['cm']['id']; // CustomerMasterID

				// 保存
				$C->CustomerKojin->create($saveData);
				if (!$C->CustomerKojin->save(null, false)){
					$C->CustomerKojin->invalidate('error', "CustomerKojinの更新に失敗しました。");
					$C->CustomerKojin->rollback();
					return false;
				}

			}
			// 表示番号の並び替え
			if (!$C->CustomerKojin->sortRank($userId)){
				$C->CustomerKojin->invalidate('error', 'CustomerKojinの並び順の整列に失敗しました。');
				$C->CustomerKojin->rollback();
				return false;
			}
		}

		// BusiKojinデータ保存
		if (!empty($C->data['User']['BusiKojin'])) {
			foreach ($C->data['User']['BusiKojin'] as $key => $val) {
				$saveData = $val['bk'];
				$saveData['number'] = (empty($val['bk']['view_flag']))? null: $val['bk']['number'];

				$saveData['user_id'] = $userId; // ユーザID
				$saveData['busi_master_id'] = $val['bm']['id']; // BusiMasterID

				// 保存
				$C->BusiKojin->create($saveData);
				if (!$C->BusiKojin->save(null, false)){
					$C->BusiKojin->invalidate('error', "BusiKojinの更新に失敗しました。");
					$C->BusiKojin->rollbabk();
					return false;
				}

			}
			// 表示番号の並び替え
			if (!$C->BusiKojin->sortRank($userId)){
				$C->BusiKojin->invalidate('error', 'BusiKojinの並び順の整列に失敗しました。');
				$C->BusiKojin->rollbabk();
				return false;
			}
		}

		// 変更コミット
		$C->User->commit();
		$C->Session->write('system-message', "データを更新・保存しました。");

		return true;
	}
}
?>