<?php
/**
 * ファイルアップロードテーブルサービスコンポーネント
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note     コントローラーとモデルの間でイベント処理を記述するクラス
 */
class UploadServiceComponent extends BasicServiceComponent {

	/**
	 * 検索条件作成
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note     ページネート用検索条件作成
	 * @param    arr    $data               入力された検索条件
	 * @param    arr    $defaultPaginate    デフォルト検索条件
	 */
	function createParams($data, $defaultPaginate) {
		$C    =& $this->Controller;
		$cond =  array();

		// デフォルトの検索条件取得
		if (isset($defaultPaginate['Upload']['conditions'])) {
			$cond = $defaultPaginate['Upload']['conditions'];
		}

		// 検索条件追加-------------------------------------------


		$defaultPaginate['Upload']['conditions'] = $cond;

		// 検索条件を一旦セッションに保存
		$C->HoldPaginate->saveParams($defaultPaginate);
	}


	/**
	 * 削除処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * $param    int    $id     削除したいデータの主キー
	 * @return   bool    処理結果真偽値
	 */
	function delete($id) {
		$C =& $this->Controller;

		// 入力チェック
		if (!$C->Upload->deleteValidates($id)){
			return false;
		}
		// 削除
		if (!$C->Upload->delete($id)){
			$C->Upload->invalidate('error', 'データの削除に失敗しました。');
			return false;
		}

		$C->Session->write('system-message', 'データを削除しました。');
		return true;
	}


	/**
	 * 保存処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @return   bool    処理結果真偽値
	 */
	function save() {
		$C       =& $this->Controller;
		$mode    =  (empty($C->data['Upload']['id'])) ? '追加' : '更新';
		$errFlag = false;

		$C->Upload->create($C->data);

		// 入力チェック
		if (!$C->Upload->validates()){
			$errFlag = true;
		}

		if ($errFlag) {
			return false;
		}

		$C->Upload->begin();

		$C->Upload->checkUpdated = true;	// 更新時間チェックON

		// 保存
		if (!$C->Upload->save(null, false)){
			$C->Upload->invalidate('error', "データの{$mode}に失敗しました。");
			$C->Upload->rollback();
			return false;
		}

		$C->Session->write('system-message', "データを{$mode}しました。");
		$C->Upload->commit();
		return true;
	}


	/**
	 * 初期データ取得
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note     編集画面の初期値設定で使用
	 * @return   arr    初期データ配列
	 */
	function getIni() {
		$data = array();

		return $data;
	}


	/**
	 * 複数ファイルアップロード登録処理（削除含む）
	 * @author   nishishita@okushin.co.jp
	 * @date     2015/02/12
	 * @note     複数ファイルをアップロードする。
	 * @param    arr   イメージかイメージではないファイル情報
	 * @param    arr   変換テーブル番号からフィールド名
	 * @return   bool  真偽値
	 */
	function saveUpload($parentTable,$parentId,$files, $henArr) {
		$C       =& $this->Controller;
		foreach($files as $key => $val) {
			$idArr = array();
			$fieldArr = explode("_",$key);
			// アップファイルを指定するフィールドは無視
			if($fieldArr[0] == "up") {
				continue;
			}
			// フィールド名で繰り返す。
			foreach ($val as $field => $v) {
				// フィールド名がimage_(no)_idかimage_(no)_upの場合Idテーブルを作成する。
				$fieldArr = explode("_",$field);
				if(count($fieldArr) == 3) {
					if($fieldArr[2] == "id") {
						$idArr[$fieldArr[1]]["id"] = $v;
					}
					if($fieldArr[2] == "up") {
						$idArr[$fieldArr[1]]["up"] = $v;
					}
				}
			}
			// Idテーブルで繰り返す
			foreach ($idArr as $k => $v) {
				// idありでupがない場合は、削除されているので、イメージ削除処理を行う。
				if($v['id'] && (empty($v["up"]))) {
					if(!$C->Upload->deleteFile($v['id'])) {
						$C->Upload->invalidate('error', "イメージデータ削除に失敗しました。");
						return false;
					}
					// 削除したIDはテーブルから抹消する。
					unset($idArr[$k]);
				}
			}
			// 純粋にIdのテーブルを作成する。
			$idTbl = array();
			foreach ($idArr as $k => $v) {
				$idTbl[] = $v['id'];
			}
			// DB保存フィールド名を取得する。現状は仮の形で実現しています。詳細が決まれば改修が必要
			$fieldname = $henArr[$key];
			// 親種別と親Idと親フィールドをidTbl分セットする。
			if(!$C->Upload->saveThird($idTbl,$parentTable,$parentId,$fieldname)) {
				$C->User->invalidate('error', "イメージデータ最終更新に失敗しました。");
				return false;
			}
		}
		return true;
	}
	/**
	 * ファイルアップロード情報 Viewへの定義
	 * @author   nishishita@okushin.co.jp
	 * @date     2015/02/12
	 * @note
	 * @param    str    テーブル名
	 * @param    int    親ID
	 * @param    arr    $this->data
	 * @return   arr    初期データ配列
	 */
	function setUploadFile($parentTable, $parentId, &$data) {
		$C       =& $this->Controller;
		$imgArr = array();
		$notimgArr = array();

		// イメージ取得のフィールドテーブル作成（現状仮作成）フィールド項目追加機能が実装されれば修正が必要
		$params = array(
			'fields' => array("*"),
			'conditions' => array(),
			'order' => array('UserMaster.number asc'),
			);
		$userMst = $C->UserMaster->find('all', $params);
		foreach ($userMst as $key => $val) {
			$val = $val['UserMaster'];
			if ($val['type'] == 9) {
				$imgArr[$val['field']] = $val['number'];
			}
		}
		// イメージファイルアップロード情報 Viewへの定義
		$C->Upload->setImageFile($parentTable,$parentId,$imgArr,$data);
		// ファイル取得のフィールドテーブル作成（現状仮作成）フィールド項目追加機能が実装されれば修正が必要
		foreach ($userMst as $key => $val) {
			$val = $val['UserMaster'];
			if ($val['type'] == 10) {
				$notimgArr[$val['field']] = $val['number'];
			}
		}
		// イメージ以外のファイルアップロード情報 Viewへの定義
		$C->Upload->setNotimageFile($parentTable,$parentId,$notimgArr,$data);
	}

	/**
	 * ファイルアップロード処理
	 * @author   nishishita@okushin.co.jp
	 * @date     2015/02/12
	 * @note
	 * @param    str    テーブル名
	 * @param    int    ID
	 * @param    arr    イメージファイル群
	 * @param    arr    イメージ以外ファイル群
	 * @return   arr    初期データ配列
	 */
	function saveUploadFile($parentTable, $parentId, $image,  $notimage) {
		$C       =& $this->Controller;
		$imgArr = array();
		$notimgArr = array();

		// フィールド変換テーブル
		$params = array(
			'fields' => array("*"),
			'conditions' => array(),
			'order' => array('UserMaster.number asc'),
			);
		$userMst = $C->UserMaster->find('all', $params);
		foreach ($userMst as $key => $val) {
			$val = $val['UserMaster'];
			if ($val['type'] == 9) {
				$imgArr[$val['number']] = $val['field'];
			}
		}
		// イメージファイルの最終登録
		if (!$C->UploadService->saveUpload($parentTable,$parentId,$image, $imgArr)) {
			return false;
		}
		foreach ($userMst as $key => $val) {
			$val = $val['UserMaster'];
			if ($val['type'] == 10) {
				$notimgArr[$val['number']] = $val['field'];
			}
		}
		// イメージ以外のファイルの最終登録
		if (!$C->UploadService->saveUpload($parentTable,$parentId,$notimage, $notimgArr)) {
			return false;
		}
		return true;
	}

}

?>