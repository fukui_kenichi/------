<?php
/**
 * メールテンプレートテーブルサービスコンポーネント
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note     コントローラーとモデルの間でイベント処理を記述するクラス
 */
class MailTemplateServiceComponent extends BasicServiceComponent {

	/**
	 * 検索条件作成
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note     ページネート用検索条件作成
	 * @param    arr    $data               入力された検索条件
	 * @param    arr    $defaultPaginate    デフォルト検索条件
	 */
	function createParams($data, $defaultPaginate) {
		$C    =& $this->Controller;
		$cond =  array();
		
		// デフォルトの検索条件取得
		if (isset($defaultPaginate['MailTemplate']['conditions'])) {
			$cond = $defaultPaginate['MailTemplate']['conditions'];
		}
		
		// 検索条件追加-------------------------------------------

		
		$defaultPaginate['MailTemplate']['conditions'] = $cond;
		
		// 検索条件を一旦セッションに保存
		$C->HoldPaginate->saveParams($defaultPaginate);
	}


	/**
	 * 削除処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note     
	 * $param    int    $id     削除したいデータの主キー
	 * @return   bool    処理結果真偽値
	 */
	function delete($id) {
		$C =& $this->Controller;

		// 入力チェック
		if (!$C->MailTemplate->deleteValidates($id)){
			return false;
		}
		// 削除
		if (!$C->MailTemplate->delete($id)){
			$C->MailTemplate->invalidate('error', 'データの削除に失敗しました。');
			return false;
		}

		$C->Session->write('system-message', 'データを削除しました。');
		return true;
	}


	/**
	 * 保存処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @return   bool    処理結果真偽値
	 */
	function save() {
		$C       =& $this->Controller;
		$mode    =  (empty($C->data['MailTemplate']['id'])) ? '追加' : '更新';
		$errFlag = false;
		
		$C->MailTemplate->create($C->data);
		
		// 入力チェック
		if (!$C->MailTemplate->validates()){
			$errFlag = true;
		}
		
		if ($errFlag) {
			return false;
		}

		$C->MailTemplate->begin();

		$C->MailTemplate->checkUpdated = true;	// 更新時間チェックON
		
		// 保存
		if (!$C->MailTemplate->save(null, false)){
			$C->MailTemplate->invalidate('error', "データの{$mode}に失敗しました。");
			$C->MailTemplate->rollback();
			return false;
		}

		$C->Session->write('system-message', "データを{$mode}しました。");
		$C->MailTemplate->commit();
		return true;
	}


	/**
	 * 初期データ取得
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note     編集画面の初期値設定で使用
	 * @return   arr    初期データ配列
	 */
	function getIni() {
		$data = array();
		
		return $data;
	}



}

?>