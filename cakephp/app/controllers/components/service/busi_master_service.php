<?php
/**
 * 商談マスターテーブルサービスコンポーネント
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note     コントローラーとモデルの間でイベント処理を記述するクラス
 */
class BusiMasterServiceComponent extends BasicServiceComponent {

	/**
	 * 検索条件作成
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note     ページネート用検索条件作成
	 * @param    arr    $data               入力された検索条件
	 * @param    arr    $defaultPaginate    デフォルト検索条件
	 */
	function createParams($data, $defaultPaginate) {
		$C    =& $this->Controller;
		$cond =  array();

		// デフォルトの検索条件取得
		if (isset($defaultPaginate['BusiMaster']['conditions'])) {
			$cond = $defaultPaginate['BusiMaster']['conditions'];
		}

		// 検索条件追加-------------------------------------------


		$defaultPaginate['BusiMaster']['conditions'] = $cond;

		// 検索条件を一旦セッションに保存
		$C->HoldPaginate->saveParams($defaultPaginate);
	}


	/**
	 * 一覧画面用データ取得・整形関数
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/13
	 * @return   array    整形済みの単位一覧画面用データ
	 */
	function getDataByIndex() {
		$C       =& $this->Controller;
		$returnData = array();	// 返却用配列

		// DBより必要なデータを取得します
		$result = $C->BusiMaster->getDataByIndex();

		if (!empty($result)){
			// データを整形します
			foreach ($result as $key => $val) {
				$returnData['BusiMaster'][$key] = $val['BusiMaster'];
				$returnData['BusiMaster'][$key]['db_id'] = $val['BusiMaster']['id'];
			}
		}
		$returnData['Master']['newRowNo'] = -1;

		// 整形したデータを返します
		return $returnData;
	}


	/**
	 * 削除処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * $param    int    $id     削除したいデータの主キー
	 * @return   bool    処理結果真偽値
	 */
	function delete($id) {
		$C =& $this->Controller;

		// 入力チェック
		if (!$C->BusiMaster->deleteValidates($id)){
			return false;
		}
		// 削除
		if (!$C->BusiMaster->delete($id)){
			$C->BusiMaster->invalidate('error', 'データの削除に失敗しました。');
			return false;
		}

		$C->Session->write('system-message', 'データを削除しました。');
		return true;
	}


	/**
	 * 一覧保存処理
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/18
	 * @return   bool    処理結果真偽値
	 */
	function saveByIndex() {
		$C       =& $this->Controller;
		$busiChangeFlag = false;

		if (isset($C->data['BusiMaster']['ZZZ'])) unset($C->data['BusiMaster']['ZZZ']); // Viewのテンプレートは項目削除

		// アラート値を選択値に置き換える
		$this->changeAlertToSelect();

		// 入力チェック
		$C->BusiMaster->create($C->data);
		if (!$C->BusiMaster->validatesByIndex()){
			return false;
		}

		$C->BusiMaster->begin();

		foreach ($C->data['BusiMaster'] as $key => $val) {
			if (isset($val['db_id']) && empty($val['id'])) { // 削除
				// BusiMasterのデータ削除
				$C->BusiMaster->delete($val['db_id']);

				// Busiのフィールド削除
				$sql = 'ALTER TABLE busies DROP free'. $val['db_id']. ';';
				if ($C->Busy->query($sql) === false) {
					$C->Busy->invalidate('error', 'Busiテーブルのfield項目更新に失敗しました。');
					$C->Busy->rollback();
					return false;
				}

				// BusiKojinのデータ削除
				$sql = 'DELETE FROM busi_kojins WHERE busi_master_id = '. $val['db_id']. ';';
				if ($C->BusiKojin->query($sql) === false) {
					$C->BusiKojin->invalidate('error', 'BusiKojinテーブルのでーた削除に失敗しました。');
					$C->BusiKojin->rollback();
					return false;
				}

				// Busiを変更したフラグを立てる
				if ($busiChangeFlag == false) $busiChangeFlag = true;

			} else { // 追加・更新
				// BusiMasterデータ保存
				$saveData = $val;
				$saveData['select_ti'] = mb_rtrim($saveData['select_ti'], "\r\n"); // 最後の改行を除去

				$C->BusiMaster->create($saveData);
				// 保存
				if (!$C->BusiMaster->save(null, false)){
					$C->BusiMaster->invalidate('error', "データの保存に失敗しました。");
					$C->BusiMaster->rollback();
					return false;
				}

				// 新規の場合はBusiにフィールド追加
				if (empty($val['id']) && empty($val['db_id'])) {
					// cusotmer_mastersテーブルのID
					$busiMasterId = $C->BusiMaster->getID();

					// Busiのフィールド名追加
					$sql = "UPDATE busi_masters SET field = 'free{$busiMasterId}' WHERE id = {$busiMasterId};";
					if ($C->BusiMaster->query($sql) === false) {
						$C->BusiMaster->invalidate('error', 'BusiMasterテーブルのfield項目更新に失敗しました。');
						$C->BusiMaster->rollback();
						return false;
					}

					// Busiにフィールド追加のSQL作成
					$sql = 'ALTER TABLE busies ADD free'. $busiMasterId;
					switch ($val['type']) {
						case MASTER_TYPE_CHAR:		// 文字列（制限あり）
						case MASTER_TYPE_LIST:		// リスト型
						case MASTER_TYPE_RADI:		// ラジオ型
						case MASTER_TYPE_CHEK:		// チェック型
						case MASTER_TYPE_FIMG:		// ファイル（イメージ）
						case MASTER_TYPE_FILE:		// ファイル（その他）
						case MASTER_TYPE_TELE:		// 電話型
							$sql .= ' VARCHAR(256)';
							break;
						case MASTER_TYPE_TEXT:		// 文字列（制限なし）
							$sql .= ' TEXT';
							break;
						case MASTER_TYPE_INTG:		// 数値型
							$sql .= ' FLOAT(10,2)';
							break;
						case MASTER_TYPE_DTTM:		// 日付時刻型
							$sql .= ' DATETIME';
							break;
						case MASTER_TYPE_DATE:		// 日付型
						case MASTER_TYPE_DATE_A:	// 日付型（アラート）
							$sql .= ' DATE';
							break;
						case MASTER_TYPE_TIME:		// 時間型
							$sql .= ' TIME';
							break;
						default :
							$sql .= ' VARCHAR(256)';
					}

					// Busiへのフィールド追加を実行
					$sql .= ";";
					if ($C->Busy->query($sql) === false) {
						$C->Busy->invalidate('error', 'Busiテーブルへのフィールド追加に失敗しました。');
						$C->Busy->rollback();
						return false;
					}

					// Busiを変更したフラグを立てる
					if ($busiChangeFlag == false) $busiChangeFlag = true;
				}
			}
		}

		// 表示番号の並び替え
		if (!$C->BusiMaster->sortRank()){
			$C->BusiMaster->invalidate('error', '並び順の整列に失敗しました。');
			$C->BusiMaster->rollback();
			return false;
		}

		// Busyテーブルの項目が追加・削除されている場合
		if ($busiChangeFlag){ // Busiを変更したらtmp/cache/models/以下のファイルを削除4
			$modelFileDir = TMP. 'cache/models/';
			if (delDirFiles($modelFileDir) === false) {
				$C->BusiMaster->invalidate('error', 'tmp/cache/models以下のファイルの削除に失敗しました。');
				$C->BusiMaster->rollback();
				return false;
			}
		}

		// 件数が一定以下なら検索フィールドを作り直す
		$count = $C->Busy->find('count');
		if ($count < BUSY_COUNT){
			if (!$C->BusiRefer->updateAll()){
				return false;
			}
		}


		// 変更コミット
		$C->BusiMaster->commit();

		// メッセージの設定
		$C->Session->write('system-message', "データを更新・保存しました。");

		return true;
	}


	/**
	 * アラート値を選択値に置き換える
	 * @author   fukui@okushin.co.jp
	 * @date     2015/05/08
	 * @note
	 */
	function changeAlertToSelect() {
		$C       =& $this->Controller;
		if (isset($C->data)) {
			foreach ($C->data['BusiMaster'] as $key => $val) {
				// 既存データはスキップ
				if (!empty($val['id'])) continue;

				// 削除データはスキップ
				if (isset($val['db_id']) && empty($val['id'])) continue;

				if ($val['type'] == MASTER_TYPE_DATE_A){
					$C->data['BusiMaster'][$key]['select_ti'] = $val['alert'];
				}
			}
		}
	}


	/**
	 * 保存処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @return   bool    処理結果真偽値
	 */
	function save() {
		$C       =& $this->Controller;
		$mode    =  (empty($C->data['BusiMaster']['id'])) ? '追加' : '更新';
		$errFlag = false;

		$C->BusiMaster->create($C->data);

		// 入力チェック
		if (!$C->BusiMaster->validates()){
			$errFlag = true;
		}

		if ($errFlag) {
			return false;
		}

		$C->BusiMaster->begin();

		$C->BusiMaster->checkUpdated = true;	// 更新時間チェックON

		// 保存
		if (!$C->BusiMaster->save(null, false)){
			$C->BusiMaster->invalidate('error', "データの{$mode}に失敗しました。");
			$C->BusiMaster->rollback();
			return false;
		}

		$C->Session->write('system-message', "データを{$mode}しました。");
		$C->BusiMaster->commit();
		return true;
	}


	/**
	 * 初期データ取得
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note     編集画面の初期値設定で使用
	 * @return   arr    初期データ配列
	 */
	function getIni() {
		$data = array();

		return $data;
	}



}

?>