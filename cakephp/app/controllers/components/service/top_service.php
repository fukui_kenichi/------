<?php
/**
 * トップサービスコンポーネント
 * @author   fukui@okushin.co.jp
 * @date     2015/05/08
 * @note     コントローラーとモデルの間でイベント処理を記述するクラス
 */
class TopServiceComponent extends BasicServiceComponent {
	/**
	 * お知らせ(顧客)アラート一覧取得
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @return   arr    お知らせ一覧
	 */
	function getAlertCustomerList() {
		$C       =& $this->Controller;
		$alertArr = array();

		// 日付型(アラート)のデータ取得
		$alertField = $C->CustomerMaster->getAlertField();
		if (empty($alertField)){
			return $alertArr;
		}

		foreach ($alertField as $key=>$val){
			// お知らせに引っかかる顧客取得
			$alertCustomer = $C->Customer->getAlertCustomer($val);
			if (empty($alertCustomer)){
				continue;
			}
			// お知らせを配列に入れていく
			$alertArr[$key]['title'] = $val['CustomerMaster']['koumoku'];
			$customerIdArr = "";
			foreach ($alertCustomer as $i=>$customer){
				$alertArr[$key]['value'][$i]['id'] = $customer['customers']['id'];
				$alertArr[$key]['value'][$i]['content'] =
					$customer['customers']['name'] . '様の' .
					$val['CustomerMaster']['koumoku'] . 'が' .
					$customer[0]['day'] . 'です';
				$customerIdArr .= $customer['customers']['id'] . ',';
			}
			$alertArr[$key]['customer_id'] = trim($customerIdArr, ',');

		}
		return $alertArr;
	}


	/**
	 * お知らせ(商談)アラート一覧取得
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @return   arr    お知らせ一覧
	 */
	function getAlertBusyList() {
		$C       =& $this->Controller;
		$alertArr = array();

		// 日付型(アラート)のデータ取得
		$alertField = $C->BusiMaster->getAlertField();
		if (empty($alertField)){
			return $alertArr;
		}

		foreach ($alertField as $key=>$val){
			// お知らせに引っかかる顧客取得
			$alertBusy = $C->Busy->getAlertBusy($val);
			if (empty($alertBusy)){
				continue;
			}
			// お知らせを配列に入れていく
			$alertArr[$key]['title'] = $val['BusiMaster']['koumoku'];
			$customerIdArr = "";
			foreach ($alertBusy as $i=>$busy){
				$customer = $C->Customer->read(null, $busy['busies']['customer_id']);
				$alertArr[$key]['value'][$i]['id'] = $busy['busies']['id'];
				$alertArr[$key]['value'][$i]['content'] =
					$customer['Customer']['name'] . '様の' .
					$val['BusiMaster']['koumoku'] . 'が' .
					$busy[0]['day'] . 'です';
				$customerIdArr .= $busy['busies']['customer_id'] . ',';
			}
			$alertArr[$key]['customer_id'] = trim($customerIdArr, ',');

		}
		return $alertArr;
	}

}

?>