<?php
/**
 * 顧客マスターテーブルサービスコンポーネント
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note     コントローラーとモデルの間でイベント処理を記述するクラス
 */
class CustomerMasterServiceComponent extends BasicServiceComponent {

	/**
	 * 検索条件作成
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note     ページネート用検索条件作成
	 * @param    arr    $data               入力された検索条件
	 * @param    arr    $defaultPaginate    デフォルト検索条件
	 */
	function createParams($data, $defaultPaginate) {
		$C    =& $this->Controller;
		$cond =  array();

		// デフォルトの検索条件取得
		if (isset($defaultPaginate['CustomerMaster']['conditions'])) {
			$cond = $defaultPaginate['CustomerMaster']['conditions'];
		}

		// 検索条件追加-------------------------------------------


		$defaultPaginate['CustomerMaster']['conditions'] = $cond;

		// 検索条件を一旦セッションに保存
		$C->HoldPaginate->saveParams($defaultPaginate);
	}


	/**
	 * 一覧画面用データ取得・整形関数
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/13
	 * @return   array    整形済みの単位一覧画面用データ
	 */
	function getDataByIndex() {
		$C       =& $this->Controller;
		$returnData = array();	// 返却用配列

		// DBより必要なデータを取得します
		$result = $C->CustomerMaster->getDataByIndex();

		if (!empty($result)){
			// データを整形します
			foreach ($result as $key => $val) {
				$returnData['CustomerMaster'][$key] = $val['CustomerMaster'];
				$returnData['CustomerMaster'][$key]['db_id'] = $val['CustomerMaster']['id'];
			}
		}
		$returnData['Master']['newRowNo'] = -1;

		// 整形したデータを返します
		return $returnData;
	}


	/**
	 * 削除処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * $param    int    $id     削除したいデータの主キー
	 * @return   bool    処理結果真偽値
	 */
	function delete($id) {
		$C =& $this->Controller;

		// 入力チェック
		if (!$C->CustomerMaster->deleteValidates($id)){
			return false;
		}
		// 削除
		if (!$C->CustomerMaster->delete($id)){
			$C->CustomerMaster->invalidate('error', 'データの削除に失敗しました。');
			return false;
		}

		$C->Session->write('system-message', 'データを削除しました。');
		return true;
	}


	/**
	 * 一覧保存処理
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/16
	 * @return   bool    処理結果真偽値
	 */
	function saveByIndex() {
		$C       =& $this->Controller;
		$customerChangeFlag = false;

		if (isset($C->data['CustomerMaster']['ZZZ'])) unset($C->data['CustomerMaster']['ZZZ']); // Viewのテンプレートは項目削除

		// アラート値を選択値に置き換える
		$this->changeAlertToSelect();

		// 入力チェック
		$C->CustomerMaster->create($C->data);
		if (!$C->CustomerMaster->validatesByIndex()){
			return false;
		}

		$C->CustomerMaster->begin();

		foreach ($C->data['CustomerMaster'] as $key => $val) {
			if (isset($val['db_id']) && empty($val['id'])) { // 削除
				// CustomerMasterのデータ削除
				$C->CustomerMaster->delete($val['db_id']);

				// Customerのフィールド削除
				$sql = 'ALTER TABLE customers DROP free'. $val['db_id']. ';';
				if ($C->Customer->query($sql) === false) {
					$C->Customer->invalidate('error', 'Customerテーブルのfield項目更新に失敗しました。');
					$C->Customer->rollback();
					return false;
				}

				// CustomerKojinのデータ削除
				$sql = 'DELETE FROM customer_kojins WHERE customer_master_id = '. $val['db_id']. ';';
				if ($C->CustomerKojin->query($sql) === false) {
					$C->CustomerKojin->invalidate('error', 'CustomerKojinテーブルのでーた削除に失敗しました。');
					$C->CustomerKojin->rollback();
					return false;
				}

				// Customerを変更したフラグを立てる
				if ($customerChangeFlag == false) $customerChangeFlag = true;

			} else { // 追加・更新
				// CustomerMasterデータ保存
				$saveData = $val;
				$saveData['select_ti'] = mb_rtrim($saveData['select_ti'], "\r\n"); // 最後の改行を除去

				$C->CustomerMaster->create($saveData);
				// 保存
				if (!$C->CustomerMaster->save(null, false)){
					$C->CustomerMaster->invalidate('error', "データの保存に失敗しました。");
					$C->CustomerMaster->rollback();
					return false;
				}

				// 新規の場合はCustomerにフィールド追加
				if (empty($val['id']) && empty($val['db_id'])) {
					// cusotmer_mastersテーブルのID
					$customerMasterId = $C->CustomerMaster->getID();

					// Customerのフィールド名追加
					$sql = "UPDATE customer_masters SET field = 'free{$customerMasterId}' WHERE id = {$customerMasterId};";
					if ($C->CustomerMaster->query($sql) === false) {
						$C->CustomerMaster->invalidate('error', 'CustomerMasterテーブルのfield項目更新に失敗しました。');
						$C->CustomerMaster->rollback();
						return false;
					}

					// Customerにフィールド追加のSQL作成
					$sql = 'ALTER TABLE customers ADD free'. $customerMasterId;
					switch ($val['type']) {
						case MASTER_TYPE_CHAR:		// 文字列（制限あり）
						case MASTER_TYPE_LIST:		// リスト型
						case MASTER_TYPE_RADI:		// ラジオ型
						case MASTER_TYPE_CHEK:		// チェック型
						case MASTER_TYPE_FIMG:		// ファイル（イメージ）
						case MASTER_TYPE_FILE:		// ファイル（その他）
						case MASTER_TYPE_TELE:		// 電話型
							$sql .= ' VARCHAR(256)';
							break;
						case MASTER_TYPE_TEXT:		// 文字列（制限なし）
							$sql .= ' TEXT';
							break;
						case MASTER_TYPE_INTG:		// 数値型
							$sql .= ' FLOAT(10,2)';
							break;
						case MASTER_TYPE_DTTM:		// 日付時刻型
							$sql .= ' DATETIME';
							break;
						case MASTER_TYPE_DATE:		// 日付型
						case MASTER_TYPE_DATE_A:	// 日付型（アラート）
							$sql .= ' DATE';
							break;
						case MASTER_TYPE_TIME:		// 時間型
							$sql .= ' TIME';
							break;
						default :
							$sql .= ' VARCHAR(256)';
					}

					// Customerへのフィールド追加を実行
					$sql .= ";";
					if ($C->Customer->query($sql) === false) {
						$C->Customer->invalidate('error', 'Customerテーブルへのフィールド追加に失敗しました。');
						$C->Customer->rollback();
						return false;
					}

					// Customerを変更したフラグを立てる
					if ($customerChangeFlag == false) $customerChangeFlag = true;
				}
			}
		}

		// 表示番号の並び替え
		if (!$C->CustomerMaster->sortRank()){
			$C->CustomerMaster->invalidate('error', '並び順の整列に失敗しました。');
			$C->CustomerMaster->rollback();
			return false;
		}

		// CustomerReferの更新
		if (!$C->CustomerRefer->updateAll()){
			$C->CustomerRefer->rollback();
			return false;
		}

		// Customerテーブルの項目が追加・削除されている場合
		if ($customerChangeFlag){
			// tmp/cache/models/以下のファイルを削除
			$modelFileDir = TMP. 'cache/models/';
			if (delDirFiles($modelFileDir) === false) {
				$C->CustomerMaster->invalidate('error', 'tmp/cache/models以下のファイルの削除に失敗しました。');
				$C->CustomerMaster->rollback();
				return false;
			}
		}

		// コミット
		$C->CustomerMaster->commit();

		// メッセージ設定
		$C->Session->write('system-message', "データを更新・保存しました。");

		return true;
	}


	/**
	 * アラート値を選択値に置き換える
	 * @author   fukui@okushin.co.jp
	 * @date     2015/05/08
	 * @note
	 */
	function changeAlertToSelect() {
		$C       =& $this->Controller;
		if (isset($C->data)) {
			foreach ($C->data['CustomerMaster'] as $key => $val) {
				// 既存データはスキップ
				if (!empty($val['id'])) continue;

				// 削除データはスキップ
				if (isset($val['db_id']) && empty($val['id'])) continue;

				if ($val['type'] == MASTER_TYPE_DATE_A){
					$C->data['CustomerMaster'][$key]['select_ti'] = $val['alert'];
				}
			}
		}
	}


	/**
	 * 保存処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @return   bool    処理結果真偽値
	 */
	function save() {
		$C       =& $this->Controller;
		$mode    =  (empty($C->data['CustomerMaster']['id'])) ? '追加' : '更新';
		$errFlag = false;

		$C->CustomerMaster->create($C->data);

		// 入力チェック
		if (!$C->CustomerMaster->validates()){
			$errFlag = true;
		}

		if ($errFlag) {
			return false;
		}

		$C->CustomerMaster->begin();

		$C->CustomerMaster->checkUpdated = true;	// 更新時間チェックON

		// 保存
		if (!$C->CustomerMaster->save(null, false)){
			$C->CustomerMaster->invalidate('error', "データの{$mode}に失敗しました。");
			$C->CustomerMaster->rollback();
			return false;
		}

		$C->Session->write('system-message', "データを{$mode}しました。");
		$C->CustomerMaster->commit();
		return true;
	}


	/**
	 * 初期データ取得
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note     編集画面の初期値設定で使用
	 * @return   arr    初期データ配列
	 */
	function getIni() {
		$data = array();

		return $data;
	}



}

?>