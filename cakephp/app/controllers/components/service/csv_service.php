<?php
/**
 * CSV出力サービスコンポーネント
 * @author   fukui@okushin.co.jp
 * @date     2015/09/02
 * @note     コントローラーとモデルの間でイベント処理を記述するクラス
 */
class CsvServiceComponent extends BasicServiceComponent {
	/**
	 * 顧客情報CSVデータ取得
	 * @author   fukui@okushin.co.jp
	 * @date     2015/09/02
	 * @note
	 * @return   arr    顧客情報CSVデータ
	 */
	function getCustomerCsv() {
		$C       =& $this->Controller;
		$returnData = array(); // 返却用配列

		// 顧客マスタ取得
		$customerMaster = $C->CustomerMaster->getDataByIndex();
		$title = array("ID");
		$fieldArr = array("id");
		foreach ((array)$customerMaster as $val){
			// UPファイルは無視
			if ($val['CustomerMaster']['type'] == MASTER_TYPE_FIMG || $val['CustomerMaster']['type'] == MASTER_TYPE_FILE){
				continue;
			}
			if ($val['CustomerMaster']['field'] == 'address_info'){
				$title[] = '郵便番号';
				$fieldArr[] = 'zip_code';
				$title[] = '住所';
				$fieldArr[] = 'address';
			}else{
				$title[] = $val['CustomerMaster']['koumoku'];
				$fieldArr[] = $val['CustomerMaster']['field'];
			}
		}

		// 顧客情報取得
		$params = array('conditions'=>array('Customer.del_flag'=>0));
		$customer = $C->Customer->find('all', $params);
		$data = array();
		foreach ((array)$customer as $key=>$val){
			foreach ($fieldArr as $field){
				$data[$key][] = $val['Customer'][$field];
			}
		}
		$returnData['title'] = $title;
		$returnData['data'] = $data;
		$returnData['fieldArr'] = $fieldArr;

	    $search = array('/,/', '/"/s');
		$replace = array('，', '”');
		$returnData = reVal($search, $replace, $returnData);	//エスケープ

		return $returnData;
	}

	/**
	 * 商談情報CSVデータ取得
	 * @author   fukui@okushin.co.jp
	 * @date     2015/09/02
	 * @note
	 * @return   arr    商談情報CSVデータ
	 */
	function getBusyCsv() {
		$C       =& $this->Controller;
		$returnData = array(); // 返却用配列

		// 商談マスタ取得
		$busyMaster = $C->BusiMaster->getDataByIndex();
		$title = array("ID", "顧客ID", "顧客名");
		$fieldArr = array("id", "customer_id", "customer_name");
		foreach ((array)$busyMaster as $val){
			// UPファイルは無視
			if ($val['BusiMaster']['type'] == MASTER_TYPE_FIMG || $val['BusiMaster']['type'] == MASTER_TYPE_FILE){
				continue;
			}
			if ($val['BusiMaster']['field'] == 'user_id'){
				$title[] = '担当者ID';
				$fieldArr[] = 'user_id';
				$title[] = '担当者名';
				$fieldArr[] = 'user_name';
			}else{
				$title[] = $val['BusiMaster']['koumoku'];
				$fieldArr[] = $val['BusiMaster']['field'];
			}
		}

		// 商談情報取得
		$busy = $C->Busy->getBusyArrByCsv();
		$data = array();
		foreach ((array)$busy as $key=>$val){
			foreach ($fieldArr as $field){
				if ($field == 'user_name'){
					$data[$key][] = $val['User']['name'];
				}elseif ($field == 'customer_name'){
					$data[$key][] = $val['Customer']['name'];
				}else{
					$data[$key][] = $val['Busy'][$field];
				}
			}
		}
		$returnData['title'] = $title;
		$returnData['data'] = $data;
		$returnData['fieldArr'] = $fieldArr;

	    $search = array('/,/', '/"/s');
		$replace = array('，', '”');
		$returnData = reVal($search, $replace, $returnData);	//エスケープ

		return $returnData;
	}
}
?>