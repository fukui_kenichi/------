<?php
/**
 * メール配信テーブルサービスコンポーネント
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note     コントローラーとモデルの間でイベント処理を記述するクラス
 */
class MailServiceComponent extends BasicServiceComponent {

	/**
	 * 検索条件作成
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note     ページネート用検索条件作成
	 * @param    arr    $data               入力された検索条件
	 * @param    arr    $defaultPaginate    デフォルト検索条件
	 */
	function createParams($data, $defaultPaginate) {
		$C    =& $this->Controller;
		$cond =  array();

		// デフォルトの検索条件取得
		if (isset($defaultPaginate['Mail']['conditions'])) {
			$cond = $defaultPaginate['Mail']['conditions'];
		}

		// 検索条件追加-------------------------------------------


		$defaultPaginate['Mail']['conditions'] = $cond;

		// 検索条件を一旦セッションに保存
		$C->HoldPaginate->saveParams($defaultPaginate);
	}


	/**
	 * 削除処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * $param    int    $id     削除したいデータの主キー
	 * @return   bool    処理結果真偽値
	 */
	function delete($id) {
		$C =& $this->Controller;

		// 入力チェック
		if (!$C->Mail->deleteValidates($id)){
			return false;
		}

		$C->Mail->begin(); // トランザクション開始

		// Mailのデータを物理削除
		if (!$C->Mail->delete($id)){
			$C->Mail->invalidate('error', 'Mailデータの削除に失敗しました。');
			return false;
		}

		// MailSenderのデータを物理削除
		// SQL文を作成して実行
		$sql ="DELETE FROM mail_senders WHERE mail_id = {$id};";
		if (!$C->MailSender->query($sql)){
			$C->MailSender->invalidate('error', 'MailSenderデータの物理削除に失敗しました。');
			$C->MailSender->rollback();
			return false;
		}

		// コミットしてメッセージ設定
		$C->Mail->commit();
		$C->Session->write('system-message', 'メール送信データを削除しました。');
		return true;
	}


	/**
	 * 保存処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @return   bool    処理結果真偽値
	 */
	function save() {
		$C       =& $this->Controller;
		$mode    =  (empty($C->data['Mail']['id'])) ? '追加' : '更新';
		$errFlag = false;

		/* メール保存処理 */
		$C->data['Mail']['status'] = 0;
		// send_datetimeのデータを保存するために日付と時間を統合
		$C->data['Mail']['send_datetime'] = $C->data['Mail']['send_date']. ' '. $C->data['Mail']['send_time']. ':00';
		// 配信状態を現日付時刻と比較して更新
		$now = date('Y/m/d H:i:s');
		if (strtotime($now) >= strtotime($C->data['Mail']['send_datetime'])) {
			$C->data['Mail']['status'] = 1;
		}
		$C->Mail->create($C->data);

		// 入力チェック
		if (!$C->Mail->validates()){
			return false;
		}

		$C->Mail->begin();

		$C->Mail->checkUpdated = true;	// 更新時間チェックON

		// 保存
		if (!$C->Mail->save(null, false)){
			$C->Mail->invalidate('error', "メールデータの{$mode}に失敗しました。");
			$C->Mail->rollback();
			return false;
		}

		// メールID取得
		$mailId = $C->Mail->getID();

		/* メール配信処理 */
		foreach ($C->data['MailSender'] as $key => $val) {
			if (!empty($val['id'])) $saveData['id'] = $val['id'];
			$saveData['mail_id'] = $mailId;
			$saveData['customer_id'] = $key;
			$saveData['send_flag'] = $val['send_flag'];

			// 顧客情報じゃなければスルーする
			if ($saveData['customer_id'] == -1){
				continue;
			}
			// 保存
			$C->MailSender->create($saveData);
			if (!$C->MailSender->save(null, false)){
				$C->MailSender->invalidate('error', "メール配信データの{$mode}に失敗しました。");
				$C->MailSender->rollback();
				return false;
			}
		}

		$C->Session->write('system-message', "メールデータ、メール配信データを{$mode}しました。");
		$C->Mail->commit();
		return true;
	}


	/**
	 * 初期データ取得
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note     編集画面の初期値設定で使用
	 * @return   arr    初期データ配列
	 */
	function getIni() {
		$data = array();

		return $data;
	}



}

?>