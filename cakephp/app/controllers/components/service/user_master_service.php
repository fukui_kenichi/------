<?php
/**
 * ユーザーマスターテーブルサービスコンポーネント
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note     コントローラーとモデルの間でイベント処理を記述するクラス
 */
class UserMasterServiceComponent extends BasicServiceComponent {

	/**
	 * 検索条件作成
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note     ページネート用検索条件作成
	 * @param    arr    $data               入力された検索条件
	 * @param    arr    $defaultPaginate    デフォルト検索条件
	 */
	function createParams($data, $defaultPaginate) {
		$C    =& $this->Controller;
		$cond =  array();

		// デフォルトの検索条件取得
		if (isset($defaultPaginate['UserMaster']['conditions'])) {
			$cond = $defaultPaginate['UserMaster']['conditions'];
		}

		// 検索条件追加-------------------------------------------


		$defaultPaginate['UserMaster']['conditions'] = $cond;

		// 検索条件を一旦セッションに保存
		$C->HoldPaginate->saveParams($defaultPaginate);
	}


	/**
	 * 一覧画面用データ取得・整形関数
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/18
	 * @return   array    整形済みの単位一覧画面用データ
	 */
	function getDataByIndex() {
		$C       =& $this->Controller;
		$returnData = array();	// 返却用配列

		// DBより必要なデータを取得します
		$result = $C->UserMaster->getDataByIndex();

		if (!empty($result)){
			// データを整形します
			foreach ($result as $key => $val) {
				$returnData['UserMaster'][$key] = $val['UserMaster'];
				$returnData['UserMaster'][$key]['db_id'] = $val['UserMaster']['id'];
			}
		}
		$returnData['Master']['newRowNo'] = -1;

		// 整形したデータを返します
		return $returnData;
	}


	/**
	 * 削除処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * $param    int    $id     削除したいデータの主キー
	 * @return   bool    処理結果真偽値
	 */
	function delete($id) {
		$C =& $this->Controller;

		// 入力チェック
		if (!$C->UserMaster->deleteValidates($id)){
			return false;
		}
		// 削除
		if (!$C->UserMaster->delete($id)){
			$C->UserMaster->invalidate('error', 'データの削除に失敗しました。');
			return false;
		}

		$C->Session->write('system-message', 'データを削除しました。');
		return true;
	}


	/**
	 * 一覧保存処理
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/18
	 * @return   bool    処理結果真偽値
	 */
	function saveByIndex() {
		$C       =& $this->Controller;
		$userChangeFlag = false;

		if (isset($C->data['UserMaster']['ZZZ'])) unset($C->data['UserMaster']['ZZZ']); // Viewのテンプレートは項目削除

		// 入力チェック
		$C->UserMaster->create($C->data);
		if (!$C->UserMaster->validatesByIndex()){
			return false;
		}

		$C->UserMaster->begin();

		foreach ($C->data['UserMaster'] as $key => $val) {
			if (isset($val['db_id']) && empty($val['id'])) { // 削除
				// UserMasterのデータ削除
				$C->UserMaster->delete($val['db_id']);

				// Userのフィールド削除
				$sql = 'ALTER TABLE users DROP free'. $val['db_id']. ';';
				if ($C->User->query($sql) === false) {
					$C->User->invalidate('error', 'Userテーブルのfield項目更新に失敗しました。');
					$C->User->rollback();
					return false;
				}

				// Userを変更したフラグを立てる
				if ($userChangeFlag == false) $userChangeFlag = true;

			} else { // 追加・更新
				// UserMasterデータ保存
				$saveData = $val;
				$saveData['select_ti'] = mb_rtrim($saveData['select_ti'], "\r\n"); // 最後の改行を除去

				$C->UserMaster->create($saveData);
				// 保存
				if (!$C->UserMaster->save(null, false)){
					$C->UserMaster->invalidate('error', "データの保存に失敗しました。");
					$C->UserMaster->rollback();
					return false;
				}

				// 新規の場合はUserにフィールド追加
				if (empty($val['id']) && empty($val['db_id'])) {
					// cusotmer_mastersテーブルのID
					$userMasterId = $C->UserMaster->getID();

					// Userのフィールド名追加
					$sql = "UPDATE user_masters SET field = 'free{$userMasterId}' WHERE id = {$userMasterId};";
					if ($C->UserMaster->query($sql) === false) {
						$C->UserMaster->invalidate('error', 'UserMasterテーブルのfield項目更新に失敗しました。');
						$C->UserMaster->rollback();
						return false;
					}

					// Userにフィールド追加のSQL作成
					$sql = 'ALTER TABLE users ADD free'. $userMasterId;
					switch ($val['type']) {
						case MASTER_TYPE_CHAR:		// 文字列（制限あり）
						case MASTER_TYPE_LIST:		// リスト型
						case MASTER_TYPE_RADI:		// ラジオ型
						case MASTER_TYPE_CHEK:		// チェック型
						case MASTER_TYPE_FIMG:		// ファイル（イメージ）
						case MASTER_TYPE_FILE:		// ファイル（その他）
						case MASTER_TYPE_TELE:		// 電話型
							$sql .= ' VARCHAR(256)';
							break;
						case MASTER_TYPE_TEXT:		// 文字列（制限なし）
							$sql .= ' TEXT';
							break;
						case MASTER_TYPE_INTG:		// 数値型
							$sql .= ' FLOAT(10,2)';
							break;
						case MASTER_TYPE_DTTM:		// 日付時刻型
							$sql .= ' DATETIME';
							break;
						case MASTER_TYPE_DATE:		// 日付型
							$sql .= ' DATE';
							break;
						case MASTER_TYPE_TIME:		// 時間型
							$sql .= ' TIME';
							break;
						default :
							$sql .= ' VARCHAR(256)';
					}

					// Userへのフィールド追加を実行
					$sql .= ";";
					if ($C->User->query($sql) === false) {
						$C->User->invalidate('error', 'Userテーブルへのフィールド追加に失敗しました。');
						$C->User->rollback();
						return false;
					}

					// Userを変更したフラグを立てる
					if ($userChangeFlag == false) $userChangeFlag = true;
				}
			}
		}

		// 表示番号の並び替え
		if (!$C->UserMaster->sortRank()){
			$C->UserMaster->invalidate('error', '並び順の整列に失敗しました。');
			$C->UserMaster->rollback();
			return false;
		}

		// UserReferの更新
		if (!$C->UserRefer->updateAll()){
			$C->UserRefer->rollback();
			return false;
		}

		// Userテーブルの項目が追加・削除されている場合
		if ($userChangeFlag){ // Userを変更したらtmp/cache/models/以下のファイルを削除4
			$modelFileDir = TMP. 'cache/models/';
			if (delDirFiles($modelFileDir) === false) {
				$C->UserMaster->invalidate('error', 'tmp/cache/models以下のファイルの削除に失敗しました。');
				$C->UserMaster->rollback();
				return false;
			}
		}

		//コミット
		$C->UserMaster->commit();

		// 変更コミット
		$C->Session->write('system-message', "データを更新・保存しました。");

		return true;
	}


	/**
	 * 保存処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @return   bool    処理結果真偽値
	 */
	function save() {
		$C       =& $this->Controller;
		$mode    =  (empty($C->data['UserMaster']['id'])) ? '追加' : '更新';
		$errFlag = false;

		$C->UserMaster->create($C->data);

		// 入力チェック
		if (!$C->UserMaster->validates()){
			$errFlag = true;
		}

		if ($errFlag) {
			return false;
		}

		$C->UserMaster->begin();

		$C->UserMaster->checkUpdated = true;	// 更新時間チェックON

		// 保存
		if (!$C->UserMaster->save(null, false)){
			$C->UserMaster->invalidate('error', "データの{$mode}に失敗しました。");
			$C->UserMaster->rollback();
			return false;
		}

		$C->Session->write('system-message', "データを{$mode}しました。");
		$C->UserMaster->commit();
		return true;
	}


	/**
	 * 初期データ取得
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note     編集画面の初期値設定で使用
	 * @return   arr    初期データ配列
	 */
	function getIni() {
		$data = array();

		return $data;
	}



}

?>