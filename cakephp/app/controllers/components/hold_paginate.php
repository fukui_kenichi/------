<?php
/**
 * ページネート保持コンポーネント
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note     ページネートの検索条件を保持するクラス
 *          （編集画面から一覧へ戻ってきた時等で使用）
 */
class HoldPaginateComponent extends Object {

	/**
	 * 初期処理
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     controllerのbeforeFilterメソッドの前に呼び出される
	 * @param    obj    $controller    コントローラオブジェクト
	 */
	function initialize(&$controller) {
		$this->Controller =& $controller;
	}


	/**
	 * 初期処理
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     controllerのbeforeFilterメソッドの後に呼び出される
	 * @param    obj    $controller    コントローラオブジェクト
	 */
	function startup(&$controller) {
		$C               =& $this->Controller;
		$search_box_disp = false;					// 検索ボックス表示フラグ

		//検索条件、ページング保持初期化(GETに「search:clear」があると保持解除)
		if (isset($C->passedArgs['search']) && empty($C->data)) {
			$C->Session->delete('holdPaginate');
			$C->Session->delete('searchForm');
			$search_box_disp = true;
		}

		// 検索ボックス表示フラグ
		$C->set('search_box_disp', $search_box_disp);
	}


	/**
	 * ページネート情報セット
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     検索条件をセッション→デフォルト（引数）→コントローラ（初期設定）の
	 *           優先順位でコントローラーのページネートに設定
	 * @param    arr    $defaultPaginate    ページネート検索条件
	 */
	function setPaginate($defaultPaginate = null) {
		$C            =& $this->Controller;
		$holdPaginate =  $C->Session->read('holdPaginate');

		// セッションに現在のページのページネート情報がある場合はセッションのページネート情報を使用
		if (isset($holdPaginate[$C->name][$C->action])) {
			$C->paginate = $holdPaginate[$C->name][$C->action];
		} else {
			// デフォルトのページネート情報がある場合はデフォルトのページネート情報を使用
			if (isset($defaultPaginate)) {
				$C->paginate = $defaultPaginate;
			}
		}
	}


	/**
	 * ページネート情報保持
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     現在のページネート検索条件をセッションに保存
	 *           コントローラーで$this->paginate();を実行した後に使用
	 *           ページ番号を含めて保存する
	 */
	function savePaginate() {
		$C            =& $this->Controller;
		$holdPaginate =  array();
		$paging       =  $C->params['paging'];
		$pagingArr    =  array();

		foreach ($paging as $model => $model_val) {
			foreach ($model_val['options'] as $key => $val) {
				$pagingArr[$model][$key] = $val;
			}
		}

		$holdPaginate[$C->name][$C->action] = $pagingArr;
		$C->Session->write('holdPaginate', $holdPaginate);
	}


	/**
	 * 検索条件情報保存
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     検索条件作成時に使用。検索条件をセッションに保存
	 * @param    arr    $paginate    ページネート情報
	 */
	function saveParams($paginate) {
		$C			  =& $this->Controller;
		$holdPaginate =	 array();

		$holdPaginate[$C->name][$C->action] = $paginate;
		$C->Session->write('holdPaginate', $holdPaginate);
	}


	/**
	 * 検索条件入力内容セット
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     検索条件のフォームの値をセッションから取得
	 *           編集画面から一覧に戻った時に検索フォームの値を保持したい時に使用
	 * @param    str    $modelName    検索条件が入力されるモデル名
	 */
	function setSearchForm($modelName = 'Search') {
		$C          =& $this->Controller;
		$searchForm =  $C->Session->read('searchForm');

		if (isset($searchForm[$C->name][$C->action])) {
			//既に現在のページの検索情報がある場合はセッション情報を使用
			$C->data[$modelName] = $searchForm[$C->name][$C->action];
		} else {
			//ない場合は新しくデフォルトのページング情報を使用
			$C->data[$modelName] = null;
		}
	}


	/**
	 * 検索条件入力内容保持
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note     検索条件のフォームの値をセッションに保存
	 *           編集画面から一覧に戻った時に検索フォームの値を保持したい時に使用
	 * @param    str    $modelName    検索条件が入力されるモデル名
	 */
	function saveSearchForm($modelName = 'Search') {
		$C          =& $this->Controller;
		$searchForm =  array();

		if ($C->data[$modelName]) {
			$searchForm[$C->name][$C->action] = $C->data[$modelName];
		}

		$C->Session->write('searchForm', $searchForm);
	}

}

?>