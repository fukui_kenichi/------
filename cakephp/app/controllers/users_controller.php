<?php
/**
 * ユーザーテーブルコントローラー
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note
 */
class UsersController extends AppController {
	var $name = 'Users';
	var $uses = array('User', 'UserMaster', 'UserRefer', 'Upload', 'CustomerKojin', 'BusiKojin', 'Favorite');
	var $components = array('UserService', 'UploadService');
	var $paginate = array(
		'User' => array(
			'fields' => array(
				'*',
			),
			'conditions' => array('User.del_flag' => 0, 'User.mente_flag' => 0),
			'order' => array('User.id asc'),
			'limit' => 10,
			'recursive' => -1,
		)
	);


	/**
	 * 初期処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function beforeFilter() {
		parent::beforeFilter();

		// ここに追加検索初期値があれば定義
		//$this->paginate['User']['condition']['xxx'] = 'xxx';
	}


	/**
	 * ユーザーテーブル一覧ページ
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function index() {
		// 戻る
		if (isset($this->params['form']['cancel_x'])) {
			$prevPage = $this->Session->read('prevPage');
			if (!empty($prevPage)){
				$this->redirect("/{$prevPage['url']}");
			}else{
				$this->redirect("/top/index");
			}
		}

		// ページ取得
		$page = null;
		if(isset($this->passedArgs['page'])){
			$page = $this->passedArgs['page'];
		}
		// ソートキー取得
		$sort = null;
		if(isset($this->passedArgs['sort'])){
			$sort = $this->passedArgs['sort'];
		}
		// 並び順取得
		$direction = null;
		if(isset($this->passedArgs['direction'])){
			$direction = $this->passedArgs['direction'];
		}
		// ソートキー取得
		$g_sort = null;
		if(isset($this->passedArgs['g_sort'])){
			$sort = $this->passedArgs['g_sort'];
		}
		// 表示件数
		$limit = null;
		if(isset($this->passedArgs['limit'])){
			$limit = $this->passedArgs['limit'];
			$this->paginate['Customer']['limit'] = $limit;
		}
		$this->set('page',$page);
		$this->set('sort',$sort);
		$this->set('direction',$direction);
		$this->set('g_sort',$g_sort);
		$this->set('limit',$limit);

		// 検索条件作成
		if (isset($this->params['form']['search'])) {
			$this->UserService->createParams($this->data['Search'], $this->paginate);
		}

		// ページ切替
		if (isset($this->params['form']['refresh'])) {
			// ページリミット設定
			$this->HoldPaginate->saveParams($this->paginate);
			if (!empty($this->params['form']['page_num']) && ereg("^[0-9]+$", $this->params['form']['page_num'])){
				$this->redirect('/users/index/page:' . $this->params['form']['page_num'] . "/limit:" . $this->params['form']['page_row']);
			}
		}

		$this->HoldPaginate->setPaginate();				// ページネート情報取得
		$list = $this->paginate();						// 一覧データ取得
		$this->HoldPaginate->savePaginate();			// ページネート情報保持
		$this->set('list', $list);
		$this->__viewSet();
		$holdPaginate =  $this->Session->read('holdPaginate');

	}


	/**
	 * ユーザーテーブル編集ページ
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @param    int    $id    ユーザーテーブルID
	 */
	function edit($id = null) {

		// 戻る
		if (isset($this->params['form']['cancel_x'])) {
			$this->redirect('/users/index');
		}

		// 登録
		if (isset($this->params['form']['save_x'])) {
			if ($this->UserService->save()){
				$this->redirect('/users/index');
			}
		}

		// 編集開始時のデータ読み込み
		if (empty($this->data) && $id != "") {
			$params = array(
				'conditions' => array('User.id' => $id),
				'fields' => array('User.*'),
			);
			$this->data = $this->User->find('first', $params);
			if (!$this->data || !empty($this->data['User']['del_flag'])) {
				$this->redirect('/users/index');
			}
			// パスワードの分割
			$this->data['User']['password_01'] = $this->data['User']['password'];
			$this->data['User']['password_02'] = $this->data['User']['password'];
			// UserMastar個別の設定
			$this->UserService->setUserByUserMaster($this->data);
			// イメージ、イメージではないファイル情報をセットする。
			$this->UploadService->setUploadFile('users',$this->data['User']['id'],$this->data);
		}

		// 初期設定
		if (empty($this->data)) {
			$this->data = $this->UserService->getIni(); //初期値設定
		}
		$this->__viewSet();
	}


	/**
	 * ユーザーテーブル編集ページ
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @param    int    $id    ユーザーテーブルID
	 */
	function detail($id = null) {
		// 編集開始時のデータ読み込み
		if (empty($this->data) && $id != "") {
			$params = array(
				'conditions' => array('User.id' => $id),
				'fields' => array('User.*'),
			);
			$this->data = $this->User->find('first', $params);
			if (!$this->data || !empty($this->data['User']['del_flag'])) {
				$this->redirect('/users/index');
			}
			// パスワードの分割
			$this->data['User']['password_01'] = $this->data['User']['password'];
			$this->data['User']['password_02'] = $this->data['User']['password'];
			// UserMastar個別の設定
			$this->UserService->setUserByUserMaster($this->data);
			// イメージ、イメージではないファイル情報をセットする。
			$this->UploadService->setUploadFile('users',$this->data['User']['id'],$this->data);
		}

		// 初期設定
		if (empty($this->data)) {
			$this->data = $this->UserService->getIni(); //初期値設定
		}
		$this->__viewSet();
	}


	/**
	 * ユーザーテーブル編集ページ
	 * @author   nakata@okushin.co.jp
	 * @date     2015/03/03
	 * @note
	 * @param    int    $id    ユーザーテーブルID
	 */
	function del_conf($id = null) {
		// 戻る
		if (isset($this->params['form']['cancel_x'])) {
			$this->redirect('/users/index');
		}

		// 削除
		if (isset($this->params['form']['del_x'])) {
			if ($this->UserService->delFlag($id)) {
				$this->redirect('/users/index');
			}
		}

		// 編集開始時のデータ読み込み
		if (empty($this->data) && $id != "") {
			$params = array(
				'conditions' => array('User.id' => $id),
				'fields' => array('User.*'),
			);
			$this->data = $this->User->find('first', $params);
			if (!$this->data || !empty($this->data['User']['del_flag'])) {
				$this->redirect('/users/index');
			}
			// パスワードの分割
			$this->data['User']['password_01'] = $this->data['User']['password'];
			$this->data['User']['password_02'] = $this->data['User']['password'];
			// UserMastar個別の設定
			$this->UserService->setUserByUserMaster($this->data);
			// イメージ、イメージではないファイル情報をセットする。
			$this->UploadService->setUploadFile('users',$this->data['User']['id'],$this->data);
		}

		// 初期設定
		if (empty($this->data)) {
			$this->data = $this->UserService->getIni(); //初期値設定
		}
		$this->__viewSet();
	}


	/**
	 * ユーザーテーブル編集ページ
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 * @param    int    $id    ユーザーテーブルID
	 */
	function __viewSet() {
		$params = array(
			'fields' => array("*"),
			'conditions' => array(),
			'order' => array('UserMaster.number asc'),
			);
		$userMst = $this->UserMaster->find('all', $params);
		$this->set('userMst', $userMst);
	}


	/**
	 * 画像アップロード
	 * @author   nishishita@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function upload() {
		$this->layout = 'ajax';
		/// レンダリングなし
		$this->autoRender = false;
		/// debugコードを非表示
		Configure::write('debug', 0);
		$extStr = '(png|jpg|jpeg)';	// アップロードできる拡張子

		// GETの値からどのアップロードか取得
		$koumoku = $this->params['url']['koumoku'];
		$this->set('koumoku', $koumoku);
		$model = $this->params['url']['model'];
		$this->set('model', $model);
		if (!$this->data[$model.'Edit']['up_'.$koumoku]) {
			$this->set('error', 'アップロードするファイルを選択してください');
			$this->render('upload','ajax');
		} else {
			$filename = '';
			$path_parts = pathinfo($this->data[$model.'Edit']['up_'.$koumoku]['name']);
			$ext = pathinfo($this->data[$model.'Edit']['up_'.$koumoku]['name'], PATHINFO_EXTENSION);
			// 拡張子チェック
			if (!preg_match('/'. $extStr. '/i', $ext)) {
				$extErr = preg_replace('/\|/', ' | ', $extStr);
				$this->set('error', "アップロードできない拡張子のファイルがあります。　※アップロード可能：{$extErr}");
				$this->render('upload','ajax');
				return;
			}
			// 最大文字数
			if (!$this->User->mb_maxLength($this->data[$model.'Edit']['up_'.$koumoku]['name'], 200)) {
				$this->set('error', "200文字以下のファイル名のファイルを選択してください。");
				$this->render('upload','ajax');
				return;
			}
			// マックスファイルサイズチェック
			if ($this->data[$model.'Edit']['up_'.$koumoku]['size'] > MAX_IMAGE_FILE_SIZE) {
				$max_size = MAX_IMAGE_FILE_SIZE / 1000000;
				$this->set('error', "ファイルサイズが大きすぎるため、アップロードできません。{$max_size}MB以下のファイルを選択してください。");
				$this->render('upload','ajax');
				return;
			}
			$filename = uniqid(rand(0,1000)). '.' . $ext;
			$upName = $this->data[$model.'Edit']['up_'.$koumoku]['name'];
			if ($filename) {
				$upPath = ROOT_PATH . 'picture/temp/';
				if (is_writeable($upPath)) {
					$this->User->upImageUser($this->data[$model.'Edit']['up_'.$koumoku]['tmp_name'],$filename,'temp');
					rename($this->data[$model.'Edit']['up_'.$koumoku]['tmp_name'], $upPath . $filename);
					@chmod($upPath . $filename,0777);

					$this->set('filename', $filename);
					$upFile = ROOT_URL . 'picture/temp/';
					$fileNm = pathinfo($filename, PATHINFO_FILENAME);
					$this->set('upFile', $upFile . $fileNm . '-s.jpg');
					$this->render('upload','ajax');
				} else {
					$this->set('error', '画像フォルダ(' . $upPath . ')に書き込みできません。');
					$this->render('upload','ajax');
				}
			}
		}
	}

    //--------------------------------------------------------------------------------
	// 画像の取り消し
	//--------------------------------------------------------------------------------
	function delete() {
		$this->layout = 'ajax';
		/// レンダリングなし
		$this->autoRender = false;
		/// debugコードを非表示
		Configure::write('debug', 0);

		// GETの値からどのアップロードか取得
		$koumoku = $this->params['url']['koumoku'];
		$this->set('koumoku', $koumoku);
		$model = $this->params['url']['model'];
		$this->set('model', $model);
		$this->render('delete','ajax');
	}


}
?>