<?php
/**
 * 商談テーブルコントローラー
 * @author   fukui@okushin.co.jp
 * @date     2015/02/10
 * @note
 */
class BusiesController extends AppController {
	var $name = 'Busies';
	var $uses = array('Busy', 'BusiMaster', 'BusiRefer', 'Upload', 'User', 'UserRefer', 'Customer', 'CustomerRefer', 'CustomerMaster', 'BusiKojin', 'Calender');
	var $components = array('BusyService', 'UploadService', 'CustomerService');
	var $paginate = array(
		'Busy' => array(
			'fields' => array('*'),
			'order' => array('Busy.busi_date DESC', 'Busy.id ASC'),
			'limit' => 10,
			'recursive' => -1,
			'joins' => array(
				array(
					'type' => 'left',
					'table' => 'users',
					'alias' => 'User',
					'conditions' => 'User.id = Busy.user_id'
				),
			)
		)
	);


	/**
	 * 初期処理
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function beforeFilter() {
		parent::beforeFilter();

		// ここに追加検索初期値があれば定義
		//$this->paginate['Busy']['condition']['xxx'] = 'xxx';
	}

	/**
	 * 顧客テーブル参照・商談テーブル一覧ページ
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/26
	 * @note
	 * @param    int    $id    ユーザーテーブルID
	 */
	function index($id = null) {
		// 顧客IDはここでセットしておく
		$this->paginate['Busy']['conditions']['Busy.customer_id'] = $id;

		// 戻る
		if (isset($this->params['form']['cancel_x'])) {
			$prevPage = $this->Session->read('prevPage');
			if (!empty($prevPage)){
				// 顧客地図情報から来てたら経度・緯度を引数に渡す
				if ($prevPage['name'] == 'Maps'){
					$this->redirect("/{$prevPage['url']}/{$this->data['Customer']['latitude']}/{$this->data['Customer']['longitude']}");
				}else{
					$this->redirect("/{$prevPage['url']}");
				}
			}else{
				$this->redirect("/customers/index");
			}
		}

		// 編集開始時のデータ読み込み
		if (empty($this->data) && $id != "") {
			$params = array(
				'conditions' => array('Customer.id' => $id),
				'fields' => array('Customer.*'),
			);
			$this->data = $this->Customer->find('first', $params);
			if (!$this->data || !empty($this->data['Customer']['del_flag'])) {
				$this->redirect('/customers/index');
			}
			// CustomerMastar個別の設定
			$this->CustomerService->setCustomerByCustomerMaster($this->data);

			// イメージ、イメージではないファイル情報をセットする。
			$this->CustomerService->setUploadFile($this->data['Customer']['id'], $this->data);
		}

		// 初期設定
		if (empty($this->data)) {
			$this->data = $this->CustomerService->getIni(); //初期値設定
		}
		// 商談一覧のリスト準備
		if (!empty($id)) {
			// ページ取得
			$page = null;
			if(isset($this->passedArgs['page'])){
				$page = $this->passedArgs['page'];
			}
			// ソートキー取得
			$sort = null;
			if(isset($this->passedArgs['sort'])){
				$sort = $this->passedArgs['sort'];
			}
			// 並び順取得
			$direction = null;
			if(isset($this->passedArgs['direction'])){
				$direction = $this->passedArgs['direction'];
			}
			$this->set('page',$page);
			$this->set('sort',$sort);
			$this->set('direction',$direction);

			// ページ切替
			if (isset($this->params['form']['refresh'])) {
				// ページリミット設定
				$this->paginate['Busy']['limit'] = $this->params['form']['page_row'];
				$this->HoldPaginate->saveParams($this->paginate);
				if (!empty($this->params['form']['page_num']) && ereg("^[0-9]+$", $this->params['form']['page_num'])){
					$this->redirect("/busies/index/{$id}/page:" . $this->params['form']['page_num']);
				}
			}
			$this->HoldPaginate->setPaginate();				// ページネート情報取得
			$list = $this->paginate('Busy');						// 一覧データ取得
			$this->HoldPaginate->savePaginate();			// ページネート情報保持
			$this->set('list', $list);
			$holdPaginate =  $this->Session->read('holdPaginate');
			$this->set('limit',$holdPaginate[$this->name][$this->action]['Busy']['limit']); // 行数
		}

		$this->__setViewByIndex();	// viewに必要な情報をセット

	}


	/**
	 * 顧客テーブル情報ページに必要な変数設定
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function __setViewByIndex() {
		// CustomerMaster
		$customerMst = $this->CustomerMaster->find('all', array('order'=>array('CustomerMaster.number asc')));
		$this->set('customerMst', $customerMst);

		// BusiKojin
		$busiKojin = $this->BusiKojin->getDataByUserId($this->login['User']['id']);
		$this->set('busiKojin', $busiKojin);
	}


	/**
	 * 商談テーブル編集ページ
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/27
	 * @note
	 * @param    int    $id    商談テーブルID
	 */
	function edit($id = null) {
		// 戻る
		if (isset($this->params['form']['cancel_x'])) {
			if (isset( $this->data['Busy']['customer_id'])) {
				$this->redirect('/busies/index/'. $this->data['Busy']['customer_id']);
			}
		}

		// 登録
		if (isset($this->params['form']['save_x'])) {
			if ($this->BusyService->save()){
				if (isset( $this->data['Busy']['customer_id'])) {
					$this->redirect('/busies/index/'. $this->data['Busy']['customer_id']);
				}
			}
		}

		// 編集開始時のデータ読み込み
		if (empty($this->data) && $id != "") {
			$params = array(
				'conditions' => array('Busy.id' => $id),
				'fields' => array('Busy.*'),
			);
			$this->data = $this->Busy->find('first', $params);
			if (!$this->data || !empty($this->data['Busy']['del_flag'])) {
				$this->redirect('/busies/index');
			}

			// BusyMastar個別の設定
			$this->BusyService->setBusyByBusiMaster($this->data);

			// イメージ、イメージではないファイル情報をセットする。
			$this->BusyService->setUploadFile($this->data['Busy']['id'],$this->data);
		}

		// 初期設定
		if (empty($this->data)) {
			if (!empty($this->passedArgs['customer_id'])) {
				$this->data = $this->BusyService->getIni($this->passedArgs['customer_id']); //初期値設定
			}
			// カレンダーのセッションがあれば初期値で持たしておく
			$calender = $this->Session->read('calender');
			if (!empty($calender)){
				$this->Session->delete('calender');
				$this->data['Busy']['busi_date'] = $calender['busi_date'];
				$this->data['Busy']['memo'] = $calender['memo'];
				$this->data['Busy']['start_time'] = $calender['start_time'];
				$this->data['Busy']['end_time'] = $calender['end_time'];
			}
		}

		$this->__setViewByEdit();	// viewに必要な情報をセット

	}


	/**
	 * 商談テーブル編集ページに必要な変数準備
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/27
	 * @note
	 */
	function __setViewByEdit() {
		// BusiMaster
		$busyMst = $this->BusiMaster->find('all', array('order'=>array('BusiMaster.number ASC')));
		$this->set('busyMst', $busyMst);

		// User
		if (empty($this->data['Busy']['user_id'])) {
			$userArr = $this->User->getUserArr();
		} else {
			$userArr = $this->User->getUserArr($this->data['Busy']['user_id']);
		}
		$this->set('userArr', $userArr);
	}


	/**
	 * 商談テーブル参照ページ
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/26
	 * @note
	 * @param    int    $id    ユーザーテーブルID
	 */
	function detail($id = null) {
		// 戻る
		if (isset($this->params['form']['cancel_x'])) {
			if (isset( $this->data['Busy']['customer_id'])) {
				$this->redirect('/busies/index/'. $this->data['Busy']['customer_id']);
			}
		}

		// 編集開始時のデータ読み込み
		if (empty($this->data) && $id != "") {
			$params = array(
				'conditions' => array('Busy.id' => $id),
				'fields' => array('Busy.*'),
			);
			$this->data = $this->Busy->find('first', $params);
			if (!$this->data || !empty($this->data['Busy']['del_flag'])) {
				$this->redirect('/busies/index');
			}
			// BusyMastar個別の設定
			$this->BusyService->setBusyByBusiMaster($this->data);

			// イメージ、イメージではないファイル情報をセットする。
			$this->BusyService->setUploadFile($this->data['Busy']['id'], $this->data);
		}

		$this->__setViewByDetail();	// viewに必要な情報をセット

	}


	/**
	 * 商談テーブル情報ページに必要な変数設定
	 * @author   fukui@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function __setViewByDetail() {
		// BusiMaster
		$busyMst = $this->BusiMaster->find('all', array('order'=>array('BusiMaster.number asc')));
		$this->set('busyMst', $busyMst);

		// User
		$userArr = $this->User->getUserArr($this->data['Busy']['user_id']);
		$this->set('userArr', $userArr);
	}


	/**
	 * 商談テーブル参照ページ
	 * @author   nakata@okushin.co.jp
	 * @date     2015/02/26
	 * @note
	 * @param    int    $id    ユーザーテーブルID
	 */
	function del_conf($id = null) {
		// 戻る
		if (isset($this->params['form']['cancel_x'])) {
			if (isset( $this->data['Busy']['customer_id'])) {
				$this->redirect('/busies/index/'. $this->data['Busy']['customer_id']);
			}
		}

		// 削除
		if (isset($this->params['form']['del_x'])) {
			if ($this->BusyService->delete($id)) {
				$this->redirect('/busies/index/'. $this->data['Busy']['customer_id']);
			}
		}

		// 編集開始時のデータ読み込み
		if (empty($this->data) && $id != "") {
			$params = array(
				'conditions' => array('Busy.id' => $id),
				'fields' => array('Busy.*'),
			);
			$this->data = $this->Busy->find('first', $params);
			if (!$this->data || !empty($this->data['Busy']['del_flag'])) {
				$this->redirect('/busies/index');
			}
			// BusyMastar個別の設定
			$this->BusyService->setBusyByBusiMaster($this->data);

			// イメージ、イメージではないファイル情報をセットする。
			$this->BusyService->setUploadFile($this->data['Busy']['id'], $this->data);
		}

		$this->__setViewByDetail();	// viewに必要な情報をセット

	}


	/**
	 * 画像アップロード
	 * @author   nishishita@okushin.co.jp
	 * @date     2015/02/10
	 * @note
	 */
	function upload() {
		$this->layout = 'ajax';
		/// レンダリングなし
		$this->autoRender = false;
		/// debugコードを非表示
		Configure::write('debug', 0);
		$extStr = '(png|jpg|jpeg)';	// アップロードできる拡張子

		// GETの値からどのアップロードか取得
		$koumoku = $this->params['url']['koumoku'];
		$this->set('koumoku', $koumoku);
		$model = $this->params['url']['model'];
		$this->set('model', $model);
		if (!$this->data[$model.'Edit']['up_'.$koumoku]) {
			$this->set('error', 'アップロードするファイルを選択してください');
			$this->render('upload','ajax');
		} else {
			$filename = '';
			$path_parts = pathinfo($this->data[$model.'Edit']['up_'.$koumoku]['name']);
			$ext = pathinfo($this->data[$model.'Edit']['up_'.$koumoku]['name'], PATHINFO_EXTENSION);
			// 拡張子チェック
			if (!preg_match('/'. $extStr. '/i', $ext)) {
				$extErr = preg_replace('/\|/', ' | ', $extStr);
				$this->set('error', "アップロードできない拡張子のファイルがあります。　※アップロード可能：{$extErr}");
				$this->render('upload','ajax');
				return;
			}
			// 最大文字数
			if (!$this->User->mb_maxLength($this->data[$model.'Edit']['up_'.$koumoku]['name'], 200)) {
				$this->set('error', "200文字以下のファイル名のファイルを選択してください。");
				$this->render('upload','ajax');
				return;
			}
			// マックスファイルサイズチェック
			if ($this->data[$model.'Edit']['up_'.$koumoku]['size'] > MAX_IMAGE_FILE_SIZE) {
				$max_size = MAX_IMAGE_FILE_SIZE / 1000000;
				$this->set('error', "ファイルサイズが大きすぎるため、アップロードできません。{$max_size}MB以下のファイルを選択してください。");
				$this->render('upload','ajax');
				return;
			}
			$filename = uniqid(rand(0,1000)). '.' . $ext;
			$upName = $this->data[$model.'Edit']['up_'.$koumoku]['name'];
			if ($filename) {
				$upPath = ROOT_PATH . 'picture/temp/';
				if (is_writeable($upPath)) {
					$this->Busy->upImageBusy($this->data[$model.'Edit']['up_'.$koumoku]['tmp_name'],$filename,'temp');
					rename($this->data[$model.'Edit']['up_'.$koumoku]['tmp_name'], $upPath . $filename);
					@chmod($upPath . $filename,0777);

					$this->set('filename', $filename);
					$upFile = ROOT_URL . 'picture/temp/';
					$fileNm = pathinfo($filename, PATHINFO_FILENAME);
					$this->set('upFile', $upFile . $fileNm . '-s.jpg');
					$this->render('upload','ajax');
				} else {
					$this->set('error', '画像フォルダ(' . $upPath . ')に書き込みできません。');
					$this->render('upload','ajax');
				}
			}
		}
	}


    //--------------------------------------------------------------------------------
	// 画像の取り消し
	//--------------------------------------------------------------------------------
	function delete() {
		$this->layout = 'ajax';
		/// レンダリングなし
		$this->autoRender = false;
		/// debugコードを非表示
		Configure::write('debug', 0);

		// GETの値からどのアップロードか取得
		$koumoku = $this->params['url']['koumoku'];
		$this->set('koumoku', $koumoku);
		$model = $this->params['url']['model'];
		$this->set('model', $model);
		$this->render('delete','ajax');

	}
}
?>