<?php
/**
 * SESSION設定
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note     core.phpのConfigure::write('Session.save', 'xxx');
 *           のxxx部分にこのファイル名（拡張子無し）で使用
 *           /cake/libs/session.phpのsession設定部分を任意で設定できる
 */

ini_set('session.use_trans_sid', 0);
ini_set('url_rewriter.tags', '');
ini_set('session.serialize_handler', 'php');
ini_set('session.use_cookies', 1);
ini_set('session.name', Configure::read('Session.cookie'));
//ini_set('session.cookie_lifetime', $this->cookieLifeTime);
ini_set('session.cookie_lifetime', 0);	// ブラウザ終了で破棄
ini_set('session.cookie_path', $this->path);
ini_set('session.auto_start', 0);
ini_set('session.save_path', TMP . 'sessions');
	
?>