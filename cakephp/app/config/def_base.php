<?php
/**
 * オブジェクト型定数基本クラス
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note
 */

class DefBaseClass {
	var $_dataArr;

	/**
	 * 要素の配列を返す
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @return   arr    要素の配列
	 */
	function getArr() {
		return $this->_dataArr;
	}

	/**
	 * 指定された要素の値を返す
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    int    要素キー
	 * @return   str    要素の値
	 */
	function getVal($strNo) {
		if (isset($this->_dataArr[$strNo])) {
			return $this->_dataArr[$strNo];
		} else {
			return '';
		}
	}

	/**
	 * 指定された要素の値の要素キーを返す
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @param    str    要素の値
	 * @return   int    要素キー
	 */
	function getKey($strVal) {
		return array_search($strVal, $this->_dataArr);
	}

}

?>