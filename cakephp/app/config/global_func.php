<?php
/**
 * グローバル共通関数
 * @author	 konishi@okushin.co.jp
 * @date	 2011/06/09
 * @note
 */

/**
 * ブランクチェック
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note     ブランクでない値(0, '0'も含む)は真
 *           ブランク（ \t\n\r\fのみを含む）は偽
 * @param    mix(str,int,float)    $val    検索値
 * @return   mix    真：int 1    偽：int 0, bool false
 */
function notEmpty($val) {
	if (empty($val) && $val != '0') {
		return false;
	}
	return preg_match('/[^\s]+/m', $val);
}


/**
 * ltrimマルチバイト対応版
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note     全角空白も除去 UTF-8用
 * @param    mix(str,int,float)    $val         変換したい文字列
 * @param    str                   $charlist    除去対象文字列（省略可能）
 * @return   str    変換後文字列
 */
function mb_ltrim($val, $charlist = ' 　\n\r\t\0\x0B\v' ) {
	return preg_replace('/^['. $charlist. ']*/u', '', $val);
}


/**
 * rtrimマルチバイト対応版
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note     全角空白も除去 UTF-8用
 * @param    mix(str,int,float)    $val         変換したい文字列
 * @param    str                   $charlist    除去対象文字列（省略可能）
 * @return   str    変換後文字列
 */
function mb_rtrim($val, $charlist = ' 　\n\r\t\0\x0B\v' ) {
	return preg_replace('/['. $charlist. ']*$/u', '', $val);
}


/**
 * trimマルチバイト対応版
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note
 * @param    mix(str,int,float)    $val         変換したい文字列
 * @param    str                   $charlist    除去対象文字列（省略可能）
 * @return   str    変換後文字列
 */
function mb_trim($val, $charlist = ' 　\n\r\t\0\x0B\v' ) {
	return preg_replace('/^['. $charlist. ']*(.*?)['. $charlist. ']*$/u', '$1', $val);
}


/**
 * 数値フォーマット
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note     number_format関数のブランクでnull返す板（通常は0を返す）
 * @param    mix(str,int,float)    $val              フォーマットする数値
 * @param    int                   $decimals         小数点以下の桁数（省略可能）
 * @param    str                   $dec_point        小数点を表す区切り文字（省略可能）
 * @param    str                   $thousands_sep    千位毎の区切り文字（省略可能）
 * @return   mix(null, str)    フォーマット後文字列
 */
function num_format($val, $decimals = 0, $dec_point = '.', $thousands_sep = ',') {
	if (!notEmpty($val)) {
		return null;
	}
	return number_format($val, $decimals, $dec_point, $thousands_sep);
}


/**
 * 文字列後方切り出し
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note
 * @param    str    $str         切り出し文字列
 * @param    int    $backLen     後方から何文字切り出すか
 * @param    str    $encoding    エンコード（省略可）
 * @return   str    切り出し後文字列
 */
function back_substr($str, $backLen = 3, $encoding = 'utf-8') {
	$len = mb_strlen($str, $encoding);
	return mb_substr($str, $len - $backLen, $backLen, $encoding);
}


/**
 * HTMLエスケープ
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note     HTMLエスケープ多次元配列対応版
 * @param    mix(str, arr)    $data        エスケープしたい文字列(配列)
 * @param    int              $maxLen      最大表示文字数（省略可）
 * @param    str              $afterStr    文字カットした場合に文字列の後に連結する文字列（省略可）
 * @param    bool             $mode        htmlspecialcharsのモード(省略可)
 * @return   mix(str,arr)     エスケープした文字列(配列)
 */
function esHtml($data = null, $maxLen = null, $afterStr = '...',  $mode = ENT_QUOTES){
	$arr = array();

	if (!is_array($data)) {
			$val = $data;
			if (isset($maxLen)) {
				$valLen = mb_strlen($val);
				if ($valLen > $maxLen) {
					$val = mb_substr($val, 0, $maxLen). $afterStr;
				}
			}
		return htmlspecialchars($val, $mode);
	}

	foreach ($data as $key => $val) {
		if (is_array($val)) {
			$str = esHtml($val, $maxLen, $afterStr, $mode);
		} else {
			if (isset($maxLen)) {
				$valLen = mb_strlen( $val );
				if ($valLen > $maxLen) {
					$val = mb_substr($val, 0, $maxLen). $afterStr;
				}
			}

			$str = htmlspecialchars($val, $mode);
		}
		$arr["$key"] = $str;
	}

	return $arr;
}


/**
 * SQLエスケープ(mySQL用)
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note     SQLエスケープ多次元配列対応版
 * @param    mix(str,arr)   $data    エスケープしたい文字列(配列)
 * @return   mix(str,arr)   $data    エスケープした文字列(配列)
 */
function esSql($data){
	$arr = array();

	if (!is_array($data)) {
		$val = $data;
		return mysql_real_escape_string($val);
	}

	foreach ($data as $key => $val) {
		if (is_array($val)) {
			$str = esSql($val);
		} else {
			$str = mysql_real_escape_string( $val );
		}
		$arr["$key"] = $str;
	}
	return $arr;
}


/**
 * フィールド値SQL用変換
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note
 * @param    str    $val     変換したい文字列
 * @param    str    $type    フィールドタイプ
 * @return   str    変換後文字列
 */
function formatSql($val, $type = "int") {
	$result = "";

	switch ($type) {
		case "int":
			$result = (notEmpty($val)) ? $val : "null";
			break;

		case "str":
			$result = (notEmpty($val)) ? "'{$val}'": "null";
			break;

		case "bool":
			$result = ($val == "false" || $val == "f") ? "false" : "true";
			break;

		default:
			break;
	}

	return $result;
}


/**
 * 文字列置換
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note     文字列置換（多次元配列対応版）
 * @param    mix(str,arr)    $search     検索文字列(配列)
 * @param    mix(str,arr)    $replace    置換文字列（配列）
 * @param    mix(str,arr)    $data       置換したい文字列(配列)
 * @return   mix(str,arr)    置換した文字列(配列)
 */
function reVal($search, $replace, $data){
	$arr = array();

	if (!is_array($data)) {
		return preg_replace($search, $replace, $data);
	}

	foreach ($data as $key => $val) {
		if (is_array($val)) {
			$str = reVal($search, $replace, $val);
		} else {
			$str = preg_replace($search, $replace, $val);
		}
		$arr["$key"] = $str;
	}
	return $arr;
}


/**
 * 文字列連結
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note     文字列連結（多次元配列対応版）
 * @param    mix(str,arr)    $data         連結したい文字列(配列)
 * @param    str             $separator    間に挿入する文字列（省略可）
 * @return   str    連結後文字列
 */
function joinArr($data, $separator = null){
	$str = null;

	if (!is_array($data)) {
		return $data;
	}

	foreach ($data as $key => $val) {
		if (is_array($val)) {
			$str .= joinArr($val, $separator);
		} else {
			if (notEmpty($val)) {
				$str .= $val. $separator;
			}
		}
	}

	if (notEmpty($separator)) {
		$str = mb_rtrim($str, $separator);
	}

	return $str;
}


/**
 * fgetcsvをphp5使用可能版
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note
 * @param    resource    $handle    ファイルハンドル
 * @param    int         $length    サイズ(読み込むバイト数)
 * @param    str         $d         区切り文字
 * @param    str         $e         囲み文字
 * @return   mix    データあり：arr    データなし：false
 */
function fgetcsv_reg (&$handle, $length = null, $d = ',', $e = '"') {
		$d = preg_quote($d);
		$e = preg_quote($e);
		$_line = "";
		$eof = false;
		while (($eof != true)and(!feof($handle))) {
			$_line .= (empty($length) ? fgets($handle) : fgets($handle, $length));
			$itemcnt = preg_match_all('/'.$e.'/', $_line, $dummy);
			if ($itemcnt % 2 == 0) $eof = true;
		}
		$_csv_line = preg_replace('/(?:\\r\\n|[\\r\\n])?$/', $d, trim($_line));
		$_csv_pattern = '/('.$e.'[^'.$e.']*(?:'.$e.$e.'[^'.$e.']*)*'.$e.'|[^'.$d.']*)'.$d.'/';
		preg_match_all($_csv_pattern, $_csv_line, $_csv_matches);
		$_csv_data = $_csv_matches[1];
		for($_csv_i=0;$_csv_i<count($_csv_data);$_csv_i++){
			$_csv_data[$_csv_i]=preg_replace('/^'.$e.'(.*)'.$e.'$/s','$1',$_csv_data[$_csv_i]);
			$_csv_data[$_csv_i]=str_replace($e.$e, $e, $_csv_data[$_csv_i]);
		}
		return empty($_line) ? false : $_csv_data;
	}


/**
 * 日付を任意の形式にフォーマット
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note
 * @param    str    $val       変換したい文字列（日付形式）
 * @param    str    $format    変換形式（省略可能）
 * @return   string
 */
function dateFormat($val, $format = 'Y/m/d') {
	// 「/」区切りの日付だとうまく動かないときがあるので(例：0000/00/00 00:00:00)
	$val = preg_replace('/\//u', '-', $val);

	if (!strtotime($val)) {
		return '';
	}
	return date($format, strtotime($val));
}


/**
 * 月初取得
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note
 * @param    str    $val       変換したい文字列（日付形式）
 * @param    str    $format    変換形式（省略可能）
 * @return   str    フォーマット形式の1日の文字列
 */
function getStartMonth($val, $format = 'Y/m/d') {
	// 「/」区切りの日付だとうまく動かないときがあるので(例：0000/00/00 00:00:00)
	$val = preg_replace('/\//u', '-', $val);

	if (!strtotime($val)) {
		return '';
	}
	$year = date('Y', strtotime($val));
	$month = date('m', strtotime($val));

	return dateFormat("$year/$month/01", $format);
}


/**
 * 月末取得
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note
 * @param    str    $val       変換したい文字列（日付形式）
 * @param    str    $format    変換形式（省略可能）
 * @return   str    フォーマット形式の末日の文字列
 */
function getEndMonth($val, $format = 'Y/m/d') {
	// 「/」区切りの日付だとうまく動かないときがあるので(例：0000/00/00 00:00:00)
	$val = preg_replace('/\//u', '-', $val);

	if (!strtotime($val)) {
		return '';
	}
	$year = date('Y', strtotime($val));
	$month = date('m', strtotime($val));
	$day = date('d', strtotime($val));

	return date($format, mktime(0, 0, 0, $month	 + 1, 0, $year) );
}


/**
 * 翌月指定日取得
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note     年繰り上がりや、翌月に31日等がない時は末日になるように調整
 * @param    int    $year      基準年
 * @param    int    $month     基準月
 * @param    int    $day       基準日(99=末日でもOK)
 * @param    int    $months    何ヵ月後(0は当月)
 * @param    str    $format    変換形式（省略可能）
 * @return   str    フォーマット形式の文字列
 */
function getNextMonthDate($year, $month, $day, $months = 1, $format = 'Y/m/d') {
	$n_year = date("Y", strtotime(date("$year/$month/01") . "+$months month"));
	$n_month = date("m", strtotime(date("$year/$month/01") . "+$months month"));
	$n_day = $day;

	//31日以降
	if ($n_day >= 31) {
		$n_day = getEndMonth("$n_year/$n_month/01", 'd');
	}

	//2月29日以降
	if ($n_month == 2 && $n_day >= 29) {
		$n_day = getEndMonth("$n_year/$n_month/01", 'd');
	}

	return dateFormat("$n_year/$n_month/$n_day", $format);
}


/**
 * 先月指定日取得
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note     年繰り下がりや、先月月に31日等がない時は末日になるように調整
 * @param    int    $year      基準年
 * @param    int    $month     基準月
 * @param    int    $day       基準日(99=末日でもOK)
 * @param    int    $months    何ヵ月前(0は当月)
 * @param    str    $format    変換形式（省略可能）
 * @return   str    フォーマット形式の文字列
 */
function getLastMonthDate($year, $month, $day, $months = 1, $format = 'Y/m/d') {
	$l_year = date("Y", strtotime(date("$year/$month/01") . "-$months month"));
	$l_month = date("m", strtotime(date("$year/$month/01") . "-$months month"));
	$l_day = $day;

	//31日以降
	if ($l_day >= 31) {
		$l_day = getEndMonth("$l_year/$l_month/01", 'd');
	}

	//2月29日以降
	if ($l_month == 2 && $l_day >= 29) {
		$l_day = getEndMonth("$l_year/$l_month/01", 'd');
	}

	return dateFormat("$l_year/$l_month/$l_day", $format);
}


/**
 * 小数点切捨て
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note     (負の値で切り捨てると値がずれるので負の値だと切り上げる)
 *           (一旦文字列にして、数値型に変換しないと小数以下切捨てで少し値が狂う)
 * @param    mix(str,float) $val    切り捨てたい数字（数値）
 * @return   int    整数
 */
function getFloor($val) {
	$val = (string)$val;
	$val = (float)$val;

	if ($val<=0){
		$val = $val*(-1);
		$val = floor($val);
		$val = $val*(-1);
	}else{
		$val = floor($val);
	}
	return $val;
}


/**
 * ランダムパスワード取得
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note
 * @param    int    $len    桁数
 * @return   str    ランダムパスワード文字列
 */
function getRandPass($len = 10) {
	$pass = null;
	$useStr = "abcdefghkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ2345679";
	$strArr = preg_split("//", $useStr, 0, PREG_SPLIT_NO_EMPTY);
	for ($i = 0; $i < $len; $i++) {
		$pass .= $strArr[array_rand($strArr, 1)];
	}
	return $pass;
}


/**
 * ランダムパスワード取得
 * @author  nakata@okushin.co.jp
 * @date    2015/02/17
 * @note
 * @param   str   $dir   該当ファイルがあるディレクトリの絶対パス
 * @return  bool  結果の真偽
 */
function delDirFiles($dir) {
	if ($dirHandle = opendir($dir)) {
		while (false !== ($fileName = readdir($dirHandle))) {
			if ($fileName != "." && $fileName != ".." && $fileName != ".svn" ) {
				if (unlink($dir. $fileName) === false) {
					return false;
				}
			}
		}
		closedir ( $dirHandle );
	}

	return true;
}


/*********************************************/
/* Fonction: ImageCreateFromBMP              */
/* Author:   DHKold                          */
/* Contact:  admin@dhkold.com                */
/* Date:     The 15th of June 2005           */
/* Version:  2.0B                            */
/*********************************************/

function ImageCreateFromBMP($filename)
{
//Ouverture du fichier en mode binaire
	if (! $f1 = fopen($filename,"rb")) return FALSE;

//1 : Chargement des ent?tes FICHIER
	$FILE = unpack("vfile_type/Vfile_size/Vreserved/Vbitmap_offset", fread($f1,14));
	if ($FILE['file_type'] != 19778) return FALSE;

//2 : Chargement des ent?tes BMP
	$BMP = unpack('Vheader_size/Vwidth/Vheight/vplanes/vbits_per_pixel'.
					'/Vcompression/Vsize_bitmap/Vhoriz_resolution'.
					'/Vvert_resolution/Vcolors_used/Vcolors_important', fread($f1,40));
	$BMP['colors'] = pow(2,$BMP['bits_per_pixel']);
	if ($BMP['size_bitmap'] == 0) $BMP['size_bitmap'] = $FILE['file_size'] - $FILE['bitmap_offset'];
	$BMP['bytes_per_pixel'] = $BMP['bits_per_pixel']/8;
	$BMP['bytes_per_pixel2'] = ceil($BMP['bytes_per_pixel']);
	$BMP['decal'] = ($BMP['width']*$BMP['bytes_per_pixel']/4);
	$BMP['decal'] -= floor($BMP['width']*$BMP['bytes_per_pixel']/4);
	$BMP['decal'] = 4-(4*$BMP['decal']);
	if ($BMP['decal'] == 4) $BMP['decal'] = 0;

//3 : Chargement des couleurs de la palette
	$PALETTE = array();
	if ($BMP['colors'] < 16777216)
	{
		$PALETTE = unpack('V'.$BMP['colors'], fread($f1,$BMP['colors']*4));
	}

//4 : Cr?ation de l'image
	$IMG = fread($f1,$BMP['size_bitmap']);
	$VIDE = chr(0);

	$res = imagecreatetruecolor($BMP['width'],$BMP['height']);
	$P = 0;
	$Y = $BMP['height']-1;
	while ($Y >= 0)
	{
		$X=0;
		while ($X < $BMP['width'])
		{
			if ($BMP['bits_per_pixel'] == 24)
				$COLOR = unpack("V",substr($IMG,$P,3).$VIDE);
			elseif ($BMP['bits_per_pixel'] == 16)
			{
				$COLOR = unpack("n",substr($IMG,$P,2));
				$COLOR[1] = $PALETTE[$COLOR[1]+1];
			}
			elseif ($BMP['bits_per_pixel'] == 8)
			{
				$COLOR = unpack("n",$VIDE.substr($IMG,$P,1));
				$COLOR[1] = $PALETTE[$COLOR[1]+1];
			}
			elseif ($BMP['bits_per_pixel'] == 4)
			{
				$COLOR = unpack("n",$VIDE.substr($IMG,floor($P),1));
				if (($P*2)%2 == 0) $COLOR[1] = ($COLOR[1] >> 4) ; else $COLOR[1] = ($COLOR[1] & 0x0F);
				$COLOR[1] = $PALETTE[$COLOR[1]+1];
			}
			elseif ($BMP['bits_per_pixel'] == 1)
			{
				$COLOR = unpack("n",$VIDE.substr($IMG,floor($P),1));
				if     (($P*8)%8 == 0) $COLOR[1] =  $COLOR[1]        >>7;
				elseif (($P*8)%8 == 1) $COLOR[1] = ($COLOR[1] & 0x40)>>6;
				elseif (($P*8)%8 == 2) $COLOR[1] = ($COLOR[1] & 0x20)>>5;
				elseif (($P*8)%8 == 3) $COLOR[1] = ($COLOR[1] & 0x10)>>4;
				elseif (($P*8)%8 == 4) $COLOR[1] = ($COLOR[1] & 0x8)>>3;
				elseif (($P*8)%8 == 5) $COLOR[1] = ($COLOR[1] & 0x4)>>2;
				elseif (($P*8)%8 == 6) $COLOR[1] = ($COLOR[1] & 0x2)>>1;
				elseif (($P*8)%8 == 7) $COLOR[1] = ($COLOR[1] & 0x1);
				$COLOR[1] = $PALETTE[$COLOR[1]+1];
			}
			else
				return FALSE;
			imagesetpixel($res,$X,$Y,$COLOR[1]);
			$X++;
			$P += $BMP['bytes_per_pixel'];
		}
		$Y--;
		$P+=$BMP['decal'];
	}

//Fermeture du fichier
	fclose($f1);

	return $res;
}

//****************************************************************************
// モジュール名 : 共通関数
// Create       : 2009/06/01 Konishi
// Production   : Okushin System Ltd.
//****************************************************************************
function js2PhpTime($jsdate){
	if(preg_match('@(\d{4})/(\d+)/(\d+)\s+(\d+):(\d+)@', $jsdate, $matches)==1){
		$ret = mktime($matches[4], $matches[5], 0, $matches[2], $matches[3], $matches[1]);
	}else if(preg_match('@(\d+)/(\d+)/(\d+)\s+(\d+):(\d+)@', $jsdate, $matches)==1){
		$ret = mktime($matches[4], $matches[5], 0, $matches[1], $matches[2], $matches[3]);
	}else if(preg_match('@(\d{4})/(\d+)/(\d+)@', $jsdate, $matches)==1){
		$ret = mktime(0, 0, 0, $matches[2], $matches[3], $matches[1]);
	}else if(preg_match('@(\d+)/(\d+)/(\d+)@', $jsdate, $matches)==1){
		$ret = mktime(0, 0, 0, $matches[1], $matches[2], $matches[3]);
	}
	return $ret;
}

function php2JsTime($phpDate){
	return date("Y/m/d H:i", $phpDate);
}

function php2MySqlTime($phpDate){
	return date("Y-m-d H:i:s", $phpDate);
}

function mySql2PhpTime($sqlDate){
	$arr = date_parse($sqlDate);
	return mktime($arr["hour"],$arr["minute"],$arr["second"],$arr["month"],$arr["day"],$arr["year"]);
}
?>