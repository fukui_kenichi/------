<?php
/**
 * オブジェクト型定数
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note
 */

require_once("def_base.php");

// 定数
// HTTPS設定
define('HTTPS_MODE', false);

// http://....等のドメインを付けたい時は任意で文字列追加
define('ROOT_URL', preg_replace('/index\.php/', '', env('SCRIPT_NAME')));
define('ROOT_PATH',  preg_replace('/index\.php/', '',env('SCRIPT_FILENAME')));   //絶対パス
// 消費税
define('TAX', 0.08);
// ファイルMAXサイズ
define('MAX_IMAGE_FILE_SIZE', 5000000 );
define('MAX_OTHER_FILE_SIZE', 100000000 );
// サムネールの画像サイズ
define('S_WIDTH_SIZE', 100);
define('S_HEIGHT_SIZE', 100);
define('S_USER_WIDTH_SIZE', 100);
define('S_USER_HEIGHT_SIZE', 100);

// マスタータイプデファイン
define('MASTER_TYPE_CHAR', 1);	// 文字列（制限あり）
define('MASTER_TYPE_TEXT', 2);	// 文字列（制限なし）
define('MASTER_TYPE_INTG', 3);	// 数値型
define('MASTER_TYPE_DTTM', 4);	// 日付時刻型
define('MASTER_TYPE_DATE', 5);	// 日付型
define('MASTER_TYPE_TIME', 6);	// 時刻型
define('MASTER_TYPE_LIST', 7);	// リスト型
define('MASTER_TYPE_RADI', 8);	// ラジオ型
define('MASTER_TYPE_FIMG', 9);	// ファイル（イメージ）
define('MASTER_TYPE_FILE', 10);	// ファイル（その他）
define('MASTER_TYPE_TELE', 11);	// 電話番号型
define('MASTER_TYPE_DATE_A', 12);	// 日付型(アラート)
define('MASTER_TYPE_CHEK', 13);	// チェック型

// 商談マスタ登録時にBusiReferを更新するかどうかの件数
define('BUSY_COUNT', 1000);


/**
 * 都道府県
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note
 */
class PrefDef extends DefBaseClass {
    var $_dataArr = array(
		1 => '北海道',
		2 => '青森県',
		3 => '岩手県',
		4 => '宮城県',
		5 => '秋田県',
		6 => '山形県',
		7 => '福島県',
		8 => '茨城県',
		9 => '栃木県',
		10 => '群馬県',
		11 => '埼玉県',
		12 => '千葉県',
		13 => '東京都',
		14 => '神奈川県',
		15 => '新潟県',
		16 => '富山県',
		17 => '石川県',
		18 => '福井県',
		19 => '山梨県',
		20 => '長野県',
		21 => '岐阜県',
		22 => '静岡県',
		23 => '愛知県',
		24 => '三重県',
		25 => '滋賀県',
		26 => '京都府',
		27 => '大阪府',
		28 => '兵庫県',
		29 => '奈良県',
		30 => '和歌山県',
		31 => '鳥取県',
		32 => '島根県',
		33 => '岡山県',
		34 => '広島県',
		35 => '山口県',
		36 => '徳島県',
		37 => '香川県',
		38 => '愛媛県',
		39 => '高知県',
		40 => '福岡県',
		41 => '佐賀県',
		42 => '長崎県',
		43 => '熊本県',
		44 => '大分県',
		45 => '宮崎県',
		46 => '鹿児島県',
		47 => '沖縄県'
    );
}


/**
 * 変換テーブル
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note
 */
class HenDef extends DefBaseClass {
	var $_dataArr = array(
		0 => array('type' => 9,'number' => 1,'field' => 'free1'),
		1 => array('type' => 10,'number' => 2,'field' => 'free2'),
		2 => array('type' => 9,'number' => 3,'field' => 'free3'),
		3 => array('type' => 10,'number' => 4,'field' => 'free4'),
	);
}


/**
 * はいいいえ型
 * @author   nakata@okushin.co.jp
 * @date     2015/02/13
 * @note
 */
class YesNoTypeDef extends DefBaseClass {
    var $_dataArr = array(
		0 => 'いいえ',
		1 => 'はい',
    );
}


/**
 * マスタ：項目型
 * @author   nakata@okushin.co.jp
 * @date     2015/02/13
 * @note
 */
class MFieldTypeDef extends DefBaseClass {
    var $_dataArr = array(
		MASTER_TYPE_CHAR => '文字列(制限あり)',
		MASTER_TYPE_TEXT => '文字列(制限なし)',
		MASTER_TYPE_TELE => '電話番号型',
		MASTER_TYPE_INTG => '数値型',
		MASTER_TYPE_DTTM => '日付時刻型',
		MASTER_TYPE_DATE => '日付型',
		MASTER_TYPE_TIME => '時間型',
		MASTER_TYPE_LIST => 'リスト型',
		MASTER_TYPE_RADI => 'ラジオ型',
		MASTER_TYPE_FIMG => 'ファイル型(写真)',
		MASTER_TYPE_FILE => 'ファイル型(その他)',
		MASTER_TYPE_CHEK => 'チェック型',
	);
}


/**
 * マスタ：項目型(顧客用)
 * @author   nakata@okushin.co.jp
 * @date     2015/02/13
 * @note
 */
class MFieldTypeCusDef extends DefBaseClass {
    var $_dataArr = array(
		MASTER_TYPE_CHAR => '文字列(制限あり)',
		MASTER_TYPE_TEXT => '文字列(制限なし)',
		MASTER_TYPE_TELE => '電話番号型',
		MASTER_TYPE_INTG => '数値型',
		MASTER_TYPE_DTTM => '日付時刻型',
		MASTER_TYPE_DATE => '日付型',
		MASTER_TYPE_DATE_A => '日付型(アラート)',
		MASTER_TYPE_TIME => '時間型',
		MASTER_TYPE_LIST => 'リスト型',
		MASTER_TYPE_RADI => 'ラジオ型',
		MASTER_TYPE_FIMG => 'ファイル型(写真)',
		MASTER_TYPE_FILE => 'ファイル型(その他)',
		MASTER_TYPE_CHEK => 'チェック型',
	);
}


/**
 * メール配信状態型
 * @author   nakata@okushin.co.jp
 * @date     2015/03/20
 * @note
 */
class SendMailTypeDef extends DefBaseClass {
	var $_dataArr = array(
		0 => '配信待ち',
		1 => '配信中',
		2 => '配信済',
		3 => '配信中',
	);
}

/**
 * 何日前アラート
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note
 */
class AlertDef extends DefBaseClass {
    var $_dataArr = array(
		0 => 0,
    	1 => 1,
		2 => 2,
		3 => 3,
		4 => 4,
		5 => 5,
		6 => 6,
		7 => 7,
		8 => 8,
		9 => 9,
		10 => 10,
		11 => 11,
		12 => 12,
		13 => 13,
		14 => 14,
		15 => 15,
		16 => 16,
		17 => 17,
		18 => 18,
		19 => 19,
		20 => 20,
		21 => 21,
		22 => 22,
		23 => 23,
		24 => 24,
		25 => 25,
		26 => 26,
		27 => 27,
		28 => 28,
		29 => 29,
		30 => 30,
    );
}

?>