<?php
/**
 * 基本コントローラー
 * @author   konishi@okushin.co.jp
 * @date     2011/06/09
 * @note
 */
class AppController extends Controller {
	var $ext		= '.php';	// viewの拡張子をphp
	var $components = array('Session', 'Cookie', 'HoldPaginate', 'Login', 'BasicService');
	var $uses		= array('User', 'Favorite');
	var $helpers	= array('Session', 'Html', 'AppForm');

	var $login		= array();	// ログイン情報

	/**
	 * 初期処理
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 */
	function beforeFilter() {
		// トークンチェック
		if (strpos($this->action, 'ajax_') === false){
			if (!$this->_checkToken()) {
				$this->logout();
			}
		}

		// ログインチェック
		if(!($this->name == 'Login' && $this->action == 'index')) {
			if (!$this->Login->checkLogin()) {
				$this->redirect('/login/index');
			}
		}

		// ログ記録
		$this->_writeLog();

		// 現在のページと1つ前のページを保存
		$this->savePage();

		// お気に入り一覧
		$params = array(
			'conditions' => array('Favorite.user_id' => $this->login['User']['id']),
			'order' => array('Favorite.id asc'),
			'limit' => 1000,
			'recursive' => -1,
		);
		$favoriteList = $this->Favorite->find('all', $params);	// 一覧データ取得
		$this->set('favoriteList', $favoriteList);

	}


	/**
	 * ログアウト処理
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 */
	function logout() {
		$this->Session->destroy();
		$this->redirect('/login/index');
	}

	/**
	 * トークンチェック POSTの時にチェック
	 * @author   konishi@okushin.co.jp
	 * @date     2011/06/09
	 * @note
	 * @return   bool    処理結果真偽値
	 */
	function _checkToken() {
		if(empty($_POST)) {
			return true;
		}

		if (!$this->Session->check('token')) {
			return false;
		}
		$token = $this->Session->read('token');

		if ($this->params['form']['token'] != $token) {
			return false;
		}
		return true;
	}


	/**
	 * 現在のページと1つ前のページを保存
	 * @author   fukui@okushin.co.jp
	 * @date     2014/03/11
	 * @note
	 */
	function savePage() {
		// CTI受信画面は無視
		if ($this->name == 'Customers' && $this->action == 'tel'){
			return;
		}
		// Cron画面は無視
		if ($this->name == 'Cron'){
			return;
		}
		// ファイルアップロード画面は無視
		if ($this->name == 'Uploads'){
			return;
		}
		// 詳細画面はhistory.backで戻るので無視
		if ($this->action == 'detail'){
			return;
		}
		// AJAX画面は無視
		if (strpos($this->action, 'ajax_') !== false){
			return;
		}

		// コントローラーが変わっていたら1つ前のページ保持
		$nowPage = $this->Session->read('nowPage');
		if(empty($nowPage) || $this->name != $nowPage['name']){
			$prevPage = $nowPage;
			$this->Session->write('prevPage', $prevPage);
		}
		$nowPage['name'] = $this->name;
		$nowPage['action'] = $this->action;
		$url = rtrim($this->params['url']['url'], "/search:clear"); // 検索初期化の記述除去
		$nowPage['url'] = $url;

		$this->Session->write('nowPage', $nowPage);
	}

	/**
	 * ログを記録
	 * @author   fukui@okushin.co.jp
	 * @date     2014/04/23
	 * @note
	 */
	function _writeLog() {
		$submitArr = array(
			'cancel', 'save', 'del', 'search', 'add_favorite', 'send_mail', 'refresh',
			'cancel_x', 'save_x', 'del_x', 'search_x', 'add_favorite_x', 'send_mail_x', 'refresh_x'
		);

		//debug($_SERVER);
		//exit;

		if (empty($_SERVER['HTTP_REFERER'])) {
			$msg  = "{$_SERVER['REMOTE_ADDR']} {$this->login['User']['name']} ";
			$msg .= "{$_SERVER['REQUEST_METHOD']} http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']} ";
		} else {
			$msg  = "{$_SERVER['REMOTE_ADDR']} {$this->login['User']['name']} ";
			$msg .= "{$_SERVER['REQUEST_METHOD']} {$_SERVER['HTTP_REFERER']} ";
		}

		foreach ($submitArr as $key => $val) {
			if (isset($this->params['form'][$val])) {
				$msg .= $val;
				break;
			}
		}

		$this->log($msg, date('Ym'). 'access');
	}


}
?>