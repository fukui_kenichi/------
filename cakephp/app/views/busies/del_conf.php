<?php
	echo $appForm->create();
?>
<div class="row">
   	<div class="xs-invisible">
        <img src="<?php echo ROOT_URL ?>img/shodan-sakujo.png" alt="商談削除確認">
    </div>
   	<div class="xs-visible">
        <img src="<?php echo ROOT_URL ?>img/shodan-sakujo_s.png" alt="商談削除確認">
    </div>
</div>

<table class="table table-bordered table-navy table-edit">
<?php
	foreach ($busyMst as $key => $val) {
		$val = $val['BusiMaster'];
		if ($val['field'] == 'user_id') { // 担当者
			echo '<tr>';
			$th = '<th width="150">' . esHtml($val['koumoku']);
			if ($val['required']) {
				$th .= '<span class="err-msg">【必須】</span>';
			}
			$th .= '</th>';
			echo $th;
			echo '<td>';
			if (isset($userArr[$appForm->data['Busy']['user_id']])) {
				echo '<a href="'.  $html->url('/users/detail/'. $appForm->data['Busy']['user_id']). '">'. $userArr[$appForm->data['Busy']['user_id']]. '</a>';
			}
			echo '</td></tr>';
		} else {
			echo '<tr>';
			$th = '<th width="150">' . esHtml($val['koumoku']);
			$th .= '</th>';
			echo $th;
			echo '<td class="sm-center">';
			switch ($val['type']) {
				case MASTER_TYPE_CHAR:		// 文字列（制限あり）
				case MASTER_TYPE_INTG:		// 数値
				case MASTER_TYPE_TELE:		// 電話型
					echo esHtml($appForm->data['Busy']["{$val['field']}"]);
					break;
				case MASTER_TYPE_DTTM:		// 日付時刻型
					if (isset($appForm->data['Busy']["{$val['field']}_d"])){
						echo esHtml($appForm->data['Busy']["{$val['field']}_d"]);
						echo '　';
					}
					if (isset($appForm->data['Busy']["{$val['field']}_t"])) {
						echo esHtml($appForm->data['Busy']["{$val['field']}_t"]);
					}
					break;
				case MASTER_TYPE_DATE:		// 日付型
					echo esHtml($appForm->data['Busy']["{$val['field']}"]);
					break;
				case MASTER_TYPE_TIME:		// 時刻型
					echo esHtml($appForm->data['Busy']["{$val['field']}"]);
					break;
				case MASTER_TYPE_TEXT:		// 文字列（制限なし）
				case MASTER_TYPE_CHEK:		// チェック型
					echo nl2br(esHtml($appForm->data['Busy']["{$val['field']}"]));
					break;
				case MASTER_TYPE_LIST:		// リスト型
					echo esHtml($appForm->data['Busy']["{$val['field']}"]);
					break;
				case MASTER_TYPE_RADI:		// ラジオ型
					echo esHtml($appForm->data['Busy']["{$val['field']}"]);
					break;
				case MASTER_TYPE_FIMG:
					$img_i = $val['number'];
					if (!empty($appForm->data['Image'][$img_i])) {
						echo "<div id='slider_image_{$img_i}'>";
						foreach ($appForm->data['Image'][$img_i] as $field => $v) {
							$fieldArr = explode("_",$field);
							$key = $fieldArr[1];
							if(!isset($imageId_fArr[$img_i][$key])) {
								$imageId_fArr[$img_i][$key] = 0;
							}
							// image_(no)だけを処理するように選別
							if ($fieldArr[0] == "imagename") {
								continue;
							}
							if (count($fieldArr) == 3) {
								continue;
							}
							if (count($fieldArr) == 2) {
								if (!empty($appForm->data['Image'][$img_i]["image_{$key}"])) {
									$fileName = $appForm->data['Image'][$img_i]["imagename_{$key}"];
									$ext = pathinfo($fileName, PATHINFO_EXTENSION);
									$upFile = ROOT_URL . 'upload/' . $appForm->data['Image'][$img_i]["image_{$key}_id"] . '.' . $ext;
									$upFile_s = ROOT_URL . 'upload/' . $appForm->data['Image'][$img_i]["image_{$key}_id"] . '-s.' . $ext;
								}
								$div = "<div><a class='example-image-link' href='{$upFile}' data-lightbox='group1'><img src='{$upFile_s}'></a></div>";
								echo $div;
							}
						}
						echo "</div>";
					}
					break;
				case MASTER_TYPE_FILE:
					$notimg_i = $val['number'];
					echo "<div id='notimage_{$notimg_i}'>";
					if (!empty($appForm->data['Notimage'][$notimg_i])) {
						$notimageId_fArr[$notimg_i][] = array();
						// 画像を表示する処理
						foreach ($appForm->data['Notimage'][$notimg_i] as $field => $v) {
							$fieldArr = explode("_",$field);
							$key = $fieldArr[1];
							if(!isset($notimageId_fArr[$notimg_i][$key])) {
								$notimageId_fArr[$notimg_i][$key] = 0;
							}
							// image_(no)だけを処理するように選別
							if ($fieldArr[0] == "notimagename") {
								continue;
							}
							if (count($fieldArr) == 3) {
								continue;
							}
							if (count($fieldArr) == 2) {
								$downloadUrl = $html->url("/uploads/download");
								$fileName = $appForm->data['Notimage'][$notimg_i]["notimagename_{$key}"];
								$id = $appForm->data['Notimage'][$notimg_i]["notimage_{$key}_id"];
								echo "<a id='notimg_notimage_{$notimg_i}_{$key}' href='{$downloadUrl}/{$id}'>{$fileName}</a>";
								echo "<br id='br1_notimage_{$notimg_i}_{$key}'>";
							}
						}
					}
					echo "</div>";
					break;
			}
			echo '</td>';
			echo '</tr>';

		}
	}
?>
</table>

<div class="row">
	<div class="center">
        <span class="err-msg">※削除すると、この商談情報は失われます。</span><br>
        <div class="xs-invisible">
        	<input type="image" name="del" alt="削除" src="<?php echo ROOT_URL ?>img/bt_delete.png" title="削除" />
        	<input type="image" name="cancel" alt="キャンセル" src="<?php echo ROOT_URL ?>img/bt_cancel.png" title="キャンセル" />
        </div>
        <div class="xs-visible">
        	<input type="image" name="del" alt="削除" src="<?php echo ROOT_URL ?>img/bt_delete_s.png" title="削除" />
        	<input type="image" name="cancel" alt="キャンセル" src="<?php echo ROOT_URL ?>img/bt_cancel_s.png" title="キャンセル" />
        </div>
	</div>
</div>

<div class="hide">
<?php
	echo $appForm->input('Busy.customer_id', array('type' => 'hidden'));
?>
</div>

<?php
	echo $appForm->end();
?>
<script type="text/javascript">
$(function(){
	$("div[id^='slider_image_']").each( function() {
		var id = this.id;
		$('#' + id).bxSlider({
			auto: true, /* 自動再生 */
			adaptiveHeight: 'true', /* 高さを自動調整 */
			autoControls: false,  /* スタート、ストップボタン */
			pager: true, /* ページャー */
			mode: 'horizontal', /* fade,vertical など */
			speed: 1000, /* エフェクトのスピード */
			controls: true, /* 前へ、次へボタンの表示 */
				prevText: '&lt;', /* 前へボタンのテキスト */
			nextText: '&gt;', /* 次へボタンのテキスト */
			pause: 4000, /* 間隔の時間 */
			easing: 'swing', /* Easing */
			autoHover: true /* マウスホバーで停止 */
		});
	});
});
</script>
