<?php
configure::write('debug', 0); 		// debugコードを非表示
$filename = date('YmdHis');
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Disposition: attachment;filename={$filename}.csv");
header("Content-Transfer-Encoding: binary ");

$output = '';
// タイトル行出力
foreach ((array)$dataArr['title'] as $val) {
	$output .= '"' . $val . '",';
}
$output .= "\r\n";;	// 改行

//インポート機能実装まで要らない
//// フィールド名出力
//foreach ((array)$dataArr['fieldArr'] as $val) {
//	$output .= '"' . $val . '",';
//}
//$output .= "\r\n";;	// 改行

// 内容出力
foreach ((array)$dataArr['data'] as $val) {
	foreach ($val as $val2){
		$output .= '"' . $val2 . '",';
	}
	$output .= "\r\n";;	// 改行
}
print(mb_convert_encoding($output, "sjis-win", "UTF-8")); // 出力
