<?php
	echo $appForm->create();
?>
<div class="row">
   	<div class="xs-invisible"><img src="<?php echo ROOT_URL ?>img/csv-output.png" alt="CSV出力"></div>
   	<div class="xs-visible"><img src="<?php echo ROOT_URL ?>img/csv-output_s.png" alt="CSV出力"></div>
</div>
<table class="table table-bordered table-navy table-edit">
	<tr>
		<th width="200px">顧客情報CSV</th>
		<td class="sm-center"><input type="submit" name="down_customer" value="出力する"></td>
	</tr>
	<tr>
		<th>商談情報CSV</th>
		<td class="sm-center"><input type="submit" name="down_busy" value="出力する"></td>
	</tr>
</table>
<div class="row">
	<div class="center">
        <div class="xs-invisible">
            <input type="image" name="cancel" alt="戻る" src="<?php echo ROOT_URL ?>img/bt_back.png" title="戻る" />
        </div>
        <div class="xs-visible">
            <input type="image" name="cancel" alt="戻る" src="<?php echo ROOT_URL ?>img/bt_back_s.png" title="戻る" />
        </div>
	</div>
</div>

<?php
	echo $appForm->end();
?>
