<?php
	echo $appForm->create('ImageEdit', array('name' => 'form','type' => 'file','id' => 'uploadForm', 'enctype' => 'multipart/form-data'));
?>

<table class="table table-bordered table-navy table-edit">
	<tr>
		<th>担当者</th>
		<td class="sm-center">
			<select>
				<option value="1">ライト　A太郎</option>
				<option value="2">ライト　B次郎</option>
				<option value="3">ライト　C三郎</option>
				<option value="4" selected="selected">ライト　D四郎</option>
			</select>
        </td>
	</tr>
<!--    <tr>-->
<!--      <th>写真1</th>-->
<!--      <td class="sm-center">-->
<!--            <input type="file" id="file_select_1" name="photos_1[]" multiple>-->
<!--            <button>アップロード</button>-->
<!--      </td>-->
<!--    </tr>-->
<?php
	for($i = 1; $i <= 5; $i++) {
	?>
    <tr>
      <th>写真<?php echo $i ?></th>
      <td class="sm-center">
		<?php
		echo $appForm->input("Image.up_image_{$i}. ", array('type' => 'file','multiple' => 'multiple'));
		?>
		<button type="button" id="upbtn_image_<?php echo $i ?>">アップロード</button>
		<?php
		echo "<div id='image_{$i}'>";
		echo "</div>";
		echo "<div id='image_temp_{$i}'>";
		echo "</div>";
		?>
      </td>
    </tr>
<?php
	}
?>
</table>
<input type="button" id="btn" value="div要素の追加" />
<div>テスト</div>
<div>テスト</div>
<?php
	echo $appForm->end();
?>

<div class="hide">
	<table>
	<tr id="upTr">
		<td id="upTd"><?php echo $appForm->input("Tmp.file", array('type' => 'file')) ?></td>
	</tr>
	</table>
</div>
<script type="text/javascript">
$(function(){
	var no = 1;
	var g_koumoku = null;

	// 画像を削除する。
	$(document).on("click", "a[id^='adel_image_']" ,function(ev){
		var idname = ev.target.id;
		var arr = idname.split('_');
		var koumoku = arr[2];
		var key = arr[3];

		var delimgid = "img_image_" + koumoku + "_" + key;
		$("#" + delimgid).remove();
		var delbr1 = "br1_image_" + koumoku + "_" + key;
		$("#" + delbr1).remove();
		var delspanid = "spandel_image_" + koumoku + "_" + key;
		$("#" + delspanid).remove();
		var delfilenameid = "Image" + key + "Imagename" + koumoku;
		$("#" + delfilenameid).remove();
		var delfileid = "Image" + key + "Image" + koumoku;
		$("#" + delfileid).remove();
		var delfileupid = "Image" + key + "Image" + koumoku + "Up";
		$("#" + delfileupid).remove();
		var delbr2 = "br2_image_" + koumoku + "_" + key;
		$("#" + delbr2).remove();

	});
	// アップロードボタンクリック
	$(document).on("click", "button[id^='upbtn_image_']" ,function(ev){
		var idname = ev.target.id;
		var arr = idname.split('_');
		var koumoku = arr[2];
		g_koumoku = koumoku;
		<?php $upload_url = $html->url('/users/img_upload'); ?>
		var url = "<?php echo "{$upload_url}?koumoku=" ?>" + koumoku + "&model=Image";
		var target = '#image_temp_' + koumoku;

		$('#uploadForm').ajaxSubmit({
			target: target, // サーバの戻りを出力する場所を指定
			url: url, // URL
			success: hogeResponse     // サーバからの応答時に呼び出す関数
		});
		return false;
	});

	//サーバ応答時実行関数
	function hogeResponse(responseText, statusText) {

//		alert('status: ' + statusText + '\n\nresponseText: \n' + responseText);
		// 現状アップされているイメージを最大値を取得する。
		var koumoku = g_koumoku;
		var max_no = 0;
		$('img[id^="img_image_'+koumoku+'_"]').each( function() {
			var idname = this.id;
			var arr = idname.split('_');
			var target_no = arr[3];
			target_no = eval(target_no);

			// 最後の行数取得
			if (eval(max_no) < eval(target_no)) {
				max_no = target_no;
			}
		});
		// ファイルをテンポラリ領域に上がっているので、クーロンを作成する。
		var tpl = $("#image_temp_" + koumoku).clone();
		// エラーメッセージがある場合
		var ermsg = tpl.find('p[id="cerror_image_'+koumoku+'"]').text();
		if (ermsg) {
			// エラーを消して
			$('p[id="error_image_'+koumoku+'"]').remove();
			var img_f = false;
			var topid = null;
			$('img[id^="img_image_'+koumoku+'"]').each( function() {
				img_f = true;
				topid = this.id;
			});
			// アップロードボタンの下にエラーを表示する。
			var pid = "cerror_image_" + koumoku;
			tpl.find('p[id=' + pid + ']').attr('id',  'error_image_' + koumoku);
			if (topid) {
				$('#' + topid).before(tpl.find('p[id=error_image_'+koumoku+']').show());
			} else {
				$('div[id="image_'+koumoku+'"]').append(tpl.find('p[id=error_image_'+koumoku+']').show());
			}
		} else {
			// エラー表示を消す
			$('p[id="error_image_'+koumoku+'"]').remove();
			max_no = max_no + 1;
			tpl.find('img[id^="cimg_image_'+koumoku+'_"]').each( function() {
				var idname = this.id;
				var arr = idname.split('_');
				var target_no = arr[3];
				target_no = eval(target_no);

				// イメージ表示域のコピー表示
				var imgid = "cimg_image_" + koumoku + "_" + target_no;
				tpl.find('img[id=' + imgid + ']').attr('id',  'img_image_' + koumoku + '_' + max_no);
				$('div[id="image_' + koumoku + '"]').append(tpl.find('img[id=img_image_' + koumoku + '_' + max_no + ']').show());

				// 改行表示域のコピー表示
				var br1id = "cbr1_image_" +koumoku+"_" + target_no;
				tpl.find('br[id=' + br1id + ']').attr('id', 'br1_image_' +koumoku+'_' + max_no);
				$('div[id="image_' + koumoku + '"]').append(tpl.find('br[id=br1_image_' + koumoku + '_' + max_no + ']').show());

				// 【画像を削除する】表示域のコピー表示
				var spanid = "cspandel_image_" +koumoku+"_" + target_no;
				tpl.find('span[id=' + spanid + ']').attr('id', 'spandel_image_' + koumoku + '_' + max_no);
				var btnid = "cadel_image_" + koumoku+ "_" + target_no;
				tpl.find('a[id=' + btnid + ']').attr('id', 'adel_image_' + koumoku + '_' + max_no);
				$('div[id="image_' + koumoku + '"]').append(tpl.find('span[id=spandel_image_' + koumoku + '_' + max_no + ']').show());

				// ファイル名域のコピー表示
				var imageid = "cImage" + target_no + "Imagename"+koumoku;
				tpl.find('input[id=' + imageid + ']').attr('name',  'data[Image][' + max_no + '][imagename' + koumoku + ']');
				tpl.find('input[id=' + imageid + ']').attr('id', 'Image' + max_no + 'Imagename' + koumoku);
				$('div[id="image_' + koumoku + '"]').append(tpl.find('input[id=' + 'Image' + max_no + 'Imagename' + koumoku + ']').show());

				// ファイル名域のコピー表示
				var imageid = "cImage" + target_no + "Image"+koumoku;
				tpl.find('input[id=' + imageid + ']').attr('name',  'data[Image][' + max_no + '][image' + koumoku + ']');
				tpl.find('input[id=' + imageid + ']').attr('id', 'Image' + max_no + 'Image' + koumoku);
				$('div[id="image_' + koumoku + '"]').append(tpl.find('input[id=' + 'Image' + max_no + 'Image' + koumoku + ']').show());

				// ファイル名UP域のコピー表示
				var imageupid = "cImage" + target_no + "Image"+koumoku+"Up";
				tpl.find('input[id=' + imageupid + ']').attr('name',  'data[Image][' + max_no + '][image_' + koumoku + 'up]');
				tpl.find('input[id=' + imageupid + ']').attr('id', 'Image' + max_no + 'Image' + koumoku + 'Up');
				$('div[id="image_' + koumoku + '"]').append(tpl.find('input[id=' + 'Image' + max_no + 'Image' + koumoku + 'Up' + ']').show());

				// 改行表示域のコピー表示
				var br2id = "cbr2_image_" +koumoku+"_" + target_no;
				tpl.find('br[id=' + br2id + ']').attr('id', 'br2_image_' + koumoku + '_' + max_no);
				$('div[id="image_' + koumoku + '"]').append(tpl.find('br[id=br2_image_' + koumoku + '_' + max_no + ']').show());
				// インクリメント
				max_no = max_no + 1;
			});
		}
		// 仮に上げた領域をクリアする。
		$("#image_temp_"+koumoku).html("");
		// ファイル指定をクリアする。
		$("#ImageUpImage"+koumoku).val("");
	}

});
</script>
