<?php
	echo $appForm->create();

?>
<h3>ファイルアップロードテーブル編集</h3>
<table class="table table-bordered table-navy">
	<tr>
	<th>拡張子</th>
	<td><?php echo $appForm->input('Upload.extension', array('type' => 'hidden', 'size' => 257, 'maxlength' => 256)) ?></td>
</tr>
<tr>
	<th>ファイル名</th>
	<td><?php echo $appForm->input('Upload.file_name', array('type' => 'hidden', 'size' => 257, 'maxlength' => 256)) ?></td>
</tr>
<tr>
	<th>UPファイル名</th>
	<td><?php echo $appForm->input('Upload.up_file_name', array('type' => 'hidden', 'size' => 257, 'maxlength' => 256)) ?></td>
</tr>
<tr>
	<th>親テーブル名</th>
	<td><?php echo $appForm->input('Upload.parent_table', array('type' => 'hidden', 'size' => 257, 'maxlength' => 256)) ?></td>
</tr>
<tr>
	<th>親テーブルID</th>
	<td><?php echo $appForm->input('Upload.parent_id', array('type' => 'hidden')) ?></td>
</tr>
<tr>
	<th>親フィールド名</th>
	<td><?php echo $appForm->input('Upload.parent_field', array('type' => 'hidden', 'size' => 257, 'maxlength' => 256)) ?></td>
</tr>

</table>
<div class="right">
	<input type="submit" name="cancel" value="戻る" class="btn btn-default" />　
	<input type="submit" name="save" value="登録" class="btn btn-primary" />
</div>
<div class="hide">
	<?php
		echo $appForm->input('Upload.id', array('type' => 'hidden'));
		echo $appForm->input('Upload.updated', array('type' => 'hidden'));
	?>
</div>
<?php
	echo $appForm->end();
?>
