<?php
	echo $appForm->create('NotimageEdit', array('name' => 'form','type' => 'file','id' => 'uploadForm', 'enctype' => 'multipart/form-data'));
?>

<table class="table table-bordered table-navy table-edit">
	<tr>
		<th>担当者</th>
		<td class="sm-center">
			<select>
				<option value="1">ライト　A太郎</option>
				<option value="2">ライト　B次郎</option>
				<option value="3">ライト　C三郎</option>
				<option value="4" selected="selected">ライト　D四郎</option>
			</select>
        </td>
	</tr>
<!--    <tr>-->
<!--      <th>写真1</th>-->
<!--      <td class="sm-center">-->
<!--            <input type="file" id="file_select_1" name="photos_1[]" multiple>-->
<!--            <button>アップロード</button>-->
<!--      </td>-->
<!--    </tr>-->
<?php
	for($i = 1; $i <= 5; $i++) {
	?>
    <tr>
      <th>ファイル<?php echo $i ?></th>
      <td class="sm-center">
		<?php
		echo $appForm->input("Notimage.up_notimage_{$i}. ", array('type' => 'file','multiple' => 'multiple'));
		?>
		<button type="button" id="upbtn_notimage_<?php echo $i ?>">アップロード</button>
		<?php
		echo "<div id='notimage_{$i}'>";
		echo "</div>";
		echo "<div id='notimage_temp_{$i}'>";
		echo "</div>";
		?>
      </td>
    </tr>
<?php
	}
?>
</table>
<input type="button" id="btn" value="div要素の追加" />
<div>テスト</div>
<div>テスト</div>
<?php
	echo $appForm->end();
?>

<div class="hide">
	<table>
	<tr id="upTr">
		<td id="upTd"><?php echo $appForm->input("Tmp.file", array('type' => 'file')) ?></td>
	</tr>
	</table>
</div>
<script type="text/javascript">
$(function(){
	var no = 1;
	var g_koumoku = null;

	// 画像を削除する。
	$(document).on("click", "a[id^='adel_notimage_']" ,function(ev){
		var idname = ev.target.id;
		var arr = idname.split('_');
		var koumoku = arr[2];
		var key = arr[3];

		var delimgid = "notimg_notimage_" + koumoku + "_" + key;
		$("#" + delimgid).remove();
		var delbr1 = "br1_notimage_" + koumoku + "_" + key;
		$("#" + delbr1).remove();
		var delspanid = "spandel_notimage_" + koumoku + "_" + key;
		$("#" + delspanid).remove();
		var delfilenameid = "Notimage" + key + "Notimagename" + koumoku;
		$("#" + delfilenameid).remove();
		var delfileid = "Notimage" + key + "Notimage" + koumoku;
		$("#" + delfileid).remove();
		var delfileupid = "Notimage" + key + "Notimage" + koumoku + "Up";
		$("#" + delfileupid).remove();
		var delbr2 = "br2_notimage_" + koumoku + "_" + key;
		$("#" + delbr2).remove();

	});
	// アップロードボタンクリック
	$(document).on("click", "button[id^='upbtn_notimage_']" ,function(ev){
		var idname = ev.target.id;
		var arr = idname.split('_');
		var koumoku = arr[2];
		g_koumoku = koumoku;
		<?php $upload_url = $html->url('/users/notimg_upload'); ?>
		var url = "<?php echo "{$upload_url}?koumoku=" ?>" + koumoku + "&model=Notimage";
		var target = '#notimage_temp_' + koumoku;

		$('#uploadForm').ajaxSubmit({
			target: target, // サーバの戻りを出力する場所を指定
			url: url, // URL
			success: hogeResponse     // サーバからの応答時に呼び出す関数
		});
		return false;
	});

	//サーバ応答時実行関数
	function hogeResponse(responseText, statusText) {

//		alert('status: ' + statusText + '\n\nresponseText: \n' + responseText);
		// 現状アップされているイメージを最大値を取得する。
		var koumoku = g_koumoku;
		var max_no = 0;
		$('img[id^="img_notimage_'+koumoku+'_"]').each( function() {
			var idname = this.id;
			var arr = idname.split('_');
			var target_no = arr[3];
			target_no = eval(target_no);

			// 最後の行数取得
			if (eval(max_no) < eval(target_no)) {
				max_no = target_no;
			}
		});
		// ファイルをテンポラリ領域に上がっているので、クーロンを作成する。
		var tpl = $("#notimage_temp_" + koumoku).clone();
		// エラーメッセージがある場合
		var ermsg = tpl.find('p[id="cerror_notimage_'+koumoku+'"]').text();
		if (ermsg) {
			// エラーを消して
			$('p[id="error_notimage_'+koumoku+'"]').remove();
			var img_f = false;
			var topid = null;
			$('a[id^="notimg_notimage_'+koumoku+'"]').each( function() {
				img_f = true;
				topid = this.id;
			});
			// アップロードボタンの下にエラーを表示する。
			var pid = "cerror_notimage_" + koumoku;
			tpl.find('p[id=' + pid + ']').attr('id',  'error_notimage_' + koumoku);
			if (topid) {
				$('#' + topid).before(tpl.find('p[id=error_notimage_'+koumoku+']').show());
			} else {
				$('div[id="notimage_'+koumoku+'"]').append(tpl.find('p[id=error_notimage_'+koumoku+']').show());
			}
		} else {
			// エラー表示を消す
			$('p[id="error_notimage_'+koumoku+'"]').remove();
			max_no = max_no + 1;
			tpl.find('a[id^="cnotimg_notimage_'+koumoku+'_"]').each( function() {
				var idname = this.id;
				var arr = idname.split('_');
				var target_no = arr[3];
				target_no = eval(target_no);

				// イメージ表示域のコピー表示
				var imgid = "cnotimg_notimage_" + koumoku + "_" + target_no;
				tpl.find('a[id=' + imgid + ']').attr('id',  'notimg_notimage_' + koumoku + '_' + max_no);
				$('div[id="notimage_' + koumoku + '"]').append(tpl.find('a[id=notimg_notimage_' + koumoku + '_' + max_no + ']').show());

				// 改行表示域のコピー表示
				var br1id = "cbr1_notimage_" +koumoku+"_" + target_no;
				tpl.find('br[id=' + br1id + ']').attr('id', 'br1_notimage_' +koumoku+'_' + max_no);
				$('div[id="notimage_' + koumoku + '"]').append(tpl.find('br[id=br1_notimage_' + koumoku + '_' + max_no + ']').show());

				// 【画像を削除する】表示域のコピー表示
				var spanid = "cspandel_notimage_" +koumoku+"_" + target_no;
				tpl.find('span[id=' + spanid + ']').attr('id', 'spandel_notimage_' + koumoku + '_' + max_no);
				var btnid = "cadel_notimage_" + koumoku+ "_" + target_no;
				tpl.find('a[id=' + btnid + ']').attr('id', 'adel_notimage_' + koumoku + '_' + max_no);
				$('div[id="notimage_' + koumoku + '"]').append(tpl.find('span[id=spandel_notimage_' + koumoku + '_' + max_no + ']').show());

				// ファイル名域のコピー表示
				var notimageid = "cNotimage" + target_no + "Notimagename"+koumoku;
				tpl.find('input[id=' + notimageid + ']').attr('name',  'data[Notimage][' + max_no + '][notimagename' + koumoku + ']');
				tpl.find('input[id=' + notimageid + ']').attr('id', 'Notimage' + max_no + 'Notimagename' + koumoku);
				$('div[id="notimage_' + koumoku + '"]').append(tpl.find('input[id=' + 'Notimage' + max_no + 'Notimagename' + koumoku + ']').show());

				// テンポラリーファイル名域のコピー表示
				var notimageid = "cNotimage" + target_no + "Notimage"+koumoku;
				tpl.find('input[id=' + notimageid + ']').attr('name',  'data[Notimage][' + max_no + '][notimage' + koumoku + ']');
				tpl.find('input[id=' + notimageid + ']').attr('id', 'Notimage' + max_no + 'Notimage' + koumoku);
				$('div[id="notimage_' + koumoku + '"]').append(tpl.find('input[id=' + 'Notimage' + max_no + 'Notimage' + koumoku + ']').show());

				// ファイル名UP域のコピー表示
				var notimageupid = "cNotimage" + target_no + "Notimage"+koumoku+"Up";
				tpl.find('input[id=' + notimageupid + ']').attr('name',  'data[Notimage][' + max_no + '][notimage_' + koumoku + 'up]');
				tpl.find('input[id=' + notimageupid + ']').attr('id', 'Notimage' + max_no + 'Notimage' + koumoku + 'Up');
				$('div[id="notimage_' + koumoku + '"]').append(tpl.find('input[id=' + 'Notimage' + max_no + 'Notimage' + koumoku + 'Up' + ']').show());

				// 改行表示域のコピー表示
				var br2id = "cbr2_notimage_" +koumoku+"_" + target_no;
				tpl.find('br[id=' + br2id + ']').attr('id', 'br2_notimage_' + koumoku + '_' + max_no);
				$('div[id="notimage_' + koumoku + '"]').append(tpl.find('br[id=br2_notimage_' + koumoku + '_' + max_no + ']').show());
				// インクリメント
				max_no = max_no + 1;
			});
		}
		// 仮に上げた領域をクリアする。
		$("#notimage_temp_"+koumoku).html("");
		// ファイル指定をクリアする。
		$("#NotimageUpNotimage"+koumoku).val("");
	}

});
</script>
