<?php
if (!empty($error)){
	echo "<p id='cerror_image_{$koumoku}'>{$error}</p>";
}else{
	foreach($upFile as $key => $val) {
		// イメージ表示
		$dispFilename = ROOT_URL . 'upload/' . $idArr[$key] . '-s.jpg';
		echo "<img id='cimg_image_{$koumoku}_{$key}' src='{$dispFilename}'>";
		// 改行
		echo "<br id='cbr1_image_{$koumoku}_{$key}'>";
		// ダウンロードのアンカー表示
		$downloadUrl = $html->url("/uploads/download");
		$fileName = $upName[$key];
		$id = $idArr[$key];
		echo "<a  id='ca_image_{$koumoku}_{$key}' href='{$downloadUrl}/{$id}'>{$fileName}</a>";
		// 改行
		echo "<br id='cbr2_image_{$koumoku}_{$key}'>";
		// 画像を削除するのアンカー表示
		echo "<span id='cspandel_image_{$koumoku}_{$key}'>";
		echo "【<a href='#' id='cadel_image_{$koumoku}_{$key}'>画像を削除する</a>】";
		echo "</span>";
		// アップロードファイル名
		echo $appForm->input("c{$model}.{$koumoku}.imagename_{$key}", array('type' => 'hidden','value' => $upName[$key]));
		// アップ後のファイル名
		echo $appForm->input("c{$model}.{$koumoku}.image_{$key}", array('type' => 'hidden','value' => $filename[$key]));
		// Upしているときはあるが、削除されているときは消えている
		echo $appForm->input("c{$model}.{$koumoku}.image_{$key}_up", array('type' => 'hidden','value' => 1));
		// UploadのIDを保存
		echo $appForm->input("c{$model}.{$koumoku}.image_{$key}_id", array('type' => 'hidden','value' => $idArr[$key]));
		echo "<br id='cbr3_image_{$koumoku}_{$key}'>";
	}
}
?>