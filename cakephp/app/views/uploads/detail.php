<?php
	echo $appForm->create();

?>
<h3>ファイルアップロードテーブル詳細</h3>
<table class="table table-bordered table-navy">
	<tr>
	<th>拡張子</th>
	<td><?php echo esHtml($data['Upload']['extension']) ?></td>
</tr>
<tr>
	<th>ファイル名</th>
	<td><?php echo esHtml($data['Upload']['file_name']) ?></td>
</tr>
<tr>
	<th>UPファイル名</th>
	<td><?php echo esHtml($data['Upload']['up_file_name']) ?></td>
</tr>
<tr>
	<th>親テーブル名</th>
	<td><?php echo esHtml($data['Upload']['parent_table']) ?></td>
</tr>
<tr>
	<th>親テーブルID</th>
	<td><?php echo esHtml($data['Upload']['parent_id']) ?></td>
</tr>
<tr>
	<th>親フィールド名</th>
	<td><?php echo esHtml($data['Upload']['parent_field']) ?></td>
</tr>

</table>
<div class="right">
	<input type="submit" name="cancel" value="戻る" class="btn btn-default" />　
</div>

<?php
	echo $appForm->end();
?>
