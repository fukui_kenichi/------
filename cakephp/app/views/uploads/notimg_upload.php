<?php
if (!empty($error)){
	echo "<p id='cerror_notimage_{$koumoku}'>{$error}</p>";
}else{
	foreach($upFile as $key => $val) {
		// ダウンロードのアンカー表示
		$downloadUrl = $html->url("/uploads/download");
		$fileName = $upName[$key];
		$id = $idArr[$key];
		echo "<a id='cnotimg_notimage_{$koumoku}_{$key}' href='{$downloadUrl}/{$id}'>{$fileName}</a>";
		// 改行
		echo "<br id='cbr1_notimage_{$koumoku}_{$key}'>";
		// ファイルを削除するのアンカー表示
		echo "<span id='cspandel_notimage_{$koumoku}_{$key}'>";
		echo "【<a href='#' id='cadel_notimage_{$koumoku}_{$key}'>ファイルを削除する</a>】";
		echo "</span>";
		// アップロードファイル名
		echo $appForm->input("c{$model}.{$koumoku}.notimagename_{$key}", array('type' => 'hidden','value' => $upName[$key]));
		// アップ後のファイル名
		echo $appForm->input("c{$model}.{$koumoku}.notimage_{$key}", array('type' => 'hidden','value' => $filename[$key]));
		// Upしているときはあるが、削除されているときは消えている
		echo $appForm->input("c{$model}.{$koumoku}.notimage_{$key}_up", array('type' => 'hidden','value' => 1));
		// UploadのIDを保存
		echo $appForm->input("c{$model}.{$koumoku}.notimage_{$key}_id", array('type' => 'hidden','value' => $idArr[$key]));
		// 改行
		echo "<br id='cbr2_notimage_{$koumoku}_{$key}'>";
	}
}
?>