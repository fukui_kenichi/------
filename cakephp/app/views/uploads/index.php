<?php
	echo $appForm->create();
?>
<h3>
	ファイルアップロードテーブル一覧　
	
	<a href="#myModal_search" class="btn btn-default" data-toggle="modal"><i class="icon-search"></i> 検索</a>　
	<a class="btn btn-success" href="<?php echo $html->url("/uploads/edit") ?>"><strong>＋</strong>新規登録</a>
</h3>
<table class="table-header h2-inner" width="100%">
	<tr>
		<td class="right"  style="vertical-align: middle;line-height: 20px;">
			<?php echo $this->element('paginator') ?>
		</td>
	</tr>
</table>
<?php
if (!empty($list)) {
?>

<table class="table table-condensed table-bordered table-navy list">
<thead>
	<tr>
				<th width="100"></th>

	</tr>
</thead>
<tbody>
	<?php foreach ($list as $key => $val) { ?>
	<tr>
				<td>
			<a href="<?php echo $html->url("/uploads/edit/{$val['Upload']['id']}") ?>">編集</a>　
			<a href="#" onclick="return actionDelList(<?php echo $val['Upload']['id'] ?>);">削除</a>
		</td>

	</tr>
	<?php } ?>
</tbody>
</table>
<?php
} else {
	echo '<p>データがありません</p>';
}
?>

<div id="myModal_search" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 id="myModalLabel">検索</h3>
			</div>
			<div class="modal-body">
				<table class="table table-bordered table-navy">
					
				</table>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">閉じる</button>
				<input type="submit" name="search" class="btn btn-primary" value="検索">
			</div>
		</div>
	</div>
</div>

<div class="hide">
	<input type="hidden" name="hdnId">
	<input type="submit" name="del" value="削除">
</div>
<?php
	echo $appForm->end();
?>
