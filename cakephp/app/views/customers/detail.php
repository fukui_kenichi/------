<?php
	echo $appForm->create();
?>
<div class="row">
   	<div class="xs-invisible">
        <img src="<?php echo ROOT_URL ?>img/kokyaku-joho.png" alt="顧客情報">
    </div>
   	<div class="xs-visible">
        <img src="<?php echo ROOT_URL ?>img/kokyaku-joho_s.png" alt="顧客情報">
    </div>
</div>

<table class="table table-bordered table-navy table-edit">
<?php
	foreach ($customerMst as $key => $val) {
		$val = $val['CustomerMaster'];
		if ($val['field'] == "name") { // 名前
			echo '<tr>';
			$th = "<th>" . esHtml($val['koumoku']);
			$th .= '</th>';
			echo $th;
			echo '<td class="sm-center">';
			$name = $appForm->data['Customer']["name"];
			if (!empty($appForm->data['Customer']["picture"])) {
				$fileNm = pathinfo($appForm->data['Customer']["picture"], PATHINFO_FILENAME);
				$upFile = ROOT_URL . 'picture/img/' . $fileNm . '-s.jpg';
			} else {
				$upFile = ROOT_URL . 'img/no-image.jpg';
			}
			echo "<img src='{$upFile}'>";
			echo $name;
			echo '</td>';
			echo '</tr>';
		} elseif ($val['field'] == "address_info") { // 住所
			echo '<tr><th>住所</th><td class="sm-center">郵便番号：'. esHtml($appForm->data['Customer']['zip_code']);
			echo '<br/>住所：'. esHtml($appForm->data['Customer']['address']);
			echo '<br/><div id="map" class="form-control" style="height:250px"></div></td></tr>';
		} elseif ($val['field'] == "picture") { // 写真ならとばす
			continue;
		} else {
			echo '<tr>';
			$th = '<th width="150">' . esHtml($val['koumoku']);
			$th .= '</th>';
			echo $th;
			echo '<td class="sm-center">';
			switch ($val['type']) {
				case MASTER_TYPE_CHAR:		// 文字列（制限あり）
				case MASTER_TYPE_INTG:		// 数値
				case MASTER_TYPE_TELE:		// 電話型
					echo esHtml($appForm->data['Customer']["{$val['field']}"]);
					break;
				case MASTER_TYPE_DTTM:		// 日付時刻型
					if (isset($appForm->data['Customer']["{$val['field']}_d"])){
						echo esHtml($appForm->data['Customer']["{$val['field']}_d"]);
						echo '　';
					}
					if (isset($appForm->data['Customer']["{$val['field']}_t"])) {
						echo esHtml($appForm->data['Customer']["{$val['field']}_t"]);
					}
					break;
				case MASTER_TYPE_DATE:		// 日付型
				case MASTER_TYPE_DATE_A:	// 日付型(アラート)
					echo esHtml($appForm->data['Customer']["{$val['field']}"]);
					break;
				case MASTER_TYPE_TIME:		// 時刻型
					echo esHtml($appForm->data['Customer']["{$val['field']}"]);
					break;
				case MASTER_TYPE_TEXT:		// 文字列（制限なし）
				case MASTER_TYPE_CHEK:		// チェック型
					echo nl2br(esHtml($appForm->data['Customer']["{$val['field']}"]));
					break;
				case MASTER_TYPE_LIST:		// リスト型
					echo esHtml($appForm->data['Customer']["{$val['field']}"]);
					break;
				case MASTER_TYPE_RADI:		// ラジオ型
					echo esHtml($appForm->data['Customer']["{$val['field']}"]);
					break;
				case MASTER_TYPE_FIMG:
					$img_i = $val['number'];
					if (!empty($appForm->data['Image'][$img_i])) {
						echo "<div id='slider_image_{$img_i}'>";
						foreach ($appForm->data['Image'][$img_i] as $field => $v) {
							$fieldArr = explode("_",$field);
							$key = $fieldArr[1];
							if(!isset($imageId_fArr[$img_i][$key])) {
								$imageId_fArr[$img_i][$key] = 0;
							}
							// image_(no)だけを処理するように選別
							if ($fieldArr[0] == "imagename") {
								continue;
							}
							if (count($fieldArr) == 3) {
								continue;
							}
							if (count($fieldArr) == 2) {
								if (!empty($appForm->data['Image'][$img_i]["image_{$key}"])) {
									$fileName = $appForm->data['Image'][$img_i]["imagename_{$key}"];
									$ext = pathinfo($fileName, PATHINFO_EXTENSION);
									$upFile = ROOT_URL . 'upload/' . $appForm->data['Image'][$img_i]["image_{$key}_id"] . '.' . $ext;
									$upFile_s = ROOT_URL . 'upload/' . $appForm->data['Image'][$img_i]["image_{$key}_id"] . '-s.' . $ext;
								}
								$div = "<div><a class='example-image-link' href='{$upFile}' data-lightbox='group1'><img src='{$upFile_s}'></a></div>";
								echo $div;
							}
						}
						echo "</div>";
					}
					break;
				case MASTER_TYPE_FILE:
					$notimg_i = $val['number'];
					echo "<div id='notimage_{$notimg_i}'>";
					if (!empty($appForm->data['Notimage'][$notimg_i])) {
						$notimageId_fArr[$notimg_i][] = array();
						// 画像を表示する処理
						foreach ($appForm->data['Notimage'][$notimg_i] as $field => $v) {
							$fieldArr = explode("_",$field);
							$key = $fieldArr[1];
							if(!isset($notimageId_fArr[$notimg_i][$key])) {
								$notimageId_fArr[$notimg_i][$key] = 0;
							}
							// image_(no)だけを処理するように選別
							if ($fieldArr[0] == "notimagename") {
								continue;
							}
							if (count($fieldArr) == 3) {
								continue;
							}
							if (count($fieldArr) == 2) {
								$downloadUrl = $html->url("/uploads/download");
								$fileName = $appForm->data['Notimage'][$notimg_i]["notimagename_{$key}"];
								$id = $appForm->data['Notimage'][$notimg_i]["notimage_{$key}_id"];
								echo "<a id='notimg_notimage_{$notimg_i}_{$key}' href='{$downloadUrl}/{$id}'>{$fileName}</a>";
								echo "<br id='br1_notimage_{$notimg_i}_{$key}'>";
							}
						}
					}
					echo "</div>";
					break;
			}
			echo '</td>';
			echo '</tr>';

		}
	}
?>
</table>
<div class="row">
	<div class="center">
        <div class="xs-invisible">
            <a href="javascript:history.back();"><img class="help" alt="戻る" src="<?php echo ROOT_URL ?>img/bt_back.png" title="戻る"></a>
        </div>
        <div class="xs-visible">
            <a href="javascript:history.back();"><img class="help" alt="戻る" src="<?php echo ROOT_URL ?>img/bt_back_s.png" title="戻る"></a>
        </div>
	</div>
</div>

<?php
	echo $appForm->end();
?>
<!-- GoogleMap関数 -->
<?php if (empty($_SERVER['HTTPS'])) { ?>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
<?php }else{ ?>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true"></script>
<?php } ?>
<script type="text/javascript">
var map; // 地図オブジェクト
var markerObj; // マーカーオブジェクト
$(function(){
	$("div[id^='slider_image_']").each( function() {
		var id = this.id;
		$('#' + id).bxSlider({
			auto: true, /* 自動再生 */
			adaptiveHeight: 'true', /* 高さを自動調整 */
			autoControls: false,  /* スタート、ストップボタン */
			pager: true, /* ページャー */
			mode: 'horizontal', /* fade,vertical など */
			speed: 1000, /* エフェクトのスピード */
			controls: true, /* 前へ、次へボタンの表示 */
			prevText: '&lt;', /* 前へボタンのテキスト */
			nextText: '&gt;', /* 次へボタンのテキスト */
			pause: 4000, /* 間隔の時間 */
			easing: 'swing', /* Easing */
			autoHover: true /* マウスホバーで停止 */
		});
	});

<?php if (!empty($appForm->data['Customer']['latitude']) && !empty($appForm->data['Customer']['longitude'])) { // 緯度経度が入っている場合 ?>
	// 緯度経度（ない場合はJR大阪駅）
	latitude = <?php echo $appForm->data['Customer']['latitude']?>;
	longitude = <?php echo $appForm->data['Customer']['longitude']?>;

	centerLatLng = new google.maps.LatLng(latitude, longitude); // 中心点
	/* 地図のオプション設定 */
	myOptions = {
		/*初期のズーム レベル */
		zoom: 17,
		/* 地図の中心点 */
		center: centerLatLng,
		/* 地図タイプ */
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	// オプションを設定
	map = new google.maps.Map(document.getElementById("map"), myOptions);

	// マーカー
	marker = new google.maps.Marker({
		position: centerLatLng,
		map: map,
		draggable:false
	});
<?php } // 緯度経度が入っている場合 ?>
});
</script>
