<?php
	echo $appForm->create('FileEdit', array('name' => 'form','type' => 'file','id' => 'uploadForm', 'enctype' => 'multipart/form-data'));
?>

<div class="row">
   	<div class="xs-invisible">
   		<?php if (empty($appForm->data['Customer']['id'])){ ?>
    	    <img src="<?php echo ROOT_URL ?>img/kokyaku-toroku.png" alt="顧客登録">
        <?php }else{ ?>
	        <img src="<?php echo ROOT_URL ?>img/kokyaku-henshu.png" alt="顧客編集">
        <?php } ?>
    </div>
   	<div class="xs-visible">
   		<?php if (empty($appForm->data['Customer']['id'])){ ?>
	        <img src="<?php echo ROOT_URL ?>img/kokyaku-toroku_s.png" alt="顧客登録">
        <?php }else{ ?>
    	    <img src="<?php echo ROOT_URL ?>img/kokyaku-henshu_s.png" alt="顧客編集">
        <?php } ?>
    </div>
</div>

<?php if (!empty($appForm->validationErrors)) {?>
<div class="error-message">
    <div class="error-message-inner">
	    <p class="error-message-header">入力内容にエラーがあります。</p>
    </div>
</div>
<?php }?>
<table class="table table-bordered table-navy table-edit">
<?php
	foreach ($customerMst as $key => $val) {
		$val = $val['CustomerMaster'];
		if ($val['field'] == 'picture') { // 写真
			echo '<tr>';
			$th = "<th>" . esHtml($val['koumoku']);
			$th .= '</th>';
			echo $th;
			echo '<td class="sm-center">';
			echo $appForm->input("CustomerEdit.up_picture", array('type' => 'file'));
			$upload_url = $html->url('/customers/upload');
			echo $appForm->button('アップロード',array('onClick' => "$('#uploadForm').ajaxSubmit({target: '#img',url: '{$upload_url}?koumoku=picture&model=Customer'}); return false;"));
			echo "<div id='img'>";
			if (!empty($appForm->data['Customer']["picture"])) {
				if (empty($appForm->data['Customer']["picture_up"])) {
					$fileNm = pathinfo($appForm->data['Customer']["picture"], PATHINFO_FILENAME);
					$upFile = ROOT_URL . 'picture/img/' . $fileNm . '-s.jpg';
				} else {
					$fileNm = pathinfo($appForm->data['Customer']["picture"], PATHINFO_FILENAME);
					$upFile = ROOT_URL . 'picture/temp/' . $fileNm . '-s.jpg';
				}
				echo "<img src='{$upFile}'>";
				echo $appForm->input("Customer.picture", array('type' => 'hidden'));
				echo "<br>";
				$delete_url = $html->url('/customers/delete');
				echo "【<a href='#' onClick = \"$('#uploadForm').ajaxSubmit({target: '#img',url: '{$delete_url}?koumoku=picture&model=Customer'}); return false;\">画像を削除する</a>】";
			}
			echo "</div>";
			echo '</td>';
			echo '</tr>';
		} else if ($val['field'] == 'address_info') { // 住所
			echo '<tr><th>住所</th><td class="sm-center">郵便番号：';
 			echo $appForm->input('Customer.zip_code', array('type' => 'text', 'size' => '8', 'class' => 'ime-off', 'onKeyUp' => "AjaxZip2.zip2addr(this, 'data[Customer][address]', 'data[Customer][address]');"));
			echo '<br/>住所：<br/>';
			echo $appForm->input('Customer.address', array('type' => 'text', 'maxLength' => 256, 'class' => 'form-control'));
			echo '<br/><input type="button" class="btn btn-primary btn-sm" id="btnMap" value="住所を地図に反映" /><br/><div id="map" class="form-control" style="height:250px"></div>';
			echo '</td></tr>';
		} else if ($val['field'] == 'last_event_date') { // 最終イベント
			echo '<tr><th>'. esHtml($val['koumoku']). '</th><td class="sm-center">';
			if (!empty($appForm->data['Customer']['last_event_date'])) { //
				echo esHtml($appForm->data['Customer']['last_event_date']);
			}
			echo '</td></tr>';
		} else {
			echo '<tr>';
			$th = '<th width="150">' . esHtml($val['koumoku']);
			if ($val['required']) {
				$th .= '<span class="err-msg">【必須】</span>';
			}
			$th .= '</th>';
			echo $th;
			echo '<td class="sm-center">';
			switch ($val['type']) {
				case MASTER_TYPE_CHAR:		// 文字列（制限あり）
				case MASTER_TYPE_INTG:		// 数値
				case MASTER_TYPE_TELE:	// 電話型
					echo $appForm->input("Customer.{$val['field']}", array('type' => 'text', 'class' => 'form-control'));
					$span = '';
					if (isset($appForm->validationErrors['Customer']["{$val['field']}"])) {
						$span .= '<span class="err-msg">' . esHtml($appForm->validationErrors['Customer']["{$val['field']}"]). '</span>';
					}
					echo $span;
					break;
				case MASTER_TYPE_DTTM:		// 日付時刻型
					echo "日付：";
					echo $appForm->input("Customer.{$val['field']}_d", array('type' => 'text', 'class' => 'calendar'));
					echo "<br>時間：";
					echo $appForm->input("Customer.{$val['field']}_t", array('type' => 'text', 'class' => 'times', 'size' => 8));
					$span = '';
					if (isset($appForm->validationErrors['Customer']["{$val['field']}_d"])) {
						$span .= '<br><span class="err-msg">' . esHtml($appForm->validationErrors['Customer']["{$val['field']}_d"]). '</span>';
					}
					if (isset($appForm->validationErrors['Customer']["{$val['field']}_t"])) {
						$span .= '<br><span class="err-msg">' . esHtml($appForm->validationErrors['Customer']["{$val['field']}_t"]). '</span>';
					}
					echo $span;
					break;
				case MASTER_TYPE_DATE:		// 日付型
				case MASTER_TYPE_DATE_A:	// 日付型(アラート)
					echo $appForm->input("Customer.{$val['field']}", array('type' => 'text', 'class' => 'calendar'));
					$span = '';
					if (isset($appForm->validationErrors['Customer']["{$val['field']}"])) {
						$span .= '<br><span class="err-msg">' . esHtml($appForm->validationErrors['Customer']["{$val['field']}"]). '</span>';
					}
					echo $span;
					break;
				case MASTER_TYPE_TIME:		// 時刻型
					echo $appForm->input("Customer.{$val['field']}", array('type' => 'text', 'class' => 'times'));
					$span = '';
					if (isset($appForm->validationErrors['Customer']["{$val['field']}"])) {
						$span .= '<br><span class="err-msg">' . esHtml($appForm->validationErrors['Customer']["{$val['field']}"]). '</span>';
					}
					echo $span;
					break;
				case MASTER_TYPE_TEXT:		// 文字列（制限なし）
					echo $appForm->input("Customer.{$val['field']}", array('type' => 'textarea', 'rows' => "3", 'class' => 'form-control'));
					break;
				case MASTER_TYPE_LIST:		// リスト型
					// 改行を\nに統一
					$text = preg_replace(array('/\r\n/','/\r/','/\n/'), "\n", $val['select_ti'] );
					$listArr = explode("\n",$text);
					// 現状セットされている値を取得
					$ipData = isset($appForm->data['Customer']["{$val['field']}"])?$appForm->data['Customer']["{$val['field']}"]:null;
					// リストに追加
					$add_f = true;
					$selArr = array();
					foreach ($listArr as $k => $v) {
						// セットされている値がリストの中に含まれている場合は、追加なし
						if ($v == $ipData) {
							$add_f = false;
						}
						// セレクト値をセレクト名に変更
						$selArr[$v] = $v;
					}
					// セレクト値が変更になっていて、値が設定されていた場合
					if ($add_f && notEmpty($ipData)) {
						$selArr[$ipData] = $ipData;
					}
					if (!empty($ipData)) {
						echo $appForm->input("Customer.{$val['field']}", array('type' => 'select', "options" => $selArr , 'class' => 'form-control', 'empty' => '---'));
					} else {
						echo $appForm->input("Customer.{$val['field']}", array('type' => 'select', "options" => $selArr , 'class' => 'form-control', 'empty' => '---', 'value' => $val['default_ti']));
					}
					$span = '';
					if (isset($appForm->validationErrors['Customer']["{$val['field']}"])) {
						$span .= '<span class="err-msg">' . esHtml($appForm->validationErrors['Customer']["{$val['field']}"]). '</span>';
					}
					echo $span;
					break;
				case MASTER_TYPE_RADI:		// ラジオ型
					// 改行を\nに統一
					$text = preg_replace(array('/\r\n/','/\r/','/\n/'), "\n", $val['select_ti'] );
					$listArr = explode("\n",$text);
					// 現状セットされている値を取得
					$ipData = isset($appForm->data['Customer']["{$val['field']}"])?$appForm->data['Customer']["{$val['field']}"]:null;
					// リストに追加
					$add_f = true;
					$selArr = array();
					foreach ($listArr as $k => $v) {
						// セットされている値がリストの中に含まれている場合は、追加なし
						if ($v == $ipData) {
							$add_f = false;
						}
						// セレクト値をセレクト名に変更
						$selArr[$v] = $v;
					}
					// セレクト値が変更になっていて、値が設定されていた場合
					if ($add_f && notEmpty($ipData)) {
						$selArr[$ipData] = $ipData;
					}
					if (!empty($ipData)) {
						echo $appForm->input("Customer.{$val['field']}", array('type' => 'radio', "options" => $selArr ));
					} else {
						echo $appForm->input("Customer.{$val['field']}", array('type' => 'radio', "options" => $selArr , 'value' => $val['default_ti']));
					}
					$span = '';
					if (isset($appForm->validationErrors['Customer']["{$val['field']}"])) {
						$span .= '<br><span class="err-msg">' . esHtml($appForm->validationErrors['Customer']["{$val['field']}"]). '</span>';
					}
					echo $span;
					break;
				case MASTER_TYPE_CHEK:		// チェック型
					// マスターの選択値
					$text = preg_replace(array('/\r\n/','/\r/','/\n/'), "\n", $val['select_ti'] ); // 改行を\nに統一
					$listArr = explode("\n",$text);

					// 現状セットされている値を取得
					$ipData = isset($appForm->data['Customer']["{$val['field']}"])?$appForm->data['Customer']["{$val['field']}"]:null;
					$ipData = preg_replace(array('/\r\n/','/\r/','/\n/'), "\n", $ipData ); // 改行を\nに統一
					$ipDataArr = explode("\n",$ipData);

					// フォームの生成
					foreach ((array)$listArr as $k => $v) {
						$flag = false;
						foreach ((array)$ipDataArr as $ip){
							if ($v == $ip){
								$flag = true;
							}
						}
						if ($flag == true){
							echo $appForm->input("Customer.{$val['field']}.$k", array('type' => 'checkbox', "label" => $v, "checked" => "checked", "value" => 1 ));
						}else{
							echo $appForm->input("Customer.{$val['field']}.$k", array('type' => 'checkbox', "label" => $v, "value" => 0 ));
						}
						echo '<br>';
					}
					$span = '';
					if (isset($appForm->validationErrors['Customer']["{$val['field']}"])) {
						$span .= '<br><span class="err-msg">' . esHtml($appForm->validationErrors['Customer']["{$val['field']}"]). '</span>';
					}
					echo $span;
					break;
				case MASTER_TYPE_FIMG:
					$img_i = $val['number'];
					echo $appForm->input("Image.up_image_{$img_i}. ", array('type' => 'file','multiple' => 'multiple'));
					echo "<button type='button' id='upbtn_image_{$img_i}'>アップロード</button>";
					echo "<div id='image_{$img_i}'>";
					if (!empty($appForm->data['Image'][$img_i])) {
						$imageId_fArr[$img_i][] = array();
						// 画像を表示する処理
						foreach ($appForm->data['Image'][$img_i] as $field => $v) {
							$fieldArr = explode("_",$field);
							$key = $fieldArr[1];
							if(!isset($imageId_fArr[$img_i][$key])) {
								$imageId_fArr[$img_i][$key] = 0;
							}
							// image_(no)だけを処理するように選別
							if ($fieldArr[0] == "imagename") {
								continue;
							}
							if (count($fieldArr) == 3) {
								continue;
							}
							if (count($fieldArr) == 2) {
								if (!empty($appForm->data['Image'][$img_i]["image_{$key}"])) {
									$upFile = ROOT_URL . 'upload/' . $appForm->data['Image'][$img_i]["image_{$key}_id"] . '-s.jpg';
								}
								echo "<img id='img_image_{$img_i}_{$key}' src='{$upFile}'>";
								echo "<br id='br1_image_{$img_i}_{$key}'>";
								$downloadUrl = $html->url("/uploads/download");
								$fileName = $appForm->data['Image'][$img_i]["imagename_{$key}"];
								$id = $appForm->data['Image'][$img_i]["image_{$key}_id"];
								echo "<a  id='a_image_{$img_i}_{$key}' href='{$downloadUrl}/{$id}'>{$fileName}</a>";
								echo "<br id='br2_image_{$img_i}_{$key}'>";
								echo "<span id='spandel_image_{$img_i}_{$key}'>";
								echo "【<a href='#' id='adel_image_{$img_i}_{$key}'>画像を削除する</a>】";
								echo "</span>";
								echo $appForm->input("Image.{$img_i}.imagename_{$key}", array('type' => 'hidden','value' => $appForm->data['Image'][$img_i]["imagename_{$key}"]));
								echo $appForm->input("Image.{$img_i}.image_{$key}", array('type' => 'hidden','value' => $appForm->data['Image'][$img_i]["image_{$key}"]));
								echo $appForm->input("Image.{$img_i}.image_{$key}_up", array('type' => 'hidden','value' => 1));
								echo $appForm->input("Image.{$img_i}.image_{$key}_id", array('type' => 'hidden','value' => $appForm->data['Image'][$img_i]["image_{$key}_id"]));
								echo "<br id='br3_image_{$img_i}_{$key}'>";
								$imageId_fArr[$img_i][$key] = 1;
							}
						}
						// 画像が削除されていて、IDだけが残っている場合のID隠し表示処理
						foreach ($imageId_fArr[$img_i] as $key => $f) {
							if ($f == 0) {
								echo $appForm->input("Image.{$img_i}.image_{$key}_id", array('type' => 'hidden','value' => $appForm->data['Image'][$img_i]["image_{$key}_id"]));
							}
						}
					}
					echo "</div>";
					echo "<div id='image_temp_{$img_i}'>";
					echo "</div>";
					break;
				case MASTER_TYPE_FILE:
					$notimg_i = $val['number'];
					echo $appForm->input("Notimage.up_notimage_{$notimg_i}. ", array('type' => 'file','multiple' => 'multiple'));
					echo "<button type='button' id='upbtn_notimage_{$notimg_i}'>アップロード</button>";
					echo "<div id='notimage_{$notimg_i}'>";
					if (!empty($appForm->data['Notimage'][$notimg_i])) {
						$notimageId_fArr[$notimg_i][] = array();
						// 画像を表示する処理
						foreach ($appForm->data['Notimage'][$notimg_i] as $field => $v) {
							$fieldArr = explode("_",$field);
							$key = $fieldArr[1];
							if(!isset($notimageId_fArr[$notimg_i][$key])) {
								$notimageId_fArr[$notimg_i][$key] = 0;
							}
							// image_(no)だけを処理するように選別
							if ($fieldArr[0] == "notimagename") {
								continue;
							}
							if (count($fieldArr) == 3) {
								continue;
							}
							if (count($fieldArr) == 2) {
								$downloadUrl = $html->url("/uploads/download");
								$fileName = $appForm->data['Notimage'][$notimg_i]["notimagename_{$key}"];
								$id = $appForm->data['Notimage'][$notimg_i]["notimage_{$key}_id"];
								echo "<a id='notimg_notimage_{$notimg_i}_{$key}' href='{$downloadUrl}/{$id}'>{$fileName}</a>";
								echo "<br id='br1_notimage_{$notimg_i}_{$key}'>";
								echo "<span id='spandel_notimage_{$notimg_i}_{$key}'>";
								echo "【<a href='#' id='adel_notimage_{$notimg_i}_{$key}'>ファイルを削除する</a>】";
								echo "</span>";
								echo $appForm->input("Notimage.{$notimg_i}.notimagename_{$key}", array('type' => 'hidden','value' => $appForm->data['Notimage'][$notimg_i]["notimagename_{$key}"]));
								echo $appForm->input("Notimage.{$notimg_i}.notimage_{$key}", array('type' => 'hidden','value' => $appForm->data['Notimage'][$notimg_i]["notimage_{$key}"]));
								echo $appForm->input("Notimage.{$notimg_i}.notimage_{$key}_up", array('type' => 'hidden','value' => 1));
								echo $appForm->input("Notimage.{$notimg_i}.notimage_{$key}_id", array('type' => 'hidden','value' => $appForm->data['Notimage'][$notimg_i]["notimage_{$key}_id"]));
								echo "<br id='br2_notimage_{$notimg_i}_{$key}'>";
								$notimageId_fArr[$notimg_i][$key] = 1;
							}
						}
						// 画像が削除されていて、IDだけが残っている場合のID隠し表示処理
						foreach ($notimageId_fArr[$notimg_i] as $key => $f) {
							if ($f == 0) {
								echo $appForm->input("Notimage.{$notimg_i}.notimage_{$key}_id", array('type' => 'hidden','value' => $appForm->data['Notimage'][$notimg_i]["notimage_{$key}_id"]));
							}
						}
					}
					echo "</div>";
					echo "<div id='notimage_temp_{$notimg_i}'>";
					echo "</div>";
					break;
			}
			echo '</td>';
			echo '</tr>';

		}
	}
?>
</table>
<div class="row">
	<div class="center">
		<input type="image" id="save" name="save" src="<?php echo ROOT_URL ?>img/space.gif">
		<div class="xs-invisible">
			<?php if (empty($appForm->data['Customer']['id'])){ ?>
				<input type="image" name="save_conf" id="noLoading" alt="登録" src="<?php echo ROOT_URL ?>img/bt_apply.png" title="登録" onClick="checkCustomerName(); return false;" />
			<?php }else{ ?>
				<input type="image" name="save_conf" id="noLoading" alt="更新" src="<?php echo ROOT_URL ?>img/bt_update.png" title="更新" onClick="checkCustomerName(); return false;" />
			<?php } ?>
			<input type="image" name="cancel" alt="キャンセル" src="<?php echo ROOT_URL ?>img/bt_cancel.png" title="キャンセル" />
		</div>
		<div class="xs-visible">
			<?php if (empty($appForm->data['Customer']['id'])){ ?>
				<input type="image" name="save_conf" id="noLoading" alt="登録" src="<?php echo ROOT_URL ?>img/bt_apply_s.png" title="登録" onClick="checkCustomerName(); return false;" />
			<?php }else{ ?>
				<input type="image" name="save_conf" id="noLoading" alt="更新" src="<?php echo ROOT_URL ?>img/bt_update_s.png" title="更新" onClick="checkCustomerName(); return false;" />
			<?php } ?>
			<input type="image" name="cancel" alt="キャンセル" src="<?php echo ROOT_URL ?>img/bt_cancel_s.png" title="キャンセル" />
		</div>
	</div>
</div>
<div class="hide">
<?php
	echo $appForm->input('Customer.id', array('type' => 'hidden'));
	echo $appForm->input('Customer.updated', array('type' => 'hidden'));
	echo $appForm->input('Customer.last_event_date', array('type' => 'hidden'));
	echo $appForm->input('Customer.latitude', array('type' => 'hidden'));
	echo $appForm->input('Customer.longitude', array('type' => 'hidden'));
?>
</div>

<?php
	echo $appForm->end();
?>
<!-- 複数ファイルを取り扱う関数 -->
<script type="text/javascript" src="<?php echo ROOT_URL ?>js/multiple_file.js" ></script>
<!-- GoogleMap関数 -->
<?php if (empty($_SERVER['HTTPS'])) { ?>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
<?php }else{ ?>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true"></script>
<?php } ?>
<script type="text/javascript">
var img_up_url = "<?php echo $html->url('/uploads/img_upload'); ?>"; // 画像ファイルアップロードパス
var notimg_up_url = "<?php echo $html->url('/uploads/notimg_upload'); ?>"; // ファイルアップロードパス
var map; // 地図オブジェクト
var marker; // マーカーオブジェクト
$(function(){
	// 緯度経度（ない場合はJR大阪駅）
	var latitude = ($("#CustomerLatitude").val() == "")?  34.701889 : $("#CustomerLatitude").val();
	var longitude = ($("#CustomerLongitude").val() == "")?  135.494972 : $("#CustomerLongitude").val();

	centerLatLng = new google.maps.LatLng(latitude, longitude); // 中心点
	/* 地図のオプション設定 */
	myOptions = {
		/*初期のズーム レベル */
		zoom: 17,
		/* 地図の中心点 */
		center: centerLatLng,
		/* 地図タイプ */
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	// オプションを設定
	map = new google.maps.Map(document.getElementById("map"), myOptions);

	// マーカー
	marker = new google.maps.Marker({
		position: centerLatLng,
		map: map,
		draggable:true
	});

	/* イベントリスナー設定 マーカーのドラッグが終了した時 */
	google.maps.event.addListener(marker, 'dragend', function(ev) {
		$("#CustomerLatitude").val(ev.latLng.lat());
		$("#CustomerLongitude").val(ev.latLng.lng());
	});
});


// 保存時に住所が入力されていれば緯度と経度を取得する
$("input#btnMap").click(function(ev) {
	// 住所が入っているなら住所から緯度経度を取得
	if ($("#CustomerAddress").val() != "") {

		// 緯度経度検索
		var geocoder = new google.maps.Geocoder();
		geocoder.geocode(
			{
				'address': $("#CustomerAddress").val(),
				'region': 'jp'
			},
			function(results, status){
				if(status==google.maps.GeocoderStatus.OK){
					// 地図とマーカー移動
					map.panTo(results[0].geometry.location);
					marker.position = results[0].geometry.location;
					marker.setMap(map);

					// 緯度経度を取得
			        var latlng = results[0].geometry.location;
			        var latitude = latlng.lat();
			        var longitude = latlng.lng();

					// 緯度経度設定
					$("#CustomerLatitude").val(latitude);
					$("#CustomerLongitude").val(longitude);
				} else {
					// 緯度経度初期化
					$("#CustomerLatitude").val("");
					$("#CustomerLongitude").val("");
				}
			}
		);
	} else {
		// 緯度経度初期化
		$("#CustomerLatitude").val("");
		$("#CustomerLongitude").val("");
	}
});

//自分以外で同じ顧客名がいるかチェック
function checkCustomerName() {
	var name = $("#CustomerName").val();
	var id = $("#CustomerId").val();
	var count = "";

	$.ajax({
		type: "POST",
		url: "<?php echo $html->url('/customers/ajax_check_customer'); ?>",
		async: false,
		dataType: 'text',
		async: false,
		data: {id : id, name : name},
		success : function(result) {
			count = result;
		}
	});
	if (count == 1){
		jConfirm('同じ名前の顧客がすでに登録されていますが、別の顧客として登録してもよろしいですか？', 'WATSON顧客管理', function(r) {
			if(r) {
			    $("input#save").trigger("click");
			} else {
			    return false;
			}
		});
	}else{
	    $("input#save").trigger("click");
	}
}
</script>