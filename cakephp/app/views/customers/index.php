<?php
	echo $appForm->create();
	// 都道府県
	$PrefDef = new PrefDef();
	$prefArr = $PrefDef->getArr();
?>

<div class="row">
	<div class="xs-invisible">
		<?php if($this->action=='index'){ ?>
			<img src="<?php echo ROOT_URL ?>img/search_kokyaku-kanri.png" alt="顧客管理">
		<?php }elseif($this->action=='index_busi'){ ?>
			<img src="<?php echo ROOT_URL ?>img/search_shodan-toroku.png" alt="商談登録">
		<?php }elseif($this->action=='index_mail'){ ?>
			<img src="<?php echo ROOT_URL ?>img/search_mailyoyakuhaishin.png" alt="メール配信">
		<?php } ?>
		<div class="title_right">
			<a href="<?php echo $html->url("/customers/edit") ?>"><img src="<?php echo ROOT_URL ?>img/bt_add_customer.png" alt="新規顧客登録"></a>
		</div>
	</div>
	<div class="xs-visible">
		<?php if($this->action=='index'){ ?>
			<img src="<?php echo ROOT_URL ?>img/search_kokyaku-kanri_s.png" alt="顧客管理">
		<?php }elseif($this->action=='index_busi'){ ?>
			<img src="<?php echo ROOT_URL ?>img/search_shodan-toroku_s.png" alt="商談登録">
		<?php }elseif($this->action=='index_mail'){ ?>
			<img src="<?php echo ROOT_URL ?>img/search_mailyoyakuhaishin_s.png" alt="メール配信">
		<?php } ?>
		<div class="title_right">
			<a href="<?php echo $html->url("/customers/edit") ?>"><img src="<?php echo ROOT_URL ?>img/bt_add_customer_s.png" alt="新規顧客登録"></a>
		</div>
	</div>
</div>

<div class="xs-invisible">
	<img alt="検索キーワード" src="<?php echo ROOT_URL ?>img/keyword.png">
</div>
<div class="xs-visible">
	<img alt="検索キーワード" src="<?php echo ROOT_URL ?>img/keyword_s.png">
</div>
<div class="searchFrame">
	<?php echo $appForm->input('Search.text', array('type' => 'text', 'class' => 'inputBox form-control'))?>
	<div class="clearButton" onclick="searchAction();" ></div>
</div>
<br/>
<div class="row">
	<div class="sub_left">
		<?php echo $this->element('search_message_area') ?>
	</div>
	<div class="sub_right">
		<input type="image" id="search" name="search" src="<?php echo ROOT_URL ?>img/space.gif">
		<input type="image" id="send_mail" name="send_mail" src="<?php echo ROOT_URL ?>img/space.gif">
		<div class="xs-invisible">
			<input type="image" name="add_favorite" src="<?php echo ROOT_URL ?>img/bt_add_favorite.png" alt="お気入りに追加">
			<input type="image" name="mail_conf" id="noLoading" src="<?php echo ROOT_URL ?>img/bt_mail_delivery.png" alt="メール配信" onClick="mail_conf(); return false;">
		</div>
		<div class="xs-visible">
			<input type="image" name="add_favorite" src="<?php echo ROOT_URL ?>img/bt_add_favorite_s.png" alt="お気入りに追加">
			<input type="image" name="mail_conf" id="noLoading" src="<?php echo ROOT_URL ?>img/bt_mail_delivery_s.png" alt="メール配信" onClick="mail_conf(); return false;">
		</div>
	</div>
</div>
<br/>
<?php
if (!empty($list)) {
?>
<div class="smart-table full" id="customerTable">
	<div class="smart-table-header">
		<div class="smart-table-header-inner">
			<table>
				<thead>
					<tr>
						<th><div style="width:100px;"></div></th>
	<?php
	if (!empty($customerKojin)) {
		foreach($customerKojin as $key => $val) {
			if ($val['CustomerMaster']['field'] == 'name') {
				$value = esHtml($val['CustomerMaster']['koumoku']);
				$str = ($sort == $val['CustomerMaster']['field'])?(($direction == 'desc')?'<input type="button" class="btn btnSortUp" title="↑">':(($direction == 'asc')?'<input type="button" class="btn btnSortDown" title="↓">':"")):"";
				$th = "<th style='border-right-style: none;'><div style='width:101px;'></div></th>";
				echo $th;
				$th = "<th style='border-left-style: none;' class='sort'><div style='width:300px;'>{$value}{$str}</div></th>";
				echo $th;

			} else if ($val['CustomerMaster']['field'] == 'address_info') { // 住所
				// 郵便番号
				$str = ($sort == 'zip_code')?(($direction == 'desc')?'<input type="button" class="btn btnSortUp" title="↑">':(($direction == 'asc')?'<input type="button" class="btn btnSortDown" title="↓">':"")):"";
				$th = "<th class='sort'><div style='width:100px;'>郵便番号{$str}</div></th>";
				echo $th;
				// 住所
				$str = ($sort == 'address')?(($direction == 'desc')?'<input type="button" class="btn btnSortUp" title="↑">':(($direction == 'asc')?'<input type="button" class="btn btnSortDown" title="↓">':"")):"";
				$th = "<th class='sort'><div style='width:200px;'>住所{$str}</div></th>";
				echo $th;
			} else {
				switch ($val['CustomerMaster']['type']) {
					case MASTER_TYPE_CHAR:		// 文字列（制限あり）
					case MASTER_TYPE_INTG:		// 数値型
					case MASTER_TYPE_DTTM:		// 日付時刻型
					case MASTER_TYPE_DATE:		// 日付型
					case MASTER_TYPE_DATE_A:	// 日付型(アラート)
					case MASTER_TYPE_TIME:		// 時間型
					case MASTER_TYPE_LIST:		// リスト型
					case MASTER_TYPE_RADI:		// ラジオ型
					case MASTER_TYPE_TELE:		// 電話型
					case MASTER_TYPE_CHEK:		// チェック型
						$value = esHtml($val['CustomerMaster']['koumoku']);
						$str = ($sort == $val['CustomerMaster']['field'])?(($direction == 'desc')?'<input type="button" class="btn btnSortUp" title="↑">':(($direction == 'asc')?'<input type="button" class="btn btnSortDown" title="↓">':"")):"";
						$th = "<th class='sort'><div style='width:100px;'>{$value}{$str}</div></th>";
						echo $th;
						break;
					case MASTER_TYPE_TEXT:		// 文字列（制限なし）
						$value = esHtml($val['CustomerMaster']['koumoku']);
						$str = ($sort == $val['CustomerMaster']['field'])?(($direction == 'desc')?'<input type="button" class="btn btnSortUp" title="↑">':(($direction == 'asc')?'<input type="button" class="btn btnSortDown" title="↓">':"")):"";
						$th = "<th class='sort'><div style='width:100px;height:40px;overflow:hidden;'>{$value}{$str}</div></th>";
						echo $th;
						break;
				}
			}
		}
	}
	?>
						<th class="space"></th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
	<div class="smart-table-body">
		<div class="smart-table-body-inner">
			<table>
				<tbody>
	<?php
	foreach ($list as $key => $val) {
	?>
					<tr id='list_tr_<?php echo $val['Customer']['id'];?>' class="click">
						<td class="center middle first">
						<div style="width:100px;">
							<a href="#" onClick="location.href='<?php echo $html->url("/customers/edit/{$val['Customer']['id']}") ?>'"><input type="button" class="btn btnEdit" title="編　集"></a>
							<a href="#" onClick="location.href='<?php echo $html->url("/customers/del_conf/{$val['Customer']['id']}") ?>'"><input type="button" class="btn btnDel" title="削　除"></a>
						</div>
						</td>
	<?php
		foreach($customerKojin as $key2 => $val2) {
			$val2 = $val2['CustomerMaster'];
			if ($val2['field'] == "name") { // 名前
				$name = esHtml($val['Customer']["name"]);
				if (!empty($val['Customer']["picture"])) {
					$fileNm = pathinfo($val['Customer']["picture"], PATHINFO_FILENAME);
					$upFile = ROOT_URL . 'picture/img/' . esHtml($fileNm) . '-s.jpg';
				} else {
					$upFile = ROOT_URL . 'img/no-image.jpg';
				}
				$td = "<td style='background-color: #CCCCCC;'><div style='width:100px;'><div class='name-img'><div><img src='{$upFile}'></div></div></td>";
				echo $td;
				$td = "<td><div style='width:300px;'>{$name}</div></td>";
				echo $td;
			} else if ($val2['field'] == 'address_info') { // 住所
				// 郵便番号
				$value = esHtml($val['Customer']['zip_code']);
				$td = "<td><div style='width:100px;'>{$value}</div></td>";
				echo $td;
				// 住所
				$value = esHtml($val['Customer']['address']);
				$td = "<td><div style='width:200px;'>{$value}</div></td>";
				echo $td;
			} else {
				switch ($val2['type']) {
					case MASTER_TYPE_CHAR:		// 文字列（制限あり）
					case MASTER_TYPE_INTG:		// 数値型
					case MASTER_TYPE_DTTM:		// 日付時刻型
					case MASTER_TYPE_DATE:		// 日付型
					case MASTER_TYPE_DATE_A:	// 日付型(アラート)
					case MASTER_TYPE_TIME:		// 時間型
					case MASTER_TYPE_LIST:		// リスト型
					case MASTER_TYPE_RADI:		// ラジオ型
					case MASTER_TYPE_TELE:	// 電話型
					case MASTER_TYPE_CHEK:		// チェック型
						$value = esHtml($val['Customer']["{$val2['field']}"]);
						$td = "<td><div style='width:100px;'>{$value}</div></td>";
						echo $td;
						break;
					case MASTER_TYPE_TEXT:		// 文字列（制限なし）
						$value = nl2br(esHtml($val['Customer']["{$val2['field']}"]));
						$td = "<td><div style='width:100px;'>{$value}</div></td>";
						echo $td;
						break;
				}
			}
		}
	?>
						<td class="space"></td>
					</tr>
	<?php
	}
	?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="smart-table-footer">
		<div class="smart-table-footer-inner">
			<table>
				<tfoot>
					<tr>
						<td colspan="4" colspan="middle">
							<?php echo $this->element('paginatorsmart') ?>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
<?php
}
?>

<?php
	echo $appForm->end();
?>

<?php
	// 顧客一覧ソート機能のための準備
	echo $appForm->input('Sort.name', array('type' => 'hidden'));
	echo $appForm->end();
	$sortArr = array();
	$i = 1;
	foreach($customerKojin as $k => $v) {
		$v = $v['CustomerMaster'];
		if ($v['field'] == 'picture') {
			continue;
		} elseif ($v['field'] == 'address_info') { // 住所
			// 郵便番号
			$sortArr[$i] = 'zip_code';
			$i++;
			// 住所
			$sortArr[$i] = 'address';
			$i++;
		} else {
			$sortArr[$i] = $v['field'];
			$i++;
		}
	}
?>
<script type="text/javascript">
$(function(){
	// ページ移動リンクに行数リミット設定
	$("select[name=page_row]").trigger("change");
});


// 顧客テーブルの行をクリックすると情報画面に遷移
$("div#customerTable table tbody tr td:not('.first')").click(function(){
	var idname = $(this).parent("tr").attr("id");
	var arr = idname.split('_');
	var customerId = arr[2];

	window.location.href = '<?php echo $html->url("/busies/index/") ?>' + customerId;
});


// 顧客テーブルソート機能
$(document).on("click touchstart", "th[id^='customerTable_sort_col_']" ,function(ev){
	var arr = [];
	var i = 1;
	<?php
	foreach ($sortArr as $key => $val) {
	?>
		arr[i] = "<?php echo $val?>";
		i = i + 1;
	<?php
	}
	?>
	var idname = this.id;
	var array = idname.split('_');
	var colId = array[3];
	var page = '<?php
		if (empty($page)) {
			echo $paginator->current();
		} else {
			echo $page;
		}
	?>';
	var direction = '<?php
		if (empty($direction)) {
			echo 'asc';
		} else {
			if ($direction == 'asc') {
				echo 'desc';
			} else {
				echo 'asc';
			}
		}
	?>';
	var sort = '<?php
		if (empty($sort)) {
			echo '';
		} else {
			echo $sort;
		}
	?>';
	if (sort != arr[colId]) {
		direction = 'asc';
	}
	sort = arr[colId];
	window.location.href = '<?php echo $html->url('/customers/index/page:') ?>' + page + '/sort:' + sort + '/direction:' + direction;
});


// 検索アイコンをクリックすると検索アクションを開始
function searchAction() {
	$("input#search").trigger("click");
}

//確認画面出す
function mail_conf(){
	jConfirm('メールアドレスが登録されていない顧客は、メール配信リストから省かれます。', 'WATSON顧客管理', function(r) {
		if(r) {
		    $("input#send_mail").trigger("click");
		} else {
		    return false;
		}
	});
}

</script>