<script type="text/javascript">
	function jump_new(id){
	    location.href='<?php echo ROOT_URL ?>index.php/calender/jump_busy/'+id;
	}
	function jump_edit(controller, id){
	    location.href='<?php echo ROOT_URL ?>index.php/'+controller+'/edit/'+id;
	}

	$(document).ready(function() {
		var DATA_FEED_URL = "<?php echo $html->url('/calender/getCalender') ?>";
		var op = {
			view: "month",	// カレンダーの種類（初期表示）
			theme:8,	// 色
			showday: new Date(),	// 初期表示日
			weekstartday: 0,	// 先頭曜日
			EditCmdhandler:Edit,	// 更新処理関数名
			DeleteCmdhandler:Delete,	// 削除処理関数名
			ViewCmdhandler:View,	// 表示処理関数名
			onWeekOrMonthToDay:wtd,	// ???週日月切り替え時使用???
			onBeforeRequestData: cal_beforerequest,	// Ajaxイベント送信中処理
			onAfterRequestData: cal_afterrequest,	// Ajaxイベント完了後処理
			onRequestDataError: cal_onerror, // Ajaxイベント送信前処理
			autoload:true,	// 予定初期表示
			url: DATA_FEED_URL + "?method=list",  // 表示処理の処理URL
			quickAddUrl: DATA_FEED_URL + "?method=add", // 追加の処理URL
			quickUpdateUrl: DATA_FEED_URL + "?method=update",	// 更新の処理URL
			quickDeleteUrl: DATA_FEED_URL + "?method=remove"	// 削除の処理URL
		};

		var $dv = $("#calhead");
		var _MH = document.documentElement.clientHeight;
		var dvH = $dv.height() + 2;
		op.height = _MH - dvH;
		op.eventItems =[];
		var p = $("#gridcontainer").bcalendar(op).BcalGetOp();

		if (p && p.datestrshow) {
			$("#txtdatetimeshow").text(p.datestrshow);
		}

		// ツールバー未選択時
		$("#caltoolbar").noSelect();

		// Ajaxイベント送信中処理
		function cal_beforerequest(type)
		{
			var t="更新中....";
			switch(type)
			{
				case 1:
					t="更新中....";
					break;
				case 2:
				case 3:
				case 4:
					t="The request is being processed ...";
					break;
			}
			$("#errorpannel").hide();
			$("#loadingpannel").html(t).show();
		}

		// Ajaxイベント完了後処理
		function cal_afterrequest(type)
		{
			switch(type)
			{
				case 1:
					$("#loadingpannel").hide();
					break;
			}
		}

		// Ajaxイベント送信前処理
		function cal_onerror(type,data){
			$("#errorpannel").show();
		}

		// 予定編集
		function Edit(data){
		}

		// 予定表示
		function View(data){
		}

		// 予定削除
		function Delete(data,callback){
		}

		// ???週日月切り替え時使用???
		function wtd(p){
			if (p && p.datestrshow) {
				$("#txtdatetimeshow").text(p.datestrshow);
			}
			$("#caltoolbar div.fcurrent").each(function() {
				$(this).removeClass("fcurrent");
			})
			$("#showdaybtn").addClass("fcurrent");
		}

		// 日表示
		$("#showdaybtn").click(function(e) {
			$("#caltoolbar div.fcurrent").each(function() {
				$(this).removeClass("fcurrent");
			})
			$(this).addClass("fcurrent");
			var p = $("#gridcontainer").swtichView("day").BcalGetOp();
			if (p && p.datestrshow) {
				$("#txtdatetimeshow").text(p.datestrshow);
			}
		});

		// 週表示
		$("#showweekbtn").click(function(e) {
			//document.location.href="#week";
			$("#caltoolbar div.fcurrent").each(function() {
				$(this).removeClass("fcurrent");
			})
			$(this).addClass("fcurrent");
			var p = $("#gridcontainer").swtichView("week").BcalGetOp();
			if (p && p.datestrshow) {
				$("#txtdatetimeshow").text(p.datestrshow);
			}
		});

		// 月表示
		$("#showmonthbtn").click(function(e) {
			//document.location.href="#month";
			$("#caltoolbar div.fcurrent").each(function() {
				$(this).removeClass("fcurrent");
			})
			$(this).addClass("fcurrent");
			var p = $("#gridcontainer").swtichView("month").BcalGetOp();
			if (p && p.datestrshow) {
				$("#txtdatetimeshow").text(p.datestrshow);
			}
		});

		// 更新
		$("#showreflashbtn").click(function(e){
			$("#gridcontainer").reload();
		});

		// 今日
		$("#showtodaybtn").click(function(e) {
			var p = $("#gridcontainer").gotoDate().BcalGetOp();
			if (p && p.datestrshow) {
				$("#txtdatetimeshow").text(p.datestrshow);
			}
		});

		// 前へ
		$("#sfprevbtn").click(function(e) {
			var p = $("#gridcontainer").previousRange().BcalGetOp();
			if (p && p.datestrshow) {
				$("#txtdatetimeshow").text(p.datestrshow);
			}
		});

		// 次へ
		$("#sfnextbtn").click(function(e) {
			var p = $("#gridcontainer").nextRange().BcalGetOp();
			if (p && p.datestrshow) {
				$("#txtdatetimeshow").text(p.datestrshow);
			}
		});
	});
</script>
<div>
	<div class="row">
	   	<div class="xs-invisible"><img src="<?php echo ROOT_URL ?>img/calender.png" alt="カレンダー"></div>
	   	<div class="xs-visible"><img src="<?php echo ROOT_URL ?>img/calender_s.png" alt="カレンダー"></div>
	</div>
	<!-- ▼ツールバー -->
	<div id="caltoolbar" class="ctoolbar">
		<div id="showtodaybtn" class="fbutton">
			<div><span title='今日' class="showtoday">今日</span></div>
		</div>
		<div class="btnseparator"></div>
		<div id="showdaybtn" class="fbutton">
			<div><span title='日' class="showdayview">日</span></div>
		</div>
		<div class="btnseparator"></div>
		<div  id="showweekbtn" class="fbutton">
			<div><span title='週' class="showweekview">週</span></div>
		</div>
		<div class="btnseparator"></div>
		<div  id="showmonthbtn" class="fbutton">
			<div><span title='月' class="showmonthview">月</span></div>
		</div>
		<div class="btnseparator"></div>
		<div  id="showreflashbtn" class="fbutton">
			<div><span title='更新' class="showdayflash">更新</span></div>
		</div>
		<div class="btnseparator"></div>
		<div id="sfprevbtn" title="Prev"  class="fbutton"><span class="fprev"></span></div>
		<div id="sfnextbtn" title="Next" class="fbutton"><span class="fnext"></span></div>
		<div class="btnseparator"></div>
		<div class="fshowdatep fbutton">
			<div>
				<input type="hidden" name="txtshow" id="hdtxtshow" />
				<span id="txtdatetimeshow"></span>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<!-- ▲ツールバー -->
</div>
<div style="padding:1px;">
	<div class="t1 chromeColor">　</div>
		<div class="t2 chromeColor">　</div>
		<div id="dvCalMain" class="calmain printborder">
			<div id="gridcontainer" style="overflow-y: visible;"></div>
		</div>
		<div class="t2 chromeColor">　</div>
		<div class="t1 chromeColor">　</div>
	</div>
</div>