<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/tmp.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<title>WATSON顧客管理</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- jquery -->
	<script src="<?php echo ROOT_URL ?>js/jquery/jquery-1.11.1.min.js"></script>
	<script src="<?php echo ROOT_URL ?>js/jquery/jquery-migrate-1.2.1.min.js"></script>

	<!-- jquery ui -->
	<link rel="stylesheet" href="<?php echo ROOT_URL ?>js/jquery-ui-1.11.1/jquery-ui.css">
	<script src="<?php echo ROOT_URL ?>js/jquery-ui-1.11.1/jquery-ui.min.js"></script>

	<!-- jquery-ui datepicker -->
	<link rel="stylesheet" href="<?php echo ROOT_URL ?>js/jquery-ui-1.11.1/datepicker/ui.datepicker-ja.css">
	<script src="<?php echo ROOT_URL ?>js/jquery-ui-1.11.1/datepicker/ui.datepicker-ja.js"></script>
	<script src="<?php echo ROOT_URL ?>js/jquery-ui-1.11.1/datepicker/ui.holiday-ja.js"></script>

	<!-- ファイルアップロード -->
	<script type="text/javascript" src="<?php echo ROOT_URL ?>js/jquery/jquery.form.js" ></script>

	<!-- timepicker 日本語化したもの -->
	<link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL ?>js/jquery/timepicker/jquery.timepicker.css" />
	<script type="text/javascript" src="<?php echo ROOT_URL ?>js/jquery/timepicker/jquery.timepicker.ja.js"></script>

	<!-- bootstrap -->
	<script src="<?php echo ROOT_URL ?>bootstrap/js/bootstrap.min.js"></script>
	<link href="<?php echo ROOT_URL ?>bootstrap/css/bootstrap.css?20150805" rel="stylesheet">

	<!-- lightbox -->
	<link rel="stylesheet" href="<?php echo ROOT_URL ?>css/lightbox.css">
	<script src="<?php echo ROOT_URL ?>js/lightbox.js"></script>

	<!-- bxslider -->
	<link rel="stylesheet" href="<?php echo ROOT_URL ?>css/jquery.bxslider.css">
	<script src="<?php echo ROOT_URL ?>js/jquery.bxslider.min.js"></script>

	<!-- zip -->
	<script src="<?php echo ROOT_URL ?>js/zip/ajaxzip2.js" charset="UTF-8"></script>
	<script>AjaxZip2.JSONDATA = '<?php echo ROOT_URL ?>js/zip/data'</script>

	<!-- jquery.alerts -->
	<link rel="stylesheet" href="<?php echo ROOT_URL ?>js/jquery/jquery.alerts/jquery.alerts.css">
	<script src="<?php echo ROOT_URL ?>js/jquery/jquery.alerts/jquery.alerts.js"></script>

	<!-- smart-table(flexgridの代替) -->
	<link href="<?php echo ROOT_URL ?>smarttable/smarttable.css?20151005" rel="stylesheet">
	<script src="<?php echo ROOT_URL ?>smarttable/smarttable.js"></script>

	<!-- カスタマイズCSS -->
	<link href="<?php echo ROOT_URL ?>css/bootstrap_custom.css?20151005" rel="stylesheet">
	<link href="<?php echo ROOT_URL ?>css/custom.css?20151005" rel="stylesheet">

	<!-- カスタマイズJavascript -->
	<script type="text/javascript" src="<?php echo ROOT_URL ?>js/common.js"></script>

	<?php echo $this->element('analytics'); ?>

	<script>
	window.onload = function(){
		$(function() {
			$("#loading").fadeOut();
			$("#humberger_menu").fadeIn();
		});
	}
	$(function(){
	    $("a, input[type=image], .click").click(function(){
		    if (this.id != 'noLoading'){
				$("#loading").fadeIn();
				$("#humberger_menu").fadeOut();
				$('#myModal_preference').modal('hide');
				$('#myModal_master').modal('hide');
				$('#myModal_favorite').modal('hide');
		    }
	    });
	});
	</script>
	<style>
	#humberger_menu{display:none;}
	#loading{
		position:absolute;
		left:50%;
		top:20%;
		margin-left:-30px;
	}
	</style>
</head>
<body>
<img id="loading" src="<?php echo ROOT_URL ?>img/loading.gif">
<div>
	<div class="header-nav">
		<a class="header-top" href="<?php echo $html->url('/top/index') ?>"><img src="<?php echo ROOT_URL ?>img/logo_watson.png"></a>
	</div><!-- /.header-nav -->
	<div class="container-fluid" id="humberger_menu">
		<div class="login">
        	<div class="xs-invisible">
				<?php echo $login['User']['name'] ?>
			</div>
		</div>
		<div class="search">
        	<div class="xs-invisible">
	        	<img src="<?php echo ROOT_URL ?>img/humberger_menu.png">
            </div>
        	<div class="xs-visible">
	        	<img src="<?php echo ROOT_URL ?>img/humberger_menu_s.png">
            </div>
		</div>
		<div class="js-search-arrow"></div>
		<div class="js-search">
			<a href="<?php echo $html->url('/customers/index/search:clear') ?>"><img src="<?php echo ROOT_URL ?>img/humberger_menu/address_small.png"></a>
			<a href="<?php echo $html->url('/customers/index_busi/search:clear') ?>"><img src="<?php echo ROOT_URL ?>img/humberger_menu/event_small.png"></a>
			<a href="<?php echo $html->url('/customers/index_mail/search:clear') ?>"><img src="<?php echo ROOT_URL ?>img/humberger_menu/mail_small.png"></a>
			<a href="#myModal_preference" data-toggle="modal" id="noLoading"><img src="<?php echo ROOT_URL ?>img/humberger_menu/preference_small.png"></a>
			<a href="#myModal_favorite" data-toggle="modal" id="noLoading"><img src="<?php echo ROOT_URL ?>img/humberger_menu/favorite_small.png"></a>
			<a href="<?php echo $html->url('/calender/index'); ?>"><img src="<?php echo ROOT_URL ?>img/humberger_menu/calender.png"></a>
			<a href="<?php echo $html->url('/maps/before_index'); ?>"><img src="<?php echo ROOT_URL ?>img/humberger_menu/map.png"></a>
			<a href="#" id="noLoading" onClick="logout_conf();"><img src="<?php echo ROOT_URL ?>img/humberger_menu/logout_small.png"></a>
			<div class="js-search-close"><img src="<?php echo ROOT_URL ?>img/js_search_close.png"></div>
		</div>

  	</div><!-- /.container-fluid -->
</div>
<div class="container">
	<div id="main">
	<!-- InstanceBeginEditable name="EditRegion3" -->
		<?php echo $this->element('message_area') ?>
		<?php echo $content_for_layout ?>
	<!-- InstanceEndEditable -->
	</div><!-- main -->
</div><!-- container -->

<div id="myModal_preference" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<span style="float:right;padding-left:5px;cursor: pointer;opacity: 0.5;" data-dismiss="modal" aria-hidden="true">閉じる</span>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 id="myModalLabel">各種設定メニュー</h3>
			</div>
			<div class="modal-body">
				<?php
					if (!empty($login['User']) && $login['User']['auth_flag'] == 'はい'){
				?>
				<a href="<?php echo $html->url('/users/index/search:clear') ?>">ユーザー管理</a><hr>
				<a href="#myModal_master" data-toggle="modal" data-dismiss="modal" aria-hidden="true" id="noLoading">マスター管理</a><hr>
				<?php
					}
				?>
				<!--
				<a href="<?php echo $html->url('/csvs/index') ?>">CSV出力</a><hr>
				-->
				<a href="<?php echo $html->url('/kojins/edit') ?>">個人設定</a><hr>
				<a href="<?php echo $html->url('/mails/index/search:clear') ?>">メール予約配信一覧</a><hr>
			</div>
		</div>
	</div>
</div>
<div id="myModal_master" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<a href="#myModal_preference" id="noLoading" data-toggle="modal" data-dismiss="modal" aria-hidden="true">戻る</a>
				<span style="float:right;padding-left:5px;cursor: pointer;opacity: 0.5;" data-dismiss="modal" aria-hidden="true">閉じる</span>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 id="myModalLabel">マスター一覧</h3>
			</div>
			<div class="modal-body">
				<a href="<?php echo $html->url('/customer_masters/index/search:clear') ?>">顧客マスター</a><hr>
				<a href="<?php echo $html->url('/busi_masters/index/search:clear') ?>">商談マスター</a><hr>
				<a href="<?php echo $html->url('/user_masters/index/search:clear') ?>">ユーザーマスター</a>
			</div>
		</div>
	</div>
</div>
<div id="myModal_favorite" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 id="myModalLabel">お気に入り一覧</h3>
			</div>
			<div class="modal-body">
				<?php echo $this->element('favorite_list') ?>
			</div>
		</div>
	</div>
</div>
<?php echo $this->element('sql_dump') ?>
</body>
<script type="text/javascript">
$(function(){
	// ハンバーガーメニュー
	var search = $("div.js-search,div.js-search-arrow");
	search.hide();
	$("div.search").on("click", {a: search}, slide);
	$("div.js-search-close").on("click", {a: search}, slide);
	function slide(event) {
		if (event.data.a.css("display") === "none") {
			event.data.a.slideDown(250);
		} else {
			event.data.a.slideUp(250);
		}
	}

	// datepicker 初期化
	$(".calendar").datepicker({
		yearRange: '1900:2030',
	});
	$(".calendar").attr({autocomplete: "off"});

	// timeピッカー設定
	<?php $minTime = date('H'); ?>
	$(".times").timepicker({'timeFormat':'H:i','minTime':'<?php echo $minTime ?>'});
});

// 確認画面出す
function logout_conf(){
	jConfirm('ログアウトを実行すると、更新前の情報が失われます。ログアウトを実行してもよろしいですか？', 'WATSON顧客管理', function(r) {
		if(r) {
		    location.href="<?php echo $html->url('/login/logout'); ?>";
		} else {
		    return false;
		}
	});
}
</script>
<!-- InstanceEnd --></html>
