<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/tmp.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<title>WATSON顧客管理</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- jquery -->
	<script src="<?php echo ROOT_URL ?>js/jquery/jquery-1.8.3.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL ?>report/css/style.css">
	<script type="text/javascript" src="<?php echo ROOT_URL ?>js/highcharts.js"></script>

	<?php echo $this->element('analytics'); ?>
</head>
<body>
	<?php echo $this->element('message_area') ?>
	<?php echo $content_for_layout ?>
	<?php echo $this->element('sql_dump') ?>
</body>
</html>