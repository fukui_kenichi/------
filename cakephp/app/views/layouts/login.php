<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/tmp.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<title>WATSON顧客管理</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- jquery -->
	<script src="<?php echo ROOT_URL ?>js/jquery/jquery-1.8.3.min.js"></script>

	<!-- jquery ui -->
	<script src="<?php echo ROOT_URL ?>js/jquery/jquery-ui.custom.min.js"></script>
	<link rel="stylesheet" href="<?php echo ROOT_URL ?>js/jquery/jquery-ui.custom.css">
	<!-- ファイルアップロード -->
	<script type="text/javascript" src="<?php echo ROOT_URL ?>js/jquery/jquery.form.js" ></script>

	<!-- カレンダー -->
	<link rel="stylesheet" href="<?php echo ROOT_URL ?>js/jquery/datepicker/ui.datepicker-ja.css">
	<script src="<?php echo ROOT_URL ?>js/jquery/datepicker/ui.datepicker-ja.js"></script>
	<script src="<?php echo ROOT_URL ?>js/jquery/datepicker/ui.holiday-ja.js"></script>

	<!-- timepicker 日本語化したもの -->
	<link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL ?>js/jquery/timepicker/jquery.timepicker.css" />
	<script type="text/javascript" src="<?php echo ROOT_URL ?>js/jquery/timepicker/jquery.timepicker.ja.js"></script>

	<!-- bootstrap -->
	<script src="<?php echo ROOT_URL ?>bootstrap/js/bootstrap.min.js"></script>
	<link href="<?php echo ROOT_URL ?>bootstrap/css/bootstrap.css" rel="stylesheet">

	<!-- lightbox -->
	<link rel="stylesheet" href="<?php echo ROOT_URL ?>css/lightbox.css">
	<script src="<?php echo ROOT_URL ?>js/lightbox.js"></script>

	<!-- bxslider -->
	<link rel="stylesheet" href="<?php echo ROOT_URL ?>css/jquery.bxslider.css">
	<script src="<?php echo ROOT_URL ?>js/jquery.bxslider.min.js"></script>

	<!-- zip -->
	<script src="<?php echo ROOT_URL ?>js/zip/ajaxzip2.js" charset="UTF-8"></script>
	<script>AjaxZip2.JSONDATA = '<?php echo ROOT_URL ?>js/zip/data'</script>

	<!-- smart-table(flexgridの代替) -->
	<link href="<?php echo ROOT_URL ?>smarttable/smarttable.css" rel="stylesheet">
	<script src="<?php echo ROOT_URL ?>smarttable/smarttable.js"></script>

	<!-- カスタマイズCSS -->
	<link href="<?php echo ROOT_URL ?>css/bootstrap_custom.css" rel="stylesheet">
	<link href="<?php echo ROOT_URL ?>css/custom.css" rel="stylesheet">
	<link href="<?php echo ROOT_URL ?>css/login.css" rel="stylesheet">

	<!-- カスタマイズJavascript -->
	<script type="text/javascript" src="<?php echo ROOT_URL ?>js/common.js"></script>

	<?php echo $this->element('analytics'); ?>
</head>
<body>
<div class="site-all">
	<div>
		<div class="header-nav">
	        <span class="header-top"><img src="<?php echo ROOT_URL ?>img/logo_watson.png"></span>
		</div><!-- /.header-nav -->
	</div>
	<div class="container">
		<div id="main">
		    <div id="contentsArea">
		    <div class="login_wrapper">
		    <div class="login_wrapper_padding">
				<?php echo $this->element('message_area') ?>
				<?php echo $content_for_layout ?>
		    </div><!-- login_wrapper_padding-->
		    </div><!-- login_wrapper-->
		    </div><!-- contentsArea-->
		</div><!-- main -->
	</div><!-- container -->
</div>

<?php echo $this->element('sql_dump') ?>

</body>
</html>