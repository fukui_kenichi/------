<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/tmp.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<title>WATSON顧客管理</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- jquery -->
	<script src="<?php echo ROOT_URL ?>js/jquery/jquery-1.11.1.min.js"></script>
	<script src="<?php echo ROOT_URL ?>js/jquery/jquery-migrate-1.2.1.min.js"></script>

	<!-- jquery ui -->
	<link rel="stylesheet" href="<?php echo ROOT_URL ?>js/jquery-ui-1.11.1/jquery-ui.css">
	<script src="<?php echo ROOT_URL ?>js/jquery-ui-1.11.1/jquery-ui.min.js"></script>

	<!-- bootstrap -->
	<script src="<?php echo ROOT_URL ?>bootstrap/js/bootstrap.min.js"></script>
	<link href="<?php echo ROOT_URL ?>bootstrap/css/bootstrap.css" rel="stylesheet">

	<!-- jquery.alerts -->
	<link rel="stylesheet" href="<?php echo ROOT_URL ?>js/jquery/jquery.alerts/jquery.alerts.css">
	<script src="<?php echo ROOT_URL ?>js/jquery/jquery.alerts/jquery.alerts.js"></script>

	<!-- カスタマイズCSS -->
	<link href="<?php echo ROOT_URL ?>css/bootstrap_custom.css" rel="stylesheet">
	<link href="<?php echo ROOT_URL ?>css/top.css" rel="stylesheet">

	<!-- カスタマイズJavascript -->
	<script type="text/javascript" src="<?php echo ROOT_URL ?>js/common.js"></script>

	<?php echo $this->element('analytics'); ?>
</head>
<body>
	<?php echo $content_for_layout ?>
	<?php echo $this->element('sql_dump') ?>
	<div id="myModal_preference" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<span style="float:right;padding-left:5px;cursor: pointer;opacity: 0.5;" data-dismiss="modal" aria-hidden="true">閉じる</span>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 id="myModalLabel">各種設定メニュー</h3>
				</div>
				<div class="modal-body">
					<?php
						if ($login['User']['auth_flag'] == 'はい'){
					?>
					<a href="<?php echo $html->url('/users/index/search:clear') ?>">ユーザー管理</a><hr>
					<a href="#myModal_master" data-toggle="modal" data-dismiss="modal" aria-hidden="true">マスター管理</a><hr>
					<?php
						}
					?>
					<!--
					<a href="<?php echo $html->url('/csvs/index') ?>">CSV出力</a><hr>
					-->
					<a href="<?php echo $html->url('/kojins/edit') ?>">個人設定</a><hr>
					<a href="<?php echo $html->url('/mails/index/search:clear') ?>">メール予約配信一覧</a><hr>
				</div>
			</div>
		</div>
	</div>

	<div id="myModal_master" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<a href="#myModal_preference" data-toggle="modal" data-dismiss="modal" aria-hidden="true">戻る</a>
					<span style="float:right;padding-left:5px;cursor: pointer;opacity: 0.5;" data-dismiss="modal" aria-hidden="true">閉じる</span>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 id="myModalLabel">マスター一覧</h3>
				</div>
				<div class="modal-body">
					<a href="<?php echo $html->url('/customer_masters/index/search:clear') ?>">顧客マスター</a><hr>
					<a href="<?php echo $html->url('/busi_masters/index/search:clear') ?>">商談マスター</a><hr>
					<a href="<?php echo $html->url('/user_masters/index/search:clear') ?>">ユーザーマスター</a>
				</div>
			</div>
		</div>
	</div>

	<div id="myModal_info" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<span style="float:right;padding-left:5px;cursor: pointer;opacity: 0.5;" data-dismiss="modal" aria-hidden="true">閉じる</span>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 id="myModalLabel">お知らせ一覧</h3>
				</div>
				<div class="modal-body">
					<div class="list-group" id="accordion3">
						<?php
						foreach ((array)$alertCustomerArr as $key=>$val){
						?>
						<a data-toggle="collapse" data-parent="#accordion3" href="#<?php echo $key ?>" class="list-group-item">
						<?php echo esHtml($val['title']) ?>
						</a>
						<div id="<?php echo $key ?>" class="collapse">
							<p class="list-group-item">
								<input type="image" name="mail_conf" id="noLoading" src="<?php echo ROOT_URL ?>img/bt_mail_delivery_s.png" alt="メール配信" onClick="mail_conf('<?php echo esHtml($val['customer_id']) ?>'); return false;"><br>
								<?php
								foreach ((array)$val['value'] as $naiyou){
								?>
								・<a href="#" onClick="location.href='<?php echo $html->url("/busies/index/{$naiyou['id']}") ?>'"><?php echo esHtml($naiyou['content'])?></a><br>
								<?php
								}
								?>
							</p>
						</div>
						<?php } ?>
						<?php
						foreach ((array)$alertBusyArr as $key=>$val){
							$key = '000'.$key;
						?>
						<a data-toggle="collapse" data-parent="#accordion3" href="#<?php echo $key ?>" class="list-group-item">
						<?php echo esHtml($val['title']) ?>
						</a>
						<div id="<?php echo $key ?>" class="collapse">
							<p class="list-group-item">
								<input type="image" name="mail_conf" id="noLoading" src="<?php echo ROOT_URL ?>img/bt_mail_delivery_s.png" alt="メール配信" onClick="mail_conf('<?php echo esHtml($val['customer_id']) ?>'); return false;"><br>
								<?php
								foreach ((array)$val['value'] as $naiyou){
								?>
								・<a href="#" onClick="location.href='<?php echo $html->url("/busies/detail/{$naiyou['id']}") ?>'"><?php echo esHtml($naiyou['content'])?></a><br>
								<?php
								}
								?>
							</p>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
function liquid_image(){
	var ww = jQuery(window).width();
	jQuery('#top_logo').css("width", "");
	jQuery('#bottom_logo').css("width", "");
	jQuery('#menu_1').css("width", "");
	jQuery('#menu_2').css("width", "");
	jQuery('#menu_3').css("width", "");
	jQuery('#menu_4').css("width", "");
	jQuery('#menu_5').css("width", "");
	jQuery('#menu_6').css("width", "");
	jQuery('#menu_7').css("width", "");
	jQuery('#info').css("width", "189px");
	jQuery('#side_2').css("padding-right", "");
	jQuery('#info').css("font-size", "");
	if (ww < 1000){
		jQuery('#top_logo').css("width", ww/3-10+"px");
		jQuery('#bottom_logo').css("width", ww/5-10+"px");
		jQuery('#menu_1').css("width", ww/3-10+"px");
		jQuery('#menu_2').css("width", ww/3-10+"px");
		jQuery('#menu_3').css("width", ww/3-10+"px");
		jQuery('#menu_4').css("width", ww/3-10+"px");
		jQuery('#menu_5').css("width", ww/5-10+"px");
		jQuery('#menu_6').css("width", ww/5-10+"px");
		jQuery('#menu_7').css("width", ww/5-10+"px");
		jQuery('#info').css("width", ww/5-12+"px");
		jQuery('#side_2').css("padding-right", ww/12-5+"px");
		jQuery('#info').css("font-size", ww/100+2+"px");
	}
	jQuery('#menu_s1').css("width", ww/2-10+"px");
	jQuery('#menu_s2').css("width", ww/2-10+"px");
	jQuery('#menu_s3').css("width", ww/2-10+"px");
	jQuery('#menu_s4').css("width", ww/2-10+"px");
	jQuery('#menu_s5').css("width", ww/2-10+"px");
	jQuery('#menu_s6').css("width", ww/2-10+"px");
	jQuery('#menu_s7').css("width", ww/2-10+"px");
	jQuery('#info_s').css("width", ww/2-12+"px");
}
jQuery(document).ready(function(){
	liquid_image();
});
jQuery(window).resize(function(){
	liquid_image();
});

//確認画面出す
function mail_conf(customer_id){
	jConfirm('メールアドレスが登録されていない顧客は、メール配信リストから省かれます。', 'WATSON顧客管理', function(r) {
		if(r) {
		    $("input#CustomerId").val(customer_id);
		    $("input#send_mail").trigger("click");
		} else {
		    return false;
		}
	});
}

</script>
</html>