<div class="xs-invisible">
	<div class="smart-table" id="favoriteTable">
		<div class="smart-table-header">
			<div class="smart-table-header-inner">
				<table>
					<thead>
						<tr>
							<th><div style="width:100px;"></div></th>
							<th class="sort"><div style="width:450px;">お気に入り</div></th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
		<div class="smart-table-body">
			<div class="smart-table-body-inner">
				<table>
					<tbody>
						<?php
						if (!empty($favoriteList)) {
							foreach ($favoriteList as $key => $val) {
						?>
							<tr>
								<td class="center middle first">
									<div style="width:100px;">
										<a href="#" onClick="parent.location.href='<?php echo $html->url("/favorites/edit/{$val['Favorite']['id']}") ?>';return false;"><input type="button" class="btn btnEdit" title="編　集"></a>
										<a href="#" onClick="parent.location.href='<?php echo $html->url("/favorites/del_conf/{$val['Favorite']['id']}") ?>';return false;"><input type="button" class="btn btnDel" title="削　除"></a>
									</div>
								</td>
								<td class="click" id="<?php echo 'tr_favorite_'. $val['Favorite']['id']?>"><div style="width:450px;"><?php echo esHtml($val['Favorite']['name'])?></div></td>
							</tr>
						<?php
							}
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="xs-visible">
	<div class="smart-table" id="favoriteTable">
		<div class="smart-table-header">
			<div class="smart-table-header-inner">
				<table>
					<thead>
						<tr>
							<th><div style="width:100px;"></div></th>
							<th class="sort"><div style="width:150px;">お気に入り</div></th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
		<div class="smart-table-body">
			<div class="smart-table-body-inner">
				<table>
					<tbody>
						<?php
						if (!empty($favoriteList)) {
							foreach ($favoriteList as $key => $val) {
						?>
							<tr>
								<td class="center middle first">
									<div style="width:100px;">
										<a href="#" onClick="parent.location.href='<?php echo $html->url("/favorites/edit/{$val['Favorite']['id']}") ?>';return false;"><input type="button" class="btn btnEdit" title="編　集"></a>
										<a href="#" onClick="parent.location.href='<?php echo $html->url("/favorites/del_conf/{$val['Favorite']['id']}") ?>';return false;"><input type="button" class="btn btnDel" title="削　除"></a>
									</div>
								</td>
								<td class="click" id="<?php echo 'tr_favorite_'. $val['Favorite']['id']?>"><div style="width:150px;"><?php echo esHtml($val['Favorite']['name'])?></div></td>
							</tr>
						<?php
							}
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
// 行が選択されたとき、該当お気に入りで顧客一覧画面へ
$("div#favoriteTable table tbody tr td:not('.first')").click(function(){
	var favoriteId = this.id.split('_')[2];
	parent.window.location.href = '<?php echo $html->url('/customers/index/favoriteId:') ?>' + favoriteId;
});
</script>