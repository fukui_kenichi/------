<?php
	if ($session->check('system-message')) {
		echo '<div class="system-message">'. "\n";
		echo '<div class="system-message-inner">'. "\n";
		echo $session->read('system-message'). "\n";
		echo '</div>'. "\n";
		echo '</div>'. "\n";
		$session->delete('system-message');
	}
?>