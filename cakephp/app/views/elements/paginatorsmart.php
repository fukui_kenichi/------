<?php
	unset($paginator->options['url']['search']);	// 検索初期化GET条件削除
	$limitArr = array(10=>10, 15=>15, 20=>20, 25=>25, 30=>30);
?>

<div class="pGroup">
	<select name="page_row">
	<?php
	foreach ($limitArr as $key => $val){
		if ($limit == $key) {
			$str = "<option value={$key} selected>{$val}</option>";
		} else {
			$str = "<option value={$key}>{$val}</option>";
		}
		echo $str;
	}
	?>
	</select>
</div>
<div class="pGroup">
	Page
	<input type="text" name="page_num" size="4" class="right" value="<?php echo $paginator->current() ?>"/>
	of
	<span><?php echo $paginator->counter(array('format' => '%pages%')) ?></span>
</div>
<div class="btnseparator"></div>
<div class="pGroup">
	<input type="submit" name="refresh" value="" class="btn pReload" >
</div>
<div class="btnseparator"></div>
<div class="pBlock">
	<div class="pGroup">
		<?php
		echo $paginator->first('<span class="btn pFirst"></span>', array('escape' => false, 'class' => 'first'));
		?>
	</div>
	<div class="btnseparator"></div>
	<div class="pGroup">
		<?php
		if ($paginator->current() != 1) {
			echo $paginator->prev('<span class="btn pPrev"></span>', array('escape' => false));
		}
		?>
	</div>
	<div class="btnseparator"></div>
	<div class="pGroup">
		<?php
		if ($paginator->current() != $paginator->counter(array('format' => '%pages%'))) {
			echo $paginator->next('<span class="btn pNext"></span>', array('escape' => false));
		}
		?>
		</div>
	<div class="btnseparator"></div>
	<div class="pGroup">
		<?php
		echo $paginator->last('<span class="btn pLast"></span>', array('escape' => false, 'class' => 'last'));
		?>
	</div>
</div>