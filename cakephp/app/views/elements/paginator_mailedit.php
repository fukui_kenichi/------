<?php
	unset($paginator->options['url']['search']);	// 検索初期化GET条件削除
	$limitArr = array(10=>10, 15=>15, 20=>20, 25=>25, 30=>30);
?>

<div class="pGroup">
	<select name="page_row">
	<?php
	foreach ($limitArr as $key => $val){
		if ($limit == $key) {
			$str = "<option value={$key} selected>{$val}</option>";
		} else {
			$str = "<option value={$key}>{$val}</option>";
		}
		echo $str;
	}
	?>
	</select>
</div>
<div class="pGroup">
	Page
	<input type="text" name="page_num" size="4" class="right" value="<?php echo $page ?>"/>
	of
	<span><?php echo $paginator->counter(array('format' => '%pages%')) ?></span>
</div>
<div class="btnseparator"></div>
<div class="pGroup">
	<input type="button" name="refresh" value="" class="btn pReload" >
</div>
<div class="btnseparator"></div>
<div class="pBlock">
	<div class="pGroup">
		<?php
		if ($page > 1) {
			echo '<input type="button" name="first" value="" class="btn pFirst" >';
		}
		?>
	</div>
	<div class="btnseparator"></div>
	<div class="pGroup">
		<?php
		if ($page != 1) {
			echo '<input type="button" name="prev" value="" class="btn pPrev" >';
		}
		?>
	</div>
	<div class="btnseparator"></div>
	<div class="pGroup">
		<?php
		if ($page != $paginator->counter(array('format' => '%pages%'))) {
			echo '<input type="button" name="next" value="" class="btn pNext" >';
		}
		?>
		</div>
	<div class="btnseparator"></div>
	<div class="pGroup">
		<?php
		if ($page != $paginator->counter(array('format' => '%pages%'))) {
			echo '<input type="button" name="last" value="" class="btn pLast" >';
		}
		?>
	</div>
</div>