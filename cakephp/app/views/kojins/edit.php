<?php
	echo $appForm->create('FileEdit', array('name' => 'form','type' => 'file','id' => 'uploadForm', 'enctype' => 'multipart/form-data'));
?>

<div class="row">
   	<div class="xs-invisible">
        <img src="<?php echo ROOT_URL ?>img/kojin-settei.png" alt="個人設定">
    </div>
   	<div class="xs-visible">
        <img src="<?php echo ROOT_URL ?>img/kojin-settei_s.png" alt="個人設定">
    </div>
</div>
<?php if (!empty($appForm->validationErrors)) {?>
<div class="error-message">
    <div class="error-message-inner">
	    <p class="error-message-header">入力内容にエラーがあります。</p>
	    <?php echo $appForm->errorAll(). "\n"; ?>
    </div>
</div>
<?php }?>

<div class="row">
	<div class="sub_left">
		<span class="message">ログイン情報変更</span>
	</div>
</div>

<table class="table table-bordered table-navy table-edit">
	<tbody>
		<tr>
			<th width="200">ID<span class="err-msg">【必須】</span></th>
			<td class="sm-center">
				<?php echo $appForm->input('User.account', array('type' => 'text', 'class' => 'form-control ime-off', 'maxlength' => 256)) ?>
			</td>
		</tr>
		<tr>
			<th>パスワード<span class="err-msg">【必須】</span></th>
			<td class="sm-center">
				<?php echo $appForm->input('User.password_01', array('type' => 'password', 'class' => 'form-control ime-off', 'maxlength' => 256)) ?>
			</td>
		</tr>
		<tr>
			<th>パスワード確認<span class="err-msg">【必須】</span></th>
			<td class="sm-center">
				<?php echo $appForm->input('User.password_02', array('type' => 'password', 'class' => 'form-control ime-off', 'maxlength' => 256)) ?>
			</td>
		</tr>
		<tr>
			<th>カレンダーURL</th>
			<td class="sm-center">
				<?php echo $appForm->input('User.url', array('type' => 'text', 'class' => 'form-control')) ?>
			</td>
		</tr>
	</tbody>
</table>

<div class="row">
	<div class="kojin_left">
		<span class="message">顧客一覧表示設定：</span>
		<table class="table table-condensed table-bordered table-striped table-navy list">
		<thead>
			<tr>
				<th width="45">表示</th>
				<th width="200">項目名</th>
				<th width="40">並び順</th>
			</tr>
		</thead>
		<tbody>
<?php
	if (!empty($appForm->data['User']['CustomerKojin'])) {
		foreach ($appForm->data['User']['CustomerKojin'] as $key => $val) {
?>
			<tr>
				<td>
				<?php
					echo $appForm->input("User.CustomerKojin.{$key}.ck.view_flag", array('type' => 'checkbox', 'disabled' => $val['ck']['disabled']));
					// 保存とエラー時に必要な変数
					echo $appForm->input("User.CustomerKojin.{$key}.ck.disabled", array('type' => 'hidden'));
					echo $appForm->input("User.CustomerKojin.{$key}.ck.id", array('type' => 'hidden'));
					echo $appForm->input("User.CustomerKojin.{$key}.cm.id", array('type' => 'hidden'));
					if (!empty($val['ck']['disabled'])) echo $appForm->input("User.CustomerKojin.{$key}.ck.view_flag", array('type' => 'hidden'));
				?>
				</td>
				<td>
				<?php
					echo $val['cm']['koumoku'];
					echo $appForm->input("User.CustomerKojin.{$key}.cm.koumoku", array('type' => 'hidden'));
				?>
				</td>
				<td><?php echo $appForm->input("User.CustomerKojin.{$key}.ck.number", array('type' => 'text', 'size' => 3, 'maxlength' => 3, 'class' => 'right ime-off num-only'));?></td>
			</tr>
<?php
		}
	}
?>
		<tbody>
		</table>
	</div>
	<div class="kojin_left">
		<span class="message">商談一覧表示設定：</span>
		<table class="table table-condensed table-bordered table-striped table-navy list">
		<thead>
			<tr>
				<th width="45">表示</th>
				<th width="200">項目名</th>
				<th width="40">並び順</th>
			</tr>
		</thead>
		<tbody>
<?php
	if (!empty($appForm->data['User']['BusiKojin'])) {
		foreach ($appForm->data['User']['BusiKojin'] as $key => $val) {
?>
			<tr>
				<td>
				<?php
					echo $appForm->input("User.BusiKojin.{$key}.bk.view_flag", array('type' => 'checkbox', 'disabled' => $val['bk']['disabled']));
					// 保存とエラー時に必要な変数
					echo $appForm->input("User.BusiKojin.{$key}.bk.disabled", array('type' => 'hidden'));
					echo $appForm->input("User.BusiKojin.{$key}.bk.id", array('type' => 'hidden'));
					echo $appForm->input("User.BusiKojin.{$key}.bm.id", array('type' => 'hidden'));
					if (!empty($val['bk']['disabled'])) echo $appForm->input("User.BusiKojin.{$key}.bk.view_flag", array('type' => 'hidden'));
				?>
				</td>
				<td>
				<?php
					echo $val['bm']['koumoku'];
					echo $appForm->input("User.BusiKojin.{$key}.bm.koumoku", array('type' => 'hidden'));
				?>
				</td>
				<td><?php echo $appForm->input("User.BusiKojin.{$key}.bk.number", array('type' => 'text', 'size' => 3, 'maxlength' => 3, 'class' => 'right ime-off num-only'));?></td>
			</tr>
<?php
		}
	}
?>
		</tbody>
		</table>
	</div>
</div>

<div class="hide">
	<?php
		echo $appForm->input('User.id', array('type' => 'hidden'));
		echo $appForm->input('User.updated', array('type' => 'hidden'));
	?>
</div>
<div class="row">
	<div class="center">
        <div class="xs-invisible">
   			<input type="image" name="save" alt="更新" src="<?php echo ROOT_URL ?>img/bt_update.png" title="更新" />
            <input type="image" name="cancel" alt="キャンセル" src="<?php echo ROOT_URL ?>img/bt_cancel.png" title="キャンセル" />
        </div>
        <div class="xs-visible">
   			<input type="image" name="save" alt="更新" src="<?php echo ROOT_URL ?>img/bt_update_s.png" title="更新" />
            <input type="image" name="cancel" alt="キャンセル" src="<?php echo ROOT_URL ?>img/bt_cancel_s.png" title="キャンセル" />
        </div>
	</div>
</div>

<?php
	echo $appForm->end();
?>