<?php
//*******************************************************************************
// モジュール名 : 拡張フォームヘルパー
// Create       : 2010/02/01 konishi
// Production   : Okushin System Ltd.
//*******************************************************************************

App::import('Helper', "Form");

class AppFormHelper extends FormHelper {

	/** フォーム開始タグ　初期値定義
	 * (non-PHPdoc)
	 * @see FormHelper::create()
	 * @param	$model = form名
	 * @param	$options = ‘type’、‘action’、‘url’、‘default’が指定できます
	 */
	function create($model = null, $options = array()) {
		$defaults = array('name' => 'form', 'url'  => '/'. $this->params['url']['url']);
		$options = array_merge($defaults, $options);
		if (isset($options['action'])) {
			unset($options['url']);	// 任意で定義したaction属性を優先
		}
		return parent::create($model, $options);
	}


	/** フォーム閉じタグ　自作tokenを出力するように変更
	 * (non-PHPdoc)
	 * @see FormHelper::end()
	 * @param $options = 'name','label','div'を指定できます。
	 */
	function end($options = null) {
		if (strpos($this->action, 'ajax_') === false){
			// トークン発行
			if ($this->Session->check('token')) {
				$token = $this->Session->read('token');
			} else {
				$token = getRandPass(20);
			}
			$this->Session->write('token', $token);
			echo '<input type="hidden" name="token" value="'. $token. '" />'. "\n";
		}

		return parent::end($options);
	}


	/**
	 *  エラー表示のデフォルト値変更
	 * (non-PHPdoc)
	 * @see FormHelper::error()
	 * @param	$field = フィールド名
	 * @param	$text = エラーメッセージ
	 * @param	$options = レンダリングオプション
	 */
	function error($field, $text = null, $options = array()) {
		$defaults = array('wrap' => 'span', 'class' => 'font-red', 'escape' => false);
		$options = array_merge($defaults, $options);

		return parent::error($field, $text, $options);
	}


	/**
	 * 全エラー表示(2階層のエラーメッセージのみ表示 例：○Model.field ×Model.key.field)
	 * @param unknown_type $nonMsgArr = 表示したくないメッセージ
	 * @param unknown_type $options = レンダリングオプション
	 * @return Ambigous <NULL, string>
	 */
	function errorAll($nonMsgArr = array(), $options = array()) {
		$errorMsg = null;

		foreach ($this->validationErrors as $key => $value) {
			foreach ($value as $key2 => $val) {
				$field = $key. '.'. $key2;
				//表示したくないメッセージをスルー
				if (array_search($field, $nonMsgArr) === false && !is_array($val)) {
					$errorMsg .= $this->error($field, null, $options). "<br />";
				}
			}
		}
		return $errorMsg;
	}


	/**
	 * フォーム表示のデフォルト値変更
	 * (non-PHPdoc)
	 * @see FormHelper::input()
	 * @param string　$fieldName = フィールド名
	 * @param array　$options = タイプ毎のメソッドにより設定が変わる
	 */
	function input($fieldName, $options = array()) {
		$defaults = array(
			'before' => null,		'between' => null,		'after' => null,	'div' => false,
			'label' => false,		'legend' => false,		'error' => false,
		);
		if (isset($options['type'])) {
			if ($options['type'] == 'checkbox' && isset($options['label'])) {
				$options['label'] = ' '. $options['label'];
			}
			if ($options['type'] == 'radio') {
				$options['label'] = true;

				$arr = array('separator' => ' ');
				// radioでemptyオプション使用できるように（配列に0が定義されていないときのみ）
				if (!empty($options['empty'])) {
					if (!isset($options['options'][0])) {
						$options['options'][0] = $options['empty'];
						ksort($options['options']);
						unset($options['empty']);
					}
				}
				$defaults = array_merge($defaults, $arr);
			}
			if ($options['type'] == 'date') {
				$defaults = array_merge($defaults, array(
					'monthNames' => false, 'maxYear' => date('Y') + 5, 'minYear' => date('Y') - 1,
				));
			}

		}
		$options = array_merge($defaults, $options);

		return parent::input($fieldName, $options);
	}

	/**
	 * テキストフォーム表示の日付型を「-」→「/」に置換処理追加
	 * (non-PHPdoc)
	 * @see FormHelper::text()
	 * @param string　$fieldName = フィールド名
	 * @param array　$options = 属性を設定
	 */
	function text($fieldName, $options = array()) {
		$options = $this->_initInputField(
			$fieldName,
			array_merge(array('type' => 'text'), $options)
		);

		//! insert by konishi 2009/06/01 start
		//日付の場合は区切り文字を「/」に変更（カレンダーの為）
		if (preg_match('/date/u', $fieldName) || isset($options['date'])) {
			if (!(isset($options['date']) && $options['date'] == false)) {
				if (isset($options['value'])) {
					$options['value'] = preg_replace('/[\-\.]/u', '/', $options['value']);
				}
			}
		}
		//! insert by konishi 2009/06/01 end

		if (isset($options['class']) && preg_match('/calendar/', $options['class'])) {$ua=$_SERVER['HTTP_USER_AGENT'];
			if((strpos($ua,'iPhone')!==false) || (strpos($ua,'iPad')!==false)||(strpos($ua,'iPod')!==false)||(strpos($ua,'Android')!==false)) {
				$options['value'] = preg_replace('/[\/\.]/u', '-', $options['value']);
				$options['value'] = preg_replace('/calendar/u', '', $options['value']);
				return  '<input type="date" style="width:8em;" id="'. $options['id']. '" name="'. $options['name']. '" value="'. esHtml($options['value']). '" />';
			}
		}
		if (isset($options['class']) && preg_match('/times/', $options['class'])) {$ua=$_SERVER['HTTP_USER_AGENT'];
			if((strpos($ua,'iPhone')!==false) || (strpos($ua,'iPad')!==false)||(strpos($ua,'iPod')!==false)||(strpos($ua,'Android')!==false)) {
				$options['value'] = preg_replace('/[\/\.]/u', '-', $options['value']);
				$options['value'] = preg_replace('/times/u', '', $options['value']);
				return  '<input type="time" style="width:6em;" id="'. $options['id']. '" name="'. $options['name']. '" value="'. esHtml($options['value']). '" />';
			}
		}

		return $this->output(sprintf(
			$this->Html->tags['input'],
			$options['name'],
			$this->_parseAttributes($options, array('name'), null, ' ')
		));
	}


	/**
	 *  テキストエリア　最初の文字が改行の場合ブラウザで無視されるので改行コード（PHP_EOL）追加
	 * (non-PHPdoc)
	 * @see FormHelper::textarea()
	 * @param string　$fieldName = フィールド名
	 * @param array　$options = 属性を設定
	 */
	function textarea($fieldName, $options = array()) {
		$options = $this->_initInputField($fieldName, $options);
		$value = null;

		if (array_key_exists('value', $options)) {
			$value = $options['value'];
			if (!array_key_exists('escape', $options) || $options['escape'] !== false) {
				$value = h($value);
			}
			unset($options['value']);
		}
		return $this->output(sprintf(
			$this->Html->tags['textarea'],
			$options['name'],
			$this->_parseAttributes($options, array('type', 'name'), null, ' '),
			PHP_EOL. $value
		));
	}


	/**
	 * 非表示フォーム表示の日付型を「-」→「/」に置換処理追加
	 * (non-PHPdoc)
	 * @see FormHelper::hidden()
	 * @param string　$fieldName = フィールド名
	 * @param array　$options = 属性を設定
	 */
	function hidden($fieldName, $options = array()) {
		$secure = true;

		if (isset($options['secure'])) {
			$secure = $options['secure'];
			unset($options['secure']);
		}
		$options = $this->_initInputField($fieldName, array_merge(
			$options, array('secure' => false)
		));
		$model = $this->model();

		if ($fieldName !== '_method' && $model !== '_Token' && $secure) {
			$this->__secure(null, '' . $options['value']);
		}

		//! insert by konishi 2009/06/01 start
		//日付の場合は区切り文字を「/」に変更（カレンダーの為）
		if (preg_match('/date/u', $fieldName) || isset($options['date'])) {
			if (!(isset($options['date']) && $options['date'] == false)) {
				if (isset($options['value'])) {
					$options['value'] = preg_replace('/[\-\.]/u', '/', $options['value']);
				}
			}
		}
		//! insert by konishi 2009/06/01 end

		return $this->output(
			sprintf(
				$this->Html->tags['hidden'],
				$options['name'],
				$this->_parseAttributes($options, array('name', 'class'), '', ' ')
			)
		);
	}


	/**
	 * 複数チェックボックスの表示のラップを「div」→「span」に変更
	 * (non-PHPdoc)
	 * @see FormHelper::__selectOptions()
	 * @param	$elements
	 * @param	$selected
	 * @param	$parents
	 * @param	$showParents
	 * @param	$attributes
	 */
	function __selectOptions($elements = array(), $selected = null, $parents = array(), $showParents = null, $attributes = array()) {
		$select = array();
		$attributes = array_merge(array('escape' => true, 'style' => null), $attributes);
		$selectedIsEmpty = ($selected === '' || $selected === null);
		$selectedIsArray = is_array($selected);

		foreach ($elements as $name => $title) {
			$htmlOptions = array();
			if (is_array($title) && (!isset($title['name']) || !isset($title['value']))) {
				if (!empty($name)) {
					if ($attributes['style'] === 'checkbox') {
						$select[] = $this->Html->tags['fieldsetend'];
					} else {
						$select[] = $this->Html->tags['optiongroupend'];
					}
					$parents[] = $name;
				}
				$select = array_merge($select, $this->__selectOptions(
					$title, $selected, $parents, $showParents, $attributes
				));

				if (!empty($name)) {
					if ($attributes['style'] === 'checkbox') {
						$select[] = sprintf($this->Html->tags['fieldsetstart'], $name);
					} else {
						$select[] = sprintf($this->Html->tags['optiongroup'], $name, '');
					}
				}
				$name = null;
			} elseif (is_array($title)) {
				$htmlOptions = $title;
				$name = $title['value'];
				$title = $title['name'];
				unset($htmlOptions['name'], $htmlOptions['value']);
			}

			if ($name !== null) {
				if ((!$selectedIsEmpty && $selected == $name) || ($selectedIsArray && in_array($name, $selected))) {
					if ($attributes['style'] === 'checkbox') {
						$htmlOptions['checked'] = true;
					} else {
						$htmlOptions['selected'] = 'selected';
					}
				}

				if ($showParents || (!in_array($title, $parents))) {
					$title = ($attributes['escape']) ? h($title) : $title;

					if ($attributes['style'] === 'checkbox') {
						$htmlOptions['value'] = $name;

						$tagName = Inflector::camelize(
							$this->model() . '_' . $this->field().'_'.Inflector::underscore($name)
						);
						$htmlOptions['id'] = $tagName;
						$label = array('for' => $tagName);

						if (isset($htmlOptions['checked']) && $htmlOptions['checked'] === true) {
							$label['class'] = 'selected';
						}

						list($name) = array_values($this->__name());

						if (empty($attributes['class'])) {
							$attributes['class'] = 'checkbox';
						}
						$label = $this->label(null, $title, $label);
						$item = sprintf(
							$this->Html->tags['checkboxmultiple'], $name,
							$this->_parseAttributes($htmlOptions)
						);
						//! modified by konishi 2009/06/01 start
						//$select[] = $this->Html->div($attributes['class'], $item . $label);
						$select[] = $this->Html->tag('span', $item . $label, $attributes['class']);
						//! modified by konishi 2009/06/01 end
					} else {
						$select[] = sprintf(
							$this->Html->tags['selectoption'],
							$name, $this->_parseAttributes($htmlOptions), $title
						);
					}
				}
			}
		}
		return array_reverse($select, true);
	}

}

?>