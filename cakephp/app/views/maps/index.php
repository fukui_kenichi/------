<?php
	echo $appForm->create();
?>
<div class="row">
   	<div class="xs-invisible"><img src="<?php echo ROOT_URL ?>img/map_info.png" alt="顧客地図情報"></div>
   	<div class="xs-visible"><img src="<?php echo ROOT_URL ?>img/map_info_s.png" alt="顧客地図情報"></div>
</div>
<table class="table table-bordered table-navy table-edit">
	<tr>
		<td class="sm-center">
            <div id="map" style="width:100%;height:402px"></div>
        </td>
	</tr>
</table>
<div class="row">
	<div class="center">
        <div class="xs-invisible">
            <input type="image" name="cancel" alt="戻る" src="<?php echo ROOT_URL ?>img/bt_back.png" title="戻る" />
        </div>
        <div class="xs-visible">
            <input type="image" name="cancel" alt="戻る" src="<?php echo ROOT_URL ?>img/bt_back_s.png" title="戻る" />
        </div>
	</div>
</div>

<?php
	echo $appForm->end();
?>

<?php if (empty($_SERVER['HTTPS'])) { ?>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
<?php }else{ ?>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true"></script>
<?php } ?>
<script type="text/javascript">
var currentWindow = null;
<?php if (!empty($latitude) && !empty($longitude)) { // 顧客詳細画面から戻ってきた場合?>
	//マップの表示
	initialize(<?php echo esHtml($latitude) ?>, <?php echo esHtml($longitude) ?>);
<?php }else{ ?>
	//現在位置を取得する
	navigator.geolocation.getCurrentPosition(successFunc, errorFunc);
<?php } ?>
//成功した時の関数
function successFunc(position){
	//緯度
	latitude = position.coords.latitude;
	//経度
	longitude = position.coords.longitude;

	//マップの表示
	initialize(latitude, longitude);
}

//失敗した時の関数
function errorFunc(error){
	//エラーコードのメッセージを定義
	var errorMessage = {
		0: "原因不明のエラーが発生しました…。",
		1: "位置情報の取得が許可されませんでした…。",
		2: "電波状況などで位置情報が取得できませんでした…。",
		3: "位置情報の取得に時間がかかり過ぎてタイムアウトしました…。",
	};

	//エラーコードに合わせたエラー内容を表示
	alert(errorMessage[error.code]);
}

//マップの表示
function initialize(latitude, longitude){
	centerLatLng = new google.maps.LatLng(latitude, longitude); // 中心点
	/* 地図のオプション設定 */
	myOptions = {
		/*初期のズーム レベル */
		zoom: 16,
		/* 地図の中心点 */
		center: centerLatLng,
		/* 地図タイプ */
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map=new google.maps.Map(document.getElementById("map"), myOptions);
	// リスナー登録 最初の画面で顧客を表示
	google.maps.event.addListener(map, 'projection_changed', function() {
		setCandidateCustomer();
	});
	// リスナー登録 ドラッグ終了後で顧客を表示
	google.maps.event.addListener(map, 'dragend', function() {
		setCandidateCustomer();
	});
	// リスナー登録 拡大・縮小で顧客を表示
	google.maps.event.addListener(map, 'zoom_changed', function() {
		setCandidateCustomer();
	});
}
// 候補駐車場をマーカーと共に地図上に設定する関数
function setCandidateCustomer() {
	candidate = getCandidateCustomer();
	for (var i in candidate) {
		var name = '<a href="'+'<?php echo $html->url("/busies/index/") ?>'+candidate[i].id+'">'+candidate[i].name+'</a>';
		var latlng = new google.maps.LatLng(candidate[i].latitude, candidate[i].longitude);
		createMarker(name,latlng,map);
	}
}
function createMarker(name,latlng,map){
	var infoWindow = new google.maps.InfoWindow();
	var marker = new google.maps.Marker({position: latlng,map: map});
	google.maps.event.addListener(marker, 'click', function() {
		if (currentWindow) {
			currentWindow.close();
			}
			infoWindow.setContent(name);
			infoWindow.open(map,marker);
			currentWindow = infoWindow;
		});
}


// 地図上に表示される候補駐車場を取得するAJAX関数
function getCandidateCustomer() {
	var errFlag = true; //通信エラーフラグ
	var returnData = null;

	lngHigh = map.getBounds().getNorthEast().lng();  // 経度高
	lngLow = map.getBounds().getSouthWest().lng(); // 経度低
	latHigh = map.getBounds().getNorthEast().lat();  // 緯度高
	latLow = map.getBounds().getSouthWest().lat();  // 緯度低
	centerLng = map.getCenter().lng(); // 中心経度
	centerLat = map.getCenter().lat(); // 中心緯度

	$.ajax({
		type: "POST",
		url: "<?php echo $html->url("/maps/ajax_get_candidate") ?>",
		async: false,
		dataType: "json",
		data:{
			lngHigh : lngHigh, // 経度高
			lngLow : lngLow, // 経度低
			latHigh : latHigh, // 緯度高
			latLow : latLow, // 緯度低
			centerLng : centerLng, // 中心経度
			centerLat : centerLat, // 中心緯度
		},
		success : function(result)
		{
			errFlag = false; // 通信は正常に終了

			returnData =  result;
		}
	});

	if (errFlag) { // 通信エラーの場合
		alert("サーバからデータを取得できませんでした。");
	}

	return returnData;
}
</script>