<?php
	echo $appForm->create();

	// 項目型
	$MFieldTypeDef = new MFieldTypeDef();
	$mFieldTypeArr = $MFieldTypeDef->getArr();
	// はいいいえ型
	$YesNoTypeDef = new YesNoTypeDef();
	$yesNoArr = $YesNoTypeDef->getArr();
?>
<input type="image" id="noLoading" name="space" src="<?php echo ROOT_URL ?>img/space.gif" onclick="return false;">
<div class="row">
   	<div class="xs-invisible">
        <img src="<?php echo ROOT_URL ?>img/usermaster-henshu.png" alt="ユーザーマスター編集">
        <div class="title_right">
            <a href="#target_bottom" id="noLoading"><img class="btnAddNew" src="<?php echo ROOT_URL ?>img/bt_add_field.png" alt="新規項目追加"></a>
        </div>
    </div>
   	<div class="xs-visible">
        <img src="<?php echo ROOT_URL ?>img/usermaster-henshu_s.png" alt="ユーザーマスター編集">
        <div class="title_right">
            <a href="#target_bottom" id="noLoading"><img class="btnAddNew" src="<?php echo ROOT_URL ?>img/bt_add_field_s.png" alt="新規項目追加"></a>
        </div>
    </div>
</div>
<?php if (!empty($appForm->validationErrors)) {?>
<div class="error-message">
    <div class="error-message-inner">
	    <p class="error-message-header">入力内容にエラーがあります。</p>
	    <?php echo $appForm->errorAll(). "\n"; ?>
    </div>
</div>
<?php }?>

<?php
if (!empty($appForm->data['UserMaster'])) {
?>

<div class="smart-table full" id="listTable">
	<div class="smart-table-header">
		<div class="smart-table-header-inner">
			<table>
				<thead>
					<tr>
						<th><div style="width:160px;">項目名</div></th>
						<th><div style="width:160px;">型</div></th>
						<th><div style="width:160px;">初期値</div></th>
						<th><div style="width:200px;">選択値</div></th>
						<th><div style="width:70px;">必須入力</div></th>
						<th><div style="width:60px;">並び順</div></th>
						<th><div style="width:60px;"></div></th>
						<th class="space"></th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
	<div class="smart-table-body">
		<div class="smart-table-body-inner">
			<table>
				<tbody>
				<?php
					foreach($appForm->data['UserMaster'] as $key => $val) {
						// 削除データはスキップ
						if (isset($val['db_id']) && empty($val['id'])) continue;
				?>
					<tr id="trUserMaster_<?php echo $key; ?>">
						<td>
							<div style="width:160px;">
								<?php
									// 項目名
									if (empty($val['default_flag'])) {
										echo esHtml($val['koumoku']);
										echo $appForm->input("UserMaster.{$key}.koumoku", array('type' => 'hidden'));
									} else {
										echo $appForm->input("UserMaster.{$key}.koumoku", array('type' => 'text',  'size' => 15, 'maxlength' => 256));
									}

									// 保存時のための変数
									echo $appForm->input("UserMaster.{$key}.id", array('type' => 'hidden'));
									echo $appForm->input("UserMaster.{$key}.default_flag", array('type' => 'hidden'));
								?>
							</div>
						</td>
						<td>
							<div style="width:160px;">
								<?php
									// 型
									if (empty($val['db_id'])) {
										echo $appForm->input("UserMaster.{$key}.type", array('type' => 'select', 'options' => $mFieldTypeArr, 'empty' => '選択してください'));
									} else {
										if (isset($mFieldTypeArr[$val['type']])) echo esHtml($mFieldTypeArr[$val['type']]);
										echo $appForm->input("UserMaster.{$key}.type", array('type' => 'hidden'));
									}
								?>
							</div>
						</td>
						<td>
							<div style="width:160px;">
								<?php // 初期値
									$style = ''; // クラス属性設定
									if (empty($val['default_flag'])) { // デフォルト項目の場合
										echo esHtml($val['default_ti']);
										echo $appForm->input("UserMaster.{$key}.default_ti", array('type' => 'hidden'));
									} else { // デフォルト項目ではないの場合
										if ($val['type'] != MASTER_TYPE_LIST && $val['type'] != MASTER_TYPE_RADI) $style = 'display:none'; // リスト、ラジオでない場合は隠す
										echo $appForm->input("UserMaster.{$key}.default_ti", array('type' => 'text', 'size' => 15, 'maxlength' => 256, 'style' => $style));
									}
								?>
							</div>
						</td>
						<td>
							<div style="width:200px;">
								<?php // 選択値
									if (empty($val['default_flag'])) { // デフォルト項目の場合
										echo nl2br(esHtml($val['select_ti']));
										echo $appForm->input("UserMaster.{$key}.select_ti", array('type' => 'hidden'));
									} else { // デフォルト項目ではないの場合
										if ($val['type'] == MASTER_TYPE_LIST || $val['type'] == MASTER_TYPE_RADI || $val['type'] == MASTER_TYPE_CHEK){
											echo $appForm->input("UserMaster.{$key}.select_ti", array('type' => 'textarea', 'cols' => 20, 'rows' => 5, 'wrap' => 'off'));
										}else{
											echo $appForm->input("UserMaster.{$key}.select_ti", array('type' => 'textarea', 'cols' => 20, 'rows' => 5, 'wrap' => 'off', 'style' => 'display:none'));
										}
									}
								?>
							</div>
						</td>
						<td>
							<div style="width:70px;">
								<?php // 必須入力
									if (empty($val['default_flag'])) {
										if (isset($yesNoArr[$val['required']])) {
											echo esHtml($yesNoArr[$val['required']]);
											echo $appForm->input("UserMaster.{$key}.required", array('type' => 'hidden'));
										}
									} else {
										echo $appForm->input("UserMaster.{$key}.required", array('type' => 'select', 'options' => $yesNoArr));
									}
								?>
							</div>
						</td>
						<td>
							<div style="width:60px;">
								<?php echo $appForm->input("UserMaster.{$key}.number", array('type' => 'text', 'size' => 3, 'maxlength' => 3, 'class' => 'right ime-off num-only'));?>
							</div>
						<td>
							<div style="width:60px;">
								<?php if (!empty($val['default_flag'])) { ?>
									<input type="button" class="btn btnDel" id="btnDel_<?php echo $key; ?>" value="" />
								<?php }?>
							</div>
						</td>
						<td class="space"></td>
					</tr>
			<?php } ?>
				</tbody>
			</table>
			<div class="space"></div>
		</div>
	</div>
</div>
<div class="row">
	<div class="center">
        <div class="xs-invisible">
   			<input type="image" name="save" alt="更新" src="<?php echo ROOT_URL ?>img/bt_update.png" title="更新" />
            <input type="image" name="cancel" alt="キャンセル" src="<?php echo ROOT_URL ?>img/bt_cancel.png" title="キャンセル" />
        </div>
        <div class="xs-visible">
   			<input type="image" name="save" alt="更新" src="<?php echo ROOT_URL ?>img/bt_update_s.png" title="更新" />
            <input type="image" name="cancel" alt="キャンセル" src="<?php echo ROOT_URL ?>img/bt_cancel_s.png" title="キャンセル" />
        </div>
	</div>
</div>
<?php
} else {
	echo '<p>データがありません</p>';
}
?>
<div id="target_bottom"></div>

<!-- 挿入行テンプレート start -->
<table class="hide">
	<tbody>
	<tr id="trUserMaster">
		<td>
			<div style="width:160px;">
			<?php
				echo $appForm->input("UserMaster.ZZZ.koumoku", array('type' => 'text', 'size' => 15, 'maxlength' => 256));
				echo $appForm->input("UserMaster.ZZZ.default_flag", array('type' => 'hidden', 'value' => 1));
			?>
			</div>
		</td>
		<td>
			<div style="width:160px;">
				<?php echo $appForm->input("UserMaster.ZZZ.type", array('type' => 'select', 'options' => $mFieldTypeArr, 'empty' => '選択してください')) ?>
			</div>
		</td>
		<td>
			<div style="width:160px;">
				<?php echo $appForm->input("UserMaster.ZZZ.default_ti", array('type' => 'text', 'size' => 15, 'maxlength' => 256)) ?>
			</div>
		</td>
		<td>
			<div style="width:200px;">
				<?php echo $appForm->input("UserMaster.ZZZ.select_ti", array('type' => 'textarea', 'cols' => 20, 'rows' => 5, 'wrap' => 'off')) ?>
			</div>
		</td>
		<td>
			<div style="width:70px;">
				<?php echo $appForm->input("UserMaster.ZZZ.required", array('type' => 'select', 'options' => $yesNoArr)) ?>
			</div>
		</td>
		<td>
			<div style="width:60px;">
				<?php echo $appForm->input("UserMaster.ZZZ.number", array('type' => 'text', 'size' => 3, 'maxlength' => 3, 'class' => 'right ime-off num-only'));?>
			</div>
		</td>
		<td>
			<div style="width:60px;">
				<input type="button" class="btn btnDel" id="btnDel_ZZZ"  value="">
			</div>
		</td>
		<td class="space"></td>
	</tr>
	</tbody>
</table>
<!-- 挿入行テンプレート end -->

<div class="hide">
	<input type="hidden" name="hdnId">
	<input type="submit" name="del" value="削除">
	<?php echo $appForm->input('Master.newRowNo', array('type' => 'hidden')); ?>

<?php // 削除時のための情報
	if (isset($appForm->data['UserMaster'])){
		foreach ($appForm->data['UserMaster'] as $key => $val) {
			if (isset($val['db_id'])){
				echo $appForm->input("UserMaster.{$key}.db_id",array( "type" => "hidden"));
			}
		}
	}
?>
</div>
<script type="text/javascript">
// 項目の追加
$(".btnAddNew").click(function(ev){
	newRowNo = $("input#MasterNewRowNo").val();

	// テンプレートからコピーしてフォームを作成
	tpl = $("#trUserMaster").clone();
	$("button, tr,  select, label, input, textarea", tpl).each(function(){
		id = $(this).attr("id");
		if (typeof id !== "undefined") {
			$(this).attr("id", id.replace("ZZZ", newRowNo));
		}
		name = $(this).attr("name");
		if (typeof name !== "undefined") {
			$(this).attr("name", name.replace("ZZZ",newRowNo));
		}
		labelFor = $(this).attr("for");
		if (typeof labelFor !== "undefined") {
			$(this).attr("for", labelFor.replace("ZZZ", newRowNo));
		}
	});
	tpl.attr("id","trUserMaster_" + newRowNo);

	// テーブル末尾に追加して表示
	$("div#listTable table tbody tr:last").after(tpl);
	$(tpl).show();

	// 追加した行の並び順を持たす
	var counter = 0;
    $("tr[id^=trUserMaster_]").each(function(){
        counter++;
    });
	$("#UserMaster"+newRowNo+"Number").val(counter);

	// smartTableの再描画
	setSmartTableDisplay();

	// 追加行番号をでクリメント
	$("input#MasterNewRowNo").val(newRowNo - 1);

	return true;
});

//項目の削除
$(document).on("click", "input[id^=btnDel_]", function(ev){
	trId = "tr#"+this.id.replace('btnDel_', 'trUserMaster_');
	jConfirm('削除を実行すると、対象となる項目の登録済みの情報が失われます。削除を実行してもよろしいですか？', 'WATSON顧客管理', function(r) {
		if(r) {
			// 該当行の削除
			$(trId).remove();

			// smartTableの再描画
			setSmartTableDisplay();
		} else {
		    return true;
		}
	});

	return true;
});

// 初期値、選択値の表示非表示
$(document).on("change", "select[id$=Type]", function(ev){
	defaultId = "input[id='"+this.id.replace('Type', 'DefaultTi')+"']";
	selectId = "textarea[id='"+this.id.replace('Type', 'SelectTi')+"']";

	if ($(this).val() == "<?php echo MASTER_TYPE_LIST ?>" || $(this).val() == "<?php echo MASTER_TYPE_RADI ?>") {
		// リスト型、ラジオ型
		$(defaultId).show();
		$(selectId).show();
	}else if ($(this).val() == "<?php echo MASTER_TYPE_CHEK ?>") {
		// チェック型
		$(defaultId).hide();
		$(selectId).show();
	} else {
		$(defaultId).hide();
		$(selectId).hide();
	}

	// smartTableの再描画
	setSmartTableDisplay();

	return true;
});
</script>
<?php
	echo $appForm->end();
?>