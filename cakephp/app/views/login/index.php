<?php
	echo $appForm->create();
?>
<?php
	if (!empty($errMsg)) {
		echo '<div class="error-message"><div class="error-message-inner">'. $errMsg. '</div></div>';
	}
?>
	<table class="loginTable">
		<tr>
			<td width="50%" class="right">ID</td>
			<td width="50%"><?php echo $appForm->input("User.account", array('type' => 'text', 'class' => 'form-control', 'autocapitalize' => 'off')); ?></td>
		</tr>
		<tr>
			<td class="right">パスワード</td>
			<td><?php echo $appForm->input("User.password", array('type' => 'password', 'class' => 'form-control')); ?></td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:center">
            	<div class="xs-invisible">
					<input name="login" type="image" id="login" src="<?php echo ROOT_URL ?>img/login.png" alt="ログイン" />
                </div>
            	<div class="xs-visible">
					<input name="login" type="image" id="login" src="<?php echo ROOT_URL ?>img/login_s.png" alt="ログイン" />
                </div>
			</td>
		</tr>
	</table>
	<?php echo $appForm->input("User.token", array('type' => 'hidden')); ?>
<?php
	echo $appForm->end();
?>