<?php
	echo $appForm->create();
?>
<div class="row">
	<div class="xs-invisible">
		<img src="<?php echo ROOT_URL ?>img/okiniiri-touroku.png" alt="お気入り登録">
	</div>
		<div class="xs-visible">
		<img src="<?php echo ROOT_URL ?>img/okiniiri-touroku_s.png" alt="お気入り登録">
	</div>
</div>
<?php if (!empty($appForm->validationErrors)) {?>
<div class="error-message">
    <div class="error-message-inner">
	    <p class="error-message-header">入力内容にエラーがあります。</p>
    </div>
</div>
<?php }?>
<table class="table table-bordered table-navy">
	<tr>
		<th width="150">名前<span class="err-msg">【必須】</span></th>
		<td>
			<?php
				echo $appForm->input('Favorite.name', array('type' => 'text', 'class' => 'form-control', 'maxlength' => 256));
				$span = '';
				if (isset($appForm->validationErrors['Favorite']['name'])) {
					$span .= '<span class="err-msg">' . esHtml($appForm->validationErrors['Favorite']['name']). '</span>';
				}
				echo $span;
			?>
		</td>
	</tr>
	<tr>
		<th width="150">検索キーワード</th>
		<td>
		<?php
			if (!empty($appForm->data['Favorite']['keyword'])) {
				echo esHtml($appForm->data['Favorite']['keyword']). '<br/>';
			}
			$span = '';
			if (isset($appForm->validationErrors['Favorite']['keyword'])) {
				$span .= '<span class="err-msg">' . esHtml($appForm->validationErrors['Favorite']['keyword']). '</span>';
			}
			echo $span;
			echo $appForm->input('Favorite.keyword', array('type' => 'hidden'));
		?>
		</td>
	</tr>
</table>
<div class="row">
	<div class="center">
		<div class="xs-invisible">
			<?php if (empty($appForm->data['Favorite']['id'])){ ?>
				<input type="image" name="save" alt="登録" src="<?php echo ROOT_URL ?>img/bt_apply.png" title="登録" />
			<?php }else{ ?>
				<input type="image" name="save" alt="更新" src="<?php echo ROOT_URL ?>img/bt_update.png" title="更新" />
			<?php } ?>
			<input type="image" name="cancel" alt="キャンセル" src="<?php echo ROOT_URL ?>img/bt_cancel.png" title="キャンセル" />
		</div>
		<div class="xs-visible">
			<?php if (empty($appForm->data['Customer']['id'])){ ?>
				<input type="image" name="save" alt="登録" src="<?php echo ROOT_URL ?>img/bt_apply_s.png" title="登録" />
			<?php }else{ ?>
				<input type="image" name="save" alt="更新" src="<?php echo ROOT_URL ?>img/bt_update_s.png" title="更新" />
			<?php } ?>
			<input type="image" name="cancel" alt="キャンセル" src="<?php echo ROOT_URL ?>img/bt_cancel_s.png" title="キャンセル" />
		</div>
	</div>
</div>
<div class="hide">
	<?php
		echo $appForm->input('Favorite.id', array('type' => 'hidden'));
		echo $appForm->input('Favorite.updated', array('type' => 'hidden'));
	?>
</div>
<?php
	echo $appForm->end();
?>
