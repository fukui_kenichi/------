<?php
	echo $appForm->create();
?>
<div class="row">
	<div class="xs-invisible">
		<img src="<?php echo ROOT_URL ?>img/okiniiri-sakujo.png" alt="お気入り削除確認">
	</div>
		<div class="xs-visible">
		<img src="<?php echo ROOT_URL ?>img/okiniiri-sakujo_s.png" alt="お気入り削除確認">
	</div>
</div>
<?php if (!empty($appForm->validationErrors)) {?>
<div class="error-message">
    <div class="error-message-inner">
	    <p class="error-message-header">入力内容にエラーがあります。</p>
    </div>
</div>
<?php }?>
<table class="table table-bordered table-navy">
	<tr>
		<th width="150">名前<span class="err-msg">【必須】</span></th>
		<td>
		<?php
			if (!empty($appForm->data['Favorite']['name'])) {
				echo esHtml($appForm->data['Favorite']['name']);
			}
		?>
		</td>
	</tr>
	<tr>
		<th width="150">検索キーワード</th>
		<td>
		<?php
			if (!empty($appForm->data['Favorite']['keyword'])) {
				echo esHtml($appForm->data['Favorite']['keyword']). '<br/>';
			}
		?>
		</td>
	</tr>
</table>
<div class="row">
	<div class="center">
		<span class="err-msg">※削除すると、このお気に入り検索条件は失われます。</span>
		<br>
		<div class="xs-invisible">
			<input type="image" name="del" src="<?php echo ROOT_URL ?>img/bt_delete.png" title="削除" alt="削除">
			<input type="image" name="cancel" src="<?php echo ROOT_URL ?>img/bt_cancel.png" title="キャンセル" alt="キャンセル">
		</div>
		<div class="xs-visible">
			<input type="image" name="del" src="<?php echo ROOT_URL ?>img/bt_delete_s.png" title="削除" alt="削除">
			<input type="image" name="cancel" src="<?php echo ROOT_URL ?>img/bt_cancel_s.png" title="キャンセル" alt="キャンセル">
		</div>
	</div>
</div>
<div class="hide">
	<?php
		echo $appForm->input('Favorite.id', array('type' => 'hidden'));
	?>
</div>
<?php
	echo $appForm->end();
?>
