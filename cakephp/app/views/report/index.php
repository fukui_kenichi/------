<div id="contents">
<div id="header">
	<h1>Watson CRM RoyalCustomer(優良顧客)育成プログラム　進捗レポート</h1>
</div><!--header-->
<div id="main">
	<h3>■Watson導入後入電及び新規顧客獲得推移分析</h3>
	<div id="cti" style="min-width: 310px; height: 200px; margin: 0 auto"></div>

	<h3>■顧客分布分析</h3>
	<div style="text-align:left; width:345px; margin-right:10px; float:left; border:2px solid; padding:1px;">
		<div id="customer" style="width: 340px; height: 250px; margin: 0 auto"></div>
	</div>
	<div style="text-align:left; width:345px; margin-right:10px; float:left; border:2px solid; padding:1px;">
		<div id="mail" style="width: 340px; height: 250px; margin: 0 auto"></div>
	</div>
	<div style="text-align:left; width:360px; margin-right:10px; float:left;">
		<h3>
			■商圏顧客分布
			<?php echo $appForm->input('Customer.address', array('type' => 'text', 'size' => '10')); ?>
			<input type="button" class="btn btn-primary btn-sm" id="btnMap" value="郵便番号検索" />
		</h3>
		 <div id="map" style="width:100%;height:350px"></div>
	</div>
	<div style="text-align:left; width:350px; margin:0; float:left;">
		<h3>■コンサルテーション（処方箋）</h3>
		<textarea name="textarea" id="textarea" cols="40" rows="18"></textarea>
		<img src="<?php echo ROOT_URL ?>report/img/logo.jpg">
	</div>
</div><!--main-->
<div id="footer">
	<address></address>
</div><!--footer-->
</div><!--contents-->

<?php if (empty($_SERVER['HTTPS'])) { ?>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
<?php }else{ ?>
	<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true"></script>
<?php } ?>

<script type="text/javascript">
var currentWindow = null;

//現在位置を取得する
navigator.geolocation.getCurrentPosition(successFunc, errorFunc);

//成功した時の関数
function successFunc(position){
	//緯度
	latitude = position.coords.latitude;
	//経度
	longitude = position.coords.longitude;

	//マップの表示
	initialize(latitude, longitude);
}

//失敗した時の関数
function errorFunc(error){
	//エラーコードのメッセージを定義
	var errorMessage = {
		0: "原因不明のエラーが発生しました…。",
		1: "位置情報の取得が許可されませんでした…。",
		2: "電波状況などで位置情報が取得できませんでした…。",
		3: "位置情報の取得に時間がかかり過ぎてタイムアウトしました…。",
	};

	//エラーコードに合わせたエラー内容を表示
	alert(errorMessage[error.code]);
}

//保存時に住所が入力されていれば緯度と経度を取得する
$("input#btnMap").click(function(ev) {
	// 住所が入っているなら住所から緯度経度を取得
	if ($("#CustomerAddress").val() != "") {
		// 緯度経度検索
		var geocoder = new google.maps.Geocoder();
		geocoder.geocode(
			{
				'address': $("#CustomerAddress").val(),
				'region': 'jp'
			},
			function(results, status){
				if(status==google.maps.GeocoderStatus.OK){
					var latitude = results[0].geometry.location.A;
					var longitude = results[0].geometry.location.F;
					initialize(latitude, longitude);
				}
			}
		);
	}
});

//マップの表示
function initialize(latitude, longitude){
	centerLatLng = new google.maps.LatLng(latitude, longitude); // 中心点
	/* 地図のオプション設定 */
	myOptions = {
		/*初期のズーム レベル */
		zoom: 16,
		/* 地図の中心点 */
		center: centerLatLng,
		/* 地図タイプ */
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map=new google.maps.Map(document.getElementById("map"), myOptions);
	// リスナー登録 最初の画面で顧客を表示
	google.maps.event.addListener(map, 'projection_changed', function() {
		setCandidateCustomer();
	});
	// リスナー登録 ドラッグ終了後で顧客を表示
	google.maps.event.addListener(map, 'dragend', function() {
		setCandidateCustomer();
	});
	// リスナー登録 拡大・縮小で顧客を表示
	google.maps.event.addListener(map, 'zoom_changed', function() {
		setCandidateCustomer();
	});
}
// 候補駐車場をマーカーと共に地図上に設定する関数
function setCandidateCustomer() {
	candidate = getCandidateCustomer();
	for (var i in candidate) {
		var name = candidate[i].name;
		var latlng = new google.maps.LatLng(candidate[i].latitude, candidate[i].longitude);
		createMarker(name,latlng,map);
	}
}
function createMarker(name,latlng,map){
	var infoWindow = new google.maps.InfoWindow();
	var marker = new google.maps.Marker({position: latlng,map: map});
	google.maps.event.addListener(marker, 'click', function() {
		if (currentWindow) {
			currentWindow.close();
			}
			infoWindow.setContent(name);
			infoWindow.open(map,marker);
			currentWindow = infoWindow;
		});
}


// 地図上に表示される候補駐車場を取得するAJAX関数
function getCandidateCustomer() {
	var errFlag = true; //通信エラーフラグ
	var returnData = null;

	lngHigh = map.getBounds().getNorthEast().lng();  // 経度高
	lngLow = map.getBounds().getSouthWest().lng(); // 経度低
	latHigh = map.getBounds().getNorthEast().lat();  // 緯度高
	latLow = map.getBounds().getSouthWest().lat();  // 緯度低
	centerLng = map.getCenter().lng(); // 中心経度
	centerLat = map.getCenter().lat(); // 中心緯度

	$.ajax({
		type: "POST",
		url: "<?php echo $html->url("/maps/ajax_get_candidate") ?>",
		async: false,
		dataType: "json",
		data:{
			lngHigh : lngHigh, // 経度高
			lngLow : lngLow, // 経度低
			latHigh : latHigh, // 緯度高
			latLow : latLow, // 緯度低
			centerLng : centerLng, // 中心経度
			centerLat : centerLat, // 中心緯度
		},
		success : function(result)
		{
			errFlag = false; // 通信は正常に終了

			returnData =  result;
		}
	});

	if (errFlag) { // 通信エラーの場合
		alert("サーバからデータを取得できませんでした。");
	}

	return returnData;
}

$(function () {
	var chart;
	$(document).ready(function () {
		$('#customer').highcharts({
			chart: {
				plotBackgroundColor: null,
				plotBorderWidth: null,
				plotShadow: false
			},
			credits: {
				enabled: false,
			},
			title: {
				text: '新規顧客VS保有顧客'
			},
			tooltip: {
				pointFormat: ': <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					dataLabels: {
						formatter: function() {
							return '<b>'+ this.point.name +'</b>:'+ Math.round(this.percentage*10)/10 + '%';
						}
					},
					showInLegend: true
				}
			},
			series: [{
				type: 'pie',
				data: [
					  <?php
					  foreach ($par_cus as $key=>$val){
						  echo "['{$key}', {$val}],";
					  }
					  ?>
				]
			}]
		});
		$('#mail').highcharts({
			chart: {
				plotBackgroundColor: null,
				plotBorderWidth: null,
				plotShadow: false
			},
			credits: {
				enabled: false,
			},
			title: {
				text:
					<?php
					echo "'{$title}月度　メール配信数'";
					?>
			},
			tooltip: {
				pointFormat: ': <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					dataLabels: {
						formatter: function() {
							return '<b>'+ this.point.name +'</b>:'+ Math.round(this.percentage*10)/10 + '%';
						}
					},
					showInLegend: true
				}
			},
			series: [{
				type: 'pie',
				data: [
						<?php
						foreach ($mail as $key=>$val){
							echo "['{$key}', {$val}],";
						}
						?>
				]
			}]
		});
		$('#cti').highcharts({
			chart: {
				zoomType: 'xy'
			},
			credits: {
				enabled: false,
			},
			title: {
				text: ''
			},
			subtitle: {
				text: ''
			},
			xAxis: [{
				categories: [
							 <?php
							 foreach ($cti as $key=>$val){
								 echo "'{$key}月',";
							 }
							 ?>
							 ]
			}],
			yAxis: [{ // Primary yAxis
				labels: {
					format: '{value}件',
					style: {
						color: '#89A54E'
					}
				},
				title: {
					text: '　',
					style: {
						color: '#89A54E'
					}
				}
			}, { // Secondary yAxis
				title: {
					text: '　',
					style: {
						color: '#4572A7'
					}
				},
				labels: {
					format: '{value}件',
					style: {
						color: '#4572A7'
					}
				},
				opposite: true
			}],
			tooltip: {
				shared: true
			},
			legend: {
				layout: 'vertical',
				align: 'left',
				x: 120,
				verticalAlign: 'top',
				y: 100,
				floating: true,
				backgroundColor: '#FFFFFF'
			},
			series: [{
				name: '新規顧客',
				color: '#4572A7',
				type: 'column',
				yAxis: 1,
				data: [
						<?php
						foreach ($customer as $key=>$val){
							echo "{$val},";
						}
						?>
						],
				tooltip: {
					valueSuffix: ' 件'
				}

			}, {
				name: '入電数',
				color: '#89A54E',
				type: 'spline',
				data: [
						<?php
						foreach ($cti as $key=>$val){
							echo "{$val},";
						}
						?>
						],
				tooltip: {
					valueSuffix: '件'
				}
			}]
		});
	});

});
</script>

