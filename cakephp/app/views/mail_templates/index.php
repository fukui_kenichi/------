<?php
	echo $appForm->create();
?>
<div class="row">
	<div class="xs-invisible">
		<?php $ichiran_img = ROOT_URL . 'img/mailtemplate-ichiran.png';?>
		<img src="<?php echo $ichiran_img; ?>" alt="メールテンプレート一覧">
		<div class="title_right">
			<?php $new_img = ROOT_URL . 'img/bt_add_new_template.png';?>
			<a href="<?php echo $html->url("/mail_templates/edit") ?>"><img src="<?php echo $new_img; ?>" alt="新規メールテンプレート登録"></a>
		</div>
	</div>
	<div class="xs-visible">
		<?php $ichiran_img = ROOT_URL . 'img/mailtemplate-ichiran_s.png';?>
		<img src="<?php echo $ichiran_img; ?>" alt="メールテンプレート一覧">
		<div class="title_right">
			<?php $new_img = ROOT_URL . 'img/bt_add_new_template_s.png';?>
			<a href="<?php echo $html->url("/mail_templates/edit") ?>"><img src="<?php echo $new_img; ?>" alt="新規メールテンプレート登録"></a>
		</div>
	</div>
</div>

<?php
if (!empty($list)) {
?>
<div class="smart-table full" id="templateTable">
	<div class="smart-table-header">
		<div class="smart-table-header-inner">
			<table>
				<thead>
					<tr>
						<th><div style="width:100px;"></div></th>
						<th class="sort"><div style="width:400px;">名前<?php echo $paginator->sort('','name') ?><?php $str = ($direction == 'desc')?'<input type="button" class="btn btnSortUp" title="↑">':(($direction == 'asc')?'<input type="button" class="btn btnSortDown" title="↓">':""); echo $str?></div></th>
						<th class="space"></th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
	<div class="smart-table-body">
		<div class="smart-table-body-inner">
			<table>
				<tbody>
	<?php foreach ($list as $key => $val) { ?>
					<tr  id='list_tr_<?php echo $val['MailTemplate']['id'];?>'  class="click">
						<td class="center middle first">
							<div style="width:100px;">
								<a href="#" onClick="location.href='<?php echo $html->url("/mail_templates/edit/{$val['MailTemplate']['id']}") ?>'"><input type="button" class="btn btnEdit" title="編　集"></a>
								<a href="#" onClick="location.href='<?php echo $html->url("/mail_templates/del_conf/{$val['MailTemplate']['id']}") ?>'"><input type="button" class="btn btnDel" title="削　除"></a>
							</div>
						</td>
						<td><div style="width:400px;"><?php echo esHtml($val['MailTemplate']['name'])?></div></td>
            			<td class="space"></td>
					</tr>
	<?php }?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="smart-table-footer">
		<div class="smart-table-footer-inner">
			<table>
				<tfoot>
					<tr>
						<td colspan="4" colspan="middle">
							<?php echo $this->element('paginatorsmart') ?>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
<?php
} else {
	echo '<span class="message">データがありません</span>';
}
?>
<div class="row"></div>
<div class="row">
	<div class="center">
        <div class="xs-invisible">
            <input type="image" name="cancel" alt="戻る" src="<?php echo ROOT_URL ?>img/bt_back.png" title="戻る" />
        </div>
        <div class="xs-visible">
            <input type="image" name="cancel" alt="戻る" src="<?php echo ROOT_URL ?>img/bt_back_s.png" title="戻る" />
        </div>
	</div>
</div>

<div class="hide">
	<input type="hidden" name="hdnId">
	<input type="submit" name="del" value="削除">
</div>
<?php
	echo $appForm->end();
	$sortArr = array(
		1 => 'name',
		2 => 'name',
	);
?>
<script type="text/javascript">
$(function(){
	$("div.smart-table-body table tr td:not('.first')").click(function(){
		var idname = $(this).parent("tr").attr("id");
		var arr = idname.split('_');
		var userId = arr[2];

		window.location.href = '<?php echo $html->url("/mail_templates/detail/") ?>' + userId;
	});
	$(document).on("click touchstart", "th[id^='templateTable_sort_col_']" ,function(ev){
		var arr = [];
		var i = 1;
		<?php
		foreach ($sortArr as $key => $val) {
		?>
			arr[i] = "<?php echo $val?>";
			i = i + 1;
		<?php
		}
		?>
		var idname = this.id;
		var array = idname.split('_');
		var colId = array[3];
		var page = '<?php
			if (empty($page)) {
				echo $paginator->current();
			} else {
				echo $page;
			}
		?>';
		var direction = '<?php
			if (empty($direction)) {
				echo 'asc';
			} else {
				if ($direction == 'asc') {
					echo 'desc';
				} else {
					echo 'asc';
				}
			}
		?>';
		var sort = '<?php
			if (empty($sort)) {
				echo '';
			} else {
				echo $sort;
			}
		?>';
		if (sort != arr[colId]) {
			direction = 'asc';
		}
		sort = arr[colId];
		window.location.href = '<?php echo $html->url("/mail_templates/index/page:") ?>' + page + '/sort:' + sort + '/direction:' + direction;
	});
});
</script>