<?php
	echo $appForm->create();

?>
<div class="row">
   	<div class="xs-invisible">
   		<?php $sakujo_img = ROOT_URL . 'img/mailtemplate-sakujo.png';?>
        <img src="<?php echo $sakujo_img; ?>" alt="メールテンプレート削除確認">
    </div>
   	<div class="xs-visible">
   		<?php $sakujo_img = ROOT_URL . 'img/mailtemplate-sakujo_s.png';?>
        <img src="<?php echo $sakujo_img; ?>" alt="メールテンプレート削除確認">
    </div>
</div>
<table class="table table-bordered table-navy table-edit">
	<tr>
		<th width="150">名前</th>
		<td class="sm-center"><?php echo esHtml($appForm->data['MailTemplate']['name']) ?></td>
	</tr>
	<tr>
		<th>件名</th>
		<td class="sm-center"><?php echo esHtml($appForm->data['MailTemplate']['subject']) ?></td>
	</tr>
	<tr>
		<th>本文</th>
		<td class="sm-center"><?php echo nl2br(esHtml($appForm->data['MailTemplate']['body'])) ?></td>
	</tr>
</table>

<div class="row">
	<div class="center">
        <span class="err-msg">※削除すると、このメールテンプレートは失われます。</span><br>
        <div class="xs-invisible">
        	<input type="image" name="del" alt="削除" src="<?php echo ROOT_URL ?>img/bt_delete.png" title="削除" />
        	<input type="image" name="cancel" alt="キャンセル" src="<?php echo ROOT_URL ?>img/bt_cancel.png" title="キャンセル" />
        </div>
        <div class="xs-visible">
        	<input type="image" name="del" alt="削除" src="<?php echo ROOT_URL ?>img/bt_delete_s.png" title="削除" />
        	<input type="image" name="cancel" alt="キャンセル" src="<?php echo ROOT_URL ?>img/bt_cancel_s.png" title="キャンセル" />
        </div>
	</div>
</div>

<?php
	echo $appForm->end();
?>
