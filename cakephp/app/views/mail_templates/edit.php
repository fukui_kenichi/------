<?php
	echo $appForm->create();

?>
<div class="row">
   	<div class="xs-invisible">
   		<?php if (empty($appForm->data['MailTemplate']['id'])){ ?>
    	    <img src="<?php echo ROOT_URL ?>img/mailtemplate-toroku.png" alt="メールテンプレート登録">
        <?php }else{ ?>
	        <img src="<?php echo ROOT_URL ?>img/mailtemplate-henshu.png" alt="メールテンプレート編集">
        <?php } ?>
    </div>
   	<div class="xs-visible">
   		<?php if (empty($appForm->data['MailTemplate']['id'])){ ?>
	        <img src="<?php echo ROOT_URL ?>img/mailtemplate-toroku_s.png" alt="メールテンプレート登録">
        <?php }else{ ?>
    	    <img src="<?php echo ROOT_URL ?>img/mailtemplate-henshu_s.png" alt="メールテンプレート編集">
        <?php } ?>
    </div>
</div>

<?php if (!empty($appForm->validationErrors)) {?>
<div class="error-message">
    <div class="error-message-inner">
	    <p class="error-message-header">入力内容にエラーがあります。</p>
    </div>
</div>
<?php }?>
<table class="table table-bordered table-navy table-edit">
	<tr>
		<th width="150">名前<span class="err-msg">【必須】</span></th>
		<td class="sm-center">
		<?php echo $appForm->input('MailTemplate.name', array('type' => 'text', 'class' => 'form-control', 'maxlength' => 256)) ?>
		<?php
			$span = '';
			if (isset($appForm->validationErrors['MailTemplate']["name"])) {
				$span .= '<span class="err-msg">' . esHtml($appForm->validationErrors['MailTemplate']["name"]). '</span>';
			}
			echo $span;
		?>
		</td>
	</tr>
	<tr>
		<th>件名</th>
		<td class="sm-center">
		<?php echo $appForm->input('MailTemplate.subject', array('type' => 'text', 'class' => 'form-control', 'maxlength' => 256)) ?>
		<?php
			$span = '';
			if (isset($appForm->validationErrors['MailTemplate']["subject"])) {
				$span .= '<span class="err-msg">' . esHtml($appForm->validationErrors['MailTemplate']["subject"]). '</span>';
			}
			echo $span;
		?>
		</td>
	</tr>
	<tr>
		<th>本文</th>
		<td class="sm-center">
		<?php echo $appForm->input('MailTemplate.body', array('type' => 'textarea','rows' => "6", 'class' => 'form-control')) ?>
		<?php
			$span = '';
			if (isset($appForm->validationErrors['MailTemplate']["body"])) {
				$span .= '<span class="err-msg">' . esHtml($appForm->validationErrors['MailTemplate']["body"]). '</span>';
			}
			echo $span;
		?>
		</td>
	</tr>
</table>
<div class="row">
	<div class="center">
        <div class="xs-invisible">
	   		<?php if (empty($appForm->data['MailTemplate']['id'])){ ?>
	   			<input type="image" name="save" alt="登録" src="<?php echo ROOT_URL ?>img/bt_apply.png" title="登録" />
	        <?php }else{ ?>
	   			<input type="image" name="save" alt="更新" src="<?php echo ROOT_URL ?>img/bt_update.png" title="更新" />
	        <?php } ?>
            <input type="image" name="cancel" alt="キャンセル" src="<?php echo ROOT_URL ?>img/bt_cancel.png" title="キャンセル" />
        </div>
        <div class="xs-visible">
	   		<?php if (empty($appForm->data['MailTemplate']['id'])){ ?>
	   			<input type="image" name="save" alt="登録" src="<?php echo ROOT_URL ?>img/bt_apply_s.png" title="登録" />
	        <?php }else{ ?>
	   			<input type="image" name="save" alt="更新" src="<?php echo ROOT_URL ?>img/bt_update_s.png" title="更新" />
	        <?php } ?>
            <input type="image" name="cancel" alt="キャンセル" src="<?php echo ROOT_URL ?>img/bt_cancel_s.png" title="キャンセル" />
        </div>
	</div>
</div>
<div class="hide">
	<?php
		echo $appForm->input('MailTemplate.id', array('type' => 'hidden'));
		echo $appForm->input('MailTemplate.updated', array('type' => 'hidden'));
	?>
</div>
<?php
	echo $appForm->end();
?>
