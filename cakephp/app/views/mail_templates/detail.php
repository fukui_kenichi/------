<?php
	echo $appForm->create();

?>
<div class="row">
   	<div class="xs-invisible">
   		<?php $jouhou_img = ROOT_URL . 'img/mailtemplate-jouhou.png';?>
        <img src="<?php echo $jouhou_img; ?>" alt="メールテンプレート情報">
    </div>
   	<div class="xs-visible">
   		<?php $jouhou_img = ROOT_URL . 'img/mailtemplate-jouhou_s.png';?>
        <img src="<?php echo $jouhou_img; ?>"" alt="メールテンプレート情報">
    </div>
</div>
<table class="table table-bordered table-navy table-edit">
	<tr>
		<th width="150">名前</th>
		<td class="sm-center"><?php echo esHtml($appForm->data['MailTemplate']['name']) ?></td>
	</tr>
	<tr>
		<th>件名</th>
		<td class="sm-center"><?php echo esHtml($appForm->data['MailTemplate']['subject']) ?></td>
	</tr>
	<tr>
		<th>本文</th>
		<td class="sm-center"><?php echo nl2br(esHtml($appForm->data['MailTemplate']['body'])) ?></td>
	</tr>
</table>
<div class="row">
	<div class="center">
        <div class="xs-invisible">
            <a href="javascript:history.back();"><img class="help" alt="戻る" src="<?php echo ROOT_URL ?>img/bt_back.png" title="戻る"></a>
        </div>
        <div class="xs-visible">
            <a href="javascript:history.back();"><img class="help" alt="戻る" src="<?php echo ROOT_URL ?>img/bt_back_s.png" title="戻る"></a>
        </div>
	</div>
</div>
<?php
	echo $appForm->end();
?>
