<?php
	echo $appForm->create('FileEdit', array('name' => 'form','type' => 'file','id' => 'uploadForm', 'enctype' => 'multipart/form-data'));
?>

<div class="row">
   	<div class="xs-invisible">
   		<?php if (empty($appForm->data['User']['id'])){ ?>
    	    <img src="<?php echo ROOT_URL ?>img/user-toroku.png" alt="ユーザー登録">
        <?php }else{ ?>
	        <img src="<?php echo ROOT_URL ?>img/user-henshu.png" alt="ユーザー編集">
        <?php } ?>
    </div>
   	<div class="xs-visible">
   		<?php if (empty($appForm->data['User']['id'])){ ?>
	        <img src="<?php echo ROOT_URL ?>img/user-toroku_s.png" alt="ユーザー登録">
        <?php }else{ ?>
    	    <img src="<?php echo ROOT_URL ?>img/user-henshu_s.png" alt="ユーザー編集">
        <?php } ?>
    </div>
</div>

<?php if (!empty($appForm->validationErrors)) {?>
<div class="error-message">
    <div class="error-message-inner">
	    <p class="error-message-header">入力内容にエラーがあります。</p>
    </div>
</div>
<?php }?>
<table class="table table-bordered table-navy table-edit">
<?php
	foreach ($userMst as $key => $val) {
		$val = $val['UserMaster'];
		if ($val['field'] == 'password') {
			echo '<tr>';
			$th = "<th>" . "パスワード";
			$th .= '<span class="err-msg">【必須】</span>';
			$th .= '</th>';
			echo $th;
			echo '<td class="sm-center">';
			echo $appForm->input("User.password_01", array('type' => 'password', 'class' => 'form-control'));
			$span = '';
			if (isset($appForm->validationErrors['User']['password_01'])) {
				$span .= '<span class="err-msg">' . esHtml($appForm->validationErrors['User']['password_01']). '</span>';
			}
			echo $span;
			echo '</td>';
			echo '</tr>';
			echo '<tr>';
			$th = "<th>" . "パスワード確認";
			$th .= '<span class="err-msg">【必須】</span>';
			$th .= '</th>';
			echo $th;
			echo '<td class="sm-center">';
			echo $appForm->input("User.password_02", array('type' => 'password', 'class' => 'form-control'));
			$span = '';
			if (isset($appForm->validationErrors['User']['password_02'])) {
				$span .= '<span class="err-msg">' . esHtml($appForm->validationErrors['User']['password_02']). '</span>';
			}
			echo $span;
			echo '</td>';
			echo '</tr>';
		} else if ($val['field'] == "picture") {
			echo '<tr>';
			$th = "<th>" . esHtml($val['koumoku']);
			$th .= '</th>';
			echo $th;
			echo '<td class="sm-center">';
			echo $appForm->input("UserEdit.up_picture", array('type' => 'file'));
			$upload_url = $html->url('/users/upload');
			echo $appForm->button('アップロード',array('onClick' => "$('#uploadForm').ajaxSubmit({target: '#img',url: '{$upload_url}?koumoku=picture&model=User'}); return false;"));
			echo "<div id='img'>";
			if (!empty($appForm->data['User']["picture"])) {
				if (empty($appForm->data['User']["picture_up"])) {
					$fileNm = pathinfo($appForm->data['User']["picture"], PATHINFO_FILENAME);
					$upFile = ROOT_URL . 'picture/img/' . $fileNm . '-s.jpg';
				} else {
					$fileNm = pathinfo($appForm->data['User']["picture"], PATHINFO_FILENAME);
					$upFile = ROOT_URL . 'picture/temp/' . $fileNm . '-s.jpg';
				}
				echo "<img src='{$upFile}'>";
				echo $appForm->input("User.picture", array('type' => 'hidden'));
				echo "<br>";
				$delete_url = $html->url('/users/delete');
				echo "【<a href='#' onClick = \"$('#uploadForm').ajaxSubmit({target: '#img',url: '{$delete_url}?koumoku=picture&model=User'}); return false;\">画像を削除する</a>】";
			}
			echo "</div>";
			echo '</td>';
			echo '</tr>';
		} else if ($val['field'] == "auth_flag") {
			echo '<tr>';
			$th = "<th>" . esHtml($val['koumoku']);
			$th .= '</th>';
			echo $th;
			echo '<td class="sm-center">';
			// 改行を\nに統一
			$text = preg_replace(array('/\r\n/','/\r/','/\n/'), "\n", $val['select_ti'] );
			$listArr = explode("\n",$text);
			// 現状セットされている値を取得
			$ipData = isset($appForm->data['User']["{$val['field']}"])?$appForm->data['User']["{$val['field']}"]:null;
			// リストに追加
			$add_f = true;
			$selArr = array();
			foreach ($listArr as $k => $v) {
				// セットされている値がリストの中に含まれている場合は、追加なし
				if ($v == $ipData) {
					$add_f = false;
				}
				// セレクト値をセレクト名に変更
				$selArr[$v] = $v;
			}
			// セレクト値が変更になっていて、値が設定されていた場合
			if ($add_f && notEmpty($ipData)) {
				$selArr[$ipData] = $ipData;
			}
			$enable = true;
			if(isset($appForm->data['User']['id'])){
				if ($login['User']['id'] == $appForm->data['User']['id']) {
					if ($ipData == 'はい') {
						$enable = false;
					}
				}
			}
			if (!empty($ipData)) {
				if ($enable) {
					echo $appForm->input("User.{$val['field']}", array('type' => 'radio', "options" => $selArr ));
				} else {
					echo $appForm->input("User.{$val['field']}", array('type' => 'radio', "options" => $selArr, "disabled" => "disabled" ));
					echo $appForm->input("User.{$val['field']}", array('type' => 'hidden'));
				}
			} else {
				echo $appForm->input("User.{$val['field']}", array('type' => 'radio', "options" => $selArr , 'value' => $val['default_ti']));
			}
			$span = '';
			if (isset($appForm->validationErrors['User']["{$val['field']}"])) {
				$span .= '<br><span class="err-msg">' . esHtml($appForm->validationErrors['User']["{$val['field']}"]). '</span>';
			}
			echo $span;
			echo '</td>';
			echo '</tr>';
		} else if ($val['field'] == 'last_event_date') { // 最終イベント
			echo '<tr><th>'. esHtml($val['koumoku']). '</th><td class="sm-center">';
			if (!empty($appForm->data['User']['last_event_date'])) { //
				echo esHtml($appForm->data['User']['last_event_date']);
			}
			echo '</td></tr>';
		} else {
			echo '<tr>';
			$th = '<th width="150">' . esHtml($val['koumoku']);
			if ($val['required']) {
				$th .= '<span class="err-msg">【必須】</span>';
			}
			$th .= '</th>';
			echo $th;
			echo '<td class="sm-center">';
			switch ($val['type']) {
				case MASTER_TYPE_CHAR:		// 文字列（制限あり）
				case MASTER_TYPE_INTG:		// 数値
				case MASTER_TYPE_TELE:	// 電話型
					echo $appForm->input("User.{$val['field']}", array('type' => 'text', 'class' => 'form-control'));
					$span = '';
					if (isset($appForm->validationErrors['User']["{$val['field']}"])) {
						$span .= '<span class="err-msg">' . esHtml($appForm->validationErrors['User']["{$val['field']}"]). '</span>';
					}
					echo $span;
					break;
				case MASTER_TYPE_DTTM:		// 日付時刻型
					echo "日付：";
					echo $appForm->input("User.{$val['field']}_d", array('type' => 'text', 'class' => 'calendar'));
					echo "<br>時間：";
					echo $appForm->input("User.{$val['field']}_t", array('type' => 'text', 'class' => 'times'));
					$span = '';
					if (isset($appForm->validationErrors['User']["{$val['field']}_d"])) {
						$span .= '<br><span class="err-msg">' . esHtml($appForm->validationErrors['User']["{$val['field']}_d"]). '</span>';
					}
					if (isset($appForm->validationErrors['User']["{$val['field']}_t"])) {
						$span .= '<br><span class="err-msg">' . esHtml($appForm->validationErrors['User']["{$val['field']}_t"]). '</span>';
					}
					echo $span;
					break;
				case MASTER_TYPE_DATE:		// 日付型
					echo $appForm->input("User.{$val['field']}", array('type' => 'text', 'class' => 'calendar'));
					$span = '';
					if (isset($appForm->validationErrors['User']["{$val['field']}"])) {
						$span .= '<br><span class="err-msg">' . esHtml($appForm->validationErrors['User']["{$val['field']}"]). '</span>';
					}
					echo $span;
					break;
				case MASTER_TYPE_TIME:		// 時刻型
					echo $appForm->input("User.{$val['field']}", array('type' => 'text', 'class' => 'times'));
					$span = '';
					if (isset($appForm->validationErrors['User']["{$val['field']}"])) {
						$span .= '<br><span class="err-msg">' . esHtml($appForm->validationErrors['User']["{$val['field']}"]). '</span>';
					}
					echo $span;
					break;
				case MASTER_TYPE_TEXT:		// 文字列（制限なし）
					echo $appForm->input("User.{$val['field']}", array('type' => 'textarea', 'rows' => "3", 'class' => 'form-control'));
					break;
				case MASTER_TYPE_LIST:		// リスト型
					// 改行を\nに統一
					$text = preg_replace(array('/\r\n/','/\r/','/\n/'), "\n", $val['select_ti'] );
					$listArr = explode("\n",$text);
					// 現状セットされている値を取得
					$ipData = isset($appForm->data['User']["{$val['field']}"])?$appForm->data['User']["{$val['field']}"]:null;
					// リストに追加
					$add_f = true;
					$selArr = array();
					foreach ($listArr as $k => $v) {
						// セットされている値がリストの中に含まれている場合は、追加なし
						if ($v == $ipData) {
							$add_f = false;
						}
						// セレクト値をセレクト名に変更
						$selArr[$v] = $v;
					}
					// セレクト値が変更になっていて、値が設定されていた場合
					if ($add_f && notEmpty($ipData)) {
						$selArr[$ipData] = $ipData;
					}
					if (!empty($ipData)) {
						echo $appForm->input("User.{$val['field']}", array('type' => 'select', "options" => $selArr , 'class' => 'form-control', 'empty' => '---'));
					} else {
						echo $appForm->input("User.{$val['field']}", array('type' => 'select', "options" => $selArr , 'class' => 'form-control', 'empty' => '---', 'value' => $val['default_ti']));
					}
					$span = '';
					if (isset($appForm->validationErrors['User']["{$val['field']}"])) {
						$span .= '<span class="err-msg">' . esHtml($appForm->validationErrors['User']["{$val['field']}"]). '</span>';
					}
					echo $span;
					break;
				case MASTER_TYPE_RADI:		// ラジオ型
					// 改行を\nに統一
					$text = preg_replace(array('/\r\n/','/\r/','/\n/'), "\n", $val['select_ti'] );
					$listArr = explode("\n",$text);
					// 現状セットされている値を取得
					$ipData = isset($appForm->data['User']["{$val['field']}"])?$appForm->data['User']["{$val['field']}"]:null;
					// リストに追加
					$add_f = true;
					$selArr = array();
					foreach ($listArr as $k => $v) {
						// セットされている値がリストの中に含まれている場合は、追加なし
						if ($v == $ipData) {
							$add_f = false;
						}
						// セレクト値をセレクト名に変更
						$selArr[$v] = $v;
					}
					// セレクト値が変更になっていて、値が設定されていた場合
					if ($add_f && notEmpty($ipData)) {
						$selArr[$ipData] = $ipData;
					}
					if (!empty($ipData)) {
						echo $appForm->input("User.{$val['field']}", array('type' => 'radio', "options" => $selArr ));
					} else {
						echo $appForm->input("User.{$val['field']}", array('type' => 'radio', "options" => $selArr , 'value' => $val['default_ti']));
					}
					$span = '';
					if (isset($appForm->validationErrors['User']["{$val['field']}"])) {
						$span .= '<br><span class="err-msg">' . esHtml($appForm->validationErrors['User']["{$val['field']}"]). '</span>';
					}
					echo $span;
					break;
				case MASTER_TYPE_CHEK:		// チェック型
					// マスターの選択値
					$text = preg_replace(array('/\r\n/','/\r/','/\n/'), "\n", $val['select_ti'] ); // 改行を\nに統一
					$listArr = explode("\n",$text);

					// 現状セットされている値を取得
					$ipData = isset($appForm->data['User']["{$val['field']}"])?$appForm->data['User']["{$val['field']}"]:null;
					$ipData = preg_replace(array('/\r\n/','/\r/','/\n/'), "\n", $ipData ); // 改行を\nに統一
					$ipDataArr = explode("\n",$ipData);

					// フォームの生成
					foreach ((array)$listArr as $k => $v) {
						$flag = false;
						foreach ((array)$ipDataArr as $ip){
							if ($v == $ip){
								$flag = true;
							}
						}
						if ($flag == true){
							echo $appForm->input("User.{$val['field']}.$k", array('type' => 'checkbox', "label" => $v, "checked" => "checked", "value" => 1 ));
						}else{
							echo $appForm->input("User.{$val['field']}.$k", array('type' => 'checkbox', "label" => $v, "value" => 0 ));
						}
						echo '<br>';
					}
					$span = '';
					if (isset($appForm->validationErrors['User']["{$val['field']}"])) {
						$span .= '<br><span class="err-msg">' . esHtml($appForm->validationErrors['User']["{$val['field']}"]). '</span>';
					}
					echo $span;
					break;
				case MASTER_TYPE_FIMG:
					$img_i = $val['number'];
					echo $appForm->input("Image.up_image_{$img_i}. ", array('type' => 'file','multiple' => 'multiple'));
					echo "<button type='button' id='upbtn_image_{$img_i}'>アップロード</button>";
					echo "<div id='image_{$img_i}'>";
					if (!empty($appForm->data['Image'][$img_i])) {
						$imageId_fArr[$img_i][] = array();
						// 画像を表示する処理
						foreach ($appForm->data['Image'][$img_i] as $field => $v) {
							$fieldArr = explode("_",$field);
							$key = $fieldArr[1];
							if(!isset($imageId_fArr[$img_i][$key])) {
								$imageId_fArr[$img_i][$key] = 0;
							}
							// image_(no)だけを処理するように選別
							if ($fieldArr[0] == "imagename") {
								continue;
							}
							if (count($fieldArr) == 3) {
								continue;
							}
							if (count($fieldArr) == 2) {
								if (!empty($appForm->data['Image'][$img_i]["image_{$key}"])) {
									$upFile = ROOT_URL . 'upload/' . $appForm->data['Image'][$img_i]["image_{$key}_id"] . '-s.jpg';
								}
								echo "<img id='img_image_{$img_i}_{$key}' src='{$upFile}'>";
								echo "<br id='br1_image_{$img_i}_{$key}'>";
								$downloadUrl = $html->url("/uploads/download");
								$fileName = $appForm->data['Image'][$img_i]["imagename_{$key}"];
								$id = $appForm->data['Image'][$img_i]["image_{$key}_id"];
								echo "<a  id='a_image_{$img_i}_{$key}' href='{$downloadUrl}/{$id}'>{$fileName}</a>";
								echo "<br id='br2_image_{$img_i}_{$key}'>";
								echo "<span id='spandel_image_{$img_i}_{$key}'>";
								echo "【<a href='#' id='adel_image_{$img_i}_{$key}'>画像を削除する</a>】";
								echo "</span>";
								echo $appForm->input("Image.{$img_i}.imagename_{$key}", array('type' => 'hidden','value' => $appForm->data['Image'][$img_i]["imagename_{$key}"]));
								echo $appForm->input("Image.{$img_i}.image_{$key}", array('type' => 'hidden','value' => $appForm->data['Image'][$img_i]["image_{$key}"]));
								echo $appForm->input("Image.{$img_i}.image_{$key}_up", array('type' => 'hidden','value' => 1));
								echo $appForm->input("Image.{$img_i}.image_{$key}_id", array('type' => 'hidden','value' => $appForm->data['Image'][$img_i]["image_{$key}_id"]));
								echo "<br id='br3_image_{$img_i}_{$key}'>";
								$imageId_fArr[$img_i][$key] = 1;
							}
						}
						// 画像が削除されていて、IDだけが残っている場合のID隠し表示処理
						foreach ($imageId_fArr[$img_i] as $key => $f) {
							if ($f == 0) {
								echo $appForm->input("Image.{$img_i}.image_{$key}_id", array('type' => 'hidden','value' => $appForm->data['Image'][$img_i]["image_{$key}_id"]));
							}
						}
					}
					echo "</div>";
					echo "<div id='image_temp_{$img_i}'>";
					echo "</div>";
					break;
				case MASTER_TYPE_FILE:
					$notimg_i = $val['number'];
					echo $appForm->input("Notimage.up_notimage_{$notimg_i}. ", array('type' => 'file','multiple' => 'multiple'));
					echo "<button type='button' id='upbtn_notimage_{$notimg_i}'>アップロード</button>";
					echo "<div id='notimage_{$notimg_i}'>";
					if (!empty($appForm->data['Notimage'][$notimg_i])) {
						$notimageId_fArr[$notimg_i][] = array();
						// 画像を表示する処理
						foreach ($appForm->data['Notimage'][$notimg_i] as $field => $v) {
							$fieldArr = explode("_",$field);
							$key = $fieldArr[1];
							if(!isset($notimageId_fArr[$notimg_i][$key])) {
								$notimageId_fArr[$notimg_i][$key] = 0;
							}
							// image_(no)だけを処理するように選別
							if ($fieldArr[0] == "notimagename") {
								continue;
							}
							if (count($fieldArr) == 3) {
								continue;
							}
							if (count($fieldArr) == 2) {
								$downloadUrl = $html->url("/uploads/download");
								$fileName = $appForm->data['Notimage'][$notimg_i]["notimagename_{$key}"];
								$id = $appForm->data['Notimage'][$notimg_i]["notimage_{$key}_id"];
								echo "<a id='notimg_notimage_{$notimg_i}_{$key}' href='{$downloadUrl}/{$id}'>{$fileName}</a>";
								echo "<br id='br1_notimage_{$notimg_i}_{$key}'>";
								echo "<span id='spandel_notimage_{$notimg_i}_{$key}'>";
								echo "【<a href='#' id='adel_notimage_{$notimg_i}_{$key}'>ファイルを削除する</a>】";
								echo "</span>";
								echo $appForm->input("Notimage.{$notimg_i}.notimagename_{$key}", array('type' => 'hidden','value' => $appForm->data['Notimage'][$notimg_i]["notimagename_{$key}"]));
								echo $appForm->input("Notimage.{$notimg_i}.notimage_{$key}", array('type' => 'hidden','value' => $appForm->data['Notimage'][$notimg_i]["notimage_{$key}"]));
								echo $appForm->input("Notimage.{$notimg_i}.notimage_{$key}_up", array('type' => 'hidden','value' => 1));
								echo $appForm->input("Notimage.{$notimg_i}.notimage_{$key}_id", array('type' => 'hidden','value' => $appForm->data['Notimage'][$notimg_i]["notimage_{$key}_id"]));
								echo "<br id='br2_notimage_{$notimg_i}_{$key}'>";
								$notimageId_fArr[$notimg_i][$key] = 1;
							}
						}
						// 画像が削除されていて、IDだけが残っている場合のID隠し表示処理
						foreach ($notimageId_fArr[$notimg_i] as $key => $f) {
							if ($f == 0) {
								echo $appForm->input("Notimage.{$notimg_i}.notimage_{$key}_id", array('type' => 'hidden','value' => $appForm->data['Notimage'][$notimg_i]["notimage_{$key}_id"]));
							}
						}
					}
					echo "</div>";
					echo "<div id='notimage_temp_{$notimg_i}'>";
					echo "</div>";
					break;
			}
			echo '</td>';
			echo '</tr>';

		}
	}
?>

</table>
<div class="hide">
	<?php
		echo $appForm->input('User.id', array('type' => 'hidden'));
		echo $appForm->input('User.updated', array('type' => 'hidden'));
	?>
</div>

<div class="row">
	<div class="center">
        <div class="xs-invisible">
	   		<?php if (empty($appForm->data['User']['id'])){ ?>
	   			<input type="image" name="save" alt="登録" src="<?php echo ROOT_URL ?>img/bt_apply.png" title="登録" />
	        <?php }else{ ?>
	   			<input type="image" name="save" alt="更新" src="<?php echo ROOT_URL ?>img/bt_update.png" title="更新" />
	        <?php } ?>
            <input type="image" name="cancel" alt="キャンセル" src="<?php echo ROOT_URL ?>img/bt_cancel.png" title="キャンセル" />
        </div>
        <div class="xs-visible">
	   		<?php if (empty($appForm->data['User']['id'])){ ?>
	   			<input type="image" name="save" alt="登録" src="<?php echo ROOT_URL ?>img/bt_apply_s.png" title="登録" />
	        <?php }else{ ?>
	   			<input type="image" name="save" alt="更新" src="<?php echo ROOT_URL ?>img/bt_update_s.png" title="更新" />
	        <?php } ?>
            <input type="image" name="cancel" alt="キャンセル" src="<?php echo ROOT_URL ?>img/bt_cancel_s.png" title="キャンセル" />
        </div>
	</div>
</div>
<?php
	echo $appForm->end();
?>
<script type="text/javascript">
var img_up_url = "<?php echo $html->url('/uploads/img_upload'); ?>";
var notimg_up_url = "<?php echo $html->url('/uploads/notimg_upload'); ?>";
</script>
<script type="text/javascript" src="<?php echo ROOT_URL ?>js/multiple_file.js" ></script>

