<?php
	echo $appForm->create();
?>
<div class="row">
   	<div class="xs-invisible">
        <img src="<?php echo ROOT_URL ?>img/user-ichiran.png" alt="ユーザー一覧">
        <div class="title_right">
            <a href="<?php echo $html->url("/users/edit") ?>"><img src="<?php echo ROOT_URL ?>img/bt_add_user.png" alt="新規ユーザー登録"></a>
        </div>
    </div>
   	<div class="xs-visible">
        <img src="<?php echo ROOT_URL ?>img/user-ichiran_s.png" alt="ユーザー一覧">
        <div class="title_right">
            <a href="<?php echo $html->url("/users/edit") ?>"><img src="<?php echo ROOT_URL ?>img/bt_add_user_s.png" alt="新規ユーザー登録"></a>
        </div>
    </div>
</div>

<div class="smart-table full" id="userTable">
	<div class="smart-table-header">
		<div class="smart-table-header-inner">
			<table>
				<thead>
					<tr>
						<th><div style="width:100px;"></div></th>
	<?php
		foreach($userMst as $k => $v) {
			$v = $v['UserMaster'];
			if ($v['field'] == "name") {
				$value = esHtml($v['koumoku']);
				$str = ($sort == $v['field'])?(($direction == 'desc')?'<input type="button" class="btn btnSortUp" title="↑">':(($direction == 'asc')?'<input type="button" class="btn btnSortDown" title="↓">':"")):"";
				$th = "<th style='border-right-style: none;'><div style='width:101px;'></div></th>";
				echo $th;
				$th = "<th style='border-left-style: none;' class='sort'><div style='width:100px;'>{$value}{$str}</div></th>";
				echo $th;
			} else if ($v['field'] == "password") {
				continue;
			} else if ($v['field'] == "picture") {
				continue;
			} else {
				switch ($v['type']) {
					case MASTER_TYPE_CHAR:		// 文字列（制限あり）
					case MASTER_TYPE_INTG:		// 数値型
					case MASTER_TYPE_DTTM:		// 日付時刻型
					case MASTER_TYPE_DATE:		// 日付型
					case MASTER_TYPE_TIME:		// 時間型
					case MASTER_TYPE_LIST:		// リスト型
					case MASTER_TYPE_RADI:		// ラジオ型
					case MASTER_TYPE_TELE:	// 電話型
					case MASTER_TYPE_CHEK:		// チェック型
						$value = esHtml($v['koumoku']);
						$str = ($sort == $v['field'])?(($direction == 'desc')?'<input type="button" class="btn btnSortUp" title="↑">':(($direction == 'asc')?'<input type="button" class="btn btnSortDown" title="↓">':"")):"";
						$th = "<th class='sort'><div style='width:100px;'>{$value}{$str}</div></th>";
						echo $th;
						break;
					case MASTER_TYPE_TEXT:		// 文字列（制限なし）
						$value = esHtml($v['koumoku']);
						$str = ($sort == $v['field'])?(($direction == 'desc')?'<input type="button" class="btn btnSortUp" title="↑">':(($direction == 'asc')?'<input type="button" class="btn btnSortDown" title="↓">':"")):"";
						$th = "<th class='sort'><div style='width:100px;height:40px;overflow:hidden;'>{$value}{$str}</div></th>";
						echo $th;
						break;
				}
			}
		}
	?>
						<td class="space"></td>
					</tr>
				</thead>
			</table>
		</div>
	</div>
	<div class="smart-table-body">
		<div class="smart-table-body-inner">
<?php
if (!empty($list)) {
?>

			<table>
				<tbody>
	<?php
	foreach ($list as $key => $val) {
	?>
					<tr id='list_tr_<?php echo $val['User']['id'];?>' class="click">
						<td class="center middle first">
						<div style="width:100px;">
							<a href="#" onClick="location.href='<?php echo $html->url("/users/edit/{$val['User']['id']}") ?>'"><input type="button" class="btn btnEdit" title="編　集"></a>
							<?php if ($val['User']['id'] != $login['User']['id']){ ?>
								<a href="#" onClick="location.href='<?php echo $html->url("/users/del_conf/{$val['User']['id']}") ?>'"><input type="button" class="btn btnDel" title="削　除"></a>
							<?php } ?>
						</div>
						</td>
	<?php
		foreach($userMst as $k => $v) {
			$v = $v['UserMaster'];
			if ($v['field'] == "name") {
				$name = esHtml($val['User']["name"]);
				if (!empty($val['User']["picture"])) {
					$fileNm = pathinfo($val['User']["picture"], PATHINFO_FILENAME);
					$upFile = ROOT_URL . 'picture/img/' . esHtml($fileNm) . '-s.jpg';
				} else {
					$upFile = ROOT_URL . 'img/no-image.jpg';
				}
				$td = "<td style='background-color: #CCCCCC;'><div style='width:100px;'><div class='name-img'><div><img src='{$upFile}'></div></div></td>";
				echo $td;
				$td = "<td><div style='width:100px;'>{$name}</div></td>";
				echo $td;

			} else if ($v['field'] == "password") {
				continue;
			} else if ($v['field'] == "picture") {
				continue;
			} else {
				switch ($v['type']) {
					case MASTER_TYPE_CHAR:		// 文字列（制限あり）
					case MASTER_TYPE_INTG:		// 数値型
					case MASTER_TYPE_DTTM:		// 日付時刻型
					case MASTER_TYPE_DATE:		// 日付型
					case MASTER_TYPE_TIME:		// 時間型
					case MASTER_TYPE_LIST:		// リスト型
					case MASTER_TYPE_RADI:		// ラジオ型
					case MASTER_TYPE_TELE:	// 電話型
					case MASTER_TYPE_CHEK:		// チェック型
						$value = esHtml($val['User']["{$v['field']}"]);
						$td = "<td><div style='width:100px;'>{$value}</div></td>";
						echo $td;
						break;
					case MASTER_TYPE_TEXT:		// 文字列（制限なし）
						$value = nl2br(esHtml($val['User']["{$v['field']}"]));
						$td = "<td><div style='width:100px;'>{$value}</div></td>";
						echo $td;
						break;
				}
			}
		}
	?>
						<td class="space"></td>
					</tr>
	<?php
	}
	?>
				</tbody>
			</table>
<?php
}
?>
		</div>
	</div>
	<div class="smart-table-footer">
		<div class="smart-table-footer-inner">
			<table>
				<tfoot>
					<tr>
						<td colspan="4" colspan="middle">
							<?php echo $this->element('paginatorsmart') ?>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
<div class="row"></div>
<div class="row">
	<div class="center">
        <div class="xs-invisible">
            <input type="image" name="cancel" alt="戻る" src="<?php echo ROOT_URL ?>img/bt_back.png" title="戻る" />
        </div>
        <div class="xs-visible">
            <input type="image" name="cancel" alt="戻る" src="<?php echo ROOT_URL ?>img/bt_back_s.png" title="戻る" />
        </div>
	</div>
</div>

<?php
	echo $appForm->input('Sort.name', array('type' => 'hidden'));
	echo $appForm->end();
	$sortArr = array();
	$i = 1;
	foreach($userMst as $k => $v) {
		$v = $v['UserMaster'];
		if ($v['field'] == "name") {
			$sortArr[$i] = $v['field'];
			$i ++;
		} else if ($v['field'] == "password") {
			continue;
		} else if ($v['field'] == "picture") {
			continue;
		} else {
			switch ($v['type']) {
				case MASTER_TYPE_CHAR:		// 文字列（制限あり）
				case MASTER_TYPE_INTG:		// 数値型
				case MASTER_TYPE_DTTM:		// 日付時刻型
				case MASTER_TYPE_DATE:		// 日付型
				case MASTER_TYPE_TIME:		// 時間型
				case MASTER_TYPE_LIST:		// リスト型
				case MASTER_TYPE_RADI:		// ラジオ型
				case MASTER_TYPE_TELE:	// 電話型
					$sortArr[$i] = $v['field'];
					$i ++;
					break;
				case MASTER_TYPE_TEXT:		// 文字列（制限なし）
					$sortArr[$i] = $v['field'];
					$i ++;
					break;
			}
		}
	}
	?>
<script type="text/javascript">
$(function(){
	$("div.smart-table-body table tr td:not('.first')").click(function(){
		var idname = $(this).parent("tr").attr("id");
		var arr = idname.split('_');
		var userId = arr[2];

		window.location.href = '<?php echo $html->url("/users/detail/") ?>' + userId;
	});
	$(document).on("click touchstart", "th[id^='userTable_sort_col_']" ,function(ev){
		var arr = [];
		var i = 1;
		<?php
		foreach ($sortArr as $key => $val) {
		?>
			arr[i] = "<?php echo $val?>";
			i = i + 1;
		<?php
		}
		?>
		var idname = this.id;
		var array = idname.split('_');
		var colId = array[3];
		var page = '<?php
			if (empty($page)) {
				echo $paginator->current();
			} else {
				echo $page;
			}
		?>';
		var direction = '<?php
			if (empty($direction)) {
				echo 'asc';
			} else {
				if ($direction == 'asc') {
					echo 'desc';
				} else {
					echo 'asc';
				}
			}
		?>';
		var sort = '<?php
			if (empty($sort)) {
				echo '';
			} else {
				echo $sort;
			}
		?>';
		if (sort != arr[colId]) {
			direction = 'asc';
		}
		sort = arr[colId];
		window.location.href = '<?php echo $html->url("/users/index/page:") ?>' + page + '/sort:' + sort + '/direction:' + direction;
	});

});
</script>