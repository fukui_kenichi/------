<?php
	echo $appForm->create();
?>
<div class="row">
	<div class="xs-invisible">
		<img alt="メール予約配信情報" src="<?php echo ROOT_URL ?>img/mailyoyakuhaishin-joho.png">
		<div class="title_right">
			<a href="<?php echo $html->url("/mail_templates/edit/mail_id:{$appForm->data['Mail']['id']}")?>">
				<img alt="テンプレートに追加" src="<?php echo ROOT_URL ?>img/bt_add_template.png">
			</a>
		</div>
	</div>
	<div class="xs-visible">
		<img alt="メール予約配信情報" src="<?php echo ROOT_URL ?>img/mailyoyakuhaishin-joho_s.png">
		<div class="title_right">
			<a href="<?php echo $html->url("/mail_templates/edit/mail_id:{$appForm->data['Mail']['id']}")?>">
				<img alt="テンプレートに追加" src="<?php echo ROOT_URL ?>img/bt_add_template_s.png">
			</a>
		</div>
	</div>
</div>

<table class="table table-bordered table-navy table-edit">
<tbody>
<tr>
	<th width="150">配信日時</th>
	<td>
	<?php
		echo esHtml(dateFormat($appForm->data['Mail']['send_datetime'], 'Y/m/d H:i'));
		echo $appForm->input('Mail.send_datetime', array('type'=>'hidden'));
	?>
	</td>
</tr>
<tr>
	<th width="150">件名</th>
	<td>
	<?php
		echo esHtml($appForm->data['Mail']['subject']);
		echo $appForm->input('Mail.subject', array('type'=>'hidden'));
		?>
	</td>
</tr>
<tr>
	<th>本文</th>
	<td>
	<?php
		echo nl2br(esHtml($appForm->data['Mail']['body']));
		echo $appForm->input('Mail.body', array('type'=>'hidden'));
	?>
	</td>
</tr>
</tbody>
</table>
<br/><br/>
<?php
	$span = '';
	if (isset($appForm->validationErrors['Mail']['no_customer_flag'])) {
		$span .= '<span class="err-msg">' . esHtml($appForm->validationErrors['Mail']['no_customer_flag']). '</span>';
	}
	echo $span;
?>
<div class="smart-table full" id="customerTable">
	<div class="smart-table-header">
		<div class="smart-table-header-inner">
			<table>
				<thead>
					<tr>
						<th><div style="width:30px;">配信</div></th>
	<?php
	if (!empty($customerKojin)) {
		foreach($customerKojin as $key => $val) {
			if ($val['CustomerMaster']['field'] == 'name') {
				$value = esHtml($val['CustomerMaster']['koumoku']);
				$str = ($sort == $val['CustomerMaster']['field'])?(($direction == 'desc')?'<input type="button" class="btn btnSortUp" title="↑">':(($direction == 'asc')?'<input type="button" class="btn btnSortDown" title="↓">':"")):"";
				$th = "<th style='border-right-style: none;'><div style='width:101px;'></div></th>";
				echo $th;
				$th = "<th style='border-left-style: none;' class='sort'><div style='width:300px;'>{$value}{$str}</div></th>";
				echo $th;

			} else if ($val['CustomerMaster']['field'] == 'address_info') { // 住所
				// 郵便番号
				$str = ($sort == 'zip_code')?(($direction == 'desc')?'<input type="button" class="btn btnSortUp" title="↑">':(($direction == 'asc')?'<input type="button" class="btn btnSortDown" title="↓">':"")):"";
				$th = "<th class='sort'><div style='width:100px;'>郵便番号{$str}</div></th>";
				echo $th;
				// 住所
				$str = ($sort == 'address')?(($direction == 'desc')?'<input type="button" class="btn btnSortUp" title="↑">':(($direction == 'asc')?'<input type="button" class="btn btnSortDown" title="↓">':"")):"";
				$th = "<th class='sort'><div style='width:200px;'>住所{$str}</div></th>";
				echo $th;
			} else {
				switch ($val['CustomerMaster']['type']) {
					case MASTER_TYPE_CHAR:		// 文字列（制限あり）
					case MASTER_TYPE_INTG:		// 数値型
					case MASTER_TYPE_DTTM:		// 日付時刻型
					case MASTER_TYPE_DATE:		// 日付型
					case MASTER_TYPE_TIME:		// 時間型
					case MASTER_TYPE_LIST:		// リスト型
					case MASTER_TYPE_RADI:		// ラジオ型
					case MASTER_TYPE_TELE:		// 電話型
						$value = esHtml($val['CustomerMaster']['koumoku']);
						$str = ($sort == $val['CustomerMaster']['field'])?(($direction == 'desc')?'<input type="button" class="btn btnSortUp" title="↑">':(($direction == 'asc')?'<input type="button" class="btn btnSortDown" title="↓">':"")):"";
						$th = "<th class='sort'><div style='width:100px;'>{$value}{$str}</div></th>";
						echo $th;
						break;
					case MASTER_TYPE_TEXT:		// 文字列（制限なし）
						$value = esHtml($val['CustomerMaster']['koumoku']);
						$str = ($sort == $val['CustomerMaster']['field'])?(($direction == 'desc')?'<input type="button" class="btn btnSortUp" title="↑">':(($direction == 'asc')?'<input type="button" class="btn btnSortDown" title="↓">':"")):"";
						$th = "<th class='sort'><div style='width:100px;'>{$value}{$str}</div></th>";
						echo $th;
						break;
				}
			}
		}
	}
	?>
						<th class="space"></th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
	<div class="smart-table-body">
		<div class="smart-table-body-inner">
<?php
if (!empty($list)) {
	?>
			<table>
				<tbody>
	<?php
	foreach ($list as $key => $val) {
	?>
					<tr id='list_tr_<?php echo $val['Customer']['id'];?>' class="click">
						<td class="center middle first">
							<div style="width:30px;">
								<?php
									// メール配信データ
									echo $appForm->input("MailSender.{$val['Customer']['id']}.send_flag", array('type' => 'checkbox', 'disabled' => 'disabled'));
								?>
							</div>
						</td>
	<?php
		foreach($customerKojin as $key2 => $val2) {
			$val2 = $val2['CustomerMaster'];
			if ($val2['field'] == "name") { // 名前
				$name = esHtml($val['Customer']["name"]);
				if (!empty($val['Customer']["picture"])) {
					$fileNm = pathinfo($val['Customer']["picture"], PATHINFO_FILENAME);
					$upFile = ROOT_URL . 'picture/img/' . esHtml($fileNm) . '-s.jpg';
				} else {
					$upFile = ROOT_URL . 'img/no-image.jpg';
				}
				$td = "<td><div style='width:100px;'><div class='name-img'><div><img src='{$upFile}'></div></div></td>";
				echo $td;
				$td = "<td><div style='width:300px;'>{$name}</div></td>";
				echo $td;
			} else if ($val2['field'] == 'address_info') { // 住所
				// 郵便番号
				$value = esHtml($val['Customer']['zip_code']);
				$td = "<td><div style='width:100px;'>{$value}</div></td>";
				echo $td;
				// 住所
				$value = esHtml($val['Customer']['address']);
				$td = "<td><div style='width:200px;'>{$value}</div></td>";
				echo $td;
			} else {
				switch ($val2['type']) {
					case MASTER_TYPE_CHAR:		// 文字列（制限あり）
					case MASTER_TYPE_INTG:		// 数値型
					case MASTER_TYPE_DTTM:		// 日付時刻型
					case MASTER_TYPE_DATE:		// 日付型
					case MASTER_TYPE_TIME:		// 時間型
					case MASTER_TYPE_LIST:		// リスト型
					case MASTER_TYPE_RADI:		// ラジオ型
					case MASTER_TYPE_TELE:	// 電話型
						$value = esHtml($val['Customer']["{$val2['field']}"]);
						$td = "<td><div style='width:100px;'>{$value}</div></td>";
						echo $td;
						break;
					case MASTER_TYPE_TEXT:		// 文字列（制限なし）
						$value = nl2br(esHtml($val['Customer']["{$val2['field']}"]));
						$td = "<td><div style='width:100px;'>{$value}</div></td>";
						echo $td;
						break;
				}
			}
		}
	?>
						<td class="space"></td>
					</tr>
	<?php
	}
	?>
				</tbody>
			</table>
<?php
}
?>
		</div>
	</div>
	<div class="smart-table-footer">
		<div class="smart-table-footer-inner">
			<table>
				<tfoot>
					<tr>
						<td colspan="4" colspan="middle">
							<?php echo $this->element('paginator_mailedit') ?>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>


<div class="row">
	<div class="center">
		<div class="xs-invisible">
			<input type="image" name="cancel" alt="キャンセル" src="<?php echo ROOT_URL ?>img/bt_back.png" title="戻る" />
		</div>
		<div class="xs-visible">
			<input type="image" name="cancel" alt="キャンセル" src="<?php echo ROOT_URL ?>img/bt_back_s.png" title="戻る" />
		</div>
	</div>
</div>
<div class="hide">
	<?php
		echo $appForm->input('Mail.id', array('type' => 'hidden'));
		echo $appForm->input('Mail.updated', array('type' => 'hidden'));
	?>
	<!-- 変数保持 -->
	<input type="submit" name="smartTableSubmit" />
	<input type="hidden" name="sort" value="<?php echo $sort ?>" />
	<input type="hidden" name="direction" value="<?php echo $direction ?>" />
</div>
<?php
	echo $appForm->end();
?>

<?php
	// 顧客一覧ソート機能のための準備
	echo $appForm->input('Sort.name', array('type' => 'hidden'));
	echo $appForm->end();
	$sortArr = array();
	$i = 1;
	foreach($customerKojin as $k => $v) {
		$v = $v['CustomerMaster'];
		if ($v['field'] == 'picture') {
			continue;
		} elseif ($v['field'] == 'address_info') { // 住所
			// 郵便番号
			$sortArr[$i] = 'zip_code';
			$i++;
			// 住所
			$sortArr[$i] = 'address';
			$i++;
		} else {
			$sortArr[$i] = $v['field'];
			$i++;
		}
	}
?>

<script type="text/javascript">
$(function() {


});

// メール本文にメールテンプレートの内容を反映
$("select#mailTemplate").change(function(){
	$.ajax({
		type: "POST",
		url: "<?php echo $html->url('/mails/ajax_get_template'); ?>",
		async: false,
		dataType: 'json',
		data: {id : $("select#mailTemplate").val()},
		success : function(result) {
			// 本文にテンプレートを設定
			$("textarea#MailBody").text(result);
		}
	});

	return true;
});


//顧客テーブルソート機能
$(document).on("click", "th[id^='customerTable_sort_col_']" ,function(ev){
	var arr = [];
	var i = 1;
	<?php
	foreach ($sortArr as $key => $val) {
	?>
		arr[i] = "<?php echo $val?>";
		i = i + 1;
	<?php
	}
	?>
	var idname = this.id;
	var array = idname.split('_');
	var colId = array[3];
	var page = '<?php
		if (empty($page)) {
			echo $paginator->current();
		} else {
			echo $page;
		}
	?>';
	var direction = '<?php
		if (empty($direction)) {
			echo 'asc';
		} else {
			if ($direction == 'asc') {
				echo 'desc';
			} else {
				echo 'asc';
			}
		}
	?>';
	var sort = '<?php
		if (empty($sort)) {
			echo '';
		} else {
			echo $sort;
		}
	?>';
	if (sort != arr[colId]) {
		direction = 'asc';
	}
	sort = arr[colId];
//	window.location.href = '<?php echo $html->url('/mails/edit/page:') ?>' + page + '/sort:' + sort + '/direction:' + direction;

	// ソート情報を変数に設定
	$("input[name=sort]").val(sort);
	$("input[name=direction]").val(direction);
	$("input[name=smartTableSubmit]").trigger("click");
});

// リフレッシュボタンが押された場合
$("input[name=refresh]").click(function(ev) {
	// ソート情報を変数に設定
	$("input[name=sort]").val('');
	$("input[name=direction]").val('');
	$("input[name=smartTableSubmit]").trigger("click");

	return false;
});

// 最初のページへの移動ボタンが押された場合
$("input[name=first]").click(function(ev) {
	// ページは1ページ目
	$("input[name=page_num]").val(1);
	$("input[name=sort]").val('');
	$("input[name=direction]").val('');
	$("input[name=smartTableSubmit]").trigger("click");

	return false;
});

// 前ページへの移動ボタンが押された場合
$("input[name=prev]").click(function(ev) {
	// ページは前ページ
	$("input[name=page_num]").val(Number($("input[name=page_num]").val())-1);
	$("input[name=sort]").val('');
	$("input[name=direction]").val('');
	$("input[name=smartTableSubmit]").trigger("click");

	return false;
});

// 次ページへの移動ボタンが押された場合
$("input[name=next]").click(function(ev) {
	// ページは次ページ
	$("input[name=page_num]").val(Number($("input[name=page_num]").val())+1);
	$("input[name=sort]").val('');
	$("input[name=direction]").val('');
	$("input[name=smartTableSubmit]").trigger("click");

	return false;
});

// 最終ページへの移動ボタンが押された場合
$("input[name=last]").click(function(ev) {
	// ページは最終ページ
	$("input[name=page_num]").val(<?php echo $paginator->counter(array('format' => '%pages%'))?>);
	$("input[name=sort]").val('');
	$("input[name=direction]").val('');
	$("input[name=smartTableSubmit]").trigger("click");

	return false;
});

$("div.smart-table-body table tr td:not('.first')").click(function(){
	var idname = $(this).parent("tr").attr("id");
	var arr = idname.split('_');
	var customerId = arr[2];

	window.location.href = '<?php echo $html->url("/customers/detail/") ?>' + customerId;
});

</script>