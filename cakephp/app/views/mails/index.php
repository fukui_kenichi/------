<?php
	echo $appForm->create();
	// メール配信型
	$SendMailTypeDef = new SendMailTypeDef();
	$sendMailTypeArr = $SendMailTypeDef->getArr();
?>
<div class="row">
	<div class="xs-invisible">
		<img alt="メール予約配信一覧" src="<?php echo ROOT_URL ?>img/mailyoyakuhaishin-ichiran.png">
		<div class="title_right">
			<a href="<?php echo $html->url("/mail_templates/index/search:clear")?>"><img alt="メールテンプレート管理" src="<?php echo ROOT_URL ?>img/bt_mail_template.png"></a>
		</div>
	</div>
	<div class="xs-visible">
		<img alt="メール予約配信一覧" src="<?php echo ROOT_URL ?>img/mailyoyakuhaishin-ichiran_s.png">
		<div class="title_right">
			<a href="<?php echo $html->url("/mail_templates/index/search:clear")?>"><img alt="メールテンプレート管理" src="<?php echo ROOT_URL ?>img/bt_mail_template_s.png"></a>
		</div>
	</div>
</div>

<?php
if (!empty($list)) {
?>
<div class="smart-table full" id="mailTable">
	<div class="smart-table-header">
		<div class="smart-table-header-inner">
			<table>
				<thead>
					<tr>
						<th><div style="width:100px;"></div></th>
						<th class="sort">
							<div style="width:150px;">
							配信日時
							<?php
								if ($sort == 'send_datetime') {
									$str = ($direction == 'desc')?'<input type="button" class="btn btnSortUp" title="↑">':(($direction == 'asc')?'<input type="button" class="btn btnSortDown" title="↓">':"");
									echo $str;
								}
							?>
							</div>
						</th>
						<th class="sort">
							<div style="width:70px;">
							状態
							<?php
								if ($sort == 'status') {
									$str = ($direction == 'desc')?'<input type="button" class="btn btnSortUp" title="↑">':(($direction == 'asc')?'<input type="button" class="btn btnSortDown" title="↓">':"");
									echo $str;
								}
							?>
							</div>
						</th>
						<th class="sort">
							<div style="width:500px;">
							件名
							<?php
								if ($sort == 'subject') {
									$str = ($direction == 'desc')?'<input type="button" class="btn btnSortUp" title="↑">':(($direction == 'asc')?'<input type="button" class="btn btnSortDown" title="↓">':"");
									echo $str;
								}
							?>
							</div>
						</th>
						<th class="space"></th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
	<div class="smart-table-body">
		<div class="smart-table-body-inner">
			<table>
				<tbody>
					<?php foreach ($list as $key => $val) {
							// 行のクラスを設定
							$trClass = 'click';
							if ($val['Mail']['status'] != 0) $trClass .= ' send_end';
					?>
					<tr  id="list_tr_<?php echo $val['Mail']['id']?>" class="<?php echo $trClass?>">
						<td class="center middle first">
							<div style="width:100px;">
								<?php if ($val['Mail']['status'] == 0) { // 配信待ちのメールのみ編集削除可 ?>
									<a href="#" onClick="location.href='<?php echo $html->url("/mails/edit/{$val['Mail']['id']}") ?>'"><input type="button" class="btn btnEdit" title="編　集"></a>
									<a href="#" onClick="location.href='<?php echo $html->url("/mails/del_conf/{$val['Mail']['id']}") ?>'"><input type="button" class="btn btnDel" title="削　除"></a>
								<?php } ?>
							</div>
						</td>
						<td><div style="width:150px;"><?php echo esHtml(dateFormat($val['Mail']['send_datetime'], 'Y/m/d H:i'))?></div></td>
						<td>
							<div style="width:70px;">
							<?php
							if (isset($sendMailTypeArr[$val['Mail']['status']])) {
									echo $sendMailTypeArr[$val['Mail']['status']];
								}
							?>
							</div>
						</td>
						<td><div style="width:500px;"><?php echo esHtml($val['Mail']['subject'])?></div></td>
						<td class="space"></td>
					</tr>
					<?php }?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="smart-table-footer">
		<div class="smart-table-footer-inner">
			<table>
				<tfoot>
					<tr>
						<td colspan="4" colspan="middle">
							<?php echo $this->element('paginatorsmart') ?>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
<?php
} else {
	echo '<span class="message">データがありません</span>';
}
?>
<div class="row"></div>
<div class="row">
	<div class="center">
        <div class="xs-invisible">
            <input type="image" name="cancel" alt="戻る" src="<?php echo ROOT_URL ?>img/bt_back.png" title="戻る" />
        </div>
        <div class="xs-visible">
            <input type="image" name="cancel" alt="戻る" src="<?php echo ROOT_URL ?>img/bt_back_s.png" title="戻る" />
        </div>
	</div>
</div>

<?php
	echo $appForm->end();
	$sortArr = array(
		1 => 'send_datetime',
		2 => 'subject',
		3 => 'status',
	);
?>
<script type="text/javascript">
$(function() {
	// 一覧表示とは別スレッドで配信待ち、配信中のメールを送信する
	$.ajax({
		type: "POST",
		url: "<?php echo $html->url('/cron/send_mail') ?>",
		async: true,
		data: {},
		success : function(result)
		{
		}
	});
});

// 一覧の行を押されたら情報画面にジャンプ
$("div.smart-table-body table tr td:not('.first')").click(function(){
	var idname = $(this).parent("tr").attr("id");
	var arr = idname.split('_');
	var mailId = arr[2];

	window.location.href = '<?php echo $html->url("/mails/detail/") ?>' + mailId;
});

// 一覧のソート情報を含めたのURLを作成する
$(document).on("click touchstart", "th[id^='mailTable_sort_col_']" ,function(ev){
	var arr = [];
	var i = 1;
	<?php
	foreach ($sortArr as $key => $val) {
	?>
		arr[i] = "<?php echo $val?>";
		i = i + 1;
	<?php
	}
	?>
	var idname = this.id;
	var array = idname.split('_');
	var colId = array[3];
	var page = '<?php
		if (empty($page)) {
			echo $paginator->current();
		} else {
			echo $page;
		}
	?>';
	var direction = '<?php
		if (empty($direction)) {
			echo 'asc';
		} else {
			if ($direction == 'asc') {
				echo 'desc';
			} else {
				echo 'asc';
			}
		}
	?>';
	var sort = '<?php
		if (empty($sort)) {
			echo '';
		} else {
			echo $sort;
		}
	?>';
	if (sort != arr[colId]) {
		direction = 'asc';
	}
	sort = arr[colId];
	window.location.href = '<?php echo $html->url("/mails/index/page:") ?>' + page + '/sort:' + sort + '/direction:' + direction;
});
</script>