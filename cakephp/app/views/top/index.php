<?php
	echo $appForm->create();
?>
<div>
	<div class="xs-invisible">
		<div class="line1">
			<div class="side1">
				<img src="<?php echo ROOT_URL ?>img/logo_top.png" id="top_logo">
			</div>
			<div class="nav-menu">
			<a href="<?php echo $html->url('/customers/index/search:clear') ?>"><img src="<?php echo ROOT_URL ?>img/bt_top_customer.png" id="menu_1"></a>
			</div>
			<div class="nav-menu">
			<a href="<?php echo $html->url('/customers/index_busi/search:clear') ?>"><img src="<?php echo ROOT_URL ?>img/bt_top_schedule.png" id="menu_2"></a>
			</div>
		</div>
		<div class="line2">
			<div class="side2" id="side_2">
				<span class="side-bottom">
					<a href="<?php echo $html->url('/calender/index'); ?>"><img src="<?php echo ROOT_URL ?>img/bt_calender.png" id="menu_5"></a><br>
					<a href="<?php echo $html->url('/maps/before_index'); ?>"><img src="<?php echo ROOT_URL ?>img/bt_map.png" id="menu_6"></a><br>
					<?php if (!empty($alertCount)){ ?>
						<a href="#myModal_info" data-toggle="modal">
						<img src="<?php echo ROOT_URL ?>img/bt_arart.png" id="menu_7"><br>
						<p class="infobox" id="info">
						<?php echo $alertCount; ?>件のお知らせがあります
						</p>
						</a>
					<?php }else{ ?>
						<br><br><br><br>
					<?php } ?>
					<br>
					<img src="<?php echo ROOT_URL ?>img/logo_right_top.png" id="bottom_logo">
				</span>
			</div>
			<div class="nav-menu">
			<a href="<?php echo $html->url('/customers/index_mail/search:clear') ?>"><img src="<?php echo ROOT_URL ?>img/bt_top_mail.png" id="menu_3"></a>
			</div>
			<div class="nav-menu">
			<a href="#myModal_preference" data-toggle="modal"><img src="<?php echo ROOT_URL ?>img/bt_top_control.png" id="menu_4"></a>
			</div>
		</div>
	</div>
	<div class="xs-visible">
		<div class="header-top-s">
			<img src="<?php echo ROOT_URL ?>img/logo_phone_top.png">
		</div>
		<?php if (!empty($alertCount)){ ?>
			<div class="info-s">
				<a href="#myModal_info" data-toggle="modal">
				<img src="<?php echo ROOT_URL ?>img/bt_arart.png" id="menu_s7"><br>
				<p class="infobox_s" id="info_s">
				<?php echo $alertCount; ?>件のお知らせがあります
				</p>
				</a>
			</div>
		<?php } ?>
		<div class="nav-menu-s">
		<a href="<?php echo $html->url('/customers/index/search:clear') ?>"><img src="<?php echo ROOT_URL ?>img/bg_customer_top.png" id="menu_s1"></a>
		</div>
		<div class="nav-menu-s">
		<a href="<?php echo $html->url('/customers/index_busi/search:clear') ?>"><img src="<?php echo ROOT_URL ?>img/bg_schedule_top.png" id="menu_s2"></a>
		</div>
		<div class="nav-menu-s">
		<a href="<?php echo $html->url('/customers/index_mail/search:clear') ?>"><img src="<?php echo ROOT_URL ?>img/bg_mail_top.png" id="menu_s3"></a>
		</div>
		<div class="nav-menu-s">
		<a href="#myModal_preference" data-toggle="modal"><img src="<?php echo ROOT_URL ?>img/bg_control_top.png" id="menu_s4"></a>
		</div>
		<div class="nav-menu-s">
		<a href="<?php echo $html->url('/calender/index'); ?>"><img src="<?php echo ROOT_URL ?>img/bg_calender.png" id="menu_s5"></a>
		</div>
		<div class="nav-menu-s">
		<a href="<?php echo $html->url('/maps/index'); ?>"><img src="<?php echo ROOT_URL ?>img/bg_map.png" id="menu_s6"></a>
		</div>
	</div>
</div>
<div class="container">
</div><!-- container -->

<div class="hide">
	<input type="submit" id="send_mail" name="send_mail">
<?php
	echo $appForm->input('Customer.id', array('type' => 'hidden'));
?>
</div>
<?php
	echo $appForm->end();
?>