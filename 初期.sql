-- phpMyAdmin SQL Dump
-- version 3.3.9.2
-- http://www.phpmyadmin.net
--
-- ホスト: localhost
-- 生成時間: 2015 年 3 月 26 日 17:05
-- サーバのバージョン: 5.1.60
-- PHP のバージョン: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- データベース: `fukui_right_watson`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `busies`
--

CREATE TABLE IF NOT EXISTS `busies` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自動採番される識別番号',
  `customer_id` int(11) DEFAULT NULL,
  `busi_date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `memo` text,
  `picture` varchar(256) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- テーブルのデータをダンプしています `busies`
--


-- --------------------------------------------------------

--
-- テーブルの構造 `busi_kojins`
--

CREATE TABLE IF NOT EXISTS `busi_kojins` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自動採番される識別番号',
  `busi_master_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `view_flag` int(11) DEFAULT NULL COMMENT '0=表示しない、1=表示する',
  `number` int(11) DEFAULT NULL,
  `disabled` int(11) DEFAULT NULL COMMENT '0=変更可能1=変更不可',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- テーブルのデータをダンプしています `busi_kojins`
--

INSERT INTO `busi_kojins` (`id`, `busi_master_id`, `user_id`, `view_flag`, `number`, `disabled`) VALUES
(1, 1, 1, 1, 1, 1),
(2, 2, 1, 1, 2, 0),
(3, 5, 1, 1, 3, 0),
(4, 1, 2, 1, 1, 1),
(5, 2, 2, 1, 2, 0),
(6, 5, 2, 1, 3, 0);


-- --------------------------------------------------------

--
-- テーブルの構造 `busi_masters`
--

CREATE TABLE IF NOT EXISTS `busi_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自動採番される識別番号',
  `koumoku` varchar(256) DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT '1=文字列(制限あり)、2=文字列(制限なし)、3=数値型、4=日付時刻型、\r\n5=日付型、6=時間型、7=リスト型、8=チェック型、9=ファイル型(写真)、\r\n10=ファイル型(その他)',
  `default_ti` varchar(256) DEFAULT NULL,
  `select_ti` text,
  `required` int(11) DEFAULT NULL COMMENT '0=いいえ、1=はい',
  `number` int(11) DEFAULT NULL,
  `default_flag` int(11) DEFAULT NULL COMMENT '0=初期項目、1=追加項目',
  `field` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- テーブルのデータをダンプしています `busi_masters`
--

INSERT INTO `busi_masters` (`id`, `koumoku`, `type`, `default_ti`, `select_ti`, `required`, `number`, `default_flag`, `field`) VALUES
(1, '日付', 5, NULL, NULL, 1, 1, 0, 'busi_date'),
(2, '開始時間', 6, NULL, NULL, 0, 2, 0, 'start_time'),
(3, '終了時間', 6, NULL, NULL, 0, 3, 0, 'end_time'),
(4, '担当者', 7, NULL, NULL, 0, 5, 0, 'user_id'),
(5, 'メモ', 2, NULL, NULL, 0, 4, 0, 'memo'),
(6, '写真', 9, NULL, NULL, 0, 6, 0, 'picture');

-- --------------------------------------------------------

--
-- テーブルの構造 `busi_refers`
--

CREATE TABLE IF NOT EXISTS `busi_refers` (
  `busi_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `refer` text,
  `refer_full` text,
  PRIMARY KEY (`busi_id`),
  FULLTEXT KEY `refer_full` (`refer_full`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- テーブルのデータをダンプしています `busi_refers`
--


-- --------------------------------------------------------

--
-- テーブルの構造 `calenders`
--

CREATE TABLE IF NOT EXISTS `calenders` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(1000) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `is_allday_event` smallint(6) NOT NULL,
  `color` varchar(200) DEFAULT NULL,
  `parent_table` varchar(200) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `eid` varchar(200) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index1` (`user_id`,`parent_table`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- テーブルのデータをダンプしています `calenders`
--


-- --------------------------------------------------------

--
-- テーブルの構造 `cti_counts`
--

CREATE TABLE IF NOT EXISTS `cti_counts` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自動採番される識別番号',
  `customer_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- テーブルのデータをダンプしています `cti_counts`
--


-- --------------------------------------------------------

--
-- テーブルの構造 `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自動採番される識別番号',
  `name` varchar(256) NOT NULL,
  `tel` varchar(256) DEFAULT NULL,
  `mail` varchar(256) DEFAULT NULL,
  `zip_code` varchar(8) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `latitude` varchar(256) DEFAULT NULL COMMENT '緯度',
  `longitude` varchar(256) DEFAULT NULL COMMENT '経度',
  `memo` text,
  `picture` varchar(256) DEFAULT NULL,
  `last_event_date` date DEFAULT NULL,
  `del_flag` tinyint(4) NOT NULL COMMENT '0=初期値、1=削除',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- テーブルのデータをダンプしています `customers`
--


-- --------------------------------------------------------

--
-- テーブルの構造 `customer_kojins`
--

CREATE TABLE IF NOT EXISTS `customer_kojins` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自動採番される識別番号',
  `customer_master_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `view_flag` int(11) DEFAULT NULL COMMENT '0=表示しない、1=表示する',
  `number` int(11) DEFAULT NULL,
  `disabled` int(11) DEFAULT NULL COMMENT '0=変更可能1=変更不可',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- テーブルのデータをダンプしています `customer_kojins`
--

INSERT INTO `customer_kojins` (`id`, `customer_master_id`, `user_id`, `view_flag`, `number`, `disabled`) VALUES
(1, 1, 1, 1, 1, 1),
(2, 7, 1, 1, 2, 0),
(3, 1, 2, 1, 1, 1),
(4, 7, 2, 1, 2, 0);


-- --------------------------------------------------------

--
-- テーブルの構造 `customer_masters`
--

CREATE TABLE IF NOT EXISTS `customer_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自動採番される識別番号',
  `koumoku` varchar(256) DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT '1=文字列(制限あり)、2=文字列(制限なし)、3=数値型、4=日付時刻型、\r\n5=日付型、6=時間型、7=リスト型、8=チェック型、9=ファイル型(写真)、\r\n10=ファイル型(その他)',
  `default_ti` varchar(256) DEFAULT NULL,
  `select_ti` text,
  `required` int(11) DEFAULT NULL COMMENT '0=いいえ、1=はい',
  `number` int(11) DEFAULT NULL,
  `default_flag` int(11) DEFAULT NULL COMMENT '0=初期項目、1=追加項目',
  `field` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- テーブルのデータをダンプしています `customer_masters`
--

INSERT INTO `customer_masters` (`id`, `koumoku`, `type`, `default_ti`, `select_ti`, `required`, `number`, `default_flag`, `field`) VALUES
(1, '名前', 1, NULL, NULL, 1, 2, 0, 'name'),
(2, '電話番号', 1, NULL, NULL, 0, 4, 0, 'tel'),
(3, 'メールアドレス', 1, NULL, NULL, 0, 5, 0, 'mail'),
(4, '住所', NULL, NULL, NULL, 0, 3, 0, 'address_info'),
(5, 'メモ', 2, NULL, NULL, 0, 6, 0, 'memo'),
(6, '写真', 9, NULL, NULL, 0, 1, 0, 'picture'),
(7, '最終イベント', 5, NULL, NULL, 0, 7, 0, 'last_event_date');

-- --------------------------------------------------------

--
-- テーブルの構造 `customer_refers`
--

CREATE TABLE IF NOT EXISTS `customer_refers` (
  `customer_id` int(11) NOT NULL,
  `refer` text,
  PRIMARY KEY (`customer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- テーブルのデータをダンプしています `customer_refers`
--


-- --------------------------------------------------------

--
-- テーブルの構造 `favorites`
--

CREATE TABLE IF NOT EXISTS `favorites` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自動採番される識別番号',
  `name` varchar(256) DEFAULT NULL,
  `keyword` text,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- テーブルのデータをダンプしています `favorites`
--


-- --------------------------------------------------------

--
-- テーブルの構造 `mails`
--

CREATE TABLE IF NOT EXISTS `mails` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自動採番される識別番号',
  `send_datetime` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '0=配信待ち、1=配信中、2=配信中',
  `subject` varchar(256) DEFAULT NULL,
  `body` text,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- テーブルのデータをダンプしています `mails`
--


-- --------------------------------------------------------

--
-- テーブルの構造 `mail_counts`
--

CREATE TABLE IF NOT EXISTS `mail_counts` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自動採番される識別番号',
  `customer_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- テーブルのデータをダンプしています `mail_counts`
--


-- --------------------------------------------------------

--
-- テーブルの構造 `mail_senders`
--

CREATE TABLE IF NOT EXISTS `mail_senders` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自動採番される識別番号',
  `mail_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `send_flag` int(11) DEFAULT NULL COMMENT '0=配信しない、1=配信する',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- テーブルのデータをダンプしています `mail_senders`
--


-- --------------------------------------------------------

--
-- テーブルの構造 `mail_templates`
--

CREATE TABLE IF NOT EXISTS `mail_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自動採番される識別番号',
  `name` varchar(256) DEFAULT NULL,
  `subject` varchar(256) DEFAULT NULL,
  `body` text,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- テーブルのデータをダンプしています `mail_templates`
--


-- --------------------------------------------------------

--
-- テーブルの構造 `tokens`
--

CREATE TABLE IF NOT EXISTS `tokens` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(1000) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- テーブルのデータをダンプしています `calenders`
--


-- --------------------------------------------------------

--
-- テーブルの構造 `uploads`
--

CREATE TABLE IF NOT EXISTS `uploads` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自動採番される識別番号',
  `extension` varchar(256) DEFAULT NULL,
  `file_name` varchar(256) DEFAULT NULL,
  `up_file_name` varchar(256) DEFAULT NULL,
  `parent_table` varchar(256) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `parent_field` varchar(256) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- テーブルのデータをダンプしています `uploads`
--


-- --------------------------------------------------------

--
-- テーブルの構造 `users`
--


CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自動採番される識別番号',
  `account` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` varchar(256) NOT NULL,
  `name` varchar(256) NOT NULL,
  `tel` varchar(256) DEFAULT NULL,
  `mail` varchar(256) DEFAULT NULL,
  `memo` text,
  `auth_flag` varchar(256) NOT NULL COMMENT '0=いいえ、1=はい',
  `picture` varchar(256) DEFAULT NULL,
  `mente_flag` tinyint(4) NOT NULL DEFAULT '0',
  `last_event_date` date DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `del_flag` tinyint(4) NOT NULL COMMENT '0=初期値、1=削除',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- テーブルのデータをダンプしています `users`
--

INSERT INTO `users` (`id`, `account`, `password`, `name`, `tel`, `mail`, `memo`, `auth_flag`, `picture`, `mente_flag`, `last_event_date`, `url`, `del_flag`, `created`, `updated`) VALUES
(1, 'super', 'fk15u', 'メンテナンス', NULL, NULL, NULL, 'はい', NULL, 1, NULL, NULL, 0, '2015-03-20 11:46:37', '2015-03-20 11:58:44');
INSERT INTO `users` (`id`, `account`, `password`, `name`, `tel`, `mail`, `memo`, `auth_flag`, `picture`, `mente_flag`, `last_event_date`, `url`, `del_flag`, `created`, `updated`) VALUES
(2, 'admin', 'admin', '管理者', NULL, NULL, NULL, 'はい', NULL, 0, NULL, NULL, 0, '2015-03-20 11:46:37', '2015-03-20 11:58:44');

-- --------------------------------------------------------

--
-- テーブルの構造 `user_masters`
--

CREATE TABLE IF NOT EXISTS `user_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自動採番される識別番号',
  `koumoku` varchar(256) DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT '1=文字列(制限あり)、2=文字列(制限なし)、3=数値型、4=日付時刻型、\r\n5=日付型、6=時間型、7=リスト型、8=チェック型、9=ファイル型(写真)、\r\n10=ファイル型(その他)',
  `default_ti` varchar(256) DEFAULT NULL,
  `select_ti` text,
  `required` int(11) DEFAULT NULL COMMENT '0=いいえ、1=はい',
  `number` int(11) DEFAULT NULL,
  `default_flag` int(11) DEFAULT NULL COMMENT '0=初期項目、1=追加項目',
  `field` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- テーブルのデータをダンプしています `user_masters`
--

INSERT INTO `user_masters` (`id`, `koumoku`, `type`, `default_ti`, `select_ti`, `required`, `number`, `default_flag`, `field`) VALUES
(1, 'ID', 1, NULL, NULL, 1, 1, 0, 'account'),
(2, 'パスワード', 1, NULL, NULL, 1, 2, 0, 'password'),
(3, '名前', 1, NULL, NULL, 1, 3, 0, 'name'),
(4, '電話番号', 1, NULL, NULL, 0, 4, 0, 'tel'),
(5, 'メールアドレス', 1, NULL, NULL, 0, 5, 0, 'mail'),
(6, 'メモ', 2, NULL, NULL, 0, 6, 0, 'memo'),
(7, 'システム管理者', 8, 'いいえ', 'いいえ\r\nはい', 1, 7, 0, 'auth_flag'),
(8, '写真', 9, NULL, NULL, 0, 8, 0, 'picture'),
(9, '最終作業', 5, NULL, NULL, 0, 9, 0, 'last_event_date'),
(10, 'カレンダーURL', 1, NULL, NULL, 0, 10, 0, 'url');

-- --------------------------------------------------------

--
-- テーブルの構造 `user_refers`
--

CREATE TABLE IF NOT EXISTS `user_refers` (
  `user_id` int(11) NOT NULL,
  `refer` text,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- テーブルのデータをダンプしています `user_refers`
--

INSERT INTO `user_refers` (`user_id`, `refer`) VALUES
(1, 'ID=admin 名前=管理者 電話番号= メールアドレス= メモ=');
