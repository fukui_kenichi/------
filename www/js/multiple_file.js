$(function(){
	var no = 1;
	var g_koumoku = null;
	var g_koumokunotimg = null;

	// 画像を削除する。
	$(document).on("click", "a[id^='adel_image_']" ,function(ev){
		var idname = ev.target.id;
		var arr = idname.split('_');
		var koumoku = arr[2];
		var key = arr[3];

		var delimgid = "img_image_" + koumoku + "_" + key;
		$("#" + delimgid).remove();
		var delbr1 = "br1_image_" + koumoku + "_" + key;
		$("#" + delbr1).remove();
		var delaid = "a_image_" + koumoku + "_" + key;
		$("#" + delaid).remove();
		var delbr2 = "br2_image_" + koumoku + "_" + key;
		$("#" + delbr2).remove();
		var delspanid = "spandel_image_" + koumoku + "_" + key;
		$("#" + delspanid).remove();
		var delfilenameid = "Image" + koumoku + "Imagename" + key;
		$("#" + delfilenameid).remove();
		var delfileid = "Image" + koumoku + "Image" + key;
		$("#" + delfileid).remove();
		var delfileupid = "Image" + koumoku + "Image" + key + "Up";
		$("#" + delfileupid).remove();
		var delbr3 = "br3_image_" + koumoku + "_" + key;
		$("#" + delbr3).remove();

	});
	// アップロードボタンクリック
	$(document).on("click", "button[id^='upbtn_image_']" ,function(ev){
		$("button[id^='upbtn_image_']").each( function() {
			var id = this.id;
			$('#'+id).attr('disabled', true);
		});
		var idname = this.id;
		var idname = ev.target.id;
		var arr = idname.split('_');
		var koumoku = arr[2];
		g_koumoku = koumoku;
		var url = img_up_url + "?koumoku=" + koumoku + "&model=Image";
		//alert(url);
		var target = '#image_temp_' + koumoku;

		$('#uploadForm').ajaxSubmit({
			target: target, // サーバの戻りを出力する場所を指定
			url: url, // URL
			success: ImagehogeResponse     // サーバからの応答時に呼び出す関数
		});
		return false;
	});

	//サーバ応答時実行関数
	function ImagehogeResponse(responseText, statusText) {
		$("button[id^='upbtn_image_']").each( function() {
			var id = this.id;
			$('#'+id).attr('disabled', false);
			$('#'+id).removeAttr('disabled');
		});

//		alert('status: ' + statusText + '\n\nresponseText: \n' + responseText);
		// 現状アップされているイメージを最大値を取得する。
		var koumoku = g_koumoku;
		var max_no = 0;
		$('img[id^="img_image_'+koumoku+'_"]').each( function() {
			var idname = this.id;
			var arr = idname.split('_');
			var target_no = arr[3];
			target_no = eval(target_no);

			// 最後の行数取得
			if (eval(max_no) < eval(target_no)) {
				max_no = target_no;
			}
		});
		// ファイルをテンポラリ領域に上がっているので、クーロンを作成する。
		var tpl = $("#image_temp_" + koumoku).clone();
		// エラーメッセージがある場合
		var ermsg = tpl.find('p[id="cerror_image_'+koumoku+'"]').text();
		if (ermsg) {
			// エラーを消して
			$('p[id="error_image_'+koumoku+'"]').remove();
			var img_f = false;
			var topid = null;
			var min_no = 9999;
			$('img[id^="img_image_'+koumoku+'"]').each( function() {
				var idname = this.id;
				var arr = idname.split('_');
				var target_no = arr[3];
				target_no = eval(target_no);

				// 最後の行数取得
				if (eval(min_no) > eval(target_no)) {
					min_no = target_no;
					topid = this.id;
				}
			});
			// アップロードボタンの下にエラーを表示する。
			var pid = "cerror_image_" + koumoku;
			tpl.find('p[id=' + pid + ']').attr('id',  'error_image_' + koumoku);
			if (topid) {
				$('#' + topid).before(tpl.find('p[id=error_image_'+koumoku+']').show());
			} else {
				$('div[id="image_'+koumoku+'"]').append(tpl.find('p[id=error_image_'+koumoku+']').show());
			}
		} else {
			// エラー表示を消す
			$('p[id="error_image_'+koumoku+'"]').remove();
			max_no = max_no + 1;
			tpl.find('img[id^="cimg_image_'+koumoku+'_"]').each( function() {
				var idname = this.id;
				var arr = idname.split('_');
				var target_no = arr[3];
				target_no = eval(target_no);

				// イメージ表示域のコピー表示
				var imgid = "cimg_image_" + koumoku + "_" + target_no;
				tpl.find('img[id=' + imgid + ']').attr('id',  'img_image_' + koumoku + '_' + max_no);
				$('div[id="image_' + koumoku + '"]').append(tpl.find('img[id=img_image_' + koumoku + '_' + max_no + ']').show());

				// 改行表示域のコピー表示
				var br1id = "cbr1_image_" +koumoku+"_" + target_no;
				tpl.find('br[id=' + br1id + ']').attr('id', 'br1_image_' +koumoku+'_' + max_no);
				$('div[id="image_' + koumoku + '"]').append(tpl.find('br[id=br1_image_' + koumoku + '_' + max_no + ']').show());

				// ファイルダウンロード
				var btnid = "ca_image_" + koumoku+ "_" + target_no;
				tpl.find('a[id=' + btnid + ']').attr('id', 'a_image_' + koumoku + '_' + max_no);
				$('div[id="image_' + koumoku + '"]').append(tpl.find('a[id=a_image_' + koumoku + '_' + max_no + ']').show());

				// 改行表示域のコピー表示
				var br2id = "cbr2_image_" +koumoku+"_" + target_no;
				tpl.find('br[id=' + br2id + ']').attr('id', 'br2_image_' + koumoku + '_' + max_no);
				$('div[id="image_' + koumoku + '"]').append(tpl.find('br[id=br2_image_' + koumoku + '_' + max_no + ']').show());

				// 【画像を削除する】表示域のコピー表示
				var spanid = "cspandel_image_" +koumoku+"_" + target_no;
				tpl.find('span[id=' + spanid + ']').attr('id', 'spandel_image_' + koumoku + '_' + max_no);
				var btnid = "cadel_image_" + koumoku+ "_" + target_no;
				tpl.find('a[id=' + btnid + ']').attr('id', 'adel_image_' + koumoku + '_' + max_no);
				$('div[id="image_' + koumoku + '"]').append(tpl.find('span[id=spandel_image_' + koumoku + '_' + max_no + ']').show());

				// ファイル名域のコピー表示
				var imageid = "cImage" + koumoku + "Imagename" + target_no;
				tpl.find('input[id=' + imageid + ']').attr('name',  'data[Image][' + koumoku + '][imagename_' +  max_no+ ']');
				tpl.find('input[id=' + imageid + ']').attr('id', 'Image' +  koumoku+ 'Imagename' + max_no);
				$('div[id="image_' + koumoku + '"]').append(tpl.find('input[id=' + 'Image' + koumoku + 'Imagename' +  max_no+ ']').show());

				// ファイル名域のコピー表示
				var imageid = "cImage" + koumoku + "Image"+ target_no;
				tpl.find('input[id=' + imageid + ']').attr('name',  'data[Image][' + koumoku + '][image_' +  max_no+ ']');
				tpl.find('input[id=' + imageid + ']').attr('id', 'Image' +  koumoku+ 'Image' + max_no);
				$('div[id="image_' + koumoku + '"]').append(tpl.find('input[id=' + 'Image' +  koumoku+ 'Image' + max_no + ']').show());

				// ファイル名UP域のコピー表示
				var imageupid = "cImage" +  koumoku+ "Image"+target_no+"Up";
				tpl.find('input[id=' + imageupid + ']').attr('name',  'data[Image][' + koumoku + '][image_' +  max_no+ '_up]');
				tpl.find('input[id=' + imageupid + ']').attr('id', 'Image' +  koumoku+ 'Image' + max_no + 'Up');
				$('div[id="image_' + koumoku + '"]').append(tpl.find('input[id=' + 'Image' + koumoku + 'Image' +  max_no+ 'Up' + ']').show());

				// ファイルID域のコピー表示
				var imageidid = "cImage" +  koumoku+ "Image"+target_no+"Id";
				tpl.find('input[id=' + imageidid + ']').attr('name',  'data[Image][' + koumoku + '][image_' +  max_no+ '_id]');
				tpl.find('input[id=' + imageidid + ']').attr('id', 'Image' +  koumoku+ 'Image' + max_no + 'Id');
				$('div[id="image_' + koumoku + '"]').append(tpl.find('input[id=' + 'Image' + koumoku + 'Image' +  max_no+ 'Id' + ']').show());

				// 改行表示域のコピー表示
				var br3id = "cbr3_image_" +koumoku+"_" + target_no;
				tpl.find('br[id=' + br3id + ']').attr('id', 'br3_image_' + koumoku + '_' + max_no);
				$('div[id="image_' + koumoku + '"]').append(tpl.find('br[id=br3_image_' + koumoku + '_' + max_no + ']').show());
				// インクリメント
				max_no = max_no + 1;
			});
		}
		// 仮に上げた領域をクリアする。
		$("#image_temp_"+koumoku).html("");
		// ファイル指定をクリアする。
		$("#ImageUpImage"+koumoku).val("");
	}

	// ファイルを削除する。
	$(document).on("click", "a[id^='adel_notimage_']" ,function(ev){
		var idname = ev.target.id;
		var arr = idname.split('_');
		var koumoku = arr[2];
		var key = arr[3];

		var delimgid = "notimg_notimage_" + koumoku + "_" + key;
		$("#" + delimgid).remove();
		var delbr1 = "br1_notimage_" + koumoku + "_" + key;
		$("#" + delbr1).remove();
		var delspanid = "spandel_notimage_" + koumoku + "_" + key;
		$("#" + delspanid).remove();
		var delfilenameid = "Notimage" + koumoku  + "Notimagename" + key;
		$("#" + delfilenameid).remove();
		var delfileid = "Notimage" + koumoku + "Notimage" + key;
		$("#" + delfileid).remove();
		var delfileupid = "Notimage" + koumoku + "Notimage" + key + "Up";
		$("#" + delfileupid).remove();
		var delbr2 = "br2_notimage_" + koumoku + "_" + key;
		$("#" + delbr2).remove();

	});
	// アップロードボタンクリック
	$(document).on("click", "button[id^='upbtn_notimage_']" ,function(ev){
		$("button[id^='upbtn_notimage_']").each( function() {
			var id = this.id;
			$('#'+id).attr('disabled', true);
		});
		var idname = ev.target.id;
		var arr = idname.split('_');
		var koumoku = arr[2];
		g_koumokunotimg = koumoku;
		var url = notimg_up_url + "?koumoku=" + koumoku + "&model=Notimage";
		var target = '#notimage_temp_' + koumoku;

		$('#uploadForm').ajaxSubmit({
			target: target, // サーバの戻りを出力する場所を指定
			url: url, // URL
			success: NotimagehogeResponse     // サーバからの応答時に呼び出す関数
		});
		return false;
	});

	//サーバ応答時実行関数
	function NotimagehogeResponse(responseText, statusText) {
		$("button[id^='upbtn_notimage_']").each( function() {
			var id = this.id;
			$('#'+id).attr('disabled', false);
			$('#'+id).removeAttr('disabled');
		});

//		alert('status: ' + statusText + '\n\nresponseText: \n' + responseText);
		// 現状アップされているイメージを最大値を取得する。
		var koumoku = g_koumokunotimg;
		var max_no = 0;
		$('a[id^="notimg_notimage_'+koumoku+'_"]').each( function() {
			var idname = this.id;
			var arr = idname.split('_');
			var target_no = arr[3];
			target_no = eval(target_no);

			// 最後の行数取得
			if (eval(max_no) < eval(target_no)) {
				max_no = target_no;
			}
		});
		// ファイルをテンポラリ領域に上がっているので、クーロンを作成する。
		var tpl = $("#notimage_temp_" + koumoku).clone();
		// エラーメッセージがある場合
		var ermsg = tpl.find('p[id="cerror_notimage_'+koumoku+'"]').text();
		if (ermsg) {
			// エラーを消して
			$('p[id="error_notimage_'+koumoku+'"]').remove();
			var img_f = false;
			var topid = null;
			var min_no = 9999;
			$('a[id^="notimg_notimage_'+koumoku+'"]').each( function() {
				var idname = this.id;
				var arr = idname.split('_');
				var target_no = arr[3];
				target_no = eval(target_no);

				// 最後の行数取得
				if (eval(min_no) > eval(target_no)) {
					min_no = target_no;
					topid = this.id;
				}
			});
			// アップロードボタンの下にエラーを表示する。
			var pid = "cerror_notimage_" + koumoku;
			tpl.find('p[id=' + pid + ']').attr('id',  'error_notimage_' + koumoku);
			if (topid) {
				$('#' + topid).before(tpl.find('p[id=error_notimage_'+koumoku+']').show());
			} else {
				$('div[id="notimage_'+koumoku+'"]').append(tpl.find('p[id=error_notimage_'+koumoku+']').show());
			}
		} else {
			// エラー表示を消す
			$('p[id="error_notimage_'+koumoku+'"]').remove();
			max_no = max_no + 1;
			tpl.find('a[id^="cnotimg_notimage_'+koumoku+'_"]').each( function() {
				var idname = this.id;
				var arr = idname.split('_');
				var target_no = arr[3];
				target_no = eval(target_no);

				// イメージ表示域のコピー表示
				var imgid = "cnotimg_notimage_" + koumoku + "_" + target_no;
				tpl.find('a[id=' + imgid + ']').attr('id',  'notimg_notimage_' + koumoku + '_' + max_no);
				$('div[id="notimage_' + koumoku + '"]').append(tpl.find('a[id=notimg_notimage_' + koumoku + '_' + max_no + ']').show());

				// 改行表示域のコピー表示
				var br1id = "cbr1_notimage_" +koumoku+"_" + target_no;
				tpl.find('br[id=' + br1id + ']').attr('id', 'br1_notimage_' +koumoku+'_' + max_no);
				$('div[id="notimage_' + koumoku + '"]').append(tpl.find('br[id=br1_notimage_' + koumoku + '_' + max_no + ']').show());

				// 【画像を削除する】表示域のコピー表示
				var spanid = "cspandel_notimage_" +koumoku+"_" + target_no;
				tpl.find('span[id=' + spanid + ']').attr('id', 'spandel_notimage_' + koumoku + '_' + max_no);
				var btnid = "cadel_notimage_" + koumoku+ "_" + target_no;
				tpl.find('a[id=' + btnid + ']').attr('id', 'adel_notimage_' + koumoku + '_' + max_no);
				$('div[id="notimage_' + koumoku + '"]').append(tpl.find('span[id=spandel_notimage_' + koumoku + '_' + max_no + ']').show());

				// ファイル名域のコピー表示
				var notimageid = "cNotimage" + koumoku + "Notimagename" + target_no;
				tpl.find('input[id=' + notimageid + ']').attr('name',  'data[Notimage][' + koumoku + '][notimagename_' + max_no + ']');
				tpl.find('input[id=' + notimageid + ']').attr('id', 'Notimage' + koumoku  + 'Notimagename' + max_no);
				$('div[id="notimage_' + koumoku + '"]').append(tpl.find('input[id=' + 'Notimage' +koumoku  + 'Notimagename' + max_no + ']').show());

				// テンポラリーファイル名域のコピー表示
				var notimageid = "cNotimage" + koumoku  + "Notimage" + target_no;
				tpl.find('input[id=' + notimageid + ']').attr('name',  'data[Notimage][' + koumoku  + '][notimage_' + max_no + ']');
				tpl.find('input[id=' + notimageid + ']').attr('id', 'Notimage' + koumoku  + 'Notimage' + max_no);
				$('div[id="notimage_' + koumoku + '"]').append(tpl.find('input[id=' + 'Notimage' + koumoku  + 'Notimage' + max_no + ']').show());

				// ファイル名UP域のコピー表示
				var notimageupid = "cNotimage" + koumoku  + "Notimage"+ target_no +"Up";
				tpl.find('input[id=' + notimageupid + ']').attr('name',  'data[Notimage][' + koumoku  + '][notimage_' + max_no + '_up]');
				tpl.find('input[id=' + notimageupid + ']').attr('id', 'Notimage' + koumoku  + 'Notimage' + max_no + 'Up');
				$('div[id="notimage_' + koumoku + '"]').append(tpl.find('input[id=' + 'Notimage' + koumoku  + 'Notimage' + max_no + 'Up' + ']').show());

				// ファイル名UP域のコピー表示
				var notimageidid = "cNotimage" + koumoku  + "Notimage"+ target_no +"Id";
				tpl.find('input[id=' + notimageidid + ']').attr('name',  'data[Notimage][' + koumoku  + '][notimage_' + max_no + '_id]');
				tpl.find('input[id=' + notimageidid + ']').attr('id', 'Notimage' + koumoku  + 'Notimage' + max_no + 'Id');
				$('div[id="notimage_' + koumoku + '"]').append(tpl.find('input[id=' + 'Notimage' + koumoku  + 'Notimage' + max_no + 'Id' + ']').show());

				// 改行表示域のコピー表示
				var br2id = "cbr2_notimage_" +koumoku+"_" + target_no;
				tpl.find('br[id=' + br2id + ']').attr('id', 'br2_notimage_' + koumoku + '_' + max_no);
				$('div[id="notimage_' + koumoku + '"]').append(tpl.find('br[id=br2_notimage_' + koumoku + '_' + max_no + ']').show());
				// インクリメント
				max_no = max_no + 1;
			});
		}
		// 仮に上げた領域をクリアする。
		$("#notimage_temp_"+koumoku).html("");
		// ファイル指定をクリアする。
		$("#NotimageUpNotimage"+koumoku).val("");
	}

});
