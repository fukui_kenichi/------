var i18n = $.extend({}, i18n || {}, {
    datepicker: {
        dateformat: {
            "fulldayvalue": "yyyy/MM/dd",
            "separator": "/",
            "year_index": 0,
            "month_index": 1,
            "day_index": 2,
            "sun": "日",
            "mon": "月",
            "tue": "火",
            "wed": "水",
            "thu": "木",
            "fri": "金",
            "sat": "土",
            "jan": "1月",
            "feb": "2月",
            "mar": "3月",
            "apr": "4月",
            "may": "5月",
            "jun": "6月",
            "jul": "7月",
            "aug": "8月",
            "sep": "9月",
            "oct": "10月",
            "nov": "11月",
            "dec": "12月",
            "postfix": ""
        },
        ok: " OK ",
        cancel: "Cancel",
        today: "今日",
        prev_month_title: "前の月",
        next_month_title: "次の月"
    }
});

