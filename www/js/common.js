// 一覧詳細
function actionEditList(id){
	//ボタンが押されたidを保存
	document.form.hdnId.value = id;
	document.form.edit.click();
	return true;
}

// 一覧削除確認
function actionDelList(id){
	if(confirm('本当に削除しますか?')){
		//ボタンが押されたidを保存
		document.form.hdnId.value = id;
		document.form.del.click();
		return true;
	} else {
		return false;
	}
}

//一覧ボタン処理
function actionList(id, action){
	//ボタンが押されたidを保存
	document.form.hdnId.value = id;
    document.form[action].click();
}

// 削除確認
function actionDel(){
	if(confirm('本当に削除しますか?')){
		return true;
	} else {
		return false;
	}
}

//ログアウト確認
function actionLogout(){
	if(confirm('本当にログアウトしますか?')){
		return true;
	} else {
		return false;
	}
}

//別ウィンドウで開く
function setTarget(target){
	if (target == 'blank'){
		document.form.target = "_blank";
	}else{
		document.form.target = "_self";
	}
	return true;
}

$(function(){
	// 全てチェック取り付け
	$("#AllCheck").click(function(){
		if($("#AllCheck").prop('checked')) {
			$(".all_check").attr("checked", true);
		}else{
			$(".all_check").attr("checked", false);
		}
	});
});

// 2重投稿防止------------------------------------------------------------------------
var DisableSubmit = {
   init: function() {
      this.addEvent(window, 'load', this.set());
   },

   set: function() {
      var self = this;
      return function() {
         for (var i = 0; i < document.forms.length; ++i) {
            if(document.forms[i].onsubmit) continue;
            document.forms[i].onsubmit = function() {
               self.setDisable(this.getElementsByTagName('input'));
            };
         }
      }
   },

   setDisable: function(elms) {
      for (var i = 0, elm; elm = elms[i]; i++) {
         if ((elm.type == 'submit' || elm.type == 'image') && !elm.disabled) {
            Set(elm);
            unSet(elm);
         }
      }

      function Set(button) {
         window.setTimeout(function() { button.disabled = true; }, 1);
      }
      function unSet(button) {
         window.setTimeout(function() { button.disabled = false; }, 1000);
      }
   },

   addEvent: function(elm, type, event) {
      if(elm.addEventListener) {
         elm.addEventListener(type, event, false);
      } else if(elm.attachEvent) {
         elm.attachEvent('on'+type, event);
      } else {
         elm['on'+type] = event;
      }
   }
}

DisableSubmit.init();
// 2重投稿防止------------------------------------------------------------------------


// 一覧チェック一括付けはずし
$(function(){
	// 全てチェック取り付け
	$("#CheckOn").click(function(){
		$(".all_check").attr("checked", "checked");
	});
	// 全てチェック取りはずし
	$("#CheckOff").click(function(){
		$(".all_check").attr("checked", "");
	});

	// 検索ボックス表示非表示
	$("#showBtn").click(function () {
		$("#serachBox").toggle();

		if ($("#showUp").css("display") == "block") {
			$("#showUp").css("display","none");
			$("#showDown").css("display","block");
		} else {
			$("#showUp").css("display","block");
			$("#showDown").css("display","none");
		}
	});

});

//テキスト欄入力制限（数字のみ)
//classにnumOnlyを指定すれば数字以外の入力は不可。カット、コピー、ペースト機能はそのまま残す
$(document).on("keydown", ".num-only", function(ev){
	shiftKey = ev.shiftKey;
	keyCode = ev.keyCode;	// 入力されたキー
	returnFlag = false;		// リターンするフラグ

	if(!shiftKey && ((keyCode >= 96 && keyCode <= 105) || keyCode == 109 || keyCode == 110 || // テンキー0～9-.チェック
			(keyCode >= 48 && keyCode <= 57) || keyCode == 173 || keyCode == 189 || keyCode == 190) || // メインキー0～9-.チェック
			keyCode == 37 || keyCode == 39 || // ←→キー（キャレットの移動）
			keyCode == 8 ||  keyCode == 9 || keyCode == 46 || keyCode == 13) { //バックスペース、タブキー、デリート、Enter
		returnFlag = true;
	} else if (ev.ctrlKey && (keyCode == 67 || keyCode == 86 || keyCode == 88)) { // Ctrl + C,V,Xの場合はそのまま通す
		returnFlag = true;
	}

	return returnFlag;
});

//text上でのエンター送信を禁止する処理
$(document).ready(function() {
	$("input[type='text'], select").keypress(function(ev) {
		//検索の時だけエンターを受け付ける
		if (this.id == 'SearchText'){
			return true;
		}

		if ((ev.which && ev.which === 13) || (ev.keyCode && ev.keyCode === 13)) {
			return false;
		} else {
			return true;
		}
	});
});