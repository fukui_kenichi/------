// JavaScript Document
$(function() {
	setSmartTableDisplay();	// 初期設定

	// スクロール同期設定バインド
	$("div.smart-table-body").scroll(function(ev) {
		smartTableId = "div#" + $(this).closest("div.smart-table").attr("id");
		$(smartTableId+" div.smart-table-header").scrollLeft($(smartTableId+" div.smart-table-body").scrollLeft());
	});

	// ソートする列の初期化
	sortColCount = 1;
	$(".smart-table-header table th.sort").each(function(){
		$(this).attr("col_order", ""); // 昇降順初期化

		// ソート列ID //
		smartTableId = $(this).closest("div.smart-table").attr("id");
		if (typeof smartTableId === "undefined") smartTableId = "";
		$(this).attr("id", smartTableId+"_sort_col_" + sortColCount);

		// 昇降矢印欄付加
		targetDiv = "th#"+this.id+" div";
		text = $(targetDiv).text();
//		$(targetDiv).html(text+"<span>333</span>");

		sortColCount++;　// カウントインクリメント
	});
});

// ウインドリサイズ時
$(window).resize(function(){
	setSmartTableDisplay();
});

// ウィンドウ幅高再計算
function setSmartTableDisplay() {
	containerWidth = $("div.container").width();

	$("div.smart-table.full").each(function(){
		thisId = "div#"+this.id;
		if (typeof thisId === "undefined") return true; // continue;

		// 高さ：指定なし
		//	$(".smart-table-body").height(bodyHeight-250);

		// 幅：bodyの縦スクロールが表示されているかどうかを考慮
		$(thisId).width(containerWidth); // 全体
		if ($(thisId+" div.smart-table-body").height() < $(thisId+" div.smart-table-body table").height()) {
			$(thisId+" div.smart-table-header").width(containerWidth-16); // ヘッダー
			$(thisId+" div.smart-table-body").width(containerWidth);	// ボディ
			$(thisId+" div.smart-table-footer").width(containerWidth); // フッター
		} else {
			$(thisId+" div.smart-table-header").width(containerWidth); // ヘッダー
			$(thisId+" div.smart-table-body").width(containerWidth); // ボディ
			$(thisId+" div.smart-table-footer").width(containerWidth); // フッター
		}

		// 空白エリアの調整
		$(thisId+" .space").width(0);
		spaceWidth = $(thisId+" div.smart-table-header").width() - $(thisId+" div.smart-table-header table").width() - 1;
		if (spaceWidth > 0) {
			$(thisId+" .space").width(spaceWidth);
			$(thisId+" .space").show();
		} else {
			$(thisId+" .space").hide().width(0);
		}
	});
}

// ヘッダー行ソート項目のソート情報初期化
function headerClear(tableId) {
	// セレクタ設定
	selector = "";
	if (tableId == "") {
		selector = "div.smart-table-header table th.sort";
	} else {
		selector = "div.smart-table#"+tableId+" div.smart-table-header table th.sort";
	}

	// 初期化処理
	$(selector).each(function(){
		$(this).attr("col_order", "");
		targetSpan = "th#"+this.id+" div span";
		$(targetSpan).text("");
	});

	return true;
}
